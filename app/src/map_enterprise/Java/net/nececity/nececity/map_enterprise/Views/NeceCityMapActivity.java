package net.nececity.nececity.map_enterprise.Views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;

import com.facebook.login.LoginManager;
import com.lapism.searchview.widget.SearchView;

import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Managers.NotificationManager;
import net.nececity.nececity.Managers.PermissionsManager;
import net.nececity.nececity.R;
import net.nececity.nececity.enterprise.Helpers.PagerAdapter;
import net.nececity.nececity.enterprise.Managers.AppManager;
import net.nececity.nececity.map_enterprise.Views.EnterprisePagerActivity;
import net.nececity.nececity.user.Views.SettingsActivity;
import net.nececity.nececity.map_enterprise.Managers.CityMapManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NeceCityMapActivity extends net.nececity.nececity.user.Views.NeceCityMapActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setListeners(){
        super.setListeners();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(){
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                String stackTrace = Log.getStackTraceString(ex);
                System.out.println(stackTrace);
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.exit(0);
            }
        });
    }

    @Override
    public void uiSetUp(){
        super.uiSetUp();
        navigationView.getMenu().clear();
        navigationView.inflateMenu(R.menu.activity_enterprise_pager_drawer);
    }

    @Override
    public void appManagerSetUp(){
        permissionsManager = new PermissionsManager(this);

        mapManager = new CityMapManager(map,this);
        mapManager.prepareSearch(searchView);

        notificationManager = new NotificationManager(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.nav_companies:
                intent = new Intent(this, EnterprisePagerActivity.class);
                startActivity(intent);
                drawer.closeDrawer(Gravity.LEFT);
                break;
            case R.id.nav_promotions:
                intent = new Intent(this, EnterprisePagerActivity.class);
                startActivity(intent);
                drawer.closeDrawer(Gravity.LEFT);
                break;
            case R.id.nav_posts:
                intent = new Intent(this, EnterprisePagerActivity.class);
                startActivity(intent);
                drawer.closeDrawer(Gravity.LEFT);
                break;
            case R.id.nav_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_share:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage("com.whatsapp");
                intent.putExtra(Intent.EXTRA_TEXT, AppVariables.appLink);
                try {
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    showMessage("Whatsapp has not been installed.");
                }
                break;
        }
        return false;
    }

    @Override
    public void logout(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        if (preferences.contains("userID")){
            editor.remove("userID").commit();
        }

        if (preferences.contains("fbID") && preferences.contains("fbEmail") && preferences.contains("fbName")){
            editor.remove("fbID").commit();
            editor.remove("fbEmail").commit();
            editor.remove("fbName").commit();
        }

        LoginManager.getInstance().logOut();

        Intent a = new Intent(context,LoginActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(a);
        System.exit(0);
    }
}
