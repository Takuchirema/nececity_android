package net.nececity.nececity.map_enterprise.Views;


import net.nececity.nececity.map_enterprise.Managers.FacebookLoginManager;
import net.nececity.nececity.map_enterprise.Managers.LoginManager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import net.nececity.nececity.R;

public class LoginActivity extends net.nececity.nececity.enterprise.Views.LoginActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void appManagerSetUp(){
        loginManager = new LoginManager();
        fbLoginManager = new FacebookLoginManager(this);
    }

    @Override
    public void attemptSharedPreferenceLogin(){
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        LoginManager loginManager = new LoginManager(email,password);
        loginManager.attemptLogin(this);
    }
}
