package net.nececity.nececity.map_enterprise.Views;

import android.content.Intent;
import android.os.Bundle;
import net.nececity.nececity.user.Views.SettingsActivity;

public class EnterprisePagerActivity extends net.nececity.nececity.enterprise.Views.EnterprisePagerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiSetUp();
        appSetUp();
    }

    @Override
    public void startApp(){

    }

    @Override
    public void appManagerSetUp(){

    }

    @Override
    public void appSetUp(){
        setListeners();
    }

    @Override
    public void setAppCodes() {

    }

    @Override
    public void startSettingsActivity(){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
}
