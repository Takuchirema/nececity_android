package net.nececity.nececity.map_enterprise.Managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.maps.GoogleMap;

import net.nececity.nececity.Abstracts.ESettingKeys;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Data.CompaniesDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RefreshDataSet;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Takunda on 10/12/2019.
 */

public class CityMapManager extends net.nececity.nececity.user.Managers.CityMapManager{

    public CityMapManager(GoogleMap map, Context context) {
        super(map, context);
    }

    @Override
    public void startManager(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AppVariables.mainActivity.getContext());
        int companyInterval = Integer.parseInt(prefs.getString(ESettingKeys.COMPANY_REFRESH_INTERVAL.toString(),"3600"));

        String url = UrlConstructor.getCompanyDetailsUrl(AppVariables.getEnterpriseId());
        CompaniesDataSet companiesDataSet = new CompaniesDataSet(null, AppVariables.user.getId());
        companiesDataSet.setUrl(url);

        refreshCompaniesDataSet = new RefreshDataSet(companiesDataSet,handler,updateCompanyResultsRunnable);
        refreshCompaniesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(companyInterval));
        refreshCompaniesDataSet.start();
    }

    final protected Runnable updateCompanyResultsRunnable = new Runnable() {
        public void run() {
            CompaniesDataSet companiesDataSet = (CompaniesDataSet)refreshCompaniesDataSet.getResultsDataSet();
            ((IMapActivity)AppVariables.mainActivity).stopMapRefresh(companiesDataSet);

            if (companiesDataSet.success) {
                followCompanies(companiesDataSet.getCompanies());
                ObjectCache.refreshCompanies(companiesDataSet.getCompanies());
            }
            updateCompaniesMap(companiesDataSet.getCompanies());
            updateEmployeesRefreshDataSets();
            updatePostsRefreshDataSets();
            updatePromotionsRefreshDataSets();
        }
    };

    private void followCompanies(HashMap<String, Company> companies){
        for (Map.Entry<String,Company> e:companies.entrySet()) {
            Company company = e.getValue();
            company.setFollowing(true);
        }
    }
}
