package net.nececity.nececity.map_enterprise.Managers;

import android.content.Intent;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.map_enterprise.Views.NeceCityMapActivity;
import net.nececity.nececity.user.Data.LoginDataSet;

/**
 * Created by Takunda on 10/12/2019.
 */

public class LoginManager extends net.nececity.nececity.enterprise.Managers.LoginManager {

    public LoginManager(String userId,String password){
        super(userId,password);
    }

    public LoginManager(){}

    @Override
    public void postGetDataSet(DataSet dataSet){
        LoginDataSet loginDataSet = (LoginDataSet)dataSet;

        if (loginDataSet.success){
            Intent intent = new Intent(loginActivity, NeceCityMapActivity.class);
            loginActivity.startActivity(intent);
            loginActivity.finish();
        }else{
            if (!userId.isEmpty() && !password.isEmpty()) {
                loginActivity.loginFailed(loginDataSet.message);
            }
        }
    }
}
