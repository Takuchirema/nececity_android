package net.nececity.nececity.map_enterprise.Managers;

import android.content.Intent;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.map_enterprise.Views.NeceCityMapActivity;
import net.nececity.nececity.user.Data.FacebookLoginDataSet;
import net.nececity.nececity.user.Views.LoginActivity;

/**
 * Created by Takunda on 10/12/2019.
 */

public class FacebookLoginManager extends net.nececity.nececity.enterprise.Managers.FacebookLoginManager {

    public FacebookLoginManager(LoginActivity loginActivity) {
        super(loginActivity);
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        FacebookLoginDataSet loginDataset = (FacebookLoginDataSet)dataSet;

        if (loginDataset.success){
            Intent intent = new Intent(loginActivity, NeceCityMapActivity.class);
            loginActivity.startActivity(intent);
            loginActivity.finish();
        }else{
            com.facebook.login.LoginManager.getInstance().logOut();
            loginActivity.loginFailed(loginDataset.message);
        }
    }
}
