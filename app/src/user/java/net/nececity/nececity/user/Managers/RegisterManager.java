package net.nececity.nececity.user.Managers;

import android.content.Intent;
import android.widget.Toast;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Data.RegisterDataSet;
import net.nececity.nececity.user.Views.LoginActivity;
import net.nececity.nececity.user.Views.RegisterActivity;

/**
 * Created by Takunda on 28/05/2018.
 */

public class RegisterManager implements IGetDataSet {

    private String username;
    private String email;
    private String password;
    private String phoneNumber;

    protected RegisterActivity registerActivity;

    public RegisterManager (String username, String email, String password, String phoneNumber){
        this.username = username;
        this.email = email;
        this.password = password;
        this.phoneNumber=phoneNumber;
    }

    public RegisterManager(){}

    public void register(RegisterActivity registerActivity){
        this.registerActivity = registerActivity;
        RegisterDataSet registerDataSet = new RegisterDataSet(this,username,email,password,phoneNumber);
        registerDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        RegisterDataSet registerDataSet = (RegisterDataSet)dataSet;

        if (registerDataSet.success){
            Toast.makeText(registerActivity,"Registration Successful",Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(registerActivity, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            registerActivity.startActivity(intent);
            registerActivity.finish();
        }else{
            registerActivity.registrationFailed(registerDataSet.message);
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public RegisterActivity getRegisterActivity() {
        return registerActivity;
    }

    public void setRegisterActivity(RegisterActivity registerActivity) {
        this.registerActivity = registerActivity;
    }
}
