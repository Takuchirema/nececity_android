package net.nececity.nececity.user.Data;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Data.CustomJSONObject;
import net.nececity.nececity.Data.GetServerData;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.Objects.Settings;
import net.nececity.nececity.user.Views.LoginActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import net.nececity.nececity.Data.CustomJSONArray;
import org.json.JSONException;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 24/05/2018.
 */

public class LoginDataSet extends DataSet{

    private String userId;
    private String password;
    private LoginActivity loginActivity;

    SharedPreferences preferences;

    public LoginDataSet(IGetDataSet getDataset, LoginActivity activity, String userId, String password)
    {
        super(getDataset);
        this.loginActivity =activity;
        this.userId=userId;
        this.password=password;
    }

    @Override
    public void getDataSet(){

        if (sharedPreferenceLogin()){
            success=true;
            getDataset.postGetDataSet(this);
            return;
        }

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userID", userId));
        params.add(new BasicNameValuePair("password", password));

        String url = UrlConstructor.getNewUserLoginUrl(userId);

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.POST);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {

        CustomJSONArray userJsonArray = json.getJSONArray("users");
        CustomJSONObject userJson = null;

        if (userJsonArray.length() == 0) {
            message = "User was not found";
            success = false;
        } else {
            userJson = (CustomJSONObject)userJsonArray.getJSONObject(0);
        }

        //do this early so that the update profile has a token and userID
        AppVariables.token = json.getString("token");

        User user = new User(userJson.getString("userId"));
        user.setStatus("online");
        user.setId(userJson.getString("userId"));
        user.setEmail(userJson.getString("email"));
        user.setPhoneNumber(userJson.getString("phoneNumber"));

        //set the alert
        if (!userJson.isNull("alert")){
            System.out.println("user is on alert");
            user.setOnAlert("true");
        }

        user.setPictureUrl(userJson.getString("profilePicture"));

        setUserPreferences(user,json.getString("token"));
        AppVariables.user = user;
    }

    public void setUserPreferences(User user,String token){
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("userID", user.getId());
        editor.putString("phoneNumber", user.getPhoneNumber());
        editor.putString("email", user.getEmail());
        editor.putString("pictureUrl", user.getPictureUrl());
        editor.putString("token", token);
        editor.putString("password",password);

        editor.apply();
    }

    public boolean sharedPreferenceLogin(){
        //If shared preferences are set then automatic login
        preferences = PreferenceManager.getDefaultSharedPreferences(loginActivity);
        if (preferences.contains("fbID") && preferences.contains("fbEmail") && preferences.contains("fbName")){
            System.out.println("fb preferences");
            String fbID = preferences.getString("fbID", "").trim();
            String fbEmail = preferences.getString("fbEmail","").trim();
            String fbName = preferences.getString("fbName","").trim();

            if (!fbID.isEmpty() && !fbEmail.isEmpty() && !fbName.isEmpty()) {
                System.out.println("shared pref login");
                loginWithSharedPreference(fbID);
                return true;
            }
        } else if (preferences.contains("userID")){
            System.out.println("preferences");
            String userID = preferences.getString("userID", "").trim();

            if (!userID.isEmpty()) {
                System.out.println("preferences login");
                loginWithSharedPreference(null);
                return true;
            }
        }

        return false;
    }

    public void loginWithSharedPreference(String fbId){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(loginActivity);

        String userID = preferences.getString("userID", "").trim();
        String phoneNumber = preferences.getString("phoneNumber","").trim();
        String pictureUrl = preferences.getString("pictureUrl","");
        String email = preferences.getString("email","");
        String token = preferences.getString("token","");
        String userName = "";

        if (fbId != null && !fbId.isEmpty()) {
            userID = preferences.getString("fbID", "").trim();
            email = preferences.getString("fbEmail","");
            userName = preferences.getString("fbName","");
        }

        User user = new User(userName);
        user.setStatus("online");
        user.setId(userID);
        user.setPhoneNumber(phoneNumber);
        user.setEmail(email);
        user.setPhoneNumber(phoneNumber);
        user.setPictureUrl(pictureUrl);

        if (fbId != null && !fbId.isEmpty()) {
            user.setFbId(userID);
        }

        Settings settings = new Settings();

        try {
            setUserSettings(settings, preferences);
        }catch(Exception ex){}

        user.setSettings(settings);

        AppVariables.user = user;
        AppVariables.token = token;
    }

    public void setUserSettings(Settings userSettings,SharedPreferences preferences) throws Exception{

        if (preferences.contains("visibilityRegion"))
            userSettings.setRegionVisibility(preferences.getString("visibilityRegion",""));

        if (preferences.contains("messagesRefreshInterval"))
            userSettings.setMessagesRefreshInterval(preferences.getInt("messagesRefreshInterval",0));

        if (preferences.contains("usersRefreshInterval"))
            userSettings.setUsersRefreshInterval(preferences.getInt("usersRefreshInterval",0));

        if (preferences.contains("postsRefreshInterval"))
            userSettings.setPostsRefreshInterval(preferences.getInt("postsRefreshInterval",0));

        if (preferences.contains("companiesRefreshInterval"))
            userSettings.setCompaniesRefreshInterval(preferences.getInt("companiesRefreshInterval",0));
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
