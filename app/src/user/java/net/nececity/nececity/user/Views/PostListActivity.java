package net.nececity.nececity.user.Views;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Abstracts.PostListObject;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.PostListAdapter;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.CommentsActivity;
import net.nececity.nececity.user.Managers.PostsManager;

import java.util.ArrayList;
import java.util.HashMap;

public class PostListActivity extends AppCompatActivity implements IListLayout{

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static PostsManager postsManager;
    private PostListActivity postListActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Posts");
        actionBar.setDisplayHomeAsUpEnabled(true);

        postListActivity = this;
        postsManager = new PostsManager();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);

        setListeners();

        if (ObjectCache.getPosts().isEmpty()){
            postsManager.refreshPosts(postListActivity);
        }else {
            createListAdapter();
        }
    }

    public void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        postsManager.refreshPosts(postListActivity);
                    }
                }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){
        clearRefresh();

        HashMap<String,? extends PostListObject> posts =  ObjectCache.getPosts();

        ArrayList<String> accountIds = new ArrayList<>(posts.keySet());
        PostListAdapter adapter = new PostListAdapter(this, null, CommentsActivity.class, accountIds, R.layout.post_list_view);
        adapter.setMapObjects(posts);
        adapter.sort();

        listView = findViewById(R.id.posts_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public void clearRefresh(){
        System.out.println("clear refresh");
        swipeRefreshLayout.setRefreshing(false);
    }
}
