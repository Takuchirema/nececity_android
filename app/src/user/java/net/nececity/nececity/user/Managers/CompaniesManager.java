package net.nececity.nececity.user.Managers;

import android.content.Intent;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Data.CompaniesDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.user.Views.CompanyListActivity;
import net.nececity.nececity.user.Views.NeceCityMapActivity;

import java.util.HashMap;

/**
 * Created by Takunda on 22/05/2018.
 */

public class CompaniesManager implements IGetDataSet{

    private CompanyListActivity companyListActivity;

    public CompaniesManager(){

    }

    public void getCompanies(CompanyListActivity activity){
        HashMap<String,Company> companies = ObjectCache.getCompanies();
        if (companies.isEmpty()){
            CompaniesDataSet companiesDataSet = new CompaniesDataSet(this, AppVariables.user.getId());
            companiesDataSet.getDataSet();
        }else{
            activity.createListAdapter();
        }
    }

    public void refreshCompanies(CompanyListActivity activity){
        companyListActivity = activity;
        System.out.println("refresh companies");
        CompaniesDataSet companiesDataSet = new CompaniesDataSet(this, AppVariables.user.getId());
        companiesDataSet.getDataSet();
    }

    public void showList(){
        Intent intent = new Intent(NeceCityMapActivity.context, CompanyListActivity.class);
        NeceCityMapActivity.context.startActivity(intent);
    }


    @Override
    public void postGetDataSet(DataSet dataSet) {
        CompaniesDataSet companiesDataSet = (CompaniesDataSet)dataSet;
        if (companiesDataSet.success) {
            ObjectCache.refreshCompanies(companiesDataSet.getCompanies());
            if (companyListActivity != null){
                companyListActivity.createListAdapter();
            }
        }else{
            if (companyListActivity != null){
                companyListActivity.clearRefresh();
            }
            AppVariables.mainActivity.showMessage(companiesDataSet.message);
        }
    }

    public static void follow(String companyId){
        Company company = ObjectCache.getCompanies().get(companyId);
        if (company != null)
            company.setFollowing(true);
    }

    public static void unFollow(String companyId){
        Company company = ObjectCache.getCompanies().get(companyId);
        if (company != null)
            company.setFollowing(false);
    }
}
