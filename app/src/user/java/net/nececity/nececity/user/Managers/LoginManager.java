package net.nececity.nececity.user.Managers;

import android.content.Intent;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Data.JSONParser;
import net.nececity.nececity.user.Data.LoginDataSet;
import net.nececity.nececity.user.Views.LoginActivity;
import net.nececity.nececity.user.Views.NeceCityMapActivity;

/**
 * Created by Takunda on 22/05/2018.
 */

public class LoginManager implements IGetDataSet {

    protected String userId, password;
    JSONParser jsonParser = new JSONParser();
    private String failureMessage;
    protected LoginActivity loginActivity;

    public LoginManager(String userId,String password){
        this.userId = userId.trim();
        this.password = password;
    }

    public LoginManager(){}

    public void attemptLogin(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
        LoginDataSet loginDataSet = new LoginDataSet(this,loginActivity,userId,password);
        loginDataSet.getDataSet();
    }

    public void postGetDataSet(DataSet dataSet){
        LoginDataSet loginDataSet = (LoginDataSet)dataSet;

        if (loginDataSet.success){
            Intent intent = new Intent(loginActivity, NeceCityMapActivity.class);
            loginActivity.startActivity(intent);
            loginActivity.finish();
        }else{
            if (!userId.isEmpty() && !password.isEmpty()) {
                loginActivity.loginFailed(loginDataSet.message);
            }
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
