package net.nececity.nececity.user.Managers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.lapism.searchview.widget.SearchAdapter;
import com.lapism.searchview.widget.SearchItem;
import com.lapism.searchview.widget.SearchView;

import net.nececity.nececity.Abstracts.ECompanyType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EMapMarkerType;
import net.nececity.nececity.Abstracts.ESearchItem;
import net.nececity.nececity.Abstracts.ESettingKeys;
import net.nececity.nececity.Abstracts.EStatus;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Data.CompaniesDataSet;
import net.nececity.nececity.Data.EmployeesDataSet;
import net.nececity.nececity.Data.PostsDataSet;
import net.nececity.nececity.Data.PromotionsDataSet;
import net.nececity.nececity.Data.RouteDataSet;
import net.nececity.nececity.Data.UsersDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.CustomRunnable;
import net.nececity.nececity.Helpers.GetDataSet;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RefreshDataSet;
import net.nececity.nececity.Helpers.SetAlarm;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.LocationCluster;
import net.nececity.nececity.Objects.MapMarker;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.user.Views.NeceCityMapActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Created by Takunda on 22/05/2018.
 */

public class CityMapManager extends net.nececity.nececity.Managers.CityMapManager{

    // All markers on the map are kept here
    // For employees, the Key is the companyId. The inner hashmap is employees by id.
    // This is done in case companies have the employees with same id's.
    public static HashMap<String,MapMarker> companyMapMarkers = new HashMap<>();
    public static HashMap<String,HashMap<String,MapMarker>> employeesMapMarkers = new HashMap<>();

    static {
        routeMapEmployees = new HashMap<>();
    }

    public static RefreshDataSet refreshUsersDataSet;
    public static RefreshDataSet refreshCompaniesDataSet;
    public static RefreshDataSet refreshPostsDataSet;
    public static RefreshDataSet refreshPromotionsDataSet;
    public static RefreshDataSet refreshEmployeesDataSet;
    public static RefreshDataSet refreshRoutesDataSet;

    public CityMapManager(GoogleMap map, Context context) {
        super(map, context);
    }

    public void startManager(){

        /*UsersDataSet usersDataSet = new UsersDataSet(null, AppVariables.user.getId());
        refreshUsersDataSet = new RefreshDataSet(usersDataSet,handler,updateUserResultsRunnable);
        refreshUsersDataSet.setRefreshInterval(TimeUnit.MINUTES.toMillis(30));
        refreshUsersDataSet.start();*/

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
        int companyInterval = Integer.parseInt(prefs.getString(ESettingKeys.COMPANY_REFRESH_INTERVAL.toString(),"3600"));

        CompaniesDataSet companiesDataSet = new CompaniesDataSet(null, AppVariables.user.getId());
        refreshCompaniesDataSet = new RefreshDataSet(companiesDataSet,handler,updateCompanyResultsRunnable);
        refreshCompaniesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(companyInterval));
        refreshCompaniesDataSet.start();
    }

    final Runnable updateUserResultsRunnable = new Runnable() {
        public void run() {
            UsersDataSet usersDataSet = (UsersDataSet)refreshUsersDataSet.getResultsDataSet();
            if (usersDataSet.success) {
                ObjectCache.refreshUsers(usersDataSet.getUsers());
            }
            updateUserMap(usersDataSet.getUsers());
        }
    };

    final protected Runnable updateCompanyResultsRunnable = new Runnable() {
        public void run() {
            CompaniesDataSet companiesDataSet = (CompaniesDataSet)refreshCompaniesDataSet.getResultsDataSet();
            ((IMapActivity)AppVariables.mainActivity).stopMapRefresh(companiesDataSet);

            if (companiesDataSet.success) {
                ObjectCache.refreshCompanies(companiesDataSet.getCompanies());
            }
            updateCompaniesMap(companiesDataSet.getCompanies());
            updateEmployeesRefreshDataSets();
            updatePostsRefreshDataSets();
            updatePromotionsRefreshDataSets();
        }
    };

    final Runnable updateRoutesResultsRunnable = new Runnable() {
        public void run() {
            RouteDataSet routeDataSet = (RouteDataSet) refreshRoutesDataSet.getResultsDataSet();
            //((IMapActivity)AppVariables.mainActivity).stopMapRefresh(routeDataSet);

            if (routeDataSet.success) {
                System.out.println("routes runnable");
                HashMap<String,Route> routes = routeDataSet.getRoutes();
                updateRouteClustersMap(routes);
            }
        }
    };

    final Runnable updateEmployeesResultsRunnable = new Runnable() {
        public void run() {
            EmployeesDataSet employeesDataSet = (EmployeesDataSet) refreshEmployeesDataSet.getResultsDataSet();
            //((IMapActivity)AppVariables.mainActivity).stopMapRefresh(employeesDataSet);

            if (employeesDataSet.success) {
                ObjectCache.refreshEmployees(employeesDataSet.getEmployees());
                updateEmployeesMap(employeesDataSet.getEmployees());

                //Update the dashboard
                IMapActivity mapActivity = (IMapActivity)AppVariables.mainActivity;
                mapActivity.updateDashboard();
            }
        }
    };

    final Runnable updatePostsResultsRunnable = new Runnable() {
        public void run() {
            PostsDataSet postsDataSet = (PostsDataSet) refreshPostsDataSet.getResultsDataSet();
            if (postsDataSet.success) {
                ObjectCache.refreshPosts(postsDataSet.getPosts());
                AppVariables.mainActivity.unseenPosts(postsDataSet.unseenPosts(),postsDataSet.getLatestPost());
            }
        }
    };

    final Runnable updatePromotionsResultsRunnable = new Runnable() {
        public void run() {
            PromotionsDataSet promotionsDataSet = (PromotionsDataSet) refreshPromotionsDataSet.getResultsDataSet();
            if (promotionsDataSet.success) {
                ObjectCache.refreshPromotions(promotionsDataSet.getPromotions());
                AppVariables.mainActivity.promotionsToday(promotionsDataSet.getPromotionsToday());
            }
        }
    };

    protected void updateEmployeesRefreshDataSets(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
        int mapInterval = Integer.parseInt(prefs.getString(ESettingKeys.MAP_REFRESH_INTERVAL.toString(),"20"));

        if (refreshEmployeesDataSet != null && !refreshEmployeesDataSet.isStop()){
            return;
        }

        // If no company has active employees we will not request employee refreshes.
        List<NameValuePair> params = getCompaniesParams();

        if (params.isEmpty()){
            if (refreshEmployeesDataSet != null) {
                refreshEmployeesDataSet.requestStop();
                System.out.println("employees refresh stop requested ");
            }
            return;
        }

        EmployeesDataSet employeesDataSet = new EmployeesDataSet(null, null);
        employeesDataSet.setHttpMethod(EHttpMethod.POST);
        employeesDataSet.setParams(params);

        refreshEmployeesDataSet = new RefreshDataSet(employeesDataSet,handler,updateEmployeesResultsRunnable);
        refreshEmployeesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(mapInterval));
        refreshEmployeesDataSet.start();
    }

    protected void updatePostsRefreshDataSets(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
        int postsInterval = Integer.parseInt(prefs.getString(ESettingKeys.POSTS_REFRESH_INTERVAL.toString(),"60"));

        if (refreshPostsDataSet != null && !refreshPostsDataSet.isStop()){
            return;
        }

        PostsDataSet postsDataSet = new PostsDataSet(null);
        refreshPostsDataSet = new RefreshDataSet(postsDataSet,handler,updatePostsResultsRunnable);
        refreshPostsDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(postsInterval));
        refreshPostsDataSet.start();
    }

    protected void updatePromotionsRefreshDataSets(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
        int promotionsInterval = Integer.parseInt(prefs.getString(ESettingKeys.PROMOTIONS_REFRESH_INTERVAL.toString(),"60"));

        if (refreshPromotionsDataSet != null && !refreshPostsDataSet.isStop()){
            return;
        }

        PromotionsDataSet promotionsDataSet = new PromotionsDataSet(null);
        refreshPromotionsDataSet = new RefreshDataSet(promotionsDataSet,handler,updatePromotionsResultsRunnable);
        refreshPromotionsDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(promotionsInterval));
        refreshPromotionsDataSet.start();
    }

    /**
     * This creates a thread that will refresh the showing routes at intervals specified in map clusters
     * Its to get location clusters on those routes.
     * If no routes are showing the refresh is stopped.
     */
    private void updateRoutesRefreshDataSets(){
        System.out.println("params update refresh ds");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
        int showRouteClusters = Integer.parseInt(prefs.getString(ESettingKeys.SHOW_ROUTE_CLUSTERS.toString(),"0"));

        if (showRouteClusters != 1){
            System.out.println("params do not show clusters");
            return;
        }

        // If no route is showing this will be empty.
        // So you can stop refreshing the dataset.
        List<NameValuePair> params = getRouteParams();

        if (params.isEmpty()){
            System.out.println("route params empty ");
            if (refreshRoutesDataSet != null) {
                refreshRoutesDataSet.requestStop();
                System.out.println(" params stop requested "+refreshRoutesDataSet.isStop());
            }
            return;
        }

        if (refreshRoutesDataSet == null || refreshRoutesDataSet.isStop() ) {
            System.out.println(" params refresh routes data set");
            int mapInterval = Integer.parseInt(prefs.getString(ESettingKeys.MAP_REFRESH_INTERVAL.toString(), "10"));

            RouteDataSet routeDataSet = new RouteDataSet(null, null, null);
            routeDataSet.setHttpMethod(EHttpMethod.POST);
            routeDataSet.setUrl(UrlConstructor.getRoutesUrl());
            routeDataSet.setParams(params);

            refreshRoutesDataSet = new RefreshDataSet(routeDataSet, handler, updateRoutesResultsRunnable);
            refreshRoutesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(mapInterval));

            refreshRoutesDataSet.start();
        }else{
            ((RouteDataSet)refreshRoutesDataSet.getDataSet()).setParams(params);
        }
    }

    public List<NameValuePair> getRouteParams(){

        List<NameValuePair> params = new ArrayList<>();

        for (Map.Entry<String,Route> rm: routes.entrySet()) {
            Route route = rm.getValue();
            String routeId = rm.getKey();
            String companyId = route.getCompanyId();
            // If the route has employees moving we are not getting the route clusters.
            if (route.getPolyline().isVisible() && !hasEmployees(companyId,routeId)) {
                System.out.println(routeId+" polyline visible");
                params.add(new BasicNameValuePair("companies[" + companyId + "]", routeId));
            }
        }

        return params;
    }

    /*****
     * The method checks if there are recent active employees on a route.
     * Currently last seen 5 minutes or below on the route will be returned as true.
     * @param companyId
     * @param routeId
     * @return
     */
    private boolean hasEmployees(String companyId, String routeId){
        HashMap<String, MapMarker> employeeMarkers = employeesMapMarkers.get(companyId);
        if (employeeMarkers == null){
            return false;
        }

        for (Map.Entry<String,MapMarker> e: employeeMarkers.entrySet()){
            Employee employee = (Employee) e.getValue().getObject();
            String employeeRouteId = employee.getRouteId();
            long minutesLastSeen = Math.abs(TimeUnit.SECONDS.toMinutes(employee.getLastSeenAgo()));
            System.out.println(employee.getName()+" minutes last seen "+minutesLastSeen);
            if (employeeRouteId !=null && routeId.equalsIgnoreCase(employeeRouteId) && minutesLastSeen < 5){
                return true;
            }
        }
        return false;
    }

    public List<NameValuePair> getCompaniesParams(){
        int count = 0;
        List<NameValuePair> params = new ArrayList<>();

        for(Map.Entry<String,Company> crm: ObjectCache.getCompanies().entrySet()) {
            String companyId = crm.getKey();
            Company company = crm.getValue();

            if (crm.getValue().isFollowing()) {
                // If not set to view employees publicly yet.
                if (!company.getType().equalsIgnoreCase(ECompanyType.PUBLIC.toString())){
                    continue;
                }

                params.add(new BasicNameValuePair("companies[" + count + "]", companyId));
                count++;
            }
        }
        return params;
    }

    private void removeRoutesRefreshDataSets(){
        System.out.println("hide routes 2");
        if (refreshRoutesDataSet!=null){
            refreshRoutesDataSet.requestStop();
        }
    }

    public void updateSettings(String key){

        if (key.equals(ESettingKeys.MAP_REFRESH_INTERVAL.toString())) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
            int mapInterval = Integer.parseInt(prefs.getString(ESettingKeys.MAP_REFRESH_INTERVAL.toString(),"20"));

            if (refreshEmployeesDataSet != null)
                refreshEmployeesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(mapInterval));
        }else if (key.equals(ESettingKeys.COMPANY_REFRESH_INTERVAL.toString())){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
            int companyInterval = Integer.parseInt(prefs.getString(ESettingKeys.MAP_REFRESH_INTERVAL.toString(),"20"));

            if (refreshCompaniesDataSet != null)
                refreshCompaniesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(companyInterval));
        }else if (key.equals(ESettingKeys.POSTS_REFRESH_INTERVAL.toString())){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
            int postsInterval = Integer.parseInt(prefs.getString(ESettingKeys.POSTS_REFRESH_INTERVAL.toString(),"60"));

            if (refreshPostsDataSet != null)
                refreshPostsDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(postsInterval));
        }else if (key.equals(ESettingKeys.PROMOTIONS_REFRESH_INTERVAL.toString())){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
            int promotionsInterval = Integer.parseInt(prefs.getString(ESettingKeys.PROMOTIONS_REFRESH_INTERVAL.toString(),"60"));

            if (refreshPromotionsDataSet != null)
                refreshPromotionsDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(promotionsInterval));
        }else if (key.equals(ESettingKeys.POSTS_REFRESH_INTERVAL.toString())){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
            int showRouteClusters = Integer.parseInt(prefs.getString(ESettingKeys.SHOW_ROUTE_CLUSTERS.toString(),"0"));
            System.out.println("update settings: "+showRouteClusters);
            if (showRouteClusters != 1) {
                removeRoutesRefreshDataSets();
            }
        }else if (key.equals(ESettingKeys.SHOW_TUTORIAL.toString())){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
            int showTutorial = Integer.parseInt(prefs.getString(ESettingKeys.SHOW_TUTORIAL.toString(),"0"));
            if (showTutorial == 1) {
                AppVariables.mainActivity.setTutorial();
            }
        }
    }

    public void refreshMap(){
        if (refreshCompaniesDataSet != null)
            refreshCompaniesDataSet.immediateRefresh();
        if (refreshEmployeesDataSet != null)
            refreshEmployeesDataSet.immediateRefresh();
        if (refreshRoutesDataSet != null)
            refreshRoutesDataSet.immediateRefresh();
    }

    private void updateUserMap(HashMap<String,User> users) {

        for (Map.Entry<String, User> e : users.entrySet()) {
            System.out.println(e.getKey()+" "+e.getValue().getLocation());
            if (e.getValue().getLocation() != null);
        }
    }

    protected void updateCompaniesMap(HashMap<String,Company> companies) {

        for (Map.Entry<String, Company> e : companies.entrySet()) {
            Company company = e.getValue();

            if (companyMapMarkers.containsKey(company.getId())){
                MapMarker mapMarker = companyMapMarkers.get(company.getId());
                mapMarker.changeLocation(company.getLocation());
                continue;
            }

            if (company.getLocation() != null){
                System.out.println(company.getId()+" "+company.getLocation());
                MapMarker mapMarker = new MapMarker(EMapMarkerType.COMPANY,company,company.getId(),NeceCityMapActivity.map,NeceCityMapActivity.context);
                mapMarker.createMarker();
                companyMapMarkers.put(company.getId(),mapMarker);
            }
        }
    }

    private void updateEmployeesMap(HashMap<String, HashMap<String,Employee>> companyEmployees) {
        routeMapEmployees.clear();

        for (Map.Entry<String, HashMap<String, Employee>> ce : companyEmployees.entrySet()) {
            HashMap<String, Employee> employees = ce.getValue();
            String companyId = ce.getKey();

            HashMap<String, MapMarker> employeeMarkers = employeesMapMarkers.get(companyId);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
            int hideInactivity = Integer.parseInt(prefs.getString(ESettingKeys.HIDE_INACTIVITY.toString(), "0"));

            if (employeeMarkers == null) {
                employeeMarkers = new HashMap<>();
            }

            for (Map.Entry<String, Employee> e : employees.entrySet()) {
                Employee employee = e.getValue();
                Route employeeRoute = employee.getRoute();

                //System.out.println("inactivity " + hideInactivity + " " + employee.getLastSeenAgo());
                if ((hideInactivity > 0 && employee.getLastSeenAgo() > hideInactivity) || employee.getStatus() == EStatus.OFFLINE) {
                    if (employeeMarkers.containsKey(employee.getId())) {
                        employeeMarkers.get(employee.getId()).removeMarker();
                        employeeMarkers.remove(employee.getId());
                    }
                    continue;
                }

                if (employeeRoute != null) {
                    HashMap<String, Employee> routeEmployees = routeMapEmployees.get(employeeRoute.getId());
                    if (routeEmployees == null){
                        routeEmployees = new HashMap<>();
                        routeMapEmployees.put(employeeRoute.getId(),routeEmployees);
                    }
                    // Insert the employees for each route
                    routeEmployees.put(employee.getId(),employee);
                }

                routeStopAlarm(employeeRoute, employee.getLocation(), employee.getPrevLocation());

                if (employeeMarkers.containsKey(employee.getId())) {
                    MapMarker mapMarker = employeeMarkers.get(employee.getId());
                    mapMarker.changeLocation(employee.getLocation());
                    continue;
                }

                if (employee.getLocation() != null) {
                    System.out.println(employee.getId() + " " + employee.getLocation());
                    MapMarker mapMarker = new MapMarker(EMapMarkerType.EMPLOYEE, employee, employee.getId(), NeceCityMapActivity.map, NeceCityMapActivity.context);
                    mapMarker.createMarker();
                    employeeMarkers.put(employee.getId(), mapMarker);
                }
            }

            if (!employeeMarkers.isEmpty()){
                employeesMapMarkers.put(companyId,employeeMarkers);
            }else{
                employeesMapMarkers.remove(companyId);
            }
        }

        // If there are no map markers then stop refreshing.
        // Next check will be when companies are refreshed.
        if (employeesMapMarkers.isEmpty()){
            System.out.println("stopping refresh. no markers to show");
            refreshEmployeesDataSet.requestStop();
        }else {
            System.out.println("starting refresh markers to show");
            updateEmployeesRefreshDataSets();
        }
    }

    private void updateRouteClustersMap(HashMap<String,Route> newRoutes) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);

        for (Map.Entry<String, Route> e : newRoutes.entrySet()) {
            Route route = e.getValue();
            ObjectCache.getCompanies().get(route.getCompanyId()).getRoutes().put(route.getId(),route);

            // If the route is no longer being shown skip it.
            if (!routes.containsKey(route.getId())){
                continue;
            }

            //Map route with all info
            Route oldRoute = routes.get(route.getId());

            // routeMarkers are a mix of cluster markers and bus-stop markers.
            // first iteration will put it into a separate list.
            HashMap<Integer,MapMarker> routeClusterMarkers = new HashMap<>();
            HashMap<Integer, LocationCluster> oldLocationClusters = oldRoute.getLocationClusters();
            HashMap<Integer, LocationCluster> locationClusters = route.getLocationClusters();

            Iterator<Integer> it = oldLocationClusters.keySet().iterator();
            while (it.hasNext()) {
                int clusterId = it.next();
                LocationCluster cluster = oldLocationClusters.get(clusterId);
                MapMarker mapMarker = cluster.getMapMarker();

                if (locationClusters.containsKey(cluster.getId())) {
                    routeClusterMarkers.put(clusterId,mapMarker);
                }else{
                    mapMarker.removeMarker();
                    oldLocationClusters.remove(clusterId);
                }
            }

            int hideInactivity = Integer.parseInt(prefs.getString(ESettingKeys.HIDE_INACTIVITY.toString(), "0"));

            System.out.println("cluster size: " + locationClusters.size());
            for (Map.Entry<Integer, LocationCluster> lc : locationClusters.entrySet()) {
                LocationCluster cluster = lc.getValue();
                System.out.println("clusters inactivity " + hideInactivity + " " + cluster.getLastSeenAgo());
                if (hideInactivity > 0 && cluster.getLastSeenAgo() > hideInactivity) {
                    if (routeClusterMarkers.containsKey(cluster.getId())) {
                        routeClusterMarkers.get(cluster.getId()).removeMarker();
                        routeClusterMarkers.remove(cluster.getId());
                    }
                    continue;
                }

                Location currentLocation = new Location("Current cluster location");
                currentLocation.setLatitude(cluster.getLatitude());
                currentLocation.setLongitude(cluster.getLongitude());

                Location oldLocation = null;
                if (routeClusterMarkers.containsKey(cluster.getId())) {
                    System.out.println("old clusters: " + cluster.getStructuredId() + " " + currentLocation);
                    oldLocation = new Location("Old cluster location");
                    LatLng latLng = routeClusterMarkers.get(cluster.getId()).getMarker().getPosition();
                    oldLocation.setLatitude(latLng.latitude);
                    oldLocation.setLongitude(latLng.longitude);
                }

                routeStopAlarm(route, currentLocation, oldLocation);

                if (routeClusterMarkers.containsKey(cluster.getId())) {
                    System.out.println("clusters change location: " + cluster.getStructuredId() + " " + currentLocation);
                    MapMarker mapMarker = routeClusterMarkers.get(cluster.getId());
                    mapMarker.updateMarker();
                    mapMarker.changeLocation(currentLocation);
                    continue;
                }

                if (currentLocation != null) {
                    System.out.println("update clusters map: " + cluster.getStructuredId() + " " + currentLocation);
                    MapMarker mapMarker = new MapMarker(EMapMarkerType.ROUTE_LOCATION_CLUSTER, cluster, "" + cluster.getId(), NeceCityMapActivity.map, NeceCityMapActivity.context);
                    mapMarker.createMarker();

                    cluster.setMapMarker(mapMarker);
                    oldLocationClusters.put(cluster.getId(),cluster);
                }
            }
        }
    }

    /**
     * Set an alarm if the employee is on a route that is being watched
     * @param route
     */
    public void routeStopAlarm(Route route, Location location, Location prevLocation){
        //if (route == null)System.out.println("route null");
        //if (!watchRoutesPoints.containsKey(route.getId())) System.out.println("route not watched");
        if (route != null && watchRoutesPoints.containsKey(route.getId())){
            ArrayList<String> watchRoutePoints = watchRoutesPoints.get(route.getId());

            for (String routePointId:watchRoutePoints){
                RoutePoint stopRoutePoint = route.getRoutePoints().get(routePointId);
                if (stopRoutePoint.isApproaching(location,prevLocation,AppVariables.alarmProximity)){
                    String message = route.getName()+" bus is arriving at "+stopRoutePoint.getName();
                    SetAlarm setAlarm = new SetAlarm(NeceCityMapActivity.context,10,message);
                    setAlarm.startAlarm();
                    watchRoutePoints.remove(stopRoutePoint.getId());
                }
            }
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean showRoute(GoogleMap map, String routeId, String companyId, boolean panToRoute, boolean fetchRoute){

        boolean showing = super.showRoute(map,routeId,companyId,panToRoute,fetchRoute);
        Route route = ObjectCache.getRoute(routeId,companyId);
        if (showing) {
            setEnRoute(route);
            updateRoutesRefreshDataSets();
            NeceCityMapActivity.hideRoutes.setVisibility(View.VISIBLE);
        }

        return showing;
    }

    @Override
    public void hideRoute(String routeId, boolean remove){
        super.hideRoute(routeId,remove);
        updateRoutesRefreshDataSets();
    }

    public void showRoutePopUp(Route route){
        // Show the bottom sheet view
        NeceCityMapActivity.handleRoutesBottomSheet(route,true);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void hideRoutes(){
        super.hideRoutes();
        System.out.println("hide routes 1");
        //removeRoutesRefreshDataSets();
        //NeceCityMapActivity.hideRoutes.setVisibility(View.GONE);
    }

    @Override
    public void prepareSearch(final SearchView searchView){
        searchView.setHint("Where to?");

        searchAdapter.setSuggestionsList(suggestions);
        searchAdapter.setOnSearchItemClickListener(new SearchAdapter.OnSearchItemClickListener() {
            @Override
            public void onSearchItemClick(int position, CharSequence title, CharSequence subtitle) {
                searchView.clearFocus();

                SearchItem item = searchAdapter.getResultsList().get(position);

                onSearchItemClicked(item);
            }
        });
        searchView.setAdapter(searchAdapter);
    }

    @Override
    public void onSearchItemClicked(SearchItem item) {
        searchHandler(item);
        return;
    }

    public void oldSearchHandler(SearchItem item){
        MapMarker mapMarker = (MapMarker) item.getObject();
        ESearchItem eSearchItem = (ESearchItem)item.getSearchItem();
        switch (eSearchItem){
            case MARKER:
                mapMarker.getMarker().showInfoWindow();
                panToPosition(mapMarker,0,0);
                break;
            case ROUTE:
                Company company = (Company) mapMarker.getObject();
                Route route = company.getRoutes().get(item.getId());
                boolean showing = showRoute(NeceCityMapActivity.map, route.getId(), route.getCompanyId(), true, true);
                if (showing){
                    showRoutePopUp(route);
                }
                break;
        }
    }

    @Override
    public void searchHandler(SearchItem item){
        ESearchItem eSearchItem = (ESearchItem)item.getSearchItem();
        System.out.println("In search handler "+eSearchItem.toString());
        switch (eSearchItem){
            case COMPANY:
                Company company = (Company)item.getObject();
                MapMarker companyMapMarker = companyMapMarkers.get(company.getId());
                if (companyMapMarker != null) {
                    companyMapMarker.getMarker().showInfoWindow();
                    panToPosition(companyMapMarker, 0, 0);
                }
                break;
            case EMPLOYEE:
                Employee employee = (Employee) item.getObject();
                MapMarker employeeMapMarker = employeesMapMarkers.get(employee.getCompanyId()).get(employee.getId());

                if (employeeMapMarker != null) {
                    employeeMapMarker.getMarker().showInfoWindow();
                    panToPosition(employeeMapMarker, 0, 0);
                }
                break;
            case ROUTE_CLUSTER:
                LocationCluster cluster = (LocationCluster) item.getObject();
                Company clusterCompany = ObjectCache.getCompanies().get(cluster.getCompanyId());
                Route clusterRoute = clusterCompany.getRoutes().get(cluster.getRouteId());
                if (clusterRoute != null) {
                    //employeeMapMarker.getMarker().showInfoWindow();
                    //panToPosition(employeeMapMarker, 0, 0);
                }
                break;
            case ROUTE:
                Route route = (Route)item.getObject();
                boolean showing = showRoute(NeceCityMapActivity.map, route.getId(), route.getCompanyId(), true, true);
                if (showing){
                    showRoutePopUp(route);
                }
                break;
            case ROUTE_POINT:
                RoutePoint routePoint = (RoutePoint)item.getObject();
                Company routeCompany = ObjectCache.getCompanies().get(routePoint.getCompanyId());
                Route pointRoute = routeCompany.getRoutes().get(routePoint.getRouteId());
                if (pointRoute != null && !isShowing(pointRoute)) {
                    showRoute(map, pointRoute.getId(), pointRoute.getCompanyId(), false, true);
                }
                showStopInformation(routePoint, true);
                break;
        }
    }

    @Override
    public void postGetRoute(Route route, boolean panToRoute){
        boolean showing = showRoute(NeceCityMapActivity.map, route.getId(), route.getCompanyId(), panToRoute, false);
        if (showing){
            showRoutePopUp(route);
        }
    }
}
