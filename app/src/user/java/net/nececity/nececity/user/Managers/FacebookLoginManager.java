package net.nececity.nececity.user.Managers;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.user.Data.FacebookLoginDataSet;
import net.nececity.nececity.user.Views.LoginActivity;
import net.nececity.nececity.user.Views.NeceCityMapActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Takunda on 31/05/2018.
 */

public class FacebookLoginManager implements IGetDataSet {

    private String fbEmail;
    private String fbId;
    private String fbName;
    protected LoginActivity loginActivity;

    public FacebookLoginManager(LoginActivity loginActivity){
        this.loginActivity=loginActivity;
    }

    public void queryFBGraph(final LoginResult loginResult){
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            String details = object.toString();
                            System.out.println(details);

                            fbEmail = object.getString("email");
                            fbName = object.getString("name");
                            fbId = object.getString("id");
                            attemptLogin();
                        } catch (JSONException e) {
                            LoginManager.getInstance().logOut();
                            Toast.makeText(loginActivity,"Please check you internet connection or try the normal Login",
                                    Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday,friends");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void attemptLogin(){
        FacebookLoginDataSet loginDataset = new FacebookLoginDataSet(this,loginActivity,fbId,fbEmail,fbName);
        loginDataset.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        FacebookLoginDataSet loginDataset = (FacebookLoginDataSet)dataSet;

        if (loginDataset.success){
            Intent intent = new Intent(loginActivity, NeceCityMapActivity.class);
            loginActivity.startActivity(intent);
            loginActivity.finish();
        }else{
            LoginManager.getInstance().logOut();
            loginActivity.loginFailed(loginDataset.message);
        }
    }
}
