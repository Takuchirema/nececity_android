package net.nececity.nececity.user.Managers;

import android.content.Intent;

import net.nececity.nececity.user.Views.NeceCityMapActivity;
import net.nececity.nececity.user.Views.SettingsActivity;

/**
 * Created by Takunda on 30/06/2018.
 */

public class SettingsManager {

    public void showSettings(){
        Intent intent = new Intent(NeceCityMapActivity.context, SettingsActivity.class);
        NeceCityMapActivity.context.startActivity(intent);
    }
}
