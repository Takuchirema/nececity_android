package net.nececity.nececity.user.Views;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Abstracts.PromotionListObject;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.PromotionListAdapter;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.CommentsActivity;
import net.nececity.nececity.user.Managers.PromotionsManager;

import java.util.ArrayList;
import java.util.HashMap;

public class PromotionListActivity extends AppCompatActivity implements IListLayout{

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static PromotionsManager promotionsManager;
    private PromotionListActivity promotionListActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_list);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Promotions");
        actionBar.setDisplayHomeAsUpEnabled(true);

        promotionListActivity = this;
        promotionsManager = new PromotionsManager();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);

        setListeners();
        if (ObjectCache.getPromotions().isEmpty()){
            promotionsManager.refreshPromotions(promotionListActivity);
        }else{
            createListAdapter();
        }
    }

    public void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(
            new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    promotionsManager.refreshPromotions(promotionListActivity);
                }
            }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){
        clearRefresh();

        HashMap<String,? extends PromotionListObject> promotions =  ObjectCache.getPromotions();

        ArrayList<String> accountIds = new ArrayList<>(promotions.keySet());
        PromotionListAdapter adapter = new PromotionListAdapter(this, null, CommentsActivity.class, accountIds, R.layout.promotion_list_view);
        adapter.setMapObjects(promotions);
        adapter.sort();

        listView = findViewById(R.id.promotions_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public void clearRefresh(){
        System.out.println("clear refresh");
        swipeRefreshLayout.setRefreshing(false);
    }
}
