package net.nececity.nececity.user.Data;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Data.CustomJSONObject;
import net.nececity.nececity.Data.GetServerData;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.user.Views.LoginActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import net.nececity.nececity.Data.CustomJSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 31/05/2018.
 */

public class FacebookLoginDataSet extends DataSet {

    private String fbEmail;
    private String fbId;
    private String fbName;

    SharedPreferences preferences;
    private LoginActivity loginActivity;

    public FacebookLoginDataSet(IGetDataSet getDataset, LoginActivity loginActivity, String fbId, String fbEmail, String fbName) {
        super(getDataset);
        this.fbEmail=fbEmail;
        this.fbId=fbId;
        this.fbName=fbName;
        this.loginActivity=loginActivity;
    }

    @Override
    public void getDataSet(){

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("fbID", fbId));
        params.add(new BasicNameValuePair("fbEmail", fbEmail));
        params.add(new BasicNameValuePair("fbName", fbName));

        String url = UrlConstructor.facebookLoginUrl();

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.POST);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        CustomJSONArray userJsonArray = json.getJSONArray("users");
        CustomJSONObject userJson=null;

        if (userJsonArray.length() == 0) {
            message = "User was not found";
            success = false;
        } else {
            userJson = userJsonArray.getJSONObject(0);
        }

        //do this early so that the update profile has a token and userID
        AppVariables.token = json.getString("token");

        User user = new User(userJson.getString("fbName"));
        user.setStatus("online");
        user.setFbName(userJson.getString("fbName"));
        user.setId(userJson.getString("fbId"));
        user.setFbId(userJson.getString("fbId"));
        user.setEmail(userJson.getString("email"));
        user.setPhoneNumber(userJson.getString("phoneNumber"));

        //set the alert
        if (!userJson.isNull("alert")){
            System.out.println("user is on alert");
            user.setOnAlert("true");
        }

        user.setPictureUrl(userJson.getString("profilePicture"));

        setUserPreferences(user,json.getString("token"));
        AppVariables.user = user;
    }

    public void setUserPreferences(User user,String token){
        preferences = PreferenceManager.getDefaultSharedPreferences(loginActivity);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("fbID", user.getId());
        editor.putString("fbName", user.getName());
        editor.putString("fbEmail", user.getEmail());
        editor.putString("phoneNumber", user.getPhoneNumber());
        editor.putString("pictureUrl", user.getPictureUrl());
        editor.putString("token", token);

        editor.apply();
    }
}
