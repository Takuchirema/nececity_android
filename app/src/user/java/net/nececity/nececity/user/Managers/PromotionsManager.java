package net.nececity.nececity.user.Managers;

import android.app.Activity;
import android.widget.Toast;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Abstracts.ISendServerPicture;
import net.nececity.nececity.Data.PromotionsDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.user.Views.NeceCityMapActivity;
import net.nececity.nececity.user.Views.PromotionListActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 8/4/2018.
 */

public class PromotionsManager implements IGetDataSet {

    private IListLayout listLayout;

    public PromotionsManager(){}

    public void refreshPromotions(IListLayout listLayout){
        this.listLayout = listLayout;

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("userID", AppVariables.user.getId()));

        PromotionsDataSet promotionsDataSet = new PromotionsDataSet(this);
        promotionsDataSet.setUrl(UrlConstructor.getUserPromotionsUrl(AppVariables.user.getId()));
        promotionsDataSet.setParams(params);
        promotionsDataSet.getDataSet();
    }

    public void refreshPromotions(IListLayout listLayout, String companyId){
        this.listLayout = listLayout;

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("userID", AppVariables.user.getId()));

        PromotionsDataSet promotionsDataSet = new PromotionsDataSet(this);
        promotionsDataSet.setUrl(UrlConstructor.getCompanyPromotionsUrl(companyId));
        promotionsDataSet.setParams(params);
        promotionsDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        PromotionsDataSet promotionsDataSet = (PromotionsDataSet)dataSet;

        if (listLayout != null){
            listLayout.clearRefresh();
        }else{
            return;
        }

        if (promotionsDataSet.success) {
            ObjectCache.refreshPromotions(promotionsDataSet.getPromotions());
            if (!promotionsDataSet.getPromotions().isEmpty()) {
                listLayout.createListAdapter();
                AppVariables.mainActivity.promotionsToday(promotionsDataSet.getPromotionsToday());
            }
        }else{
            AppVariables.mainActivity.showMessage(promotionsDataSet.message);
        }
    }
}
