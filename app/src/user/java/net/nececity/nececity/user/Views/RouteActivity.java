package net.nececity.nececity.user.Views;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lapism.searchview.widget.SearchAdapter;
import com.lapism.searchview.widget.SearchItem;
import com.lapism.searchview.widget.SearchView;

import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.StopListAdapter;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;
import net.nececity.nececity.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RouteActivity extends AppCompatActivity {

    private String routeId;
    private String companyId;

    private static RoutePoint fromPoint;
    private static RoutePoint toPoint;
    private static ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        routeId = getIntent().getExtras().getString("routeId");
        companyId = getIntent().getExtras().getString("companyId");

        Route route = ObjectCache.getCompanies().get(companyId).getRoutes().get(routeId);

        if (route == null || !route.isReady()){
            return;
        }

        createViewList(this.findViewById(android.R.id.content).getRootView(),route);
        //createListAdapter(this.findViewById(android.R.id.content), route);
    }

    public static View populateMainView(View view, String companyId, String routeId){
        fromPoint = null;
        toPoint = null;

        Route route = ObjectCache.getCompanies().get(companyId).getRoutes().get(routeId);
        if (route == null || !route.isReady()){
            return view;
        }

        TextView routeIdTV = view.findViewById(R.id.routeId);
        routeIdTV.setText(route.getName());

        prepareJourneyView(view,route);
        //createViewList(view,route);
        createListAdapter(view, route);
        return view;
    }

    public static void prepareJourneyView(View view, Route route){
        Context context = view.getContext();

        SearchView fromET = view.findViewById(R.id.fromStop);
        SearchView toET = view.findViewById(R.id.toStop);

        fromET.setText("");
        toET.setText("");
        fromET.setHint("Going From..");
        toET.setHint("Going To..");

        SearchAdapter fromSearchAdapter = new SearchAdapter(context);
        SearchAdapter toSearchAdapter = new SearchAdapter(context);

        setJourneyListeners(view,fromET,toET,route,fromSearchAdapter,toSearchAdapter);

        ArrayList<SearchItem> suggestions = new ArrayList<>();

        HashMap<String, RoutePoint> busStopPoints =  route.getBusStopPoints();

        for (Map.Entry<String, RoutePoint> e: busStopPoints.entrySet()){
            RoutePoint routePoint = e.getValue();

            SearchItem suggestion = new SearchItem(context);
            suggestion.setTitle(routePoint.getName());
            suggestion.setIcon1Resource(R.drawable.bus_stop);
            suggestion.setObject(routePoint);

            suggestions.add(suggestion);
        }

        fromSearchAdapter.setSuggestionsList(suggestions);
        toSearchAdapter.setSuggestionsList(suggestions);

        fromET.setAdapter(fromSearchAdapter);
        toET.setAdapter(toSearchAdapter);
    }

    private static void setJourneyListeners(final View view, final View fromView, final View toView, final Route route, final SearchAdapter fromSA, final SearchAdapter toSA){

        fromSA.setOnSearchItemClickListener(new SearchAdapter.OnSearchItemClickListener() {
            @Override
            public void onSearchItemClick(int position, CharSequence title, CharSequence subtitle) {
                SearchItem fromItem = fromSA.getResultsList().get(position);
                fromPoint = (RoutePoint)fromItem.getObject();
                ((SearchView)fromView).setText(fromPoint.getName());

                System.out.println("search position: "+position);
                fromView.clearFocus();

                if (toPoint != null){
                    filterViewList(view,route,fromPoint,toPoint);
                }
            }
        });

        toSA.setOnSearchItemClickListener(new SearchAdapter.OnSearchItemClickListener() {
            @Override
            public void onSearchItemClick(int position, CharSequence title, CharSequence subtitle) {
                SearchItem toItem = toSA.getResultsList().get(position);
                toPoint = (RoutePoint)toItem.getObject();
                ((SearchView)toView).setText(toPoint.getName());

                toView.clearFocus();

                if (fromPoint != null){
                    filterViewList(view,route,fromPoint,toPoint);
                }
            }
        });
    }

    public static void filterViewList(View view, Route route, RoutePoint fromPoint, RoutePoint toPoint){
        boolean started = false;
        Date nextArrivalTime=null;

        LinearLayout routesListView = view.findViewById(R.id.stop_list);
        routesListView.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        HashMap<String, RoutePoint> busStopPoints =  route.getBusStopPoints();

        for (Map.Entry<String, RoutePoint> e: busStopPoints.entrySet()){
            RoutePoint routePoint = e.getValue();

            if (!started) {
                if (routePoint.getId().equals(fromPoint.getId()) || routePoint.getId().equals(toPoint.getId())) {
                    started = true;
                }else{
                    continue;
                }
            }else{
                if (routePoint.getId().equals(fromPoint.getId()) || routePoint.getId().equals(toPoint.getId())) {
                    started = false;
                }
            }

            View rowView= inflater.inflate(R.layout.content_bus_stop_list_info, null, true);

            String time = prepareView(rowView,routePoint,nextArrivalTime);
            try {
                nextArrivalTime = new SimpleDateFormat("HH:mm").parse(time);
            }catch (Exception ex){}

            routesListView.addView(rowView);

            if (!started){
                break;
            }
        }
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public static void createListAdapter(View view, Route route){
        HashMap<String,? extends ListObject> stopPoints =  route.getBusStopPoints();

        ArrayList<String> stopIds = new ArrayList<>(stopPoints.keySet());
        StopListAdapter adapter = new StopListAdapter(route, (Activity)AppVariables.mainActivity, stopIds, R.layout.content_bus_stop_list_info);
        adapter.setMapObjects(stopPoints);

        listView = view.findViewById(R.id.stops_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public static void setArrivalTimes(Route route){
        Date nextArrivalTime=null;

        HashMap<String, RoutePoint> busStopPoints =  route.getBusStopPoints();

        for (Map.Entry<String, RoutePoint> e: busStopPoints.entrySet()){
            RoutePoint routePoint = e.getValue();

            String time = routePoint.getNextArrivalTime(nextArrivalTime);
            routePoint.setEstimatedArrivalTime(time);
            try {
                nextArrivalTime = new SimpleDateFormat("HH:mm").parse(time);
            }catch (Exception ex){}
        }
    }

    public static void createViewList(View view,Route route){

        LinearLayout routesListView = view.findViewById(R.id.stop_list);
        routesListView.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        HashMap<String, RoutePoint> busStopPoints =  route.getBusStopPoints();

        setArrivalTimes(route);
    }

    private static String prepareView(View rowView, final RoutePoint routePoint, Date afterTime){
        String nextArrivalTime;

        TextView stopId = rowView.findViewById(R.id.primaryTV);
        stopId.setText(routePoint.getName());

        TextView estimatedArrival = rowView.findViewById(R.id.secondaryTV);
        nextArrivalTime = routePoint.getNextArrivalTime(afterTime);
        estimatedArrival.setText(nextArrivalTime);

        View watchRoute = rowView.findViewById(R.id.watchRoute);
        ((IMapActivity) AppVariables.mainActivity).getMapManager().watchRoutePoint(watchRoute,routePoint);

        if (routePoint.distanceFrom(AppVariables.user.getLocation()) <= AppVariables.busStopProximity){
            ((ImageView) rowView.findViewById(R.id.stopLine)).setImageResource(R.drawable.current_stop_line);
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((IMapActivity)AppVariables.mainActivity).getMapManager().showStopInformation(routePoint,false);
            }
        });

        return nextArrivalTime;
    }
}
