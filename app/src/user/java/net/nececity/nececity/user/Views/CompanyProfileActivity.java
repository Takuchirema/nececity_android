package net.nececity.nececity.user.Views;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.R;
import net.nececity.nececity.Managers.ViewManager;

public class CompanyProfileActivity extends AppCompatActivity {

    private String companyId;
    private ScrollView mainView;
    private CompanyProfileActivity companyProfileActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        companyProfileActivity = this;

        companyId = getIntent().getExtras().getString("companyId");

        populateMainView(this.findViewById(android.R.id.content).getRootView(),companyId);
        setListeners();
        setLayoutParameters(this.findViewById(android.R.id.content).getRootView());
    }

    public void setListeners(){
        LinearLayout viewMenu = findViewById(R.id.viewMenu);
        LinearLayout phoneNumberLayout = findViewById(R.id.phoneNumberLayout);
        final String phoneNumber = ((TextView)findViewById(R.id.phoneNumber)).getText().toString();

        viewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(companyProfileActivity,CataloguePostListActivity.class);
                intent.putExtra("companyId",companyId);
                startActivity(intent);
            }
        });

        phoneNumberLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+phoneNumber));
                    startActivity(intent);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });
    }

    public static View populateMainView(View view, String companyId){
        final Company company = ObjectCache.getCompanies().get(companyId);

        if (company == null) {
            return view;
        }

        ImageView mainPictureIV = view.findViewById(R.id.mainPicture);
        final ImageView followIV = view.findViewById(R.id.secondaryIV);

        TextView companyIdTV = view.findViewById(R.id.companyId);
        TextView aboutTV = view.findViewById(R.id.about);
        TextView address = view.findViewById(R.id.address);
        TextView email = view.findViewById(R.id.email);
        TextView phoneNumber = view.findViewById(R.id.phoneNumber);

        if (company.getBmp() != null) {
            Drawable drawable = new BitmapDrawable(NeceCityMapActivity.context.getResources(), company.getBmp());
            mainPictureIV.setImageDrawable(drawable);
        }else{
            ViewManager viewManager = new ViewManager(mainPictureIV, EActionType.DOWNLOAD_PICTURE,(Activity) NeceCityMapActivity.context);
            viewManager.setUrl(company.getPictureUrl());
            viewManager.setObject(company);
            viewManager.getPicture();
        }

        setFollowView(followIV,companyId);
        followCompany(followIV,companyId);

        companyIdTV.setText(company.getName());
        aboutTV.setText(company.getAbout());
        address.setText(company.getAddress());
        email.setText(company.getEmail());
        phoneNumber.setText(company.getPhoneNumber());

        return view;
    }

    public static void setFollowView(ImageView followIV, String companyId){
        final Company company = ObjectCache.getCompanies().get(companyId);

        if (company.isFollowing()){
            followIV.setImageResource(R.drawable.done);
            followIV.setBackgroundResource(R.drawable.round_green_button);
        }else{
            followIV.setImageResource(R.drawable.ic_add_white);
            followIV.setBackgroundResource(R.drawable.round_orange_button);
        }
    }

    public static void followCompany(final ImageView followIV, String companyId){
        final Company company = ObjectCache.getCompanies().get(companyId);

        followIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = UrlConstructor.followCompanyUrl(AppVariables.user.getId(),company.getId());
                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.FOLLOW_COMPANY;

                if (company.isFollowing()){
                    url = UrlConstructor.unfollowCompanyUrl(AppVariables.user.getId(),company.getId());
                    httpMethod = EHttpMethod.DELETE;
                    actionType = EActionType.UNFOLLOW_COMPANY;
                }

                ViewManager viewManager = new ViewManager(followIV,actionType,(Activity) NeceCityMapActivity.context);
                viewManager.setObjectId(company.getId());
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.followCompany();
            }
        });
    }


    public void setLayoutParameters(View view){
        ScrollView.LayoutParams lp =
                new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT,ScrollView.LayoutParams.MATCH_PARENT);
        lp.setMargins(0,0,0,0);
        view.setLayoutParams(lp);
    }
}
