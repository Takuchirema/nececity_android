package net.nececity.nececity.user.Views;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.lapism.searchview.Search;
import com.lapism.searchview.widget.SearchAdapter;
import com.lapism.searchview.widget.SearchView;
import com.squareup.picasso.Picasso;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EMapMarkerType;
import net.nececity.nececity.Abstracts.ENotificationType;
import net.nececity.nececity.Abstracts.ESettingKeys;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Abstracts.LocationResult;
import net.nececity.nececity.BuildConfig;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.LocationHandler;
import net.nececity.nececity.Helpers.BackgroundService;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Managers.FileManager;
import net.nececity.nececity.Managers.LocationManager;
import net.nececity.nececity.Managers.NotificationManager;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;
import net.nececity.nececity.user.Managers.CityMapManager;
import net.nececity.nececity.Managers.PermissionsManager;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.Objects.MapMarker;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.R;
import net.nececity.nececity.user.Managers.CompaniesManager;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import me.toptas.fancyshowcase.listener.OnViewInflateListener;

public class NeceCityMapActivity extends AppCompatActivity implements OnMapReadyCallback, IMapActivity,
                            NavigationView.OnNavigationItemSelectedListener, GoogleMap.OnMarkerClickListener {

    public static Context context;
    public static GoogleMap map;
    public static GoogleMap.InfoWindowAdapter mapAdapter;
    public static CityMapManager mapManager;
    public DrawerLayout drawer;
    private LayoutInflater inflater;

    public static LocationManager locationManager;
    protected PermissionsManager permissionsManager;
    private LocationHandler myLocationHandler;
    protected static NotificationManager notificationManager;

    private static BottomSheetBehavior sheetBehavior;
    private static NestedScrollView companyLayoutBottomSheet;
    private static NestedScrollView employeeLayoutBottomSheet;
    private static NestedScrollView routeLayoutBottomSheet;
    private static LinearLayout dashboardLayoutBottomSheet;
    private RelativeLayout searchLayoutBottomSheet;

    protected SearchView searchView;
    protected static NavigationView navigationView;
    public static FloatingActionButton hideRoutes;
    public static FloatingActionButton myLocation;
    public static FloatingActionButton refreshMap;
    private Animation rotate;

    public FancyShowCaseView showCaseView;

    public View locationButton;
    public SupportMapFragment mapFragment;

    public boolean deprecationWarned=false;
    private int UPDATE_VERSION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        context = this;

        uiSetUp();

        appManagerSetUp();

        setAppCodes();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsManager.canAccessLocation()) {
                System.out.println("can access location");
                appSetUp();
            } else {
                System.out.println("cannot access location");
                permissionsManager.requestPermissions();
            }
        }else{
            appSetUp();
        }
    }

    public void uiSetUp(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        inflater = (LayoutInflater) NeceCityMapActivity.context.getSystemService(LAYOUT_INFLATER_SERVICE);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        searchView = findViewById(R.id.searchView);
        hideRoutes = findViewById(R.id.hideRoutes);
        myLocation = findViewById(R.id.myLocation);
        refreshMap = findViewById(R.id.refreshMap);

        companyLayoutBottomSheet = findViewById(R.id.company_profile_bottom_sheet);
        employeeLayoutBottomSheet = findViewById(R.id.employee_profile_bottom_sheet);
        routeLayoutBottomSheet = findViewById(R.id.routes_bottom_sheet);
        searchLayoutBottomSheet = findViewById(R.id.search_map_bottom_sheet);
        dashboardLayoutBottomSheet = findViewById(R.id.dashboard_bottom_sheet);

        sheetBehavior = BottomSheetBehavior.from(employeeLayoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        sheetBehavior = BottomSheetBehavior.from(companyLayoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        sheetBehavior = BottomSheetBehavior.from(searchLayoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        sheetBehavior = BottomSheetBehavior.from(routeLayoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        sheetBehavior = BottomSheetBehavior.from(dashboardLayoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    public void appManagerSetUp(){
        permissionsManager = new PermissionsManager(this);

        mapManager = new CityMapManager(map,this);
        mapManager.prepareSearch(searchView);

        notificationManager = new NotificationManager(this);
    }

    public void appSetUp(){
        termsAndConditions();
        monitorLocation();
        mapManager.startManager();
        setListeners();
        setTutorial();
        setPicasso();
        startBackgroundService();
        //showCompanyAds();
    }

    public void showCompanyAds(){

        AlertDialog.Builder companyAdsDialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup companyAds = findViewById(R.id.companyAds);

        View layout = inflater.inflate(R.layout.content_company_ads, companyAds);
        companyAdsDialog.setView(layout);

        AlertDialog alert = companyAdsDialog.create();
        alert.show();
    }

    @Override
    public void startBackgroundService(){
        Intent serviceIntent = new Intent(this, BackgroundService.class);
        putIntentExtras(serviceIntent);

        stopService(new Intent(this, BackgroundService.class));
        startService(serviceIntent);
    }

    public void putIntentExtras(Intent intent){
        Bundle extras = new Bundle();

        if (AppVariables.company != null) {
            extras.putString("companyId",AppVariables.company.getId());
        }

        if (AppVariables.enRoute != null){
            extras.putSerializable("routeId",AppVariables.enRoute.getId());
            extras.putSerializable("routeCompanyId",AppVariables.enRoute.getCompanyId());
        }

        extras.putString("userName",AppVariables.user.getName());
        extras.putString("userId",AppVariables.user.getId());
        extras.putString("countryCode",AppVariables.getCountryCode());
        extras.putString("token",AppVariables.token);

        try{
            extras.putInt("versionCode",AppVariables.setVersionCode(this));
            extras.putString("versionName",AppVariables.setVersionName(this));
            extras.putString("versionSuffix",AppVariables.setVersionNameSuffix(this));
        }catch (Exception ex){}

        intent.putExtras(extras);
    }

    public void setPicasso(){
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }

    public void setListeners(){

        setCrashListener();

        hideRoutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapManager.hideRoutes();
            }
        });

        refreshMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_clockwise);
                refreshMap.startAnimation(rotate);

                mapManager.refreshMap();
            }
        });

        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationButton.callOnClick();
            }
        });

        searchView.setOnMenuClickListener(new Search.OnMenuClickListener() {
            @Override
            public void onMenuClick() {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        searchView.setOnLogoClickListener(new Search.OnLogoClickListener() {
            @Override
            public void onLogoClick() {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                setDrawerTutorial(drawerView);
                ImageView userIcon = drawerView.findViewById(R.id.userIcon);
                TextView userName = drawerView.findViewById(R.id.userName);
                User user = AppVariables.user;
                if (user.getPictureBmp() == null){
                    String url = user.getPictureUrl();
                    System.out.println("picture url - ");
                    if (!user.hasPic() && user.getFbId() != null){
                        url = "https://graph.facebook.com/" + AppVariables.user.getId() + "/picture?type=large";
                    }
                    ViewManager viewManager = new ViewManager(userIcon, EActionType.DOWNLOAD_PICTURE, (Activity)context);
                    viewManager.setObject(AppVariables.user);
                    viewManager.setImageBackground(true);
                    viewManager.setUrl(url);
                    viewManager.getPicture();
                }else {
                    Drawable drawable = new BitmapDrawable(NeceCityMapActivity.context.getResources(), user.getPictureBmp());
                    userIcon.setBackground(drawable);
                }

                LinearLayout profileLayout = drawerView.findViewById(R.id.profileLayout);
                profileLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(NeceCityMapActivity.context, UserProfileActivity.class);
                        startActivity(intent);
                    }
                });

                userName.setText(user.getName());
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        // Get the search close button image view
        ImageView closeButton = searchView.getmImageViewClear();
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.clearFocus();
            }
        });
    }

    /**
     * Only use for debugging! NOT in production code!
     */
    private void setCrashListener(){
        if (!BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
            return;
        }

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(){
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                //String timeStamp = new SimpleDateFormat(AppVariables.timeFormat).format(Calendar.getInstance().getTime());
                String stackTrace = Log.getStackTraceString(ex);
                FileManager fileManager = new FileManager(AppVariables.context);
                fileManager.writeToFile(AppVariables.errorLogName,stackTrace);
                System.exit(1);
            }
        });
    }

    private void setDashboardQueryListener(boolean on){

        if (!on){
            mapManager.prepareSearch(searchView);
            searchView.setOnQueryTextListener(null);
            return;
        }

        searchView.setAdapter(new SearchAdapter(this));
        searchView.setOnQueryTextListener(new Search.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(CharSequence query) {
                DashboardActivity.filter(query.toString());
                return true;
            }

            @Override
            public void onQueryTextChange(CharSequence query) {
                DashboardActivity.filter(query.toString());
            }
        });
    }

    public boolean showTutorial(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int showTutorial = Integer.parseInt(preferences.getString(ESettingKeys.SHOW_TUTORIAL.toString(),"1"));

        if (showTutorial == 0){
            return false;
        }

        return true;
    }

    @Override
    public void setTutorial(){
        //System.out.println("set tutorial");
        if (!showTutorial()){
            //System.out.println("dont show tutorial");
            return;
        }

        showCaseView = new FancyShowCaseView.Builder(this)
                .focusOn(searchView)
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .customView(R.layout.tutorial_template, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(@NotNull View view) {
                        setSearchTutorialItems(view);
                    }
                })
                .build();

        showCaseView.show();
    }

    public void setSearchTutorialItems(View view){
        LinearLayout container = view.findViewById(R.id.tutorial_items);

        TextView next = view.findViewById(R.id.next);
        next.setVisibility(View.VISIBLE);
        TextView titleTV = view.findViewById(R.id.title);
        titleTV.setText("Start here, search for routes, bus locations, restaurants and more!");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCaseView.hide();
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        container.addView(setTutorialItem(R.drawable.ic_location_city_white,"Search company locations"));
        container.addView(setTutorialItem(R.drawable.route_white,"Search for bus routes"));
        container.addView(setTutorialItem(R.drawable.ic_directions_bus_white_48dp,"Search for buses real time locations"));
        container.addView(setTutorialItem(R.drawable.bus_stop_white,"Search for bus stops"));
    }

    public void setDrawerTutorial(View drawerView){
        if (!showTutorial()){
            return;
        }

        ImageView userIcon = drawerView.findViewById(R.id.userIcon);

        new FancyShowCaseView.Builder(this)
                .focusOn(userIcon)
                .focusShape(FocusShape.CIRCLE)
                .customView(R.layout.tutorial_template, new OnViewInflateListener() {
                    @Override
                    public void onViewInflated(@NotNull View view) {
                        setDrawerTutorialItems(view);
                    }
                })
                .build()
                .show();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ESettingKeys.SHOW_TUTORIAL.toString(),"0");
        editor.apply();
    }

    public void setDrawerTutorialItems(View view){
        LinearLayout container = view.findViewById(R.id.tutorial_items);

        TextView titleTV = view.findViewById(R.id.title);
        titleTV.setText("Edit profile, settings and view information.");

        container.addView(setTutorialItem(R.drawable.ic_person_white,"Edit your user profile"));
        container.addView(setTutorialItem(R.drawable.companies_white,"Follow more companies"));
        container.addView(setTutorialItem(R.drawable.posts_white,"View company posts"));
        container.addView(setTutorialItem(R.drawable.promotions_white,"View company promotions"));
        container.addView(setTutorialItem(R.drawable.ic_action_settings,"Consume less data by customizing your settings"));
    }

    public View setTutorialItem(int imageResource, String itemDetail){
        View item = inflater.inflate(R.layout.tutorial_item, null, true);
        ImageView icon = item.findViewById(R.id.icon);
        TextView detail = item.findViewById(R.id.detail);
        icon.setImageResource(imageResource);
        detail.setText(itemDetail);

        return item;
    }

    @Override
    public void setAppCodes(){
        AppVariables.mainActivity =this;
        try {
            AppVariables.setVersionCode(this);
            AppVariables.setVersionName(this);
            AppVariables.setVersionNameSuffix(this);
            AppVariables.setEnterpriseId(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateSettings(String key) {
        mapManager.updateSettings(key);
    }

    @Override
    public CityMapManager getMapManager() {
        return mapManager;
    }

    @Override
    public void updateDashboard() {
        System.out.println("** update dashboard 2 **");
        handleDashboardBottomSheet();
    }

    @Override
    public void collapseDashboard() {
        sheetBehavior = BottomSheetBehavior.from(dashboardLayoutBottomSheet);
        if(sheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            return;
        }
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void logout(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        if (preferences.contains("userID")){
            editor.remove("userID").commit();
        }

        if (preferences.contains("fbID") && preferences.contains("fbEmail") && preferences.contains("fbName")){
            editor.remove("fbID").commit();
            editor.remove("fbEmail").commit();
            editor.remove("fbName").commit();
        }

        LoginManager.getInstance().logOut();

        Intent a = new Intent(context,LoginActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(a);
        System.exit(0);
    }

    public void termsAndConditions(){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = preferences.edit();
        if (preferences.contains("termsAndConditions")){
            return;
        }

        AlertDialog.Builder termsAndConditionsDialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup termsAndConditions = findViewById(R.id.termsAndConditions);

        View layout = inflater.inflate(R.layout.terms_and_conditions,
                termsAndConditions);
        termsAndConditionsDialog.setView(layout);
        termsAndConditionsDialog.setPositiveButton("Accept", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                editor.putString("termsAndConditions","accepted");
                editor.apply();
                dialog.dismiss();
            }
        });

        termsAndConditionsDialog.setNegativeButton("Reject", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        });

        AlertDialog alert = termsAndConditionsDialog.create();
        alert.show();
        Button ok = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        ok.setTextColor(Color.WHITE);
        ok.setBackgroundColor(Color.parseColor("#ffa500"));

        Button cancel = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        cancel.setTextColor(Color.WHITE);
        cancel.setBackgroundColor(Color.parseColor("#db2929"));

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ok.getLayoutParams();
        layoutParams.weight = 10;

        ok.setLayoutParams(layoutParams);
        cancel.setLayoutParams(layoutParams);
    }

    @Override
    public void deprecatedVersion(){

        if (deprecationWarned){
            return;
        }

        int maxVersion = Collections.max(AppVariables.supportedVersions);

        AlertDialog.Builder termsAndConditionsDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup termsAndConditions = findViewById(R.id.termsAndConditions);

        View layout = inflater.inflate(R.layout.terms_and_conditions, termsAndConditions);

        TextView termsAndConditionsTxt = layout.findViewById(R.id.termsAndConditionsTxt);
        termsAndConditionsTxt.setText(
                "Please update NeceCity by downloading the latest version from Google Play Store." +
                " The latest version is "+maxVersion+" and your current version is "+AppVariables.versionCode+
                        " NeceCity will not function properly without the latest updates. Thank you.");

        termsAndConditionsDialog.setView(layout);
        termsAndConditionsDialog.setPositiveButton("Accept", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                updateVersion();
                //logout();
            }
        });

        AlertDialog alert = termsAndConditionsDialog.create();
        alert.setCancelable(false);
        alert.show();

        Button ok = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        ok.setTextColor(Color.WHITE);
        ok.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ok.getLayoutParams();
        layoutParams.weight = 10;
        ok.setLayoutParams(layoutParams);

        deprecationWarned = true;
    }

    public void updateVersion(){
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        System.out.println("package: "+appPackageName);
        try {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)),UPDATE_VERSION);
        } catch (Exception ex) {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)),UPDATE_VERSION);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                MapMarker mapMarker = (MapMarker) marker.getTag();
                onMapMarkerInfoWindowClick(mapMarker);
            }
        });

        mapAdapter = new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                System.out.println("get info contents");
                MapMarker mapMarker = (MapMarker) marker.getTag();
                View markerView = mapMarker.getMarkerView(getLayoutInflater());
                return markerView;
            }
        };

        map.setInfoWindowAdapter(mapAdapter);
        mapManager.setMap(map);
        enableMyLocation();
    }

    public void monitorLocation() {
        enableMyLocation();
        LocationResult locationResult = new LocationResult(){
            @Override
            public void gotLocation(final Location location){
                if (locationManager == null){
                    locationManager = new LocationManager(location);
                }
                locationManager.onLocationChanged(map,location);
            }
        };

        myLocationHandler = new LocationHandler();
        myLocationHandler.getLocation(this, locationResult);

        if (AppVariables.user.getLocation() == null){
            AppVariables.user.setLocation(LocationHandler.getLastLocation());
        }

        if (map!=null && AppVariables.user.getLocation() != null){
            double latitude=AppVariables.user.getLocation().getLatitude();
            double longitude = AppVariables.user.getLocation().getLongitude();
            LocationManager.panToPosition(map,latitude,longitude);
        }
    }

    @SuppressLint("RestrictedApi")
    public void enableMyLocation(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsManager.requestPermissions();
            return;
        }
        if (map == null) {
            return;
        }

        map.setMyLocationEnabled(true);
        myLocation.setVisibility(View.VISIBLE);

        locationButton = ((View)mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        // Change the visibility of my location button
        if(locationButton != null) {
            locationButton.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPDATE_VERSION) {
            logout();
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        int inititalRequest = permissionsManager.INITIAL_REQUEST;
        if (permsRequestCode == inititalRequest && grantResults.length > 0) {
            boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            if (locationAccepted){
                appSetUp();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nece_city, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.nav_companies:
                intent = new Intent(this, CompanyListActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_posts:
                intent = new Intent(this, PostListActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_promotions:
                intent = new Intent(this, PromotionListActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_settings:
                //intent = new Intent(NeceCityMapActivity.context, DashboardActivity.class);
                //startActivity(intent);
                intent = new Intent(NeceCityMapActivity.context, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_share:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage("com.whatsapp");
                intent.putExtra(Intent.EXTRA_TEXT, AppVariables.appLink);
                try {
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    showMessage("Whatsapp have not been installed.");
                }
                break;
        }

        return false;
    }

    @Override
    public void unseenPosts(boolean unseen,Post latestPost){
        ImageView view = ((Activity)context).findViewById(R.id.post_notification);
        if (unseen) {
            notifyPost(latestPost);
            view.setVisibility(View.VISIBLE);
        }else{
            view.setVisibility(View.GONE);
        }
        view.invalidate();
    }

    @Override
    public void promotionsToday(HashMap<String,Promotion> promotionsToday){
        System.out.println("promotions today: "+promotionsToday.size());
        ImageView view = ((Activity)context).findViewById(R.id.promotion_notification);
        if (!promotionsToday.isEmpty()) {
            view.setVisibility(View.VISIBLE);
        }else{
            view.setVisibility(View.GONE);
        }

        for (Map.Entry<String, Promotion> e:promotionsToday.entrySet()){
            notifyPromotion(e.getValue());
        }

        view.invalidate();
    }

    public void notifyPromotion(Promotion promotion){
        System.out.println("notify promotion "+promotion.getPromotion());
        String companyId = promotion.getCompanyId();
        String title = promotion.getCompanyName()+" Special!";
        String message = "Check today's promotion from "+promotion.getCompanyName();
        ENotificationType notificationType = ENotificationType.PROMOTION;

        Intent intent = new Intent(context,PromotionListActivity.class);
        notificationManager.notify(companyId,title,message,notificationType,intent);
    }

    public void notifyPost(Post post){
        String companyId = post.getCompanyId();
        String title = "New post from "+post.getCompanyName();
        String message = post.getPost();
        ENotificationType notificationType = ENotificationType.POST;

        Intent intent = new Intent(context,PostListActivity.class);
        notificationManager.notify(companyId,title,message,notificationType,intent);
    }

    @Override
    public void showMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        System.out.println("marker clicked");
        MapMarker mapMarker = (MapMarker) marker.getTag();

        onMapMarkerClick(mapMarker);
        return false;
    }

    public void onMapMarkerClick(MapMarker mapMarker){
        EMapMarkerType mapMarkerType = mapMarker.getMarkerType();
        String companyId;

        switch (mapMarkerType){
            case COMPANY:
                System.out.println("company type");
                companyId = ((Company)mapMarker.getObject()).getId();

                sheetBehavior = BottomSheetBehavior.from(companyLayoutBottomSheet);
                companyLayoutBottomSheet = (NestedScrollView) CompanyProfileActivity.populateMainView(companyLayoutBottomSheet,companyId);

                handleCompanyBottomSheet();
                break;
            case EMPLOYEE:
                Employee employee = ((Employee)mapMarker.getObject());
                String employeeId = employee.getId();
                companyId = employee.getCompanyId();

                boolean showRoute = mapManager.showRoute(map,employee.getRouteId(),employee.getCompanyId(),false, true);

                sheetBehavior = BottomSheetBehavior.from(employeeLayoutBottomSheet);
                employeeLayoutBottomSheet = (NestedScrollView) EmployeeProfileActivity.populateMainView(employeeLayoutBottomSheet,companyId,employeeId);

                handleEmployeeBottomSheet(showRoute);
                break;
            case ROUTE_POINT:
                RoutePoint routePoint = (RoutePoint)mapMarker.getObject();
                Company company = ObjectCache.getCompanies().get(routePoint.getCompanyId());
                Route route = company.getRoutes().get(routePoint.getRouteId());

                if (route != null && !mapManager.isShowing(route)) {
                    mapManager.showRoute(map, route.getId(), route.getCompanyId(), true, true);
                }

                mapManager.showStopInformation(mapMarker);
                break;
        }
    }

    public static void onMapMarkerInfoWindowClick(MapMarker mapMarker){
        EMapMarkerType mapMarkerType = mapMarker.getMarkerType();

        switch (mapMarkerType){
            case EMPLOYEE:
                Employee employee = ((Employee)mapMarker.getObject());
                if (employee.getPhoneNumber() != null){
                    try {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+employee.getPhoneNumber()));
                        context.startActivity(intent);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
                break;
        }
    }

    public static void handleCompanyBottomSheet(){
        if(sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED){
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
    }

    public static void handleEmployeeBottomSheet(boolean showRoute){
        if (showRoute){
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }else{
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED){
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
    }

    public static void handleRoutesBottomSheet(Route route, boolean showRoute){
        sheetBehavior = BottomSheetBehavior.from(routeLayoutBottomSheet);
        routeLayoutBottomSheet = (NestedScrollView) RouteActivity.populateMainView(routeLayoutBottomSheet,route.getCompanyId(),route.getId());

        if (showRoute){
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }else{
            sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED){
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
    }

    public void handleDashboardBottomSheet(){
        sheetBehavior = BottomSheetBehavior.from(dashboardLayoutBottomSheet);

        // If it's expanded we won't interfere with user UI activities like scrolling,
        // unless user manually refreshed
        Animation animation=refreshMap.getAnimation();
        if(animation == null && sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            return;
        }

        dashboardLayoutBottomSheet = (LinearLayout) DashboardActivity.populateMainView(dashboardLayoutBottomSheet, (Activity)context);

        if(sheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED){
                    setDashboardQueryListener(true);
                }else{
                    DashboardActivity.filter("");
                    setDashboardQueryListener(false);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
    }

    @Override
    public void stopMapRefresh(DataSet dataSet) {

        if (refreshMap.getAnimation() != null) {
            if (dataSet != null && dataSet.success) {
                showMessage("Map Refreshed");
            } else {
                showMessage("Map could not refresh. Please try again later.");
            }
            refreshMap.clearAnimation();
        }
    }

    @Override
    public void showPlotRoute() {

    }

    @Override
    public void onStart(){
        //System.out.println("Notification Service main stop");
        //stopService(new Intent(this, BackgroundService.class));
        super.onStart();
    }

    @Override
    public void onDestroy(){
        //System.out.println("Notification Service main start");
        //startService(new Intent(this, BackgroundService.class));
        super.onDestroy();
    }
}
