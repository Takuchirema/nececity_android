package net.nececity.nececity.user.Views;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.R;

public class EmployeeProfileActivity extends AppCompatActivity {

    private String companyId;
    private String employeeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        companyId = getIntent().getExtras().getString("companyId");
        employeeId = getIntent().getExtras().getString("employeeId");

        populateMainView(this.findViewById(android.R.id.content).getRootView(), companyId, employeeId);
    }

    public static View populateMainView(View view, String companyId, String employeeId){
        Company company = ObjectCache.getCompanies().get(companyId);

        if (company == null){
            return view;
        }

        Employee employee = company.getEmployees().get(employeeId);

        if (employee == null){
            return view;
        }

        ImageView mainPictureIV = view.findViewById(R.id.mainPicture);
        TextView employeeIdTV = view.findViewById(R.id.employeeId);
        TextView companyIdTV = view.findViewById(R.id.companyId);
        TextView routeIdTV = view.findViewById(R.id.routeId);
        TextView address = view.findViewById(R.id.address);
        LinearLayout phoneNumberContainer = view.findViewById(R.id.phoneNumberContainer);
        TextView phoneNumber = view.findViewById(R.id.phoneNumber);

        if (employee.getBmp() != null) {
            Drawable drawable = new BitmapDrawable(AppVariables.mainActivity.getContext().getResources(), employee.getBmp());
            mainPictureIV.setImageDrawable(drawable);
        }else{
            ViewManager viewManager = new ViewManager(mainPictureIV, EActionType.DOWNLOAD_PICTURE,(Activity) NeceCityMapActivity.context);
            viewManager.setUrl(employee.getPictureUrl());
            viewManager.setObject(employee);
            viewManager.getPicture();
        }

        companyIdTV.setText(company.getName());
        routeIdTV.setText(employee.getRouteName());
        employeeIdTV.setText(employee.getName());
        address.setText(employee.getLastSeen());

        if (employee.getPhoneNumber() != null){
            phoneNumber.setText(employee.getPhoneNumber());
            phoneNumberContainer.setVisibility(View.VISIBLE);
        }

        return view;
    }
}
