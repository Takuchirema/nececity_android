package net.nececity.nececity.user.Views;

import android.app.Activity;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.lapism.searchview.widget.SearchItem;

import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ListAdapter;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.ListDivider;
import net.nececity.nececity.Objects.RoutePoint;
import net.nececity.nececity.R;

import java.util.ArrayList;
import java.util.Collections;

public class DashboardActivity extends AppCompatActivity {

    private static View bottomSheetView;
    private static ListView listView;
    private static ListAdapter adapter;
    private static Activity activity;
    private static ArrayList<String> ids;
    private static ArrayList<ListObject> oneList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        bottomSheetView = findViewById(R.id.dashboard_bottom_sheet);
        listView = bottomSheetView.findViewById(R.id.dashboard_list);
        activity = this;

        populateMainView(bottomSheetView, activity);
    }

    public static View populateMainView(View bottomSheetView, Activity activity){
        DashboardActivity.bottomSheetView = bottomSheetView;
        DashboardActivity.listView = bottomSheetView.findViewById(R.id.dashboard_list);
        DashboardActivity.activity = activity;

        createListAdapter();

        return bottomSheetView;
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public static void createListAdapter(){

        ArrayList<ListObject> nearbyList = new ArrayList<>();
        ArrayList<ListObject> availableList = new ArrayList<>();
        ids = new ArrayList<>();

        setListObjects(nearbyList, availableList, ids);

        Collections.sort(nearbyList);
        Collections.sort(availableList);

        if (!nearbyList.isEmpty()){
            nearbyList.add(0, new ListDivider(" Nearby "));
            ids.add(""+ids.size());
        }

        if (!availableList.isEmpty()){
            availableList.add(0, new ListDivider(" Available "));
            ids.add(""+ids.size());
        }

        oneList = new ArrayList<>();
        oneList.addAll(nearbyList);
        oneList.addAll(availableList);

        System.out.println("** one list: "+oneList.size()+" ids: "+ids.size());
        adapter = new ListAdapter(activity, null, ids, R.layout.object_list_view);
        listView.setAdapter(adapter);
        
        adapter.setListObjects(oneList);
        adapter.notifyDataSetChanged();
    }

    public static void filter(String search){
        if (adapter != null){
            adapter.filter(search);
        }
    }

    /**
     * Nearby list will have list objects close to the current position of user
     * Available are all other search items.
     * ids will have items of both nearby and available
     * @param nearbyList
     * @param availableList
     * @param ids
     */
    public static void setListObjects(ArrayList<ListObject> nearbyList, ArrayList<ListObject> availableList, ArrayList<String> ids){
        if (!(AppVariables.mainActivity instanceof IMapActivity)){
            return;
        }

        IMapActivity mapActivity = (IMapActivity) AppVariables.mainActivity;
        CityMapManager cityMapManager = mapActivity.getMapManager();
        ArrayList<SearchItem> searchItems = cityMapManager.suggestions;

        for (int i =0; i< searchItems.size();i++){
            SearchItem searchItem = searchItems.get(i);
            addListObject(searchItem, nearbyList, availableList, ids);
        }
    }

    public static void addListObject(SearchItem searchItem, ArrayList<ListObject> nearbyList, ArrayList<ListObject> availableList, ArrayList<String> ids){
        if (searchItem == null){
            return;
        }

        Object object = searchItem.getObject();
        if (!(object instanceof ListObject)){
            return;
        }

        ListObject listObject = (ListObject)object;
        //String id = searchItem.getId();
        ArrayList<ListObject> addToList;

        if (isNearby(listObject)) {
            addToList = nearbyList;
        }else {
            addToList = availableList;
        }

        ids.add(""+ids.size());

        if (listObject instanceof Employee) {
            addToList.add(0, listObject);
        }else{
            addToList.add(listObject);
        }

        if (listObject instanceof RoutePoint) {
            //RoutePoint routePoint = (RoutePoint) listObject;
            //addToList.add(routePoint.getRoute());
            //ids.add(routePoint.getRouteId());
        }
    }

    public static boolean isNearby(ListObject object){
        Location userLocation = AppVariables.user.getLocation();
        Location objectLocation = new Location("object location");

        if (object instanceof RoutePoint){
            RoutePoint routePoint = (RoutePoint)object;
            objectLocation.setLatitude(routePoint.getLatitude());
            objectLocation.setLongitude(routePoint.getLongitude());
        }else if (object instanceof Employee){
            Employee employee = (Employee)object;
            objectLocation = employee.getLocation();
        }else{
            return false;
        }

        float distanceBetween = RoutePoint.distanceBearingBetween(userLocation, objectLocation, false);
        if (distanceBetween < AppVariables.nearbyProximity){
            return true;
        }

        return false;
    }
}
