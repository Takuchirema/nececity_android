package net.nececity.nececity.user.Views;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.glide.slider.library.SliderTypes.BaseSliderView;

import net.nececity.nececity.Abstracts.CataloguePostListObject;
import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.CataloguePostListAdapter;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.CommentsActivity;
import net.nececity.nececity.user.Managers.CataloguePostsManager;

import java.util.ArrayList;
import java.util.HashMap;

public class CataloguePostListActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, IListLayout {

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static CataloguePostsManager catalogue_postsManager;
    private IListLayout listLayout;
    private String companyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);

        companyId = getIntent().getExtras().getString("companyId");
        Company company = ObjectCache.getCompanies().get(companyId);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Menu for "+company.getName());
        actionBar.setDisplayHomeAsUpEnabled(true);

        listLayout = this;
        catalogue_postsManager = new CataloguePostsManager();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);

        setListeners();

        if (ObjectCache.getCompanies().get(companyId).getCatalogue().isEmpty()){
            catalogue_postsManager.refreshCataloguePosts(this, companyId);
        }else {
            createListAdapter();
        }
    }

    public void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        catalogue_postsManager.refreshCataloguePosts(listLayout, companyId);
                    }
                }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){
        clearRefresh();

        HashMap<String,? extends CataloguePostListObject> catalogue_posts =  ObjectCache.getCompanies().get(companyId).getCatalogue();

        ArrayList<String> accountIds = new ArrayList<>(catalogue_posts.keySet());
        CataloguePostListAdapter adapter = new CataloguePostListAdapter(this, null, CommentsActivity.class, accountIds, R.layout.catalogue_post_list_view);
        adapter.setMapObjects(catalogue_posts);
        adapter.sort();

        listView = findViewById(R.id.posts_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public void clearRefresh(){
        System.out.println("clear refresh");
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
