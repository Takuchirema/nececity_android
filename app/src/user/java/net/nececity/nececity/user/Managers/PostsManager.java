package net.nececity.nececity.user.Managers;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EPostType;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Data.PostsDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.user.Views.PostListActivity;

/**
 * Created by Takunda on 07/07/2018.
 */

public class PostsManager implements IGetDataSet{

    private IListLayout listLayout;

    public void refreshPosts(IListLayout listLayout){
        this.listLayout = listLayout;
        System.out.println("refresh posts");
        PostsDataSet postsDataSet = new PostsDataSet(this);
        postsDataSet.getDataSet();
    }

    public void refreshPosts(IListLayout listLayout, String companyId){
        this.listLayout = listLayout;
        System.out.println("refresh posts");
        PostsDataSet postsDataSet = new PostsDataSet(this);
        postsDataSet.setUrl(UrlConstructor.getCompanyPostsUrl(companyId, EPostType.COMPANY.toString()));
        postsDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        PostsDataSet postsDataSet = (PostsDataSet)dataSet;

        if (listLayout != null){
            listLayout.clearRefresh();
        }else{
            return;
        }

        if (postsDataSet.success) {
            ObjectCache.refreshPosts(postsDataSet.getPosts());
            if (!postsDataSet.getPosts().isEmpty()){
                listLayout.createListAdapter();
            }
        }else{
            AppVariables.mainActivity.showMessage(postsDataSet.message);
        }
    }
}
