package net.nececity.nececity.user.Views;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Helpers.ListAdapter;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.R;
import net.nececity.nececity.user.Managers.CompaniesManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class CompanyListActivity extends AppCompatActivity {

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static CompaniesManager companiesManager;
    private CompanyListActivity companyListActivity;
    private SearchView searchView;
    private ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_list);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");

        companyListActivity = this;
        companiesManager = new CompaniesManager();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        companiesManager.refreshCompanies(companyListActivity);
                    }
                }
        );

        companiesManager.getCompanies(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @SuppressWarnings("unused")
            public boolean onMenuItemActionCollapse(MenuItem item) {
                adapter.filter(null);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {
                String text = arg0.toLowerCase(Locale.getDefault()).trim();
                System.out.println("text change: "+text);
                adapter.filter(text);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String text) {
                adapter.filter(text);
                return false;
            }
        });

        return true;
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){
        clearRefresh();

        HashMap<String,? extends ListObject> companies =  ObjectCache.getCompanies();

        ArrayList<String> accountIds = new ArrayList<>(companies.keySet());
        adapter = new ListAdapter(this,CompanyProfileActivity.class,accountIds, R.layout.generic_list_view);
        adapter.setMapObjects(companies);

        listView = findViewById(R.id.companies_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public void clearRefresh(){
        System.out.println("clear refresh");
        swipeRefreshLayout.setRefreshing(false);
    }
}
