package net.nececity.nececity.user.Managers;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Data.CataloguePostsDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;

/**
 * Created by Takunda on 8/28/2018.
 */

public class CataloguePostsManager implements IGetDataSet {
    private IListLayout listLayout;

    public void refreshCataloguePosts(IListLayout listLayout, String companyId){
        this.listLayout = listLayout;
        System.out.println("refresh catalogue posts");
        CataloguePostsDataSet cataloguePostsDataSet = new CataloguePostsDataSet(this, companyId);
        cataloguePostsDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        CataloguePostsDataSet cataloguePostsDataSet = (CataloguePostsDataSet)dataSet;

        if (listLayout != null){
            listLayout.clearRefresh();
        }else{
            return;
        }

        if (cataloguePostsDataSet.success) {
            ObjectCache.refreshCataloguePosts(cataloguePostsDataSet.getCataloguePosts());
            if (!cataloguePostsDataSet.getCataloguePosts().isEmpty()) {
                listLayout.createListAdapter();
            }
        }else{
            AppVariables.mainActivity.showMessage(cataloguePostsDataSet.message);
        }
    }
}
