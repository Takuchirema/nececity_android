package net.nececity.nececity.employee.Managers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EStatus;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Data.ActionDataSet;
import net.nececity.nececity.Data.JSONParser;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.employee.Data.LoginDataSet;
import net.nececity.nececity.employee.Views.LoginActivity;
import net.nececity.nececity.employee.Views.NeceCityMapActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 22/05/2018.
 */

public class LoginManager implements IGetDataSet {

    protected String companyId, userId, password;
    private Activity loginActivity;
    private EStatus status;
    private boolean login=true;
    private boolean setStatus;

    public LoginManager(String companyId, String userId,String password){
        this.userId = userId.trim();
        this.password = password;
        this.companyId=companyId;
    }

    public LoginManager(){}

    public void attemptLogin(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
        LoginDataSet loginDataSet = new LoginDataSet(this,loginActivity,companyId,userId,password);
        loginDataSet.getDataSet();
    }

    public void attemptDeviceLogin(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
        NeceCityMapActivity.deviceLogin=true;
        LoginDataSet loginDataSet = new LoginDataSet(this, loginActivity);
        loginDataSet.setUrl(UrlConstructor.getDeviceLoginUrl());
        loginDataSet.setDeviceLogin(true);
        loginDataSet.setUseSharePreference(false);
        loginDataSet.getDataSet();
    }

    /* This method logs in another employee */
    public void changeEmployee(String newEmployeeId){
        loginActivity = (Activity) NeceCityMapActivity.context;
        LoginDataSet loginDataSet = new LoginDataSet(this,(Activity) NeceCityMapActivity.context,
                AppVariables.getEmployee().getCompanyId(), newEmployeeId,password);
        loginDataSet.setUrl(UrlConstructor.getEmployeeChangeUrl(newEmployeeId,AppVariables.getEmployee().getCompanyId()));
        loginDataSet.setUseSharePreference(false);
        loginDataSet.getDataSet();
    }

    public void setStatus(EStatus onlineStatus){
        status = onlineStatus;
        setStatus = true;
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String url = UrlConstructor.editEmployeeProfileUrl();

        params.add(new BasicNameValuePair("status", onlineStatus.toString()));
        params.add(new BasicNameValuePair("companyID", ((Employee)AppVariables.user).getCompanyId()));
        params.add(new BasicNameValuePair("employeeID",AppVariables.user.getId()));

        ActionDataSet logoutAction = new ActionDataSet(this, EActionType.SET_STATUS);
        logoutAction.setHttpMethod(EHttpMethod.PATCH);
        logoutAction.setUrl(url);
        logoutAction.setParams(params);
        logoutAction.getDataSet();
    }

    public void logout(){
        login = false;
        setStatus(EStatus.OFFLINE);
    }

    public void postGetDataSet(DataSet dataSet){
        if (loginActivity instanceof LoginActivity) {
            ((LoginActivity) loginActivity).showProgress(false,"");
        }

        if(!login) {
            AppVariables.mainActivity.logout();
        }else if (login && !setStatus && dataSet.success){
            LoginDataSet loginDataSet = (LoginDataSet)dataSet;

            if (!loginDataSet.isLatestVersion()){
                ((LoginActivity) loginActivity).showProgress(true,"Downloading Application...");
                AppUpdateManager updateManager = new AppUpdateManager(loginDataSet.getUpdateUrl());
                updateManager.getUpdate(loginActivity);
                return;
            }

            Intent intent = new Intent(loginActivity, NeceCityMapActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            loginActivity.startActivity(intent);
            if (loginActivity instanceof LoginActivity) {
                loginActivity.finish();
            }else{
                ((NeceCityMapActivity)loginActivity).loseSearchFocus();
                ((NeceCityMapActivity)loginActivity).showMessage(dataSet.message);
                ((NeceCityMapActivity)loginActivity).sendCurrentLocation();
                ((NeceCityMapActivity)loginActivity).showMenu();
            }
        } else if (login && setStatus && dataSet.success) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("status", EStatus.OFFLINE.toString());

            AppVariables.user.setStatus(status.toString());
            NeceCityMapActivity.setStatus();
        } else if (!dataSet.success){

            if (AppVariables.mainActivity != null) {
                AppVariables.mainActivity.showMessage(dataSet.message);
            }

            if (login && !setStatus) {
                if (loginActivity instanceof LoginActivity) {
                    ((LoginActivity) loginActivity).loginFailed(dataSet.message);
                } else {
                    ((NeceCityMapActivity) loginActivity).loseSearchFocus();
                }
            }else{
                NeceCityMapActivity.setStatus();
            }
        }
    }

}
