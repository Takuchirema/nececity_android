package net.nececity.nececity.employee.Views;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;

import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.EditImage;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.ImageViewerActivity;
import net.nececity.nececity.employee.Managers.PostsManager;

import java.util.ArrayList;

public class EditPostActivity extends SecureAppCompatActivity {

    private SliderLayout slider;
    private ImageView addPostIV;
    private ImageView deletePostIV;
    private EditText postET;
    private TextView doneIV;
    private TextView deleteIV;


    private ArrayList<String> addImageUrls = new ArrayList<>();
    private ArrayList<String> deleteImageUrls = new ArrayList<>();

    private String postId;
    private PostsManager postsManager;
    private Post post;

    private Bitmap bmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Edit Post");
        actionBar.setDisplayHomeAsUpEnabled(true);

        postET = findViewById(R.id.postText);
        addPostIV = findViewById(R.id.addPostImage);
        deletePostIV = findViewById(R.id.deletePostImage);
        slider = findViewById(R.id.slider);
        slider.stopAutoCycle();

        doneIV = findViewById(R.id.done);
        deleteIV = findViewById(R.id.delete);

        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("postId")) {
            postId = getIntent().getExtras().getString("postId");
        }

        postsManager = new PostsManager(this);

        populatePost();
        setListeners();
    }

    public void populatePost(){

        if (postId == null){
            return;
        }

        post = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId()).getPosts().get(postId);

        ArrayList<String> pictureUrls = post.getPictureUrls();
        for (String pictureUrl:pictureUrls) {
            addSliderView(pictureUrl,null);
        }

        postET.setText(post.getPost());
    }

    public void addSliderView(String pictureUrl, Bitmap bmp){
        TextSliderView sliderView = new TextSliderView(this);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();

        if (bmp != null){
            sliderView
                    .image(bmp)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setRequestOption(requestOptions)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            viewPostImage(slider.getUrl());
                        }
                    });
        }else if (pictureUrl != null){
            sliderView
                    .image(pictureUrl)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setRequestOption(requestOptions)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            viewPostImage(slider.getUrl());
                        }
                    });
        }

        //add your extra information
        sliderView.bundle(new Bundle());
        sliderView.getBundle().putString("extra", "name");

        slider.addSlider(sliderView);
    }

    public void viewPostImage(String pictureUrl){

        Intent intent = new Intent(this, ImageViewerActivity.class);
        intent.putExtra("companyId", AppVariables.getEmployee().getCompanyId());
        intent.putExtra("pictureUrl", pictureUrl);
        intent.putExtra("title", "Post Image");
        intent.putExtra("postId", post.getId());

        startActivity(intent);
    }

    public void setListeners(){
        addPostIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });

        deletePostIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseSliderView sliderView = slider.getCurrentSlider();
                if (sliderView == null){
                    return;
                }

                String url = sliderView.getUrl();
                if (addImageUrls.contains(url)){
                    addImageUrls.remove(url);
                }else {
                    deleteImageUrls.add(url);
                }
                System.out.println("delete image url:"+url+" delete image count: "+deleteImageUrls.size());
                slider.removeSliderAt(slider.getCurrentPosition());
            }
        });

        doneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePostChanges();
            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePost();
            }
        });
    }

    public void deletePost(){
        if (postId == null){
            return;
        }

        PostsManager postsManager = new PostsManager(this);
        postsManager.deletePost(postId);
    }

    public void savePostChanges(){
        String postText = postET.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(postText)) {
            postET.setError("The post cannot be empty");
            focusView = postET;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            PostsManager postsManager = new PostsManager(this);
            postsManager.updatePost(postId, postText, addImageUrls, deleteImageUrls);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }
            Uri selectedImage = data.getData();

            String imagePath = getPath(selectedImage);
            String file_extn = imagePath.substring(imagePath.lastIndexOf(".") + 1);
            try {
                if (file_extn.equalsIgnoreCase("img") || file_extn.equalsIgnoreCase("jpg") ||
                        file_extn.equalsIgnoreCase("jpeg") || file_extn.equalsIgnoreCase("gif") ||
                        file_extn.equalsIgnoreCase("png")) {

                    EditImage editImage = new EditImage(EditPostActivity.this, imagePath);
                    bmp = editImage.compressImage(imagePath);

                    if (bmp!=null) {
                        addImageUrls.add(imagePath);
                        System.out.println("add image url: "+imagePath+"add image count: "+addImageUrls.size());
                        addSliderView(imagePath,null);
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "The file is not in a required format. jpg,jpeg,gif,png", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
        int column_index = 0;
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        cursor.getString(column_index);

        return cursor.getString(column_index);
    }
}
