package net.nececity.nececity.employee.Views;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;

import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.CustomRadioButton;
import net.nececity.nececity.Helpers.EditImage;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.ImageViewerActivity;
import net.nececity.nececity.employee.Managers.CataloguePostsManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EditCataloguePostActivity extends SecureAppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private SliderLayout slider;
    private ImageView addCataloguePostIV;
    private ImageView deleteCataloguePostIV;
    private EditText cataloguePostET;
    private TextView doneIV;
    private TextView deleteIV;
    private TextView addAmenityTV;
    private LinearLayout fileContainer;
    private TextView deleteFileTV;

    private EditText titleET,priceET,unitsET,amenityET;
    private TextView fromTV,toTV;

    private String cataloguePostId;
    private CataloguePostsManager cataloguePostsManager;
    private CataloguePost cataloguePost;
    private List<CustomRadioButton> amenitiesList = new ArrayList<>();

    private ArrayList<String> addImageUrls = new ArrayList<>();
    private ArrayList<String> deleteImageUrls = new ArrayList<>();
    private Uri fileUri;
    private String filePath;
    private boolean deleteFile;
    private Bitmap bmp;

    private boolean setStartDate;
    private DatePickerDialog datePickerDialog;
    private String fromDate;
    private String toDate;

    LayoutInflater inflater;
    GridLayout amenitiesGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_catalogue_post);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        inflater = this.getLayoutInflater();
        amenitiesGrid = findViewById(R.id.amenitiesGrid);

        titleET = findViewById(R.id.title);
        cataloguePostET = findViewById(R.id.cataloguePostText);
        addCataloguePostIV = findViewById(R.id.addCataloguePostImage);
        deleteCataloguePostIV = findViewById(R.id.deleteCataloguePostImage);
        priceET = findViewById(R.id.price);
        unitsET = findViewById(R.id.units);
        fromTV = findViewById(R.id.from);
        toTV = findViewById(R.id.to);
        amenityET = findViewById(R.id.amenityText);
        addAmenityTV = findViewById(R.id.addAmenity);
        slider = findViewById(R.id.slider);
        fileContainer = findViewById(R.id.fileContainer);
        deleteFileTV = findViewById(R.id.deleteFile);
        slider.stopAutoCycle();

        doneIV = findViewById(R.id.done);
        deleteIV = findViewById(R.id.delete);

        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("cataloguePostId")) {
            cataloguePostId = getIntent().getExtras().getString("cataloguePostId");
        }

        cataloguePostsManager = new CataloguePostsManager(this);

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(this, EditCataloguePostActivity.this, year, month, day);
        populateCataloguePost();
        setListeners();
    }

    public void populateCataloguePost(){

        if (cataloguePostId == null){
            return;
        }

        cataloguePost = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId()).getCatalogue().get(cataloguePostId);

        ArrayList<String> pictureUrls = cataloguePost.getPictureUrls();
        for (String pictureUrl:pictureUrls) {
            addSliderView(pictureUrl,null);
        }

        titleET.setText(cataloguePost.getTitle());
        fromTV.setText(cataloguePost.getFrom());
        toTV.setText(cataloguePost.getTo());
        priceET.setText(cataloguePost.getPrice());
        unitsET.setText(cataloguePost.getUnits());
        cataloguePostET.setText(cataloguePost.getPost());
        if (cataloguePost.getFileUrl() != null){
            ((TextView)fileContainer.findViewById(R.id.fileName)).setText(getFileName(cataloguePost.getFileUrl()));
        }
        populateAmenities(cataloguePost);
    }

    public void addSliderView(String pictureUrl, Bitmap bmp){
        TextSliderView sliderView = new TextSliderView(this);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();

        if (bmp != null){
            sliderView
                    .image(bmp)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setRequestOption(requestOptions)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            viewPostImage(slider.getUrl());
                        }
                    });
        }else if (pictureUrl != null){
            sliderView
                    .image(pictureUrl)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setRequestOption(requestOptions)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            viewPostImage(slider.getUrl());
                        }
                    });
        }

        //add your extra information
        sliderView.bundle(new Bundle());
        sliderView.getBundle().putString("extra", "name");

        slider.addSlider(sliderView);
    }

    public void viewPostImage(String pictureUrl){

        Intent intent = new Intent(this, ImageViewerActivity.class);
        intent.putExtra("companyId", AppVariables.getEmployee().getCompanyId());
        intent.putExtra("pictureUrl", pictureUrl);
        intent.putExtra("title", cataloguePost.getTitle());
        intent.putExtra("cataloguePostId", cataloguePost.getId());

        startActivity(intent);
    }

    public void populateAmenities(CataloguePost post){

        final String[] amenities = post.getAmenities().split(",");

        for (final String amenity:amenities){
            if (amenity.isEmpty()){
                continue;
            }
            addAmenityToGrid(amenity);
        }
    }

    public void addAmenityToGrid(String amenity){

        View amenityLayout = inflater.inflate(R.layout.amenities_item, null);
        CustomRadioButton amenityView = amenityLayout.findViewById(R.id.amenity);
        amenityView.setEnabled(true);
        amenityView.setText(amenity);
        amenitiesGrid.addView(amenityLayout);
        amenitiesList.add(amenityView);

        amenityET.setText("");
    }

    public void setListeners(){
        fromTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStartDate = true;
                datePickerDialog.show();
            }
        });

        toTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStartDate = false;
                datePickerDialog.show();
            }
        });

        addCataloguePostIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });

        deleteCataloguePostIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseSliderView sliderView = slider.getCurrentSlider();
                if (sliderView == null){
                    return;
                }

                String url = sliderView.getUrl();
                if (addImageUrls.contains(url)){
                    addImageUrls.remove(url);
                }else {
                    deleteImageUrls.add(url);
                }
                System.out.println("delete image url:"+url+" delete image count: "+deleteImageUrls.size());
                slider.removeSliderAt(slider.getCurrentPosition());
            }
        });

        addCataloguePostIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });

        addAmenityTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newAmenity = amenityET.getText().toString();
                if (!newAmenity.isEmpty()){
                    addAmenityToGrid(newAmenity);
                }
            }
        });

        doneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveCataloguePostChanges();
            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCataloguePost();
            }
        });

        fileContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent,2);
            }
        });

        deleteFileTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView)fileContainer.findViewById(R.id.fileName)).setText("Add Pdf File");
                filePath = null;
                fileUri=null;
                deleteFile = true;
            }
        });
    }

    public void deleteCataloguePost(){
        if (cataloguePostId == null){
            return;
        }

        CataloguePostsManager cataloguePostsManager = new CataloguePostsManager(this);
        cataloguePostsManager.deleteCataloguePost(cataloguePostId);
    }

    public void saveCataloguePostChanges(){
        String cataloguePostText = cataloguePostET.getText().toString();
        String price = priceET.getText().toString();
        String units = unitsET.getText().toString();
        String title = titleET.getText().toString();
        String duration = "";
        if (fromDate != null || toDate != null) {
            duration = fromDate.trim() + "," + toDate.trim();
        }else if (cataloguePost != null){
            duration = cataloguePost.getDuration();
        }

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(cataloguePostText)) {
            cataloguePostET.setError("The cataloguePost cannot be empty");
            focusView = cataloguePostET;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            String amenitiesString = "";
            for (CustomRadioButton amenity:amenitiesList){
                if (amenity.isChecked()){
                    amenitiesString = amenitiesString+","+amenity.getText().toString();
                }
            }

            CataloguePost cataloguePost = new CataloguePost(cataloguePostId);
            cataloguePost.setAmenities(amenitiesString);
            cataloguePost.setUnits(units);
            cataloguePost.setPrice(price);
            cataloguePost.setDuration(duration);
            cataloguePost.setPost(cataloguePostText);
            cataloguePost.setTitle(title);

            CataloguePostsManager cataloguePostsManager = new CataloguePostsManager(this);
            cataloguePostsManager.updateCataloguePost(cataloguePost,addImageUrls,deleteImageUrls,filePath,deleteFile);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }
            Uri selectedImage = data.getData();

            String imagePath = getPath(selectedImage);
            String file_extn = imagePath.substring(imagePath.lastIndexOf(".") + 1);
            try {
                if (file_extn.equalsIgnoreCase("img") || file_extn.equalsIgnoreCase("jpg") ||
                        file_extn.equalsIgnoreCase("jpeg") || file_extn.equalsIgnoreCase("gif") ||
                        file_extn.equalsIgnoreCase("png")) {

                    EditImage editImage = new EditImage(EditCataloguePostActivity.this, imagePath);
                    bmp = editImage.compressImage(imagePath);

                    if (bmp!=null) {
                        addImageUrls.add(imagePath);
                        System.out.println("add image url: "+imagePath+"add image count: "+addImageUrls.size());
                        addSliderView(imagePath,null);
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "The file is not in a required format. jpg,jpeg,gif,png", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }else if (requestCode == 2) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            Uri fileUri = data.getData();
            String filePath = getPath(fileUri);

            String file_extn = filePath.substring(filePath.lastIndexOf(".") + 1);
            if (!file_extn.equalsIgnoreCase("pdf")) {
                Toast.makeText(getApplicationContext(), "Please upload only PDF files.", Toast.LENGTH_LONG).show();
                return;
            }

            this.fileUri = fileUri;
            this.filePath = filePath;
            ((TextView)fileContainer.findViewById(R.id.fileName)).setText(getFileName(this.filePath));
        }
    }

    public String getPath(Uri uri) {
        int column_index = 0;
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        cursor.getString(column_index);

        return cursor.getString(column_index);
    }

    public String getFileName(String path){
        if (path != null){
            String fileName = path.substring(path.lastIndexOf('/') + 1);
            return fileName;
        }
        return null;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear += 1;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if (setStartDate) {
                Date fromDateD = formatter.parse(year+"-"+monthOfYear + "-" + dayOfMonth);

                if (toDate != null) {
                    Date toDateD = formatter.parse(toDate);
                    if (toDateD.before(fromDateD)) {
                        Toast.makeText(EditCataloguePostActivity.this, "End date must be after start date", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                String day = ""+dayOfMonth;
                String month = ""+monthOfYear;

                if ((""+dayOfMonth).length()==1) {
                    day = "0"+dayOfMonth;
                }

                if ((""+monthOfYear).length()==1) {
                    month = "0"+monthOfYear;
                }

                fromDate = year + "-" + month + "-" + day;
                fromTV.setText(year + "-" + month + "-" + day);
            } else {
                Date toDateD = formatter.parse(year+"-"+ monthOfYear+"-"+dayOfMonth);

                if (fromDate != null) {
                    Date fromDateD = formatter.parse(fromDate);
                    if (fromDateD.after(toDateD)) {
                        Toast.makeText(EditCataloguePostActivity.this, "End date must be after start date", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                String day = ""+dayOfMonth;
                String month = ""+monthOfYear;

                //month of year seems to be a day behind not starting from 0
                monthOfYear++;

                if ((""+dayOfMonth).length()==1) {
                    day = "0"+dayOfMonth;
                }

                if ((""+monthOfYear).length()==1) {
                    month = "0"+monthOfYear;
                }

                toDate = year+"-"+month+"-"+ day;
                toTV.setText(year+"-"+ monthOfYear+"-"+dayOfMonth);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
