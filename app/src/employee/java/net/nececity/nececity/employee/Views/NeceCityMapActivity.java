package net.nececity.nececity.employee.Views;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.app.AlertDialog;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lapism.searchview.widget.SearchView;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EMapMarkerType;
import net.nececity.nececity.Abstracts.EPrivilege;
import net.nececity.nececity.Abstracts.EStatus;
import net.nececity.nececity.Abstracts.IMainActivity;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Abstracts.LocationResult;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.LocationHandler;
import net.nececity.nececity.Managers.FileManager;
import net.nececity.nececity.Managers.LocationManager;
import net.nececity.nececity.Managers.PermissionsManager;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.MapMarker;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.R;
import net.nececity.nececity.employee.Helpers.PrefUtils;
import net.nececity.nececity.employee.Managers.CityMapManager;
import net.nececity.nececity.employee.Managers.LoginManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;

public class NeceCityMapActivity extends SecureAppCompatActivity implements OnMapReadyCallback,
        NavigationView.OnNavigationItemSelectedListener, GoogleMap.OnMarkerClickListener, IMapActivity {

    public static Context context;
    public static GoogleMap map;
    public static CityMapManager mapManager;
    public static LoginManager loginManager;
    public DrawerLayout drawer;
    private LayoutInflater inflater;

    public static LocationManager locationManager;
    private PermissionsManager permissionsManager;
    private FileManager fileManager;
    private LocationHandler myLocationHandler;

    private static BottomSheetBehavior sheetBehavior;
    private static RelativeLayout employeeLayoutBottomSheet;

    private SearchView searchView;
    private static NavigationView navigationView;
    public static FloatingActionButton changeRoute;
    public static FloatingActionButton changeEmployee;
    public static FloatingActionButton myLocation;
    public static FloatingActionButton plotRouteFB;
    public static SwitchCompat onlineSC;
    public static boolean deviceLogin=false;

    public View locationButton;
    public SupportMapFragment mapFragment;

    public boolean deprecationWarned=false;
    private int UPDATE_VERSION = 1000;
    private boolean plotRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AppVariables.mainActivity = this;

        context = this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        inflater = (LayoutInflater) NeceCityMapActivity.context.getSystemService(LAYOUT_INFLATER_SERVICE);

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (!AppVariables.getEmployee().isAdmin() && !AppVariables.getEmployee().isSuperUser()) {
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_menu).setVisible(false);
            nav_Menu.findItem(R.id.nav_posts).setVisible(false);
            nav_Menu.findItem(R.id.nav_promotions).setVisible(false);
        }

        searchView = findViewById(R.id.searchView);
        changeRoute = findViewById(R.id.changeRoute);
        myLocation = findViewById(R.id.myLocation);
        changeEmployee = findViewById(R.id.changeEmployee);
        plotRouteFB = findViewById(R.id.plotRoute);

        employeeLayoutBottomSheet = findViewById(R.id.employee_profile_bottom_sheet);

        sheetBehavior = BottomSheetBehavior.from(employeeLayoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        sheetBehavior = BottomSheetBehavior.from(employeeLayoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        permissionsManager = new PermissionsManager(this);

        mapManager = new CityMapManager(map,this);
        mapManager.prepareSearch(searchView);

        fileManager = new FileManager(this);
        fileManager.checkErrorLogs();

        loginManager = new LoginManager();

        setAppCodes();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsManager.canAccessLocation()) {
                appSetUp();
            } else {
                permissionsManager.requestPermissions();
            }
        }else{
            appSetUp();
        }
    }

    public void appSetUp(){
        monitorLocation();
        mapManager.startManager();
        setListeners();
    }

    @Override
    public void setAppCodes() {
        AppVariables.mainActivity=this;
        try {
            AppVariables.setVersionCode(this);
            AppVariables.setVersionName(this);
            AppVariables.setVersionNameSuffix(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateSettings(String key) {

    }

    @Override
    public void unseenPosts(boolean unseen, Post latestPost) {

    }

    @Override
    public void promotionsToday(HashMap<String, Promotion> promotionsToday) {

    }

    @Override
    public void startBackgroundService() {

    }

    @Override
    public void setTutorial() {

    }

    @Override
    public net.nececity.nececity.Managers.CityMapManager getMapManager() {
        return mapManager;
    }

    @Override
    public Context getContext() {
        return context;
    }

    public void setListeners(){

        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationButton.callOnClick();
            }
        });

        changeEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapManager.changeEmployee();
            }
        });

        changeRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapManager.changeRoute();
            }
        });

        plotRouteFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mapManager.plotRoute()){
                    startRoutePlot();
                }else{
                    stopRoutePlot();
                }
            }
        });

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                final Employee employee = (Employee)AppVariables.user;
                TextView userName = findViewById(R.id.userName);
                onlineSC = findViewById(R.id.onlineSwitch);
                ImageView userIcon = findViewById(R.id.userIcon);

                Employee user = AppVariables.getEmployee();
                if (user.getPictureBmp() == null){
                    String url = user.getPictureUrl();

                    ViewManager viewManager = new ViewManager(userIcon, EActionType.DOWNLOAD_PICTURE, (Activity)context);
                    viewManager.setObject(AppVariables.user);
                    viewManager.setImageBackground(true);
                    viewManager.setUrl(url);
                    viewManager.getPicture();
                }else {
                    Drawable drawable = new BitmapDrawable(NeceCityMapActivity.context.getResources(), user.getPictureBmp());
                    userIcon.setBackground(drawable);
                }

                userName.setText(employee.getName());
                if (employee.getStatus() == EStatus.ONLINE){
                    onlineSC.setChecked(true);
                }else{
                    onlineSC.setChecked(false);
                }

                onlineSC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked && employee.getStatus() == EStatus.OFFLINE){
                            loginManager.setStatus(EStatus.ONLINE);
                        }else if (!isChecked && employee.getStatus() == EStatus.ONLINE){
                            loginManager.setStatus(EStatus.OFFLINE);
                        }
                    }
                });

                if (deviceLogin){
                    return;
                }

                LinearLayout profileLayout = findViewById(R.id.profileLayout);
                profileLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(NeceCityMapActivity.context, EmployeeProfileActivity.class);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(){
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                String timeStamp = new SimpleDateFormat(AppVariables.timeFormat).format(Calendar.getInstance().getTime());
                String stackTrace = Log.getStackTraceString(ex);

                System.out.println(stackTrace);
                fileManager.writeToFile(AppVariables.errorLogName,"Name:"+AppVariables.user.getId()+"\nDeviceId: "+AppVariables.getDeviceId((Activity)context)+"\nTime: "+timeStamp+"\nTrace: "+stackTrace);
                Intent k = new Intent(NeceCityMapActivity.this, LoginActivity.class);
                startActivity(k);
                System.exit(1);
            }
        });
    }

    public static void setStatus(){
        EStatus status = AppVariables.user.getStatus();
        switch (status){
            case ONLINE:
                onlineSC.setChecked(true);
                break;
            case OFFLINE:
                onlineSC.setChecked(false);
                break;
        }
    }

    public static void loseSearchFocus(){
        mapManager.searchView.hideSuggestions();
        mapManager.searchView.setHint("");
        mapManager.searchView.clearFocus();
    }

    @Override
    public void logout(){
        boolean kiosk = PrefUtils.isKioskModeActive(this);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        if (kiosk){
            PrefUtils.setKioskModeActive(true,this);
        }

        Intent a = new Intent(context, LoginActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        (context).startActivity(a);
        System.exit(0);
    }

    public void startRoutePlot(){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup termsAndConditions = findViewById(R.id.startRoutePlot);
        View layout = inflater.inflate(R.layout.content_start_route_plot, termsAndConditions);
        final EditText routeNameET = layout.findViewById(R.id.routeName);

        //AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setView(layout)
                .setPositiveButton("Start", null)
                .setNegativeButton("Cancel", null)
                .show();

        // Set the button layouts and listeners
        Button start = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        Button cancel = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) start.getLayoutParams();
        layoutParams.weight = 10;

        start.setLayoutParams(layoutParams);
        cancel.setLayoutParams(layoutParams);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String routeName = routeNameET.getText().toString();

                if (TextUtils.isEmpty(routeName)) {
                    routeNameET.setError("Please enter a route name");
                }
                else{
                    Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_clockwise);
                    plotRouteFB.startAnimation(rotate);

                    mapManager.startPloteRoute(routeNameET.getText().toString());
                    alertDialog.dismiss();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    public void stopRoutePlot(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup termsAndConditions = findViewById(R.id.startRoutePlot);
        View layout = inflater.inflate(R.layout.content_stop_route_plot, termsAndConditions);

        alertDialog.setView(layout);
        alertDialog.setPositiveButton("Save", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                plotRouteFB.clearAnimation();
                mapManager.stopPloteRoute(true);
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton("Stop", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                plotRouteFB.clearAnimation();
                mapManager.stopPloteRoute(false);
                dialog.dismiss();
            }
        });

        alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        
        AlertDialog alert = alertDialog.create();
        alert.show();

        Button start = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        Button cancel = alert.getButton(DialogInterface.BUTTON_NEGATIVE);

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) start.getLayoutParams();
        layoutParams.weight = 10;

        start.setLayoutParams(layoutParams);
        cancel.setLayoutParams(layoutParams);
    }

    @Override
    public void deprecatedVersion() {
        if (deprecationWarned){
            return;
        }

        int maxVersion = Collections.max(AppVariables.supportedVersions);

        AlertDialog.Builder termsAndConditionsDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup termsAndConditions = findViewById(R.id.termsAndConditions);
        View layout = inflater.inflate(R.layout.terms_and_conditions, termsAndConditions);

        TextView termsAndConditionsTxt = layout.findViewById(R.id.termsAndConditionsTxt);
        termsAndConditionsTxt.setText(
                "Please update NeceCity by downloading the latest version from Google Play Store." +
                        " The latest version is "+maxVersion+" and your current version is "+AppVariables.versionCode+
                        " NeceCity will not function properly without the latest updates. Thank you.");

        termsAndConditionsDialog.setView(layout);
        termsAndConditionsDialog.setPositiveButton("Accept", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                updateVersion();
            }
        });

        AlertDialog alert = termsAndConditionsDialog.create();
        alert.setCancelable(false);
        alert.show();

        Button ok = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        ok.setTextColor(Color.WHITE);
        ok.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ok.getLayoutParams();
        layoutParams.weight = 10;
        ok.setLayoutParams(layoutParams);

        deprecationWarned = true;
    }

    public void updateVersion(){
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        System.out.println("package: "+appPackageName);
        try {
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)),UPDATE_VERSION);
        } catch (Exception ex) {
            System.out.println("can't open market "+ex.getMessage());
            startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)),UPDATE_VERSION);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == UPDATE_VERSION) {
            logout();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMarkerClickListener(this);
        mapManager.setMap(map);

        enableMyLocation();
    }

    public void monitorLocation() {
        enableMyLocation();
        LocationResult locationResult = new LocationResult(){
            @Override
            public void gotLocation(final Location location){
                if (locationManager == null){
                    locationManager = new LocationManager(location);
                }
                locationManager.onLocationChanged(map,location);
            }
        };
        myLocationHandler = new LocationHandler();
        myLocationHandler.getLocation(this, locationResult);
    }

    public void sendCurrentLocation(){

        if (deviceLogin){
            return;
        }

        if (myLocationHandler.getLastLocation() == null){
            return;
        }

        if (locationManager == null){
            locationManager = new LocationManager(myLocationHandler.getLastLocation());
        }

        locationManager.sendLocation(myLocationHandler.getLastLocation());
    }

    public void showMenu(){
        Menu nav_Menu = navigationView.getMenu();
        if (!AppVariables.getEmployee().isAdmin() && !AppVariables.getEmployee().isSuperUser()) {
            nav_Menu.findItem(R.id.nav_menu).setVisible(false);
            nav_Menu.findItem(R.id.nav_posts).setVisible(false);
            nav_Menu.findItem(R.id.nav_promotions).setVisible(false);
        }else{
            nav_Menu.findItem(R.id.nav_menu).setVisible(true);
            nav_Menu.findItem(R.id.nav_posts).setVisible(true);
            nav_Menu.findItem(R.id.nav_promotions).setVisible(true);
        }
    }

    public void enableMyLocation(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsManager.requestPermissions();
            return;
        }
        if (map == null) {
            return;
        }

        map.setMyLocationEnabled(true);
        myLocation.setVisibility(View.VISIBLE);

        locationButton = ((View)mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        // Change the visibility of my location button
        if(locationButton != null) {
            locationButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        int inititalRequest = permissionsManager.INITIAL_REQUEST;
        if (permsRequestCode == inititalRequest && grantResults.length > 0) {
            boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            if (locationAccepted){
                appSetUp();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.nav_logout:
                loginManager.logout();
                break;
            case R.id.nav_menu:
                intent = new Intent(this, CataloguePostListActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_posts:
                intent = new Intent(this, PostListActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_promotions:
                intent = new Intent(this, PromotionListActivity.class);
                startActivity(intent);
                break;
        }

        return false;
    }

    @Override
    public void showMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        System.out.println("marker clicked");
        MapMarker mapMarker = (MapMarker) marker.getTag();

        onMapMarkerClick(mapMarker);
        return false;
    }

    public static void onMapMarkerClick(MapMarker mapMarker){
        EMapMarkerType mapMarkerType = mapMarker.getMarkerType();

        switch (mapMarkerType){
            case ROUTE_POINT:
                mapManager.showStopInformation(mapMarker);
                break;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onDestroy() {
        System.out.println("App Activity Destroyed");
        Intent k = new Intent(NeceCityMapActivity.this, LoginActivity.class);
        startActivity(k);
        System.exit(1);
        super.onDestroy();
    }

    @Override
    public void stopMapRefresh(DataSet dataSet) {

    }

    @Override
    public void showPlotRoute() {
        plotRouteFB.setVisibility(View.VISIBLE);
    }
}
