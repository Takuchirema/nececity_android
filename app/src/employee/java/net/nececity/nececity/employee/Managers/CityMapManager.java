package net.nececity.nececity.employee.Managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.lapism.searchview.Search;
import com.lapism.searchview.database.SearchHistoryTable;
import com.lapism.searchview.widget.SearchAdapter;
import com.lapism.searchview.widget.SearchItem;
import com.lapism.searchview.widget.SearchView;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.ECompanyBusiness;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.ESearchItem;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Data.CompaniesDataSet;
import net.nececity.nececity.Data.EmployeesDataSet;
import net.nececity.nececity.Data.RouteDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.CustomRunnable;
import net.nececity.nececity.Helpers.GetDataSet;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RefreshDataSet;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.MapMarker;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;
import net.nececity.nececity.R;
import net.nececity.nececity.employee.Views.NeceCityMapActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Takunda on 21/07/2018.
 */

public class CityMapManager extends net.nececity.nececity.Managers.CityMapManager{

    final SearchHistoryTable mHistoryDatabase = new SearchHistoryTable(NeceCityMapActivity.context);
    public static RefreshDataSet refreshCompaniesDataSet;
    public static RefreshDataSet refreshEmployeesDataSet;

    // Details of a route being plotted on the map
    private boolean plotRoute;
    private String plotRouteName;
    private static LinkedHashMap<String, RoutePoint> plotRoutePoints = new LinkedHashMap<>();
    private static Polyline plotRoutePolyline;

    public SearchView searchView;
    // Need handler for callbacks to the UI thread
    final Handler handler = new Handler();

    public CityMapManager(GoogleMap map, Context context) {
        super(map, context);
    }

    public void startManager(){
        CompaniesDataSet companiesDataSet = new CompaniesDataSet(null, AppVariables.user.getId());
        companiesDataSet.setUrl(UrlConstructor.getCompanyDetailsUrl(AppVariables.getEmployee().getCompanyId()));
        refreshCompaniesDataSet = new RefreshDataSet(companiesDataSet,handler,updateCompanyResultsRunnable);
        refreshCompaniesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(AppVariables.companyRefreshInterval));
        refreshCompaniesDataSet.start();

        if (AppVariables.getEmployee().getRouteId() != null){
            setRoute(AppVariables.getEmployee().getRouteId());
        }
    }

    final Runnable updateCompanyResultsRunnable = new Runnable() {
        public void run() {
            CompaniesDataSet companiesDataSet = (CompaniesDataSet)refreshCompaniesDataSet.getResultsDataSet();
            if (companiesDataSet.success && !companiesDataSet.getCompanies().isEmpty()) {
                Company company = companiesDataSet.getCompanies().get(AppVariables.getEmployee().getCompanyId());
                ObjectCache.refreshCompanies(companiesDataSet.getCompanies());
                updateEmployeesRefreshDataSets();

                if (company != null && company.getBusiness().equalsIgnoreCase(ECompanyBusiness.TRANSPORTATION.toString())){
                    ((IMapActivity)AppVariables.mainActivity).showPlotRoute();
                }
            }
        }
    };

    final Runnable updateEmployeeResultsRunnable = new Runnable() {
        public void run() {
            EmployeesDataSet employeesDataSet = (EmployeesDataSet)refreshEmployeesDataSet.getResultsDataSet();
            if (employeesDataSet.success) {
                ObjectCache.refreshEmployees(employeesDataSet.getEmployees());
            }
            if (NeceCityMapActivity.deviceLogin){
                changeEmployee();
            }
        }
    };

    private void updateEmployeesRefreshDataSets(){
        EmployeesDataSet employeesDataSet = new EmployeesDataSet(null, AppVariables.getEmployee().getCompanyId());
        employeesDataSet.setUrl(UrlConstructor.getCompanyEmployeesUrl(AppVariables.getEmployee().getCompanyId()));
        refreshEmployeesDataSet = new RefreshDataSet(employeesDataSet,handler,updateEmployeeResultsRunnable);
        refreshEmployeesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(AppVariables.employeeRefreshInterval));
        refreshEmployeesDataSet.start();
    }

    public void prepareSearch(SearchView newSearchView){
        this.searchView = newSearchView;

        searchAdapter.setSuggestionsList(suggestions);
        searchAdapter.setOnSearchItemClickListener(new SearchAdapter.OnSearchItemClickListener() {
            @Override
            public void onSearchItemClick(int position, CharSequence title, CharSequence subtitle) {
                searchView.clearFocus();

                SearchItem item = searchAdapter.getResultsList().get(position);

                onSearchItemClicked(item);
            }
        });

        searchView.setAdapter(searchAdapter);

        searchView.setOnQueryTextListener(new Search.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(CharSequence query) {
                return true;
            }

            @Override
            public void onQueryTextChange(CharSequence newText) {
            }
        });
    }

    public void changeEmployee(){
        Company company = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId());

        if (company == null){
            System.out.println("change employee - company is null");
            return;
        }

        suggestions.clear();
        HashMap<String,Employee> employees = company.getEmployees();
        for (Map.Entry<String, Employee> e:employees.entrySet()){
            Employee employee = e.getValue();

            SearchItem searchItem = new SearchItem(NeceCityMapActivity.context);
            searchItem.setTitle(employee.getName());
            searchItem.setSubtitle(employee.getId());
            searchItem.setSearchItem(ESearchItem.EMPLOYEE);
            searchItem.setIcon1Resource(R.drawable.ic_person_white_small);
            searchItem.setObject(employee);
            searchItem.setId(employee.getId());

            suggestions.add(searchItem);
        }

        if (searchView.suggestionsVisible()){
            NeceCityMapActivity.loseSearchFocus();
            return;
        }

        searchView.setHint(AppVariables.user.getId());
        searchAdapter.notifyDataSetChanged();
        CharSequence all = "*";
        searchView.filter(all);
        searchView.showSuggestions();
    }

    public void changeRoute(){
        Company company = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId());

        if (company == null){
            System.out.println("change route - company is null");
            return;
        }

        suggestions.clear();
        HashMap<String,Route> routes = company.getRoutes();
        for (Map.Entry<String, Route> e: routes.entrySet()){

            SearchItem searchItem = new SearchItem(NeceCityMapActivity.context);
            searchItem.setTitle(e.getValue().getName());
            searchItem.setIcon1Resource(R.drawable.route_small);
            searchItem.setSearchItem(ESearchItem.ROUTE);
            searchItem.setId(e.getKey());

            suggestions.add(searchItem);
        }

        if (searchView.suggestionsVisible()){
            NeceCityMapActivity.loseSearchFocus();
            return;
        }

        searchView.setHint(AppVariables.getEmployee().getRouteId());
        searchAdapter.notifyDataSetChanged();
        CharSequence all = "*";
        searchView.filter(all);
        searchView.showSuggestions();
    }

    public boolean plotRoute(){
        return plotRoute;
    }

    public String getPlotRouteName() {
        return plotRouteName;
    }

    public void setPlotRouteName(String plotRouteName) {
        this.plotRouteName = plotRouteName;
    }

    public void startPloteRoute(String routeName){
        plotRoute = true;
        plotRouteName=routeName;
    }

    public void stopPloteRoute(boolean saveRoute){
        plotRoute = false;

        if (saveRoute){
            saveRoute();
        }

        plotRouteName = null;
        plotRoutePolyline.remove();
    }

    public void saveRoute(){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("companyID",AppVariables.getEmployee().getCompanyId()));
        params.add(new BasicNameValuePair("routeID",plotRouteName));
        params.add(new BasicNameValuePair("routeColor","#"+Integer.toHexString(Color.BLACK)));

        for (Map.Entry<String, RoutePoint> e:plotRoutePoints.entrySet()) {
            RoutePoint point = e.getValue();
            params.add(new BasicNameValuePair(point.getId()+"[busStop]",String.valueOf(point.isBusStop())));
            params.add(new BasicNameValuePair(point.getId()+"[name]",point.getName()));
            params.add(new BasicNameValuePair(point.getId()+"[id]",point.getId()));
            params.add(new BasicNameValuePair(point.getId()+"[latitude]",""+point.getLatitude()));
            params.add(new BasicNameValuePair(point.getId()+"[longitude]",""+point.getLongitude()));
            params.add(new BasicNameValuePair(point.getId()+"[timeTable]",""));

            int count = 1;
            for (String time:point.getStopTimeTable()){
                params.add(new BasicNameValuePair(point.getId()+"[timeTable]["+count+"]",time));
                count++;
            }
        }

        RouteDataSet routeDataSet = new RouteDataSet(this,AppVariables.getEmployee().getCompanyId(),null);
        routeDataSet.setParams(params);
        routeDataSet.setUrl(UrlConstructor.saveCompanyRouteUrl());
        routeDataSet.setHttpMethod(EHttpMethod.POST);
        routeDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        //System.out.println("post get dataset 1");
        if (dataSet instanceof RouteDataSet){
            //System.out.println("post get dataset 2");
            if (dataSet.success) {
                //System.out.println("post get dataset 3");
                AppVariables.mainActivity.showMessage(dataSet.message);
                Route route = ((RouteDataSet)dataSet).getRoute();

                Company company = ObjectCache.getCompanies().get(route.getCompanyId());
                if (company != null) {
                    company.getRoutes().put(route.getId(), route);
                }

                SearchItem searchItem = new SearchItem(AppVariables.mainActivity.getContext());
                searchItem.setTitle(route.getId());
                searchItem.setIcon1Resource(R.drawable.route_small);
                searchItem.setSearchItem(ESearchItem.ROUTE);
                searchItem.setId(route.getId());

                suggestions.add(searchItem);
                searchAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void plotRoute(LatLng point){
        //System.out.println("drawing route 1");

        if (!plotRoute){
            return;
        }

        //System.out.println("drawing route 2");

        String pointId = ""+(plotRoutePoints.size()+1);
        RoutePoint routePoint = new RoutePoint(pointId);
        routePoint.setLatitude(point.latitude);
        routePoint.setLongitude(point.longitude);

        plotRoutePoints.put(pointId,routePoint);

        if (plotRoutePolyline == null){
            PolylineOptions routeOptions = new PolylineOptions().width(10).color(Color.BLACK).geodesic(true);
            plotRoutePolyline = map.addPolyline(routeOptions);
        }

        List<LatLng> points = plotRoutePolyline.getPoints();
        points.add(point);
        plotRoutePolyline.setPoints(points);
    }

    public void onSearchItemClicked(SearchItem item){
        ESearchItem eSearchItem = (ESearchItem)item.getSearchItem();
        switch (eSearchItem){
            case EMPLOYEE:
                if (item.getId() != AppVariables.getEmployee().getId()) {
                    LoginManager loginManager = new LoginManager();
                    loginManager.changeEmployee(item.getId());
                }
                NeceCityMapActivity.deviceLogin=false;
                break;
            case ROUTE:
                Company company = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId());
                Route route = company.getRoutes().get(item.getId());
                if (route != null && AppVariables.getEmployee().getRoute() == route) {
                    hideRoutes();
                    showRoute(NeceCityMapActivity.map,route);
                }else {
                    setRoute(item.getId());
                }
                break;
        }
    }

    public void hideRoutes(){
        for (Map.Entry<String,Route> e: routes.entrySet()){
            hideRoute(e.getKey(),true);
        }
    }

    public void setRoute(String routeId){
        RouteDataSet routeDataSet = new RouteDataSet(null, AppVariables.getEmployee().getCompanyId(), routeId);
        routeDataSet.setUrl(UrlConstructor.setCompanyRouteUrl());
        routeDataSet.setHttpMethod(EHttpMethod.PATCH);
        routeDataSet.setEmployeeId(AppVariables.user.getId());

        CustomRunnable customRunnable = createRouteRunnable();
        GetDataSet setRouteDataSet = new GetDataSet(routeDataSet,handler,customRunnable);
        customRunnable.setObject(setRouteDataSet);
        setRouteDataSet.retrieveDataSet();
    }

    public CustomRunnable createRouteRunnable(){
        CustomRunnable runnable = new CustomRunnable() {
            public void run() {
                GetDataSet getRouteDataSet = (GetDataSet)getObject();
                RouteDataSet routeDataSet = (RouteDataSet) getRouteDataSet.getDataSet();
                if (routeDataSet.success) {
                    Route route = routeDataSet.getRoute();

                    Company company = ObjectCache.getCompanies().get(route.getCompanyId());
                    if (company != null) {
                        company.getRoutes().put(route.getId(), route);
                    }

                    AppVariables.getEmployee().setRoute(route);

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("routeID",route.getId());
                    editor.apply();

                    showRoute(NeceCityMapActivity.map, routeDataSet.getRoute());
                }
            }
        };
        return runnable;
    }
}
