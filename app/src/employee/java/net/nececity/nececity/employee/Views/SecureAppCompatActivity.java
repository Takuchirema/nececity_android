package net.nececity.nececity.employee.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import net.nececity.nececity.employee.Helpers.PrefUtils;

/**
 * Created by Takunda on 8/16/2018.
 */

public class SecureAppCompatActivity extends AppCompatActivity{

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (!PrefUtils.isKioskModeActive(getApplicationContext())) {
            return;
        }

        hideSystemUI();
        if(!hasFocus) {
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        if (!PrefUtils.isKioskModeActive(getApplicationContext()) ||
                (!(this instanceof NeceCityMapActivity) && !(this instanceof LoginActivity))) {
            super.onBackPressed();
        }
    }
}
