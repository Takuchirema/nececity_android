package net.nececity.nececity.employee.Managers;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import net.nececity.nececity.Abstracts.IGetServerFile;
import net.nececity.nececity.Data.GetServerFile;
import net.nececity.nececity.employee.Data.LoginDataSet;
import net.nececity.nececity.employee.Views.LoginActivity;

import java.io.File;

/**
 * Created by Takunda on 7/27/2018.
 */

public class AppUpdateManager implements IGetServerFile{

    private Activity loginActivity;
    private String fileUrl;

    public AppUpdateManager(String fileUrl){
        this.fileUrl=fileUrl;
    }

    public void getUpdate(Activity loginActivity){
        this.loginActivity = loginActivity;
        GetServerFile getServerFile = new GetServerFile(this,"apk");
        getServerFile.setFileUrl(fileUrl);
        getServerFile.execute();
    }

    @Override
    public void postGetServerFile(File file) {
        ((LoginActivity) loginActivity).showProgress(false,"");
        ((LoginActivity) loginActivity).showProgress(true,"Installing App...");

        // Employees must login again so as to update the device appVersion server side.
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(loginActivity);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file),"application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginActivity.startActivity(intent);
    }
}
