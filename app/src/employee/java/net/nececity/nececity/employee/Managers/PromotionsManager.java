package net.nececity.nececity.employee.Managers;

import android.app.Activity;
import android.widget.Toast;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.EPostType;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.ISendServerPicture;
import net.nececity.nececity.Data.ActionDataSet;
import net.nececity.nececity.Data.PromotionsDataSet;
import net.nececity.nececity.Data.SendServerPicture;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.employee.Views.NeceCityMapActivity;
import net.nececity.nececity.employee.Views.PromotionListActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 8/4/2018.
 */

public class PromotionsManager implements IGetDataSet, ISendServerPicture {

    private Activity activity;
    private boolean promotionUpdate;
    private ArrayList<String> addImageUrls = new ArrayList<>();
    private ArrayList<String> deleteImageUrls = new ArrayList<>();

    public PromotionsManager(Activity activity){
        this.activity = activity;
    }

    public void refreshPromotions(Activity activity){
        this.activity = activity;

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("companyID", AppVariables.getEmployee().getCompanyId()));

        String url = UrlConstructor.getCompanyPromotionsUrl(AppVariables.getEmployee().getCompanyId());
        if (!AppVariables.getEmployee().isSuperUser()){
            url = UrlConstructor.getEmployeeCompanyPromotionsUrl(AppVariables.getEmployee().getCompanyId(),AppVariables.user.getId());
        }

        PromotionsDataSet promotionsDataSet = new PromotionsDataSet(this);
        promotionsDataSet.setUrl(url);
        promotionsDataSet.setParams(params);
        promotionsDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {

        Toast.makeText(activity, dataSet.message, Toast.LENGTH_LONG).show();

        if(!dataSet.success){
            if (activity instanceof PromotionListActivity){
                ((PromotionListActivity)activity).clearRefresh();
            }
            return;
        }

        if (dataSet instanceof ActionDataSet){
            ActionDataSet actionDataSet = (ActionDataSet) dataSet;
            Company company;

            switch (actionDataSet.getActionType()){
                case DELETE_PROMOTION:
                    String promotionId;
                    try {
                        promotionId = actionDataSet.getJsonObject().getString("promotionId");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                    company = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId());
                    Promotion promotion = company.getPromotions().get(promotionId);
                    if (promotion.getListActivity() != null){
                        Activity listActivity = promotion.getListActivity();
                        if (listActivity instanceof PromotionListActivity){
                            ((PromotionListActivity)listActivity).refreshList();
                        }
                    }
                    company.getPromotions().remove(promotionId);
                    break;
            }
            activity.finish();
            return;
        }

        PromotionsDataSet promotionsDataSet = (PromotionsDataSet)dataSet;
        ObjectCache.refreshPromotions(promotionsDataSet.getPromotions());
        if (activity instanceof PromotionListActivity){
            ((PromotionListActivity)activity).createListAdapter();
        }

        if (promotionUpdate){
            HashMap<String, Promotion> promotions = promotionsDataSet.getPromotions().get(AppVariables.getEmployee().getCompanyId());
            Promotion promotion = (Promotion)promotions.values().toArray()[0];

            if (deleteImageUrls != null) {
                for(String imageUrl:deleteImageUrls) {
                    String url = UrlConstructor.deleteCompanyFile(AppVariables.getEmployee().getCompanyId());
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("url", imageUrl));
                    params.add(new BasicNameValuePair("id", promotion.getId()));
                    params.add(new BasicNameValuePair("type", EObjectType.PROMOTION.toString()));

                    ActionDataSet actionDataSet = new ActionDataSet(this,EActionType.DELETE_FILE);
                    actionDataSet.setHttpMethod(EHttpMethod.POST);
                    actionDataSet.setUrl(url);
                    actionDataSet.setParams(params);
                    actionDataSet.getDataSet();
                }
            }

            if (addImageUrls != null) {
                for (String imagePath : addImageUrls) {
                    String url = UrlConstructor.uploadCompanyPromotionPicture(promotion.getCompanyId(), promotion.getId());
                    SendServerPicture sendServerPicture = new SendServerPicture(this, imagePath, url, NeceCityMapActivity.context);
                    sendServerPicture.execute();
                }
            }
        }

        if (promotionUpdate){
            activity.finish();
        }
    }

    public void updatePromotion(Promotion promotion, ArrayList<String> addImageUrls, ArrayList<String> deleteImageUrls){
        promotionUpdate = true;
        this.addImageUrls=addImageUrls;
        this.deleteImageUrls=deleteImageUrls;

        String timeStamp = new SimpleDateFormat(AppVariables.timeFormat).format(Calendar.getInstance().getTime());

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        if (promotion.getId() != null) {
            params.add(new BasicNameValuePair("promotionID", promotion.getId()));
        }
        params.add(new BasicNameValuePair("promotionMessage", promotion.getPromotion()));
        params.add(new BasicNameValuePair("days", promotion.getDays()));
        params.add(new BasicNameValuePair("companyID", AppVariables.getEmployee().getCompanyId()));
        params.add(new BasicNameValuePair("createdBy", AppVariables.getEmployee().getId()));
        params.add(new BasicNameValuePair("time", timeStamp));
        params.add(new BasicNameValuePair("price", promotion.getPrice()));
        params.add(new BasicNameValuePair("units", promotion.getUnits()));
        params.add(new BasicNameValuePair("title", promotion.getTitle()));
        params.add(new BasicNameValuePair("duration", promotion.getDuration()));

        PromotionsDataSet promotionsDataSet = new PromotionsDataSet(this);
        promotionsDataSet.setUrl(UrlConstructor.createCompanyPromotionUrl(AppVariables.getEmployee().getCompanyId()));
        promotionsDataSet.setHttpMethod(EHttpMethod.POST);
        promotionsDataSet.setParams(params);
        promotionsDataSet.getDataSet();
    }

    public void deletePromotion(String promotionId){
        ActionDataSet deletePromotionAction = new ActionDataSet(this, EActionType.DELETE_PROMOTION);
        deletePromotionAction.setCompanyId(AppVariables.getEmployee().getCompanyId());
        deletePromotionAction.setHttpMethod(EHttpMethod.DELETE);
        deletePromotionAction.setUrl(UrlConstructor.deleteCompanyPromotionsUrl(AppVariables.getEmployee().getCompanyId(),promotionId));
        deletePromotionAction.getDataSet();
    }

    @Override
    public void postSendServerPicture(JSONObject data) throws JSONException {
        Toast.makeText(activity, data.getString("message"), Toast.LENGTH_LONG).show();
    }
}
