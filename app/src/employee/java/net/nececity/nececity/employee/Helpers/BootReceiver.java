package net.nececity.nececity.employee.Helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import net.nececity.nececity.employee.Views.LoginActivity;

/**
 * Created by Takunda on 8/16/2018.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println(" BootReceiver Broadcast");
        Intent myIntent = new Intent(context, LoginActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(myIntent);
    }
}