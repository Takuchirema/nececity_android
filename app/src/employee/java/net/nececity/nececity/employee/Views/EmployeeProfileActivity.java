package net.nececity.nececity.employee.Views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import net.nececity.nececity.Abstracts.ISendServerPicture;
import net.nececity.nececity.Data.SendServerPicture;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.EditImage;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.R;
import net.nececity.nececity.employee.Managers.ProfileManager;

import org.json.JSONException;
import org.json.JSONObject;

public class EmployeeProfileActivity extends AppCompatActivity implements ISendServerPicture{

    private ImageView doneIV;
    private EditText currentPasswordET;
    private EditText passwordET;
    private EditText confirmPasswordET;
    private EditText phoneNumberET;
    private EditText emailET;
    private ImageView mainPicture;
    private ImageView addPicture;

    private String imagePath;
    private Bitmap bmp;

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        this.context = this;

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Profile");
        actionBar.setDisplayHomeAsUpEnabled(true);

        doneIV = findViewById(R.id.done);
        currentPasswordET = findViewById(R.id.currentPassword);
        passwordET = findViewById(R.id.password);
        confirmPasswordET = findViewById(R.id.confirmPassword);
        phoneNumberET = findViewById(R.id.phoneNumber);
        emailET = findViewById(R.id.email);
        mainPicture = findViewById(R.id.mainPicture);
        addPicture = findViewById(R.id.addPicture);

        phoneNumberET.setText(AppVariables.user.getPhoneNumber());
        emailET.setText(AppVariables.user.getEmail());

        if (AppVariables.user.getPictureBmp() != null){
            Drawable drawable = new BitmapDrawable(getResources(), AppVariables.user.getPictureBmp());
            mainPicture.setImageDrawable(drawable);
        }

        setListeners();
    }

    public void setListeners(){

        doneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfileChanges();
            }
        });

        addPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }
            Uri selectedImage = data.getData();

            imagePath = getPath(selectedImage);
            String file_extn = imagePath.substring(imagePath.lastIndexOf(".") + 1);
            try {
                if (file_extn.equalsIgnoreCase("img") || file_extn.equalsIgnoreCase("jpg") ||
                        file_extn.equalsIgnoreCase("jpeg") || file_extn.equalsIgnoreCase("gif") ||
                        file_extn.equalsIgnoreCase("png")) {

                    EditImage editImage = new EditImage(EmployeeProfileActivity.this, imagePath);
                    bmp = editImage.compressImage(imagePath);

                    if (bmp!=null) {
                        Drawable drawable = new BitmapDrawable(getResources(), bmp);
                        mainPicture.setImageDrawable(drawable);
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "The file is not in a required format. jpg,jpeg,gif,png", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
        int column_index = 0;
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        cursor.getString(column_index);

        return cursor.getString(column_index);
    }

    public void saveProfileChanges(){
        String currentPassword = currentPasswordET.getText().toString();
        String password = passwordET.getText().toString();
        String confirmPassword = confirmPasswordET.getText().toString();
        String email = emailET.getText().toString();
        String phoneNumber = phoneNumberET.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(currentPassword) && currentPasswordET.isEnabled()) {
            currentPasswordET.setError("The current password is required");
            focusView = currentPasswordET;
            cancel = true;
        }

        if (!TextUtils.isEmpty(password) && !password.equals(confirmPassword)) {
            currentPasswordET.setError("The password is different from confirmation.");
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            ProfileManager profileManager=new ProfileManager();
            profileManager.setPassword(currentPassword);
            profileManager.setNewPassword(password);
            profileManager.setEmail(email);
            profileManager.setPhoneNumber(phoneNumber);
            profileManager.setEmployeeProfileActivity(this);
            profileManager.updateProfile();
        }

        if (imagePath != null) {
            String url = UrlConstructor.uploadEmployeeProfilePicture(AppVariables.user.getId(),AppVariables.getEmployee().getCompanyId());
            SendServerPicture sendServerPicture = new SendServerPicture(this, imagePath, url,NeceCityMapActivity.context);
            sendServerPicture.execute();
        }
    }

    public static void showMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void postSendServerPicture(JSONObject data) throws JSONException {
        int success = data.getInt("success");
        String message = data.getString("message");

        if (success == 1){
            String url = data.getString("url");
            AppVariables.user.setPictureUrl(url);
            AppVariables.user.setPictureBmp(bmp);

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("pictureUrl", url);
            editor.apply();
        }

        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
