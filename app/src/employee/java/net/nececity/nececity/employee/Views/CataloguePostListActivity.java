package net.nececity.nececity.employee.Views;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.nececity.nececity.Abstracts.CataloguePostListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.CataloguePostListAdapter;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.CommentsActivity;
import net.nececity.nececity.employee.Managers.CataloguePostsManager;

import java.util.ArrayList;
import java.util.HashMap;

public class CataloguePostListActivity extends AppCompatActivity {

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static CataloguePostsManager catalogue_postsManager;
    private CataloguePostListActivity cataloguePostListActivity;
    private String companyId;

    private FloatingActionButton newPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);

        companyId = AppVariables.getEmployee().getCompanyId();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Menu for "+companyId);
        actionBar.setDisplayHomeAsUpEnabled(true);

        cataloguePostListActivity = this;
        catalogue_postsManager = new CataloguePostsManager(this);

        newPost = findViewById(R.id.newPost);
        newPost.setVisibility(View.VISIBLE);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);

        setListeners();

        if (ObjectCache.getCompanies().get(companyId).getCatalogue().isEmpty()){
            catalogue_postsManager.refreshCataloguePosts(cataloguePostListActivity);
        }else {
            createListAdapter();
        }
    }

    public void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        catalogue_postsManager.refreshCataloguePosts(cataloguePostListActivity);
                    }
                }
        );

        newPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(cataloguePostListActivity, EditCataloguePostActivity.class);
                startActivity(intent);
            }
        });
    }

    public void refreshList(){
        catalogue_postsManager.refreshCataloguePosts(cataloguePostListActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){
        clearRefresh();

        HashMap<String,? extends CataloguePostListObject> catalogue_posts =  ObjectCache.getCompanies().get(companyId).getCatalogue();

        ArrayList<String> accountIds = new ArrayList<>(catalogue_posts.keySet());
        CataloguePostListAdapter adapter = new CataloguePostListAdapter(this, EditCataloguePostActivity.class, CommentsActivity.class, accountIds, R.layout.catalogue_post_list_view);
        adapter.setListObjects(catalogue_posts);
        adapter.sort();

        listView = findViewById(R.id.posts_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public void clearRefresh(){
        System.out.println("clear refresh");
        swipeRefreshLayout.setRefreshing(false);
    }
}
