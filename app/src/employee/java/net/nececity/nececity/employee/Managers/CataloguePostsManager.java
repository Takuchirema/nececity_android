package net.nececity.nececity.employee.Managers;

import android.app.Activity;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.EPrivilege;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.ISendServerFile;
import net.nececity.nececity.Abstracts.ISendServerPicture;
import net.nececity.nececity.Data.ActionDataSet;
import net.nececity.nececity.Data.CataloguePostsDataSet;
import net.nececity.nececity.Data.SendServerFile;
import net.nececity.nececity.Data.SendServerPicture;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.employee.Views.NeceCityMapActivity;
import net.nececity.nececity.employee.Views.CataloguePostListActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 9/7/2018.
 */

public class CataloguePostsManager implements IGetDataSet, ISendServerPicture, ISendServerFile {

    private Activity activity;
    private boolean cataloguePostUpdate;
    private ArrayList<String> addImageUrls;
    private ArrayList<String> deleteImageUrls;
    private Uri fileUri;
    private String filePath;
    private boolean deleteFile;

    public CataloguePostsManager(Activity activity){
        this.activity = activity;
    }

    public void refreshCataloguePosts(Activity activity){
        this.activity = activity;

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("companyID", AppVariables.getEmployee().getCompanyId()));

        String url = UrlConstructor.getCataloguePostsUrl(AppVariables.getEmployee().getCompanyId());
        if (!AppVariables.getEmployee().isSuperUser()){
            url = UrlConstructor.getEmployeeCataloguePostsUrl(AppVariables.getEmployee().getCompanyId(),AppVariables.user.getId());
        }

        CataloguePostsDataSet cataloguePostsDataSet = new CataloguePostsDataSet(this,AppVariables.getEmployee().getCompanyId());
        cataloguePostsDataSet.setUrl(url);
        cataloguePostsDataSet.setParams(params);
        cataloguePostsDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet){

        Toast.makeText(activity, dataSet.message, Toast.LENGTH_LONG).show();

        if(!dataSet.success){
            if (activity instanceof CataloguePostListActivity){
                ((CataloguePostListActivity)activity).clearRefresh();
            }
            return;
        }

        if (dataSet instanceof ActionDataSet){
            ActionDataSet actionDataSet = (ActionDataSet) dataSet;
            Company company;

            switch (actionDataSet.getActionType()){
                case DELETE_POST:
                    String postId;
                    try {
                        postId = actionDataSet.getJsonObject().getString("postId");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                    company = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId());
                    CataloguePost post = company.getCatalogue().get(postId);
                    refreshListActivity(post);
                    company.getCatalogue().remove(postId);
                    break;
            }
            activity.finish();
            return;
        }

        CataloguePostsDataSet cataloguePostsDataSet = (CataloguePostsDataSet)dataSet;
        ObjectCache.refreshCataloguePosts(cataloguePostsDataSet.getCataloguePosts());
        if (activity instanceof CataloguePostListActivity){
            ((CataloguePostListActivity)activity).createListAdapter();
        }

        if (cataloguePostUpdate){
            HashMap<String, CataloguePost> cataloguePosts = cataloguePostsDataSet.getCataloguePosts().get(AppVariables.getEmployee().getCompanyId());
            CataloguePost cataloguePost = (CataloguePost)cataloguePosts.values().toArray()[0];
            refreshListActivity(cataloguePost);

            if (deleteImageUrls != null) {
                for(String imageUrl:deleteImageUrls) {
                    String url = UrlConstructor.deleteCompanyFile(AppVariables.getEmployee().getCompanyId());
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("url", imageUrl));
                    params.add(new BasicNameValuePair("id", cataloguePost.getId()));
                    params.add(new BasicNameValuePair("type", EObjectType.CATALOGUE_POST.toString()));

                    ActionDataSet actionDataSet = new ActionDataSet(this,EActionType.DELETE_FILE);
                    actionDataSet.setHttpMethod(EHttpMethod.POST);
                    actionDataSet.setUrl(url);
                    actionDataSet.setParams(params);
                    actionDataSet.getDataSet();
                }
            }

            if (addImageUrls != null) {
                for(String imagePath:addImageUrls) {
                    String url = UrlConstructor.uploadCataloguePostPicture(cataloguePost.getCompanyId(), cataloguePost.getId());
                    SendServerPicture sendServerPicture = new SendServerPicture(this, imagePath, url, NeceCityMapActivity.context);
                    sendServerPicture.execute();
                }
            }

            if (filePath != null){
                System.out.println("sending file path: "+filePath);
                File file = new File(filePath);

                // Get length of file in bytes
                long fileSizeInBytes = file.length();
                // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                long fileSizeInKB = fileSizeInBytes / 1024;

                if (fileSizeInKB > 1000){
                    Toast.makeText(activity.getApplicationContext(), "Please upload files less than 1MB", Toast.LENGTH_LONG).show();
                    return;
                }

                String url = UrlConstructor.uploadCatalogueFileUrl(cataloguePost.getCompanyId(), cataloguePost.getId());
                SendServerFile sendServerFile = new SendServerFile(this, url);
                sendServerFile.setFile(file);
                sendServerFile.execute();
            }else if (deleteFile && cataloguePost.getFileUrl() != null){
                String url = UrlConstructor.deleteCompanyFile(AppVariables.getEmployee().getCompanyId());
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("url", cataloguePost.getFileUrl()));
                params.add(new BasicNameValuePair("id", cataloguePost.getId()));
                params.add(new BasicNameValuePair("type", EObjectType.CATALOGUE_POST.toString()));

                ActionDataSet actionDataSet = new ActionDataSet(this,EActionType.DELETE_FILE);
                actionDataSet.setHttpMethod(EHttpMethod.POST);
                actionDataSet.setUrl(url);
                actionDataSet.setParams(params);
                actionDataSet.getDataSet();
            }
        }

        if (cataloguePostUpdate){
            activity.finish();
        }
    }

    private void refreshListActivity(CataloguePost post){
        if (post.getListActivity() != null){
            Activity postListActivity = post.getListActivity();
            if (postListActivity instanceof CataloguePostListActivity){
                ((CataloguePostListActivity)postListActivity).refreshList();
            }
        }
    }

    public void updateCataloguePost(CataloguePost cataloguePost, ArrayList<String> addImageUrls,
                                    ArrayList<String> deleteImageUrls,String filePath, boolean deleteFile){
        cataloguePostUpdate = true;
        this.addImageUrls = addImageUrls;
        this.deleteImageUrls = deleteImageUrls;
        this.filePath = filePath;
        this.deleteFile=deleteFile;

        String timeStamp = new SimpleDateFormat(AppVariables.timeFormat).format(Calendar.getInstance().getTime());

        List<NameValuePair> params = new ArrayList<>();
        if (cataloguePost.getId() != null) {
            params.add(new BasicNameValuePair("cataloguePostID", cataloguePost.getId()));
        }

        params.add(new BasicNameValuePair("cataloguePostMessage", cataloguePost.getPost()));
        params.add(new BasicNameValuePair("amenities", cataloguePost.getAmenities()));
        params.add(new BasicNameValuePair("companyID", AppVariables.getEmployee().getCompanyId()));
        params.add(new BasicNameValuePair("createdBy", AppVariables.getEmployee().getId()));
        params.add(new BasicNameValuePair("time", timeStamp));
        params.add(new BasicNameValuePair("price", cataloguePost.getPrice()));
        params.add(new BasicNameValuePair("units", cataloguePost.getUnits()));
        params.add(new BasicNameValuePair("title", cataloguePost.getTitle()));
        params.add(new BasicNameValuePair("duration", cataloguePost.getDuration()));

        CataloguePostsDataSet cataloguePostsDataSet = new CataloguePostsDataSet(this,cataloguePost.getCompanyId());
        cataloguePostsDataSet.setUrl(UrlConstructor.createCataloguePostUrl(AppVariables.getEmployee().getCompanyId()));
        cataloguePostsDataSet.setHttpMethod(EHttpMethod.POST);
        cataloguePostsDataSet.setParams(params);
        cataloguePostsDataSet.getDataSet();
    }

    public void deleteCataloguePost(String cataloguePostId){
        ActionDataSet deleteCataloguePostAction = new ActionDataSet(this, EActionType.DELETE_POST);
        deleteCataloguePostAction.setCompanyId(AppVariables.getEmployee().getCompanyId());
        deleteCataloguePostAction.setHttpMethod(EHttpMethod.DELETE);
        deleteCataloguePostAction.setUrl(UrlConstructor.deleteCataloguePostUrl(AppVariables.getEmployee().getCompanyId(),cataloguePostId));
        deleteCataloguePostAction.getDataSet();
    }

    @Override
    public void postSendServerPicture(JSONObject data) throws JSONException {
        Toast.makeText(activity, data.getString("message"), Toast.LENGTH_LONG).show();
    }

    @Override
    public void postSendServerFile(JSONObject data) throws JSONException {

    }
}
