package net.nececity.nececity.employee.Views;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;

import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.EditImage;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.ImageViewerActivity;
import net.nececity.nececity.employee.Managers.PromotionsManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EditPromotionActivity extends SecureAppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private SliderLayout slider;
    private ImageView addPromotionIV;
    private ImageView deletePromotionIV;
    private EditText promotionET;
    private TextView doneIV;
    private TextView deleteIV;

    private EditText titleET,priceET,unitsET;
    private TextView fromTV,toTV;

    private String promotionId;
    private PromotionsManager promotionsManager;
    private Promotion promotion;

    private Bitmap bmp;

    private ArrayList<String> addImageUrls = new ArrayList<>();
    private ArrayList<String> deleteImageUrls = new ArrayList<>();

    private boolean setStartDate;
    private DatePickerDialog datePickerDialog;
    private String fromDate;
    private String toDate;

    private RadioButton mon,tue,wed,thu,fri,sat,sun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_promotion);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        titleET = findViewById(R.id.title);
        promotionET = findViewById(R.id.promotionText);
        addPromotionIV = findViewById(R.id.addPromotionImage);
        deletePromotionIV=findViewById(R.id.deletePromotionImage);
        priceET = findViewById(R.id.price);
        unitsET = findViewById(R.id.units);
        fromTV = findViewById(R.id.from);
        toTV = findViewById(R.id.to);

        slider = findViewById(R.id.slider);
        slider.stopAutoCycle();

        doneIV = findViewById(R.id.done);
        deleteIV = findViewById(R.id.delete);

        sun = findViewById(R.id.sun);
        sun.setEnabled(true);
        mon = findViewById(R.id.mon);
        mon.setEnabled(true);
        tue = findViewById(R.id.tue);
        tue.setEnabled(true);
        wed = findViewById(R.id.wed);
        wed.setEnabled(true);
        thu = findViewById(R.id.thu);
        thu.setEnabled(true);
        fri = findViewById(R.id.fri);
        fri.setEnabled(true);
        sat = findViewById(R.id.sat);
        sat.setEnabled(true);

        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("promotionId")) {
            promotionId = getIntent().getExtras().getString("promotionId");
        }

        promotionsManager = new PromotionsManager(this);

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(this, EditPromotionActivity.this, year, month, day);
        populatePromotion();
        setListeners();
    }

    public void populatePromotion(){

        if (promotionId == null){
            return;
        }

        promotion = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId()).getPromotions().get(promotionId);

        ArrayList<String> pictureUrls = promotion.getPictureUrls();
        for (String pictureUrl:pictureUrls) {
            addSliderView(pictureUrl,null);
        }

        titleET.setText(promotion.getTitle());
        fromTV.setText(promotion.getFrom());
        toTV.setText(promotion.getTo());
        priceET.setText(promotion.getPrice());
        unitsET.setText(promotion.getUnits());
        promotionET.setText(promotion.getPromotion());
        populateDays(promotion);
    }

    public void addSliderView(String pictureUrl, Bitmap bmp){
        TextSliderView sliderView = new TextSliderView(this);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();

        if (bmp != null){
            sliderView
                    .image(bmp)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setRequestOption(requestOptions)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            viewPostImage(slider.getUrl());
                        }
                    });
        }else if (pictureUrl != null){
            sliderView
                    .image(pictureUrl)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setRequestOption(requestOptions)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            viewPostImage(slider.getUrl());
                        }
                    });
        }

        //add your extra information
        sliderView.bundle(new Bundle());
        sliderView.getBundle().putString("extra", "name");

        slider.addSlider(sliderView);
    }

    public void viewPostImage(String pictureUrl){

        Intent intent = new Intent(this, ImageViewerActivity.class);
        intent.putExtra("companyId", AppVariables.getEmployee().getCompanyId());
        intent.putExtra("pictureUrl", pictureUrl);
        intent.putExtra("title", promotion.getTitle());
        intent.putExtra("promotionId", promotion.getId());

        startActivity(intent);
    }

    public void populateDays(Promotion promotion){
        String days = promotion.getDays();

        if (days == null){
            return;
        }

        String[] daysArr = days.split(",");

        for (String day:daysArr){
            if (day.equalsIgnoreCase("mon")){
                mon.setChecked(true);
            }

            if (day.equalsIgnoreCase("tue")){
                tue.setChecked(true);
            }

            if (day.equalsIgnoreCase("wed")){
                wed.setChecked(true);
            }

            if (day.equalsIgnoreCase("thu")){
                thu.setChecked(true);
            }

            if (day.equalsIgnoreCase("fri")){
                fri.setChecked(true);
            }

            if (day.equalsIgnoreCase("sat")){
                sat.setChecked(true);
            }

            if (day.equalsIgnoreCase("sun")){
                sun.setChecked(true);
            }
        }
    }

    public void setListeners(){
        fromTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStartDate = true;
                datePickerDialog.show();
            }
        });

        toTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStartDate = false;
                datePickerDialog.show();
            }
        });

        addPromotionIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });

        deletePromotionIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseSliderView sliderView = slider.getCurrentSlider();
                if (sliderView == null){
                    return;
                }

                String url = sliderView.getUrl();
                if (addImageUrls.contains(url)){
                    addImageUrls.remove(url);
                }else {
                    deleteImageUrls.add(url);
                }
                System.out.println("delete image url:"+url+" delete image count: "+deleteImageUrls.size());
                slider.removeSliderAt(slider.getCurrentPosition());
            }
        });

        doneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePromotionChanges();
            }
        });

        deleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePromotion();
            }
        });
    }

    public void deletePromotion(){
        if (promotionId == null){
            return;
        }

        PromotionsManager promotionsManager = new PromotionsManager(this);
        promotionsManager.deletePromotion(promotionId);
    }

    public void savePromotionChanges(){
        String promotionText = promotionET.getText().toString();
        String promotionDays = getPromotionDays();
        String price = priceET.getText().toString();
        String units = unitsET.getText().toString();
        String title = titleET.getText().toString();
        String duration = "";
        if (fromDate != null || toDate != null) {
            duration = fromDate.trim() + "," + toDate.trim();
        }else if (promotion != null){
            duration = promotion.getDuration();
        }

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(promotionText)) {
            promotionET.setError("The promotion cannot be empty");
            focusView = promotionET;
            cancel = true;
        }

        if (promotionDays.isEmpty()){
            Toast.makeText(this, "Please select promotion days", Toast.LENGTH_LONG).show();
            return;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            Promotion promotion = new Promotion(promotionId);
            promotion.setDays(promotionDays);
            promotion.setUnits(units);
            promotion.setPrice(price);
            promotion.setDuration(duration);
            promotion.setPromotion(promotionText);
            promotion.setTitle(title);

            PromotionsManager promotionsManager = new PromotionsManager(this);
            promotionsManager.updatePromotion(promotion,addImageUrls,deleteImageUrls);
        }
    }

    public String getPromotionDays(){
        String days = "";

        if (mon.isChecked()){
            days = "mon,"+days;
        }

        if (tue.isChecked()){
            days = "tue,"+days;
        }

        if (wed.isChecked()){
            days = "wed,"+days;
        }

        if (thu.isChecked()){
            days = "thu,"+days;
        }

        if (fri.isChecked()){
            days = "fri,"+days;
        }

        if (sat.isChecked()){
            days = "sat,"+days;
        }

        if (sun.isChecked()){
            days = "sun,"+days;
        }

        return days;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }
            Uri selectedImage = data.getData();

            String imagePath = getPath(selectedImage);
            String file_extn = imagePath.substring(imagePath.lastIndexOf(".") + 1);
            try {
                if (file_extn.equalsIgnoreCase("img") || file_extn.equalsIgnoreCase("jpg") ||
                        file_extn.equalsIgnoreCase("jpeg") || file_extn.equalsIgnoreCase("gif") ||
                        file_extn.equalsIgnoreCase("png")) {

                    EditImage editImage = new EditImage(EditPromotionActivity.this, imagePath);
                    bmp = editImage.compressImage(imagePath);

                    if (bmp!=null) {
                        addImageUrls.add(imagePath);
                        System.out.println("add image url: "+imagePath+"add image count: "+addImageUrls.size());
                        addSliderView(imagePath,null);
                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "The file is not in a required format. jpg,jpeg,gif,png", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
        int column_index = 0;
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        cursor.getString(column_index);

        return cursor.getString(column_index);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear += 1;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if (setStartDate) {
                Date fromDateD = formatter.parse(year+"-"+monthOfYear + "-" + dayOfMonth);

                if (toDate != null) {
                    Date toDateD = formatter.parse(toDate);
                    if (toDateD.before(fromDateD)) {
                        Toast.makeText(EditPromotionActivity.this, "End date must be after start date", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                String day = ""+dayOfMonth;
                String month = ""+monthOfYear;

                if ((""+dayOfMonth).length()==1) {
                    day = "0"+dayOfMonth;
                }

                if ((""+monthOfYear).length()==1) {
                    month = "0"+monthOfYear;
                }

                fromDate = year + "-" + month + "-" + day;
                fromTV.setText(year + "-" + month + "-" + day);
            } else {
                Date toDateD = formatter.parse(year+"-"+ monthOfYear+"-"+dayOfMonth);

                if (fromDate != null) {
                    Date fromDateD = formatter.parse(fromDate);
                    if (fromDateD.after(toDateD)) {
                        Toast.makeText(EditPromotionActivity.this, "End date must be after start date", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                String day = ""+dayOfMonth;
                String month = ""+monthOfYear;

                //month of year seems to be a day behind not starting from 0
                monthOfYear++;

                if ((""+dayOfMonth).length()==1) {
                    day = "0"+dayOfMonth;
                }

                if ((""+monthOfYear).length()==1) {
                    month = "0"+monthOfYear;
                }

                toDate = year+"-"+month+"-"+ day;
                toTV.setText(year+"-"+ monthOfYear+"-"+dayOfMonth);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
