package net.nececity.nececity.employee.Data;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Data.CustomJSONObject;
import net.nececity.nececity.Data.GetServerData;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.Objects.Settings;
import net.nececity.nececity.employee.Views.LoginActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import net.nececity.nececity.Data.CustomJSONArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 24/05/2018.
 */

public class LoginDataSet extends DataSet{

    private String companyId;
    private String employeeId;
    private String password;
    private Activity loginActivity;
    private String url;
    private List<NameValuePair> params = new ArrayList<NameValuePair>();
    private boolean useSharePreference=true;
    private boolean deviceLogin;
    private String deviceId;
    private boolean latestVersion=true;
    private String updateUrl;

    SharedPreferences preferences;

    public LoginDataSet(IGetDataSet getDataset, Activity activity, String companyId, String employeeId, String password)
    {
        super(getDataset);
        this.loginActivity =activity;
        this.companyId=companyId;
        this.employeeId=employeeId;
        this.password=password;
    }

    public LoginDataSet(IGetDataSet getDataSet, Activity activity){
        super(getDataSet);
        this.loginActivity=activity;
    }

    @Override
    public void getDataSet(){

        if (useSharePreference && sharedPreferenceLogin()){
            message="Logged in with share preferences";
            success=true;
            getDataset.postGetDataSet(this);
            return;
        }

        if (!deviceLogin && (password==null || employeeId==null)){
            return;
        }

        if (deviceId == null){
            deviceId = AppVariables.getDeviceId(loginActivity);
        }

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("employeeID", employeeId));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("deviceId", deviceId));
        try {
            params.add(new BasicNameValuePair("version", AppVariables.setVersionNameSuffix(loginActivity)));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (!this.params.isEmpty()){
            params = this.params;
        }

        String url = UrlConstructor.getEmployeeLoginUrl(employeeId, companyId);
        if (this.url != null) {
            url = this.url;
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.POST);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {

        updateUrl = json.getString("updateUrl");
        String latestVersion = json.getString("latestVersion");
        if (latestVersion.equalsIgnoreCase("false")){
            this.latestVersion=false;
        }

        CustomJSONArray employeeJsonArray = json.getJSONArray("employees");
        CustomJSONObject employeeJson = null;

        if (employeeJsonArray.length() == 0) {
            message = "User was not found";
            success = false;
        } else {
            employeeJson = employeeJsonArray.getJSONObject(0);
        }

        //do this early so that the update profile has a token and userID
        AppVariables.token = json.getString("token");

        Employee employee = new Employee(employeeJson.getString("name"));
        employee.setStatus("online");
        employee.setId(employeeJson.getString("employeeId"));
        employee.setPhoneNumber(employeeJson.getString("phoneNumber"));
        employee.setPrivilege(employeeJson.getString("privilege"));
        employee.setRouteId(employeeJson.getString("routeId"));
        employee.setPictureUrl(employeeJson.getString("profilePicture"));
        employee.setCompanyId(employeeJson.getString("companyId"));
        employee.setEmail(employeeJson.getString("email"));

        setEmployeePreferences(employee,json.getString("token"));
        System.out.println("new user - "+employee.getId());
        AppVariables.user = employee;
    }

    public void setEmployeePreferences(Employee employee,String token){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(loginActivity);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("employeeID",employee.getId());
        editor.putString("employeeName",employee.getName());
        editor.putString("phoneNumber",employee.getPhoneNumber());
        editor.putString("privilege",employee.getPrivilege());
        editor.putString("pictureUrl",employee.getPictureUrl());
        editor.putString("company",employee.getCompanyId());
        editor.putString("token",token);
        editor.putString("status",employee.getStatus().toString());
        editor.putString("email",employee.getEmail());

        editor.apply();
    }

    public boolean sharedPreferenceLogin(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(loginActivity);
        if (preferences.contains("employeeID") && preferences.contains("employeeName")){
            loginWithSharedPreference();
            return true;
        }
        return false;
    }

    public void loginWithSharedPreference(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(loginActivity);
        String name = preferences.getString("employeeName", "").trim();
        String id = preferences.getString("employeeID","").trim();
        String phoneNumber = preferences.getString("phoneNumber","").trim();
        String privilege = preferences.getString("privilege","");
        String pictureUrl = preferences.getString("pictureUrl","");
        String company = preferences.getString("company","");
        String token = preferences.getString("token","");
        String status = preferences.getString("status","online");
        String email = preferences.getString("email","");

        Employee employee = new Employee(name);
        employee.setStatus(status);
        employee.setId(id);
        employee.setPhoneNumber(phoneNumber);
        employee.setPrivilege(privilege);
        employee.setPictureUrl(pictureUrl);
        employee.setCompanyId(company);
        employee.setEmail(email);

        if (preferences.contains("routeID")){
            employee.setRouteId(preferences.getString("routeID",""));
        }

        AppVariables.user = employee;
        AppVariables.token = token;
    }

    public void setUserSettings(Settings userSettings,SharedPreferences preferences) throws Exception{

        if (preferences.contains("visibilityRegion"))
            userSettings.setRegionVisibility(preferences.getString("visibilityRegion",""));

        if (preferences.contains("messagesRefreshInterval"))
            userSettings.setMessagesRefreshInterval(preferences.getInt("messagesRefreshInterval",0));

        if (preferences.contains("usersRefreshInterval"))
            userSettings.setUsersRefreshInterval(preferences.getInt("usersRefreshInterval",0));

        if (preferences.contains("postsRefreshInterval"))
            userSettings.setPostsRefreshInterval(preferences.getInt("postsRefreshInterval",0));

        if (preferences.contains("companiesRefreshInterval"))
            userSettings.setCompaniesRefreshInterval(preferences.getInt("companiesRefreshInterval",0));
    }

    public String getUserId() {
        return employeeId;
    }

    public void setUserId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUrl(String url){
        this.url=url;
    }

    public void setUseSharePreference(boolean useSharePreference){
        this.useSharePreference=useSharePreference;
    }

    public boolean isLatestVersion() {
        return latestVersion;
    }

    public void setLatestVersion(boolean latestVersion) {
        this.latestVersion = latestVersion;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public boolean isDeviceLogin() {
        return deviceLogin;
    }

    public void setDeviceLogin(boolean deviceLogin) {
        this.deviceLogin = deviceLogin;
    }
}
