package net.nececity.nececity.employee.Managers;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Data.ProfileDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.employee.Views.NeceCityMapActivity;
import net.nececity.nececity.employee.Views.EmployeeProfileActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 11/07/2018.
 */

public class ProfileManager implements IGetDataSet {

    private String password;
    private String newPassword;
    private String email;
    private String phoneNumber;
    private EmployeeProfileActivity employeeProfileActivity;

    public void updateProfile(){

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("employeeID", AppVariables.user.getId()));
        params.add(new BasicNameValuePair("companyID", AppVariables.getEmployee().getCompanyId()));

        if (newPassword != null && !newPassword.isEmpty()){
            params.add(new BasicNameValuePair("newPassword", newPassword));
        }

        if (password != null && !password.isEmpty()){
            params.add(new BasicNameValuePair("password", password));
        }

        if (email != null && !email.isEmpty()){
            params.add(new BasicNameValuePair("email", email));
        }

        if (phoneNumber != null && !phoneNumber.isEmpty()){
            params.add(new BasicNameValuePair("phoneNumber", phoneNumber));
        }

        ProfileDataSet profileDataSet = new ProfileDataSet(this);
        profileDataSet.setUrl(UrlConstructor.editEmployeeProfileUrl());
        profileDataSet.setParams(params);
        profileDataSet.updateDataSet();
    }

    public void updateEmployee(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(NeceCityMapActivity.context);
        SharedPreferences.Editor editor = preferences.edit();

        if (email != null){
            AppVariables.user.setEmail(email);
            editor.putString("email", email);
        }

        if (phoneNumber != null){
            AppVariables.user.setPhoneNumber(phoneNumber);
            editor.putString("phoneNumber", phoneNumber);
        }

        editor.apply();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        if (dataSet.success){
            updateEmployee();
        }
        employeeProfileActivity.showMessage(dataSet.message);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public EmployeeProfileActivity getEmployeeProfileActivity() {
        return employeeProfileActivity;
    }

    public void setEmployeeProfileActivity(EmployeeProfileActivity employeeProfileActivity) {
        this.employeeProfileActivity = employeeProfileActivity;
    }
}
