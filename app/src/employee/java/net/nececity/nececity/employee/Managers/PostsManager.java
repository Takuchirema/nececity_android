package net.nececity.nececity.employee.Managers;

import android.app.Activity;
import android.widget.Toast;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.EPostType;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.ISendServerPicture;
import net.nececity.nececity.Data.ActionDataSet;
import net.nececity.nececity.Data.PostsDataSet;
import net.nececity.nececity.Data.SendServerPicture;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.employee.Views.NeceCityMapActivity;
import net.nececity.nececity.employee.Views.PostListActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 7/31/2018.
 */

public class PostsManager implements IGetDataSet, ISendServerPicture{

    private Activity activity;
    private boolean postUpdate;

    private ArrayList<String> addImageUrls = new ArrayList<>();
    private ArrayList<String> deleteImageUrls = new ArrayList<>();

    public PostsManager(Activity activity){
        this.activity = activity;
    }

    public void refreshPosts(Activity activity){
        this.activity = activity;

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("companyID", AppVariables.getEmployee().getCompanyId()));
        String url = UrlConstructor.getCompanyPostsUrl(AppVariables.getEmployee().getCompanyId(),EPostType.COMPANY.toString());
        if (!AppVariables.getEmployee().isSuperUser()){
            url = UrlConstructor.getEmployeeCompanyPostsUrl(AppVariables.getEmployee().getCompanyId(),AppVariables.user.getId());
        }

        PostsDataSet postsDataSet = new PostsDataSet(this);
        postsDataSet.setUrl(url);
        postsDataSet.setParams(params);
        postsDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        Toast.makeText(activity, dataSet.message, Toast.LENGTH_LONG).show();

        if(!dataSet.success){
            if (activity instanceof PostListActivity){
                ((PostListActivity)activity).clearRefresh();
            }
            return;
        }

        if (dataSet instanceof ActionDataSet){
            ActionDataSet actionDataSet = (ActionDataSet) dataSet;
            Company company;

            switch (actionDataSet.getActionType()){
                case DELETE_POST:
                    String postId;
                    try {
                        postId = actionDataSet.getJsonObject().getString("postId");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                    company = ObjectCache.getCompanies().get(AppVariables.getEmployee().getCompanyId());
                    Post post = company.getPosts().get(postId);
                    if (post.getListActivity() != null){
                        Activity postListActivity = post.getListActivity();
                        if (postListActivity instanceof PostListActivity){
                            ((PostListActivity)postListActivity).refreshList();
                        }
                    }
                    company.getPosts().remove(postId);
                    break;
            }
            activity.finish();
            return;
        }

        PostsDataSet postsDataSet = (PostsDataSet)dataSet;
        ObjectCache.refreshPosts(postsDataSet.getPosts());
        if (activity instanceof PostListActivity){
            ((PostListActivity)activity).createListAdapter();
        }

        if (postUpdate){
            HashMap<String, Post> posts = postsDataSet.getPosts().get(AppVariables.getEmployee().getCompanyId());
            Post post = (Post)posts.values().toArray()[0];

            if (deleteImageUrls != null) {
                for(String imageUrl:deleteImageUrls) {
                    String url = UrlConstructor.deleteCompanyFile(AppVariables.getEmployee().getCompanyId());
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("url", imageUrl));
                    params.add(new BasicNameValuePair("id", post.getId()));
                    params.add(new BasicNameValuePair("type", EObjectType.POST.toString()));

                    ActionDataSet actionDataSet = new ActionDataSet(this,EActionType.DELETE_FILE);
                    actionDataSet.setHttpMethod(EHttpMethod.POST);
                    actionDataSet.setUrl(url);
                    actionDataSet.setParams(params);
                    actionDataSet.getDataSet();
                }
            }

            if (addImageUrls != null) {
                for (String imagePath : addImageUrls) {
                    String url = UrlConstructor.uploadCompanyPostPicture(post.getCompanyId(), post.getId());
                    SendServerPicture sendServerPicture = new SendServerPicture(this, imagePath, url, NeceCityMapActivity.context);
                    sendServerPicture.execute();
                }
            }
        }

        if (postUpdate){
            activity.finish();
        }
    }

    public void updatePost(String postId, String postText, ArrayList<String> addImageUrls, ArrayList<String> deleteImageUrls){
        postUpdate = true;
        this.addImageUrls=addImageUrls;
        this.deleteImageUrls=deleteImageUrls;

        String timeStamp = new SimpleDateFormat(AppVariables.timeFormat).format(Calendar.getInstance().getTime());

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        if (postId != null) {
            params.add(new BasicNameValuePair("postID", postId));
        }
        params.add(new BasicNameValuePair("postMessage", postText));
        params.add(new BasicNameValuePair("companyID", AppVariables.getEmployee().getCompanyId()));
        params.add(new BasicNameValuePair("createdBy", AppVariables.getEmployee().getId()));
        params.add(new BasicNameValuePair("time", timeStamp));

        PostsDataSet postsDataSet = new PostsDataSet(this);
        postsDataSet.setUrl(UrlConstructor.createCompanyPostUrl(AppVariables.getEmployee().getCompanyId(), EPostType.COMPANY.toString()));
        postsDataSet.setHttpMethod(EHttpMethod.POST);
        postsDataSet.setParams(params);
        postsDataSet.getDataSet();
    }

    public void deletePost(String postId){
        ActionDataSet deletePostAction = new ActionDataSet(this, EActionType.DELETE_POST);
        deletePostAction.setCompanyId(AppVariables.getEmployee().getCompanyId());
        deletePostAction.setHttpMethod(EHttpMethod.DELETE);
        deletePostAction.setUrl(UrlConstructor.deleteCompanyPostsUrl(AppVariables.getEmployee().getCompanyId(),postId,EPostType.COMPANY.toString()));
        deletePostAction.getDataSet();
    }

    @Override
    public void postSendServerPicture(JSONObject data) throws JSONException {
        Toast.makeText(activity, data.getString("message"), Toast.LENGTH_LONG).show();
    }
}
