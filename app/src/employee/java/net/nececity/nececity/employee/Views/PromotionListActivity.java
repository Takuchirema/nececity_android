package net.nececity.nececity.employee.Views;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.nececity.nececity.Abstracts.PromotionListObject;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.PromotionListAdapter;
import net.nececity.nececity.Views.CommentsActivity;
import net.nececity.nececity.R;
import net.nececity.nececity.employee.Managers.PromotionsManager;

import java.util.ArrayList;
import java.util.HashMap;

public class PromotionListActivity extends SecureAppCompatActivity {

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static PromotionsManager promotionsManager;
    private PromotionListActivity promotionListActivity;

    private FloatingActionButton newPromotion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_list);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Promotions");
        actionBar.setDisplayHomeAsUpEnabled(true);

        promotionListActivity = this;
        promotionsManager = new PromotionsManager(this);

        newPromotion = findViewById(R.id.newPromotion);
        newPromotion.setVisibility(View.VISIBLE);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);

        promotionsManager.refreshPromotions(promotionListActivity);

        setListeners();
    }

    public void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        promotionsManager.refreshPromotions(promotionListActivity);
                    }
                }
        );

        newPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(promotionListActivity, EditPromotionActivity.class);
                startActivity(intent);
            }
        });
    }

    public void refreshList(){
        promotionsManager.refreshPromotions(promotionListActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){
        clearRefresh();

        HashMap<String,? extends PromotionListObject> promotions =  ObjectCache.getPromotions();

        ArrayList<String> accountIds = new ArrayList<>(promotions.keySet());
        PromotionListAdapter adapter = new PromotionListAdapter(this, EditPromotionActivity.class, CommentsActivity.class, accountIds, R.layout.promotion_list_view);
        adapter.setListObjects(promotions);
        adapter.sort();

        listView = findViewById(R.id.promotions_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public void clearRefresh(){
        System.out.println("clear refresh");
        swipeRefreshLayout.setRefreshing(false);
    }
}
