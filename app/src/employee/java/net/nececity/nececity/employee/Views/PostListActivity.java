package net.nececity.nececity.employee.Views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import net.nececity.nececity.Abstracts.PostListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.PostListAdapter;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.CommentsActivity;
import net.nececity.nececity.employee.Managers.PostsManager;

import java.util.ArrayList;
import java.util.HashMap;

public class PostListActivity extends SecureAppCompatActivity {

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static PostsManager postsManager;
    private PostListActivity postListActivity;

    private FloatingActionButton newPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);

        System.out.println(" Post list oncreate");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Posts");
        actionBar.setDisplayHomeAsUpEnabled(true);

        postListActivity = this;
        postsManager = new PostsManager(this);

        newPost = findViewById(R.id.newPost);
        newPost.setVisibility(View.VISIBLE);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);

        postsManager.refreshPosts(postListActivity);

        setListeners();
    }

    public void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        postsManager.refreshPosts(postListActivity);
                    }
                }
        );

        newPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(postListActivity, EditPostActivity.class);
                startActivity(intent);
            }
        });
    }

    public void refreshList(){
        postsManager.refreshPosts(postListActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){
        clearRefresh();

        HashMap<String,? extends PostListObject> posts =  ObjectCache.getPosts();

        ArrayList<String> accountIds = new ArrayList<>(posts.keySet());
        PostListAdapter adapter = new PostListAdapter(this, EditPostActivity.class, CommentsActivity.class, accountIds, R.layout.post_list_view);
        adapter.setListObjects(posts);
        adapter.sort();

        listView = findViewById(R.id.posts_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public void clearRefresh(){
        System.out.println("clear refresh");
        swipeRefreshLayout.setRefreshing(false);
    }
}
