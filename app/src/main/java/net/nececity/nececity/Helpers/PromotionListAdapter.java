package net.nececity.nececity.Helpers;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.nececity.nececity.Abstracts.PromotionListObject;
import net.nececity.nececity.Objects.Promotion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Takunda on 8/4/2018.
 */

public class PromotionListAdapter extends ListAdapter {

    private Activity listActivity;
    private Class listItemClassActivity;
    private Class commentsClassActivity;
    private ArrayList<String> ids = new ArrayList<>();
    private ArrayList<String> originalIds = new ArrayList<>();
    private int layoutResource;

    public PromotionListAdapter(Activity listActivity, Class listItemClassActivity, Class commentsClassActivity, ArrayList<String> ids, int layoutResource) {
        super(listActivity, listItemClassActivity, ids, layoutResource);

        this.listActivity = listActivity;
        this.listItemClassActivity=listItemClassActivity;
        this.commentsClassActivity = commentsClassActivity;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        String objectId = ids.get(position);
        PromotionListObject listObject = (PromotionListObject) mapObjects.get(objectId);

        if (cachedViews.containsKey(objectId)){
            return PromotionListObject.updateData(cachedViews.get(objectId),(Promotion)listObject);
        }

        listObject.setListActivity(listActivity);
        listObject.setListItemClassActivity(listItemClassActivity);
        listObject.setCommentsClassActivity(commentsClassActivity);

        if (listObject == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, null, true);

        listObject.setView(rowView);
        listObject.setAdapter(this);
        listObject.populateData();
        listObject.setListeners();

        if (position == 0 && !listObject.getPromotionObject().isSeen()) {
            listObject.promotionSeen();
        }

        cachedViews.put(objectId,rowView);
        return rowView;
    }

    String day;
    public void sort(){
        this.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                PromotionListObject p1 = (PromotionListObject) mapObjects.get(o1);
                PromotionListObject p2 = (PromotionListObject) mapObjects.get(o2);
                String p1Days = p1.getDays();
                String p2Days = p2.getDays();

                if (day == null) {
                    SimpleDateFormat format = new SimpleDateFormat("EEE");
                    day = format.format(new Date()).toLowerCase();
                }
                System.out.println("current day: "+day);

                if (!p1Days.contains(day) && p2Days.contains(day)){
                    return 1;
                }

                if (!p2Days.contains(day) && p1Days.contains(day)){
                    return -1;
                }

                return 0;
            }
        });
    }
}
