package net.nececity.nececity.Helpers;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.nececity.nececity.Abstracts.CataloguePostListObject;
import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Objects.CataloguePost;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by Takunda on 28/08/2018.
 */

public class CataloguePostListAdapter extends ListAdapter {

    private Activity listActivity;
    private Class listItemClassActivity;
    private Class commentsClassActivity;
    private ArrayList<String> ids = new ArrayList<>();
    private ArrayList<String> originalIds = new ArrayList<>();
    private HashMap<String,View> cachedViews = new HashMap<>();
    private int layoutResource;

    public CataloguePostListAdapter(Activity listActivity, Class listItemClassActivity, Class commentsClassActivity, ArrayList<String> ids, int layoutResource) {
        super(listActivity, listItemClassActivity, ids, layoutResource);
        this.listActivity = listActivity;
        this.listItemClassActivity=listItemClassActivity;
        this.commentsClassActivity = commentsClassActivity;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        String objectId = ids.get(position);
        CataloguePostListObject listObject = (CataloguePostListObject) mapObjects.get(objectId);

        if (cachedViews.containsKey(objectId)){
            return CataloguePostListObject.updateData(cachedViews.get(objectId),(CataloguePost)listObject);
        }

        listObject.setListActivity(listActivity);
        listObject.setListItemClassActivity(listItemClassActivity);
        listObject.setCommentsClassActivity(commentsClassActivity);

        if (listObject == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, null, true);

        listObject.setView(rowView);
        listObject.setAdapter(this);
        listObject.populateData();
        listObject.setListeners();

        cachedViews.put(objectId,rowView);
        return rowView;
    }

    public void sort(){
        this.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                CataloguePostListObject p1 = (CataloguePostListObject) mapObjects.get(o1);
                CataloguePostListObject p2 = (CataloguePostListObject) mapObjects.get(o2);
                if (p1.getDate().before(p2.getDate())){
                    return 1;
                }

                if (p2.getDate().before(p1.getDate())){
                    return -1;
                }

                return 0;
            }
        });
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ids.clear();
        if (charText == null || charText.length() == 0) {
            ids.addAll(originalIds);
        }
        else
        {
            for (String id: originalIds) {
                System.out.println("id: "+id);
                ListObject object = mapObjects.get(id);
                String searchString = object.getSearchString().toLowerCase();
                if (searchString.contains(charText)) {
                    ids.add(id);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void clearCachedViews(){
        cachedViews.clear();
    }
}
