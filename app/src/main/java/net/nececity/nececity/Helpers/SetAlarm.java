package net.nececity.nececity.Helpers;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.AlarmClock;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Takunda Chirema on 2017-04-06.
 */

public class SetAlarm extends BroadcastReceiver {

    private Context context;
    private String message;
    private int minutes;

    int minute, hour, day;
    Calendar cal;

    public SetAlarm(Context context, int minutes, String message){
        this.context = context;
        this.message = message;
        this.minutes = minutes;
    }

    public void startAlarm(){

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            AlarmManager alarmManager= (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5 * 1000,message,null,null);
            System.out.println("alarming soon!");
            return;
        }*/

        cal = new GregorianCalendar();
        cal.setTimeInMillis(System.currentTimeMillis());
        hour = cal.get(Calendar.HOUR_OF_DAY);
        minute = cal.get(Calendar.MINUTE);

        Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);
        i.putExtra(AlarmClock.EXTRA_HOUR, hour);
        i.putExtra(AlarmClock.EXTRA_MINUTES, minute+1);
        i.putExtra(AlarmClock.EXTRA_MESSAGE,message);
        i.putExtra(AlarmClock.EXTRA_LENGTH,minutes);
        i.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
        context.startActivity(i);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

    }
}
