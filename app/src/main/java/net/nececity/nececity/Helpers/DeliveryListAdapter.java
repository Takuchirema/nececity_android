package net.nececity.nececity.Helpers;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.nececity.nececity.Abstracts.DeliveryListObject;
import net.nececity.nececity.Abstracts.EDeliveryStatus;
import net.nececity.nececity.Objects.Delivery;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by Takunda on 5/15/2019.
 */

public class DeliveryListAdapter extends ListAdapter {

    private Activity listActivity;
    private Class listItemClassActivity;
    private ArrayList<String> ids = new ArrayList<>();
    private ArrayList<String> originalIds = new ArrayList<>();
    private HashMap<String,View> cachedViews = new HashMap<>();
    private int layoutResource;

    public DeliveryListAdapter(Activity listActivity, Class listItemClassActivity, ArrayList<String> ids, int layoutResource) {
        super(listActivity, listItemClassActivity, ids, layoutResource);

        this.listActivity = listActivity;
        this.listItemClassActivity=listItemClassActivity;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        String objectId = ids.get(position);
        DeliveryListObject listObject = (DeliveryListObject) mapObjects.get(objectId);

        if (cachedViews.containsKey(objectId)){
            return DeliveryListObject.updateData(cachedViews.get(objectId),(Delivery)listObject);
        }

        listObject.setListActivity(listActivity);
        listObject.setListItemClassActivity(listItemClassActivity);

        if (listObject == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, null, true);

        listObject.setView(rowView);
        listObject.setAdapter(this);
        listObject.populateData();
        listObject.setListeners();

        cachedViews.put(objectId,rowView);
        return rowView;
    }

    public void sort(){
        this.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                DeliveryListObject p1 = (DeliveryListObject) mapObjects.get(o1);
                DeliveryListObject p2 = (DeliveryListObject) mapObjects.get(o2);

                if (((Delivery)p1).getDeliveryStatus() == EDeliveryStatus.INTRANSIT ||
                        ((Delivery)p1).getDeliveryStatus() == EDeliveryStatus.PENDING  ){
                    return 1;
                }else if (((Delivery)p2).getDeliveryStatus() == EDeliveryStatus.INTRANSIT ||
                        ((Delivery)p2).getDeliveryStatus() == EDeliveryStatus.PENDING  ){
                    return -1;
                }

                return 0;
            }
        });
    }
}
