package net.nececity.nececity.Helpers;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.nececity.nececity.Abstracts.PostListObject;
import net.nececity.nececity.Objects.Post;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Takunda on 07/07/2018.
 */

public class PostListAdapter extends ListAdapter {

    private Activity listActivity;
    private Class listItemClassActivity;
    private Class commentsClassActivity;
    private ArrayList<String> ids = new ArrayList<>();
    private ArrayList<String> originalIds = new ArrayList<>();
    private int layoutResource;

    public PostListAdapter(Activity listActivity, Class listItemClassActivity, Class commentsClassActivity, ArrayList<String> ids, int layoutResource) {
        super(listActivity, listItemClassActivity, ids, layoutResource);

        this.listActivity = listActivity;
        this.listItemClassActivity=listItemClassActivity;
        this.commentsClassActivity = commentsClassActivity;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        String objectId = ids.get(position);
        PostListObject listObject = (PostListObject) mapObjects.get(objectId);

        if (cachedViews.containsKey(objectId)){
            return PostListObject.updateData(cachedViews.get(objectId),(Post)listObject);
        }

        listObject.setListActivity(listActivity);
        listObject.setListItemClassActivity(listItemClassActivity);
        listObject.setCommentsClassActivity(commentsClassActivity);

        if (listObject == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, null, true);

        listObject.setView(rowView);
        listObject.setAdapter(this);
        listObject.populateData();
        listObject.setListeners();

        if (position == 0 && !listObject.getPostObject().isSeen()) {
            listObject.postSeen();
        }

        cachedViews.put(objectId,rowView);
        return rowView;
    }

    public void sort(){
        this.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                PostListObject p1 = (PostListObject) mapObjects.get(o1);
                PostListObject p2 = (PostListObject) mapObjects.get(o2);
                if (p1.getDate().before(p2.getDate())){
                    return 1;
                }

                if (p2.getDate().before(p1.getDate())){
                    return -1;
                }

                return 0;
            }
        });
    }
}
