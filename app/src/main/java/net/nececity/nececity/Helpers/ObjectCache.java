package net.nececity.nececity.Helpers;

import net.nececity.nececity.Abstracts.EAmeneties;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Delivery;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Takunda on 02/06/2018.
 */

public class ObjectCache {

    private static HashMap<String,User> users = new HashMap<>();
    private static HashMap<String,Company> companies = new HashMap<>();
    private static HashMap<String,Integer> icons = new HashMap<>();
    private static HashMap<Integer,Delivery> deliveries = new HashMap<>();

    public static void refreshUsers(HashMap<String,User> newUsers){

        if (users.isEmpty()){
            copyUsers(users,newUsers);
            return;
        }

        users.clear();
        for (Map.Entry<String,User> e:newUsers.entrySet()){
            User newUser = e.getValue();
            User oldUser = users.get(newUser.getId());

            if (oldUser != null){
                newUser.setPictureBmp(oldUser.getPictureBmp());
                newUser.setPictureDrawable(oldUser.getPictureDrawable());
            }
            users.put(newUser.getId(),newUser);
        }
    }

    public static void refreshCompanies(HashMap<String,Company> newCompanies){
        System.out.println("refreshing companies - "+newCompanies.size());
        if (companies.isEmpty()){
            //Shallow copying leads to problems. Cache must be stand-alone deep copy.
            copyCompanies(companies,newCompanies);
            return;
        }

        companies.clear();
        for (Map.Entry<String,Company> e:newCompanies.entrySet()){
            Company newCompany = e.getValue();
            Company oldCompany = companies.get(newCompany.getId());

            if (oldCompany != null){
                if (oldCompany.getBmp() != null) {
                    newCompany.setBmp(oldCompany.getBmp());
                }
                newCompany.setEmployees(oldCompany.getEmployees());
                newCompany.setRoutes(oldCompany.getRoutes());
                newCompany.setPosts(oldCompany.getPosts());
                newCompany.setPromotions(oldCompany.getPromotions());
                newCompany.setCatalogue(oldCompany.getCatalogue());
            }
            companies.put(newCompany.getId(),newCompany);
        }
    }

    public static void refreshEmployees(HashMap<String, HashMap<String,Employee>> newEmployees){
        System.out.println("refreshing employees - "+newEmployees.size());

        for (Map.Entry<String,HashMap<String,Employee>> cp: newEmployees.entrySet()){
            HashMap<String,Employee> newCompanyEmployees = cp.getValue();
            String companyId = cp.getKey();

            Company company = companies.get(companyId);
            if (company == null){
                continue;
            }
            HashMap<String,Employee> employees = company.getEmployees();

            if (employees.isEmpty()){
                copyEmployees(employees,newCompanyEmployees);
                continue;
            }

            for (Map.Entry<String,Employee> e:newCompanyEmployees.entrySet()){
                Employee newEmployee = e.getValue();
                Employee oldEmployee = employees.get(newEmployee.getId());

                if (oldEmployee != null && oldEmployee.getPictureUrl() != null && oldEmployee.getPictureUrl().equalsIgnoreCase(newEmployee.getPictureUrl())){
                    newEmployee.setBmp(oldEmployee.getBmp());
                }
            }
            company.setEmployees(newCompanyEmployees);
        }
    }

    public static void refreshPosts(HashMap<String,HashMap<String,Post>> newPosts){

        for (Map.Entry<String,HashMap<String,Post>> cp: newPosts.entrySet()){
            HashMap<String,Post> newCompanyPosts = cp.getValue();
            String companyId = cp.getKey();

            Company company = companies.get(companyId);
            if (company == null){
                continue;
            }
            HashMap<String,Post> posts = company.getPosts();

            if (posts.isEmpty()){
                copyPosts(posts,newCompanyPosts);
                continue;
            }

            for (Map.Entry<String,Post> e:newCompanyPosts.entrySet()){
                Post newPost = e.getValue();
                Post oldPost = posts.get(newPost.getId());

                if (oldPost != null && oldPost.getPictureUrl() != null && oldPost.getPictureUrl().equalsIgnoreCase(newPost.getPictureUrl())){
                    newPost.setBmp(oldPost.getBmp());
                }
            }
            company.setPosts(newCompanyPosts);
        }
    }

    public static void refreshCataloguePosts(HashMap<String,HashMap<String,CataloguePost>> newCataloguePosts){

        for (Map.Entry<String,HashMap<String,CataloguePost>> cp: newCataloguePosts.entrySet()){
            HashMap<String,CataloguePost> newCompanyCataloguePosts = cp.getValue();
            String companyId = cp.getKey();

            Company company = companies.get(companyId);
            if (company == null){
                continue;
            }
            HashMap<String,CataloguePost> cataloguePosts = company.getCatalogue();

            if (cataloguePosts.isEmpty()){
                copyCataloguePosts(cataloguePosts,newCompanyCataloguePosts);
                continue;
            }

            for (Map.Entry<String,CataloguePost> e:newCompanyCataloguePosts.entrySet()){
                CataloguePost newCataloguePost = e.getValue();
                CataloguePost oldCataloguePost = cataloguePosts.get(newCataloguePost.getId());

                if (oldCataloguePost != null && oldCataloguePost.getPictureUrl() != null && oldCataloguePost.getPictureUrl().equalsIgnoreCase(newCataloguePost.getPictureUrl())){
                    newCataloguePost.setBmp(oldCataloguePost.getBmp());
                }
            }
            company.setCatalogue(newCompanyCataloguePosts);
        }
    }

    public static void refreshPromotions(HashMap<String,HashMap<String,Promotion>> newPromotions){

        for (Map.Entry<String,HashMap<String,Promotion>> cp: newPromotions.entrySet()){
            HashMap<String,Promotion> newCompanyPromotions = cp.getValue();
            String companyId = cp.getKey();

            Company company = companies.get(companyId);
            if (company == null){
                continue;
            }
            HashMap<String,Promotion> promotions = company.getPromotions();

            if (promotions.isEmpty()){
                copyPromotions(promotions,newCompanyPromotions);
                continue;
            }

            for (Map.Entry<String,Promotion> e:newCompanyPromotions.entrySet()){
                Promotion newPromotion = e.getValue();
                Promotion oldPromotion = promotions.get(newPromotion.getId());

                if (oldPromotion != null && oldPromotion.getPictureUrl() != null && oldPromotion.getPictureUrl().equalsIgnoreCase(newPromotion.getPictureUrl())){
                    newPromotion.setBmp(oldPromotion.getBmp());
                }
            }
            company.setPromotions(newCompanyPromotions);
        }
    }

    public static Route getRoute(String routeId, String companyId){
        Company company = getCompanies().get(companyId);
        if (company == null){
            return null;
        }
        return company.getRoutes().get(routeId);
    }

    public static void copyCompanies(Map<String, Company> copy,Map<String, Company> original){
        for(Map.Entry<String, Company> entry : original.entrySet()){
            copy.put(entry.getKey(), entry.getValue());
        }
    }

    public static void copyUsers(Map<String, User> copy,Map<String, User> original){
        for(Map.Entry<String, User> entry : original.entrySet()){
            copy.put(entry.getKey(), entry.getValue());
        }
    }

    public static void copyEmployees(Map<String, Employee> copy, Map<String, Employee> original){
        for(Map.Entry<String, Employee> entry : original.entrySet()){
            System.out.println("copying employees "+entry.getKey()+" "+entry.getValue().getName());
            copy.put(entry.getKey(), entry.getValue());
        }
    }

    public static void copyPosts(Map<String, Post> copy, Map<String, Post> original){
        for(Map.Entry<String, Post> entry : original.entrySet()){
            copy.put(entry.getKey(), entry.getValue());
        }
    }

    public static void copyCataloguePosts(Map<String, CataloguePost> copy, Map<String, CataloguePost> original){
        for(Map.Entry<String, CataloguePost> entry : original.entrySet()){
            copy.put(entry.getKey(), entry.getValue());
        }
    }

    public static void copyPromotions(Map<String, Promotion> copy, Map<String, Promotion> original){
        for(Map.Entry<String, Promotion> entry : original.entrySet()){
            copy.put(entry.getKey(), entry.getValue());
        }
    }

    public static HashMap<String,Post> getPosts(){
        HashMap<String,Post> posts = new HashMap<>();
        for (Map.Entry<String ,Company> e:companies.entrySet()){
            posts.putAll(e.getValue().getPosts());
        }
        return posts;
    }

    public static HashMap<String,Promotion> getPromotions(){
        HashMap<String,Promotion> promotions = new HashMap<>();
        for (Map.Entry<String ,Company> e:companies.entrySet()){
            promotions.putAll(e.getValue().getPromotions());
        }
        return promotions;
    }

    public static HashMap<String, User> getUsers() {
        return users;
    }

    public static HashMap<String, Company> getCompanies() { return companies; }

    public static HashMap<String, Integer> getIcons() {
        if (icons.isEmpty()){
            icons.put(EAmeneties.AC.toString(), R.drawable.agriculture);
            icons.put(EAmeneties.GYM.toString(), R.drawable.agriculture);
            icons.put(EAmeneties.PARKING.toString(), R.drawable.agriculture);
            icons.put(EAmeneties.POOL.toString(), R.drawable.agriculture);
            icons.put(EAmeneties.RESTAURANT.toString(), R.drawable.agriculture);
            icons.put(EAmeneties.SPA.toString(), R.drawable.agriculture);
        }
        return icons;
    }

    public static void setIcons(HashMap<String, Integer> icons) {
        ObjectCache.icons = icons;
    }

    public static HashMap<Integer, Delivery> getDeliveries() {
        return deliveries;
    }

    public static void setDeliveries(HashMap<Integer, Delivery> deliveries) {
        ObjectCache.deliveries = deliveries;
    }
}
