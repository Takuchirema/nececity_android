package net.nececity.nececity.Helpers;

import android.view.View;

/**
 * Created by Takunda on 23/06/2018.
 */

public class CustomRunnable implements Runnable {
    private Object object;
    private String id;

    @Override
    public void run() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
