package net.nececity.nececity.Helpers;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.nececity.nececity.Abstracts.IGetServerPicture;

/**
 * Created by Takunda on 8/11/2018.
 */

public class CustomTarget implements Target {

    private IGetServerPicture getServerPicture;
    private boolean cache=true;
    private String pictureUrl;

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        System.out.println("bmp loaded! cache: "+cache);
        getServerPicture.postGetServerPicture(bitmap);
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
        System.out.println("bmp failed! cache: "+cache);
        if (cache){
            cache = false;
            Picasso.with(AppVariables.mainActivity.getContext())
                    .load(pictureUrl)
                    .into(this);
        }else {
            getServerPicture.postGetServerPicture(null);
        }
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {

    }

    public IGetServerPicture getGetServerPicture() {
        return getServerPicture;
    }

    public void setGetServerPicture(IGetServerPicture getServerPicture) {
        this.getServerPicture = getServerPicture;
    }

    public boolean isCache() {
        return cache;
    }

    public void setCache(boolean cache) {
        this.cache = cache;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
