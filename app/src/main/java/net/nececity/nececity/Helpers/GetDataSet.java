package net.nececity.nececity.Helpers;

import android.os.Handler;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;

/**
 * Created by Takunda on 04/07/2018.
 *
 * This class is used when you do not want the calling class to implement IGetDataSet.
 * For example CityMapManager will end up handling too many DataSets which will cause confusion of
 * the dataset we called in the PostGetDataSet.
 */

public class GetDataSet implements IGetDataSet {

    private DataSet dataSet;
    private Handler handler;
    private DataSet resultsDataSet;
    private Runnable resultsRunnable;

    public GetDataSet(DataSet dataset, Handler handler, Runnable resultsRunnable){
        this.dataSet=dataset;
        dataset.setGetDataset(this);

        this.handler=handler;
        this.resultsRunnable=resultsRunnable;
    }

    public void retrieveDataSet(){
        dataSet.getDataSet();
    }

    public DataSet getDataSet(){
        return this.dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        resultsDataSet = dataSet;
        handler.post(resultsRunnable);
    }
}
