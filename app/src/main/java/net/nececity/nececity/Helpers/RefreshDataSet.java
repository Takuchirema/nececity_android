package net.nececity.nececity.Helpers;

import android.os.Handler;
import android.os.Looper;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;

/**
 * Created by Takunda on 09/06/2018.
 */

public class RefreshDataSet extends Thread implements IGetDataSet{

    private boolean stop = true;
    private Long refreshInterval = AppVariables.defaultRefreshInterval;

    private DataSet dataSet;
    private Handler handler;
    private DataSet resultsDataSet;
    private Runnable resultsRunnable;

    public RefreshDataSet(DataSet dataset, Handler handler, Runnable resultsRunnable){
        this.dataSet=dataset;
        dataset.setGetDataset(this);

        this.handler=handler;
        this.resultsRunnable=resultsRunnable;
    }

    public void immediateRefresh(){
        dataSet.getDataSet();
    }

    public void run(){
        stop = false;

        while(!stop){

            if (Looper.myLooper() == Looper.getMainLooper()){
                System.out.println("refresh data main looper!");
            }else{
                System.out.println("refresh data NOT main looper!");
            }

            dataSet.getDataSet();

            try {
                System.out.println("interval "+refreshInterval);
                Thread.sleep(Math.abs(refreshInterval));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void requestStop(){
        stop = true;
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        resultsDataSet = dataSet;
        handler.post(resultsRunnable);
    }

    public DataSet getResultsDataSet() {
        if (resultsDataSet == null){
            return dataSet;
        }
        return resultsDataSet;
    }

    public Long getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(Long refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    public boolean isStop() {
        return stop;
    }
}
