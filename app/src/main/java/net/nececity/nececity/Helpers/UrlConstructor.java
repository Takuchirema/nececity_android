package net.nececity.nececity.Helpers;

/**
 * Created by Takunda Chirema on 2016-12-21.
 */

public class UrlConstructor {

    //private static String homeUrl = "http://192.168.8.102/api/";
    private static String homeUrl = "http://192.168.43.234:8000/api/";
    //private static String homeUrl = "http://35.231.61.201/api/";
    //private static String homeUrl = "http://www.nececity.net/api/";

    public static String getCompaniesUrl(){
        return homeUrl+"companies";
    }

    public static String newCompanyUrl(){
        return homeUrl+"companies";
    }

    public static String editCompanyUrl(){
        return homeUrl+"companies";
    }

    public static String saveCompanyRouteUrl(){
        return homeUrl+"companies/route";
    }

    public static String getCompanyFollowersUrl(String companyID){
        return homeUrl+"companies/"+companyID+"/followers";
    }

    public static String getCompanyDetailsUrl(String companyID){
        return homeUrl+"companies/"+companyID;
    }

    public static String getCompanyPostsUrl(String companyID,String postType){
        return homeUrl+"companies/"+companyID+"/posts/"+postType;
    }

    public static String getEmployeeCompanyPostsUrl(String companyID, String employeeId){
        return homeUrl+"companies/"+companyID+"/employees/"+employeeId+"/posts";
    }

    public static String getCompanyPromotionsUrl(String companyID){
        return homeUrl+"companies/"+companyID+"/promotions";
    }

    public static String getEmployeeCompanyPromotionsUrl(String companyID, String employeeId){
        return homeUrl+"companies/"+companyID+"/employees/"+employeeId+"/promotions";
    }

    public static String deleteCompanyPromotionsUrl(String companyID,String promotionID){
        return homeUrl+"companies/"+companyID+"/promotion/"+promotionID;
    }

    public static String getCompanyUserPostsUrl(String companyID,String userID){
        return homeUrl+"companies/"+companyID+"/user/posts/"+userID;
    }

    public static String getUserPostsUrl(String userID){
        return homeUrl+"companies/user/"+userID+"/posts";
    }

    public static String getCataloguePostsUrl(String companyId){
        return homeUrl+"companies/"+companyId+"/catalogue";
    }

    public static String uploadCatalogueFileUrl(String companyId, String postId){
        return homeUrl+"companies/"+companyId+"/catalogue/"+postId+"/file";
    }

    public static String getEmployeeCataloguePostsUrl(String companyID, String employeeId){
        return homeUrl+"companies/"+companyID+"/employees/"+employeeId+"/catalogue";
    }

    public static String deleteCompanyPostsUrl(String companyID,String postId, String type){
        return homeUrl+"companies/"+companyID+"/post/"+postId+"/type/"+type;
    }

    public static String deleteCataloguePostUrl(String companyID,String postId){
        return homeUrl+"companies/"+companyID+"/catalogue/post/"+postId;
    }

    public static String deleteCatalogueFile(String companyId, String postId){
        return homeUrl+"companies/"+companyId+"/delete/catalogue/"+postId+"/file";
    }

    public static String getCompanyUserPromotionsUrl(String companyID,String userID){
        return homeUrl+"companies/"+companyID+"/user/promotions/"+userID;
    }

    public static String getUserPromotionsUrl(String userID){
        return homeUrl+"companies/user/"+userID+"/promotions";
    }

    public static String getCompanyEmployeePostsUrl(String companyID,String employeeID){
        return homeUrl+"companies/"+companyID+"/employee/posts/"+employeeID;
    }

    public static String seenCompanyUserPostsUrl(String companyID,String userID,String postID){
        return homeUrl+"companies/"+companyID+"/user/"+userID+"/seen/"+postID;
    }

    public static String seenCompanyUserPromotionsUrl(String companyID,String userID,String promotionID){
        return homeUrl+"companies/"+companyID+"/user/"+userID+"/seen/promotion/"+promotionID;
    }

    public static String seenCompanyEmployeePostsUrl(String companyID,String employeeID,String postID){
        return homeUrl+"companies/"+companyID+"/employee/"+employeeID+"/seen/"+postID;
    }

    public static String likeCompanyUserPostsUrl(String companyID,String userID,String postID){
        return homeUrl+"companies/"+companyID+"/user/"+userID+"/post/"+postID+"/like";
    }

    public static String likeCompanyUserCataloguePostsUrl(String companyID,String userID,String postID){
        return homeUrl+"companies/"+companyID+"/user/"+userID+"/catalogue/"+postID+"/like";
    }

    public static String likeCompanyUserPromotionUrl(String companyID,String userID,String promotionID){
        return homeUrl+"companies/"+companyID+"/user/"+userID+"/promotion/"+promotionID+"/like";
    }

    public static String likeCompanyEmployeePostsUrl(String companyID,String employeeID,String postID){
        return homeUrl+"companies/"+companyID+"/employee/"+employeeID+"/post/"+postID+"/like";
    }

    public static String likeCompanyEmployeePromotionUrl(String companyID,String employeeID,String promotionID){
        return homeUrl+"companies/"+companyID+"/employee/"+employeeID+"/promotion/"+promotionID+"/like";
    }

    public static String uploadCompanyPostPicture(String companyID,String postID){
        return homeUrl+"companies/"+companyID+"/posts/picture/"+postID;
    }

    public static String uploadCataloguePostPicture(String companyID,String postID){
        return homeUrl+"companies/"+companyID+"/catalogue/posts/picture/"+postID;
    }

    public static String deleteCompanyFile(String companyID){
        return homeUrl+"companies/"+companyID+"/delete/file";
    }

    public static String uploadCompanyPromotionPicture(String companyID,String promotionID){
        return homeUrl+"companies/"+companyID+"/promotions/picture/"+promotionID;
    }

    public static String uploadCompanyProfilePicture(String companyID){
        return homeUrl+"companies/"+companyID+"/profile/picture";
    }

    public static String getCompanyEmployeesUrl(String companyID){
        return homeUrl+"companies/"+companyID+"/employees";
    }

    public static String getUserEmployeesUrl(String userID){
        return homeUrl+"companies/user/"+userID+"/employees";
    }

    public static String createCompanyPostUrl(String companyID,String postType){
        return homeUrl+"companies/"+companyID+"/posts/"+postType;
    }

    public static String createCataloguePostUrl(String companyID){
        return homeUrl+"companies/"+companyID+"/catalogue/posts";
    }

    public static String createCompanyPromotionUrl(String companyID){
        return homeUrl+"companies/"+companyID+"/promotions";
    }

    public static String setCompanyLocationUrl(String companyID){
        return homeUrl+"companies/"+companyID+"/location";
    }

    public static String blockCompanyFollowerUrl(String companyID,String userID){
        return homeUrl+"companies/"+companyID+"/block/"+userID;
    }

    public static String unblockCompanyFollowerUrl(String companyID,String userID){
        return homeUrl+"companies/"+companyID+"/unblock/"+userID;
    }

    public static String getNewUserUrl(){
        return homeUrl+"users/register";
    }

    public static String newUserUrl(){
        return homeUrl+"users";
    }

    public static String editUserProfileUrl(){
        return homeUrl+"users";
    }

    public static String uploadUserProfilePicture(String userID){
        return homeUrl+"users/"+userID+"/profile/picture";
    }

    public static String editUserSettingsUrl(String userID){
        return homeUrl+"users/"+userID+"/settings";
    }

    public static String getUserSaltUrl(String userID){
        return homeUrl+"users/salt/"+userID;
    }

    public static String getUserLoginUrl(String userID){
        return homeUrl+"users/login/"+userID;
    }

    public static String getNewUserLoginUrl(String userID){
        return homeUrl+"users/newlogin/"+userID;
    }

    public static String getUserDetailsUrl(String userID){
        return homeUrl+"users/"+userID;
    }

    public static String facebookLoginUrl(){
        return homeUrl+"users/fb/login";
    }

    public static String getUserCompaniesUrl(String userID){
        return homeUrl+"users/"+userID+"/companies";
    }

    public static String getAllCompaniesUrl(String userID,String countryCode){
        return homeUrl+"users/"+userID+"/country/"+countryCode+"/companies/all";
    }

    public static String getAllCompaniesUrl(String userID){
        return homeUrl+"users/"+userID+"/companies/all";
    }

    public static String getUserFriendsUrl(String userID){
        return homeUrl+"users/"+userID+"/friends";
    }

    public static String getUserRequestsUrl(String userID){
        return homeUrl+"users/"+userID+"/requests/received";
    }

    public static String getUserRequestedUrl(String userID){
        return homeUrl+"users/"+userID+"/requests/sent";
    }

    public static String getAllUsersUrl(String userID){
        return homeUrl+"users/"+userID+"/all";
    }

    public static String getSearchForUsersUrl(String userID,String searchString){
        return homeUrl+"users/"+userID+"/search/"+searchString;
    }

    public static String getUserMessagesUrl(String userID){
        return homeUrl+"users/"+userID+"/messages";
    }

    public static String sendUserMessageUrl(String userID){
        return homeUrl+"users/"+userID+"/messages";
    }

    public static String seenUserMessageUrl(String userID){
        return homeUrl+"users/"+userID+"/messages/seen";
    }

    public static String setUserAlertUrl(String userID){
        return homeUrl+"users/"+userID+"/alert/on";
    }

    public static String seenUserAlertUrl(String userID,String friendID){
        return homeUrl+"users/"+userID+"/seen/alert/"+friendID;
    }

    public static String cancelUserAlertUrl(String userID){
        return homeUrl+"users/"+userID+"/alert/off";
    }

    public static String setUserLocationVisibilityUrl(String userID){
        return homeUrl+"users/"+userID+"/location/visibility";
    }

    public static String hideUserLocation(String userID,String friendID){
        return homeUrl+"users/"+userID+"/location/hide/"+friendID;
    }

    public static String showUserLocation(String userID,String friendID){
        return homeUrl+"users/"+userID+"/location/show/"+friendID;
    }

    public static String unfriendUrl(String userID,String friendID){
        return homeUrl+"users/"+userID+"/unfriend/"+friendID;
    }

    public static String acceptRequestUrl(String userID,String friendID){
        return homeUrl+"users/"+userID+"/accept/"+friendID;
    }

    public static String rejectRequestUrl(String userID,String friendID){
        return homeUrl+"users/"+userID+"/reject/"+friendID;
    }

    public static String sendRequestUrl(String userID,String friendID){
        return homeUrl+"users/"+userID+"/request/"+friendID;
    }

    public static String cancelRequestUrl(String userID,String friendID){
        return homeUrl+"users/"+userID+"/cancel/"+friendID;
    }

    public static String followCompanyUrl(String userID,String companyID){
        return homeUrl+"users/"+userID+"/follow/"+companyID;
    }

    public static String unfollowCompanyUrl(String userID,String companyID){
        return homeUrl+"users/"+userID+"/unfollow/"+companyID;
    }

    public static String postUserCommentUrl(String userName,String companyID,String postID){
        return homeUrl+"users/"+userName+"/comment/"+postID+"/from/"+companyID;
    }

    public static String postUserPromotionCommentUrl(String userID,String companyID,String promotionID){
        return homeUrl+"users/"+userID+"/promotion/comment/"+promotionID+"/from/"+companyID;
    }

    public static String postUserCatalogueCommentUrl(String userID,String companyID,String postId){
        return homeUrl+"users/"+userID+"/catalogue/comment/"+postId+"/from/"+companyID;
    }
    public static String editEmployeeProfileUrl(){
        return homeUrl+"employees";
    }

    public static String uploadEmployeeProfilePicture(String employeeID,String companyID){
        return homeUrl+"employees/"+employeeID+"/company/"+companyID+"/profile/picture";
    }

    public static String uploadEmployeeLog(String employeeID,String companyID){
        return homeUrl+"employees/"+employeeID+"/company/"+companyID+"/logs";
    }

    public static String editEmployeeSettingsUrl(String employeeID){
        return homeUrl+"employees/"+employeeID+"/settings";
    }

    public static String setCompanyRouteUrl(){
        return homeUrl+"employees/route";
    }

    public static String getCompanyRouteUrl(String companyID,String routeID){
        return homeUrl+"companies/"+companyID+"/route/"+routeID;
    }

    public static String getRoutesUrl(){
        return homeUrl+"companies/routes";
    }

    public static String getEmployeeSaltUrl(String employeeID,String companyID){
        return homeUrl+"employees/salt/"+employeeID+"/company/"+companyID;
    }

    public static String getEmployeeLoginUrl(String employeeID,String companyID){
        return homeUrl+"employees/login/"+employeeID+"/company/"+companyID;
    }

    public static String getEmployeeChangeUrl(String employeeID,String companyID){
        return homeUrl+"employees/change/"+employeeID+"/company/"+companyID;
    }

    public static String getDeviceLoginUrl(){
        return homeUrl+"employees/device/login";
    }

    public static String sendErrorLogUrl(String deviceId){
        return homeUrl+"companies/"+deviceId+"/logs";
    }

    public static String getGpsServiceUrl(){
        return homeUrl+"gps/service";
    }

    public static String getEmployeeDetailsUrl(String employeeID,String companyID){
        return homeUrl+"employees/"+employeeID+"/company/"+companyID;
    }

    public static String getEmployeeFriendsUrl(String employeeID,String companyID){
        return homeUrl+"employees/"+employeeID+"/company/"+companyID+"/friends";
    }

    public static String getEmployeeMessagesUrl(String employeeID,String companyID){
        return homeUrl+"employees/"+employeeID+"/company/"+companyID+"/messages";
    }

    public static String sendEmployeeMessagesUrl(String employeeID,String companyID){
        return homeUrl+"employees/"+employeeID+"/company/"+companyID+"/messages/send";
    }

    public static String seenEmployeeMessageUrl(String employeeID,String companyID){
        return homeUrl+"employees/"+employeeID+"/company/"+companyID+"/messages/seen";
    }

    public static String setEmployeeAlertUrl(String employeeID){
        return homeUrl+"employees/"+employeeID+"/alert/on";
    }

    public static String seenEmployeeAlertUrl(String employeeID,String friendID){
        return homeUrl+"employees/"+employeeID+"/seen/alert/"+friendID;
    }

    public static String seenUserAlertUrl(String employeeID,String companyID,String userID){
        return homeUrl+"employees/"+employeeID+"/company/"+companyID+"/seen/user-alert/"+userID;
    }

    public static String cancelEmployeeAlertUrl(String employeeID,String companyID){
        return homeUrl+"employees/"+employeeID+"/alert/off/"+companyID;
    }

    public static String postEmployeeCommentUrl(String employeeID,String postID){
        return homeUrl+"employees/"+employeeID+"/comment/"+postID;
    }

    public static String postEmployeePromotionCommentUrl(String employeeID,String promotionID){
        return homeUrl+"employees/"+employeeID+"/promotion/comment/"+promotionID;
    }

    public static String getLocationAlerts(String latitude,String longitude){
        return  homeUrl+"alerts/"+latitude+"/"+longitude;
    }

    public static String getAlertingUsers(String latitude,String longitude,String employeeID,String companyID){
        return  homeUrl+"users/alerting/"+latitude+"/"+longitude+"/employee/"+employeeID+"/company/"+companyID;
    }

    public static String getAlerts(){
        return  homeUrl+"alerts";
    }

}
