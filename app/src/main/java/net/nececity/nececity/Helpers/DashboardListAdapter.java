package net.nececity.nececity.Helpers;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import net.nececity.nececity.Abstracts.ListObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Takunda on 11/2/2019.
 */

public class DashboardListAdapter extends ArrayAdapter<String> {

    private Activity listActivity;
    private ArrayList<String> ids = new ArrayList<>();
    private ArrayList<String> originalIds = new ArrayList<>();
    protected HashMap<String,? extends ListObject> listObjects = new HashMap<>();
    protected HashMap<String,View> cachedViews = new HashMap<>();
    private int layoutResource;

    public DashboardListAdapter(Activity listActivity, ArrayList<String> ids, int layoutResource) {
        super(listActivity, layoutResource, ids);

        this.listActivity = listActivity;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        String objectId = ids.get(position);
        ListObject listObject = listObjects.get(objectId);

        if (cachedViews.containsKey(objectId)){
            return cachedViews.get(objectId);
        }

        listObject.setListActivity(listActivity);

        if (listObject == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, null, true);

        listObject.setView(rowView);
        listObject.setAdapter(this);
        listObject.populateData();
        listObject.setListeners();

        cachedViews.put(objectId,rowView);
        return rowView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ids.clear();
        if (charText == null || charText.length() == 0) {
            ids.addAll(originalIds);
        }
        else
        {
            for (String id: originalIds) {
                ListObject object = listObjects.get(id);
                String searchString = object.getSearchString().toLowerCase();
                if (searchString.contains(charText)) {
                    ids.add(id);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setListObjects(HashMap<String,? extends ListObject> listObjects) {
        this.listObjects = listObjects;
    }
}
