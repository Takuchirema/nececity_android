package net.nececity.nececity.Helpers;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;
import net.nececity.nececity.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Takunda on 10/30/2018.
 */

public class StopListAdapter extends ListAdapter {
    private Activity listActivity;
    private int layoutResource;
    private Route route;

    public StopListAdapter(Route route, Activity listActivity, ArrayList<String> ids, int layoutResource) {
        super(listActivity, null, ids, layoutResource);

        this.route=route;
        this.listActivity = listActivity;
        this.ids = ids;
        this.layoutResource=layoutResource;
        setArrivalTimes();
    }

    public void setArrivalTimes(){
        Date nextArrivalTime=null;

        HashMap<String, RoutePoint> busStopPoints =  route.getBusStopPoints();

        for (Map.Entry<String, RoutePoint> e: busStopPoints.entrySet()){
            RoutePoint routePoint = e.getValue();

            String time = routePoint.getNextArrivalTime(nextArrivalTime);
            routePoint.setEstimatedArrivalTime(time);
            try {
                nextArrivalTime = new SimpleDateFormat("HH:mm").parse(time);
            }catch (Exception ex){}
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        String objectId = ids.get(position);
        RoutePoint routePoint = (RoutePoint)getListObject(objectId);

        if (routePoint == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, null, true);

        routePoint.setView(rowView);
        routePoint.populateData();
        prepareView(rowView,routePoint);

        return rowView;
    }

    private void prepareView(View rowView, final RoutePoint routePoint){

        View watchRoute = rowView.findViewById(R.id.watchRoute);
        ((IMapActivity)AppVariables.mainActivity).getMapManager().watchRoutePoint(watchRoute,routePoint);

        if (routePoint.distanceFrom(AppVariables.user.getLocation()) <= AppVariables.busStopProximity){
            ((ImageView) rowView.findViewById(R.id.stopLine)).setImageResource(R.drawable.current_stop_line);
        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((IMapActivity)AppVariables.mainActivity).getMapManager().showStopInformation(routePoint,false);
            }
        });
    }

    public void sort(){
        this.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                RoutePoint p1 = (RoutePoint)getListObject(o1);
                RoutePoint p2 = (RoutePoint)getListObject(o2);
                System.out.println(" comp p1: "+p1.getName()+" - "+p1.getId()+" p2: "+p2.getName()+" - "+p2.getId());
                int id1 = Integer.parseInt(p1.getId());
                int id2 = Integer.parseInt(p2.getId());

                if (id1 < id2){
                    return 1;
                }

                if (id1 > id2){
                    return -1;
                }

                return 0;
            }
        });
    }
}
