package net.nececity.nececity.Helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;

public class RescaleBitmap {
	
	private int maxWidth,maxHeight;
	private Context context;

	public RescaleBitmap(Context context, int width, int height){
		this.context = context;
		resize(width,height);
	}

	/* This method checks the screen density of the phone and specifies MaxWidth and Height based
	* this so as to fit image on smaller screens.*/
	public void resize(int width,int height){

		float densityFactor = ((Activity)context).getResources().getDisplayMetrics().density;
		System.out.println("density is "+densityFactor);
		if(densityFactor < 2) {  // 2 -> xhdpi
			width = Math.round(width / (densityFactor + (2 - densityFactor)) );
			height = Math.round(height / (densityFactor  + (2 - densityFactor)) );
		}

		this.maxWidth = width;
		this.maxHeight = height;
	}

	public Bitmap CropBitmap(Bitmap bmp){

		Bitmap bitmap = bmp;
		try {
			int dimension = getSquareCropDimensionForBitmap(bmp);
			bitmap = ThumbnailUtils.extractThumbnail(bmp, dimension, dimension);
		} catch (OutOfMemoryError E) {
			EditImage editImage = new EditImage(context,"");
			bmp = editImage.compressImage(bitmap);

			int dimension = getSquareCropDimensionForBitmap(bmp);
			bitmap = ThumbnailUtils.extractThumbnail(bmp, dimension, dimension);
		}

		bitmap = resize(bitmap);
		
		bitmap = getCircleBitmap(bitmap);
	
		return bitmap;
	}
	
	public int getSquareCropDimensionForBitmap(Bitmap bitmap)
	{
		int dimension = 0;
	    //If the bitmap is wider than it is tall
	    //use the height as the square crop dimension
	    if (bitmap.getWidth() >= bitmap.getHeight())
	    {
	        dimension = bitmap.getHeight();
	    }
	    //If the bitmap is taller than it is wide
	    //use the width as the square crop dimension
	    else
	    {
	        dimension = bitmap.getWidth();
	    } 
	    
	    return dimension;
	}
	
	public Bitmap resize(Bitmap image) {

		try {
			int width = image.getWidth();
			int height = image.getHeight();
		} catch (OutOfMemoryError E) {
			EditImage editImage = new EditImage(context,"");
			image = editImage.compressImage(image);
		}

	    if (maxHeight > 0 && maxWidth > 0) {
	        int width = image.getWidth();
	        int height = image.getHeight();
	        float ratioBitmap = (float) width / (float) height;
	        float ratioMax = (float) maxWidth / (float) maxHeight;

	        int finalWidth = maxWidth;
	        int finalHeight = maxHeight;
	        if (ratioMax > 1) {
	            finalWidth = (int) ((float)maxHeight * ratioBitmap);
	        } else {
	            finalHeight = (int) ((float)maxWidth / ratioBitmap);
	        }
	        image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
	        return image;
	    } else {
	        return image;
	    }
	}
	
	public Bitmap drawableToBitmap (Drawable drawable) {
	    Bitmap bitmap = null;

	    if (drawable instanceof BitmapDrawable) {
	        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
	        if(bitmapDrawable.getBitmap() != null) {
	            return bitmapDrawable.getBitmap();
	        }
	    }

	    if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
	        bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
	    } else {
	        bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
	    }

	    Canvas canvas = new Canvas(bitmap);
	    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
	    drawable.draw(canvas);
	    return bitmap;
	}
	
	public Bitmap getCircleBitmap(Bitmap bitmap) {
		Bitmap output = bitmap;
		try {
			output = Bitmap.createBitmap(bitmap.getWidth(),
					bitmap.getHeight(), Config.ARGB_8888);
		} catch (OutOfMemoryError E) {
			EditImage editImage = new EditImage(context,"");
			output = editImage.compressImage(bitmap);
		}

	    Canvas canvas = new Canvas(output);

	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	    canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
	            bitmap.getWidth() / 2, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(bitmap, rect, rect, paint);
	    //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
	    //return _bmp;
	    return output;
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {

		Bitmap output = bitmap;
		try {
			output = Bitmap.createBitmap(bitmap.getWidth(),
					bitmap.getHeight(), Config.ARGB_8888);
		} catch (OutOfMemoryError E) {
		}

		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		System.out.println("width of bitmap "+bitmap.getWidth());
		final float roundPx = (pixels*bitmap.getWidth())/500;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		//draw corners on parts not with radius
		canvas.drawRect(0, bitmap.getHeight()/2, bitmap.getWidth()/2, bitmap.getHeight(), paint);
		canvas.drawRect(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth(), bitmap.getHeight(), paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}
	
	public Bitmap addBorder(Bitmap bmp, int borderSize, String colour) {

		Bitmap compressedBmp;

		try {
			compressedBmp = Bitmap.createBitmap(bmp.getWidth(),
					bmp.getHeight(), Config.ARGB_8888);
		} catch (OutOfMemoryError E) {
			EditImage editImage = new EditImage(context,"");
			compressedBmp = editImage.compressImage(bmp);
		}

		Bitmap bmpWithBorder = Bitmap.createBitmap(compressedBmp.getWidth() + borderSize * 2,
				compressedBmp.getHeight()+ borderSize * 2, compressedBmp.getConfig());
	    
		int color = Color.parseColor(colour);
	    Canvas canvas = new Canvas(bmpWithBorder);
	    final Paint paint = new Paint();
	    
	    paint.setAntiAlias(true);
	    
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    
	    canvas.drawCircle(bmpWithBorder.getWidth() / 2, bmpWithBorder.getHeight() / 2,
	    		bmpWithBorder.getWidth() / 2, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    //canvas.drawColor(colour);
	    canvas.drawBitmap(bmp, borderSize, borderSize, null);
	    return bmpWithBorder;
	}

	public Bitmap addCircle(Bitmap bmp, int borderSize, String colour) {

		Bitmap compressedBmp = bmp;

		try {
			compressedBmp = Bitmap.createBitmap(bmp.getWidth(),
					bmp.getHeight(), Config.ARGB_8888);
		} catch (OutOfMemoryError E) {
			EditImage editImage = new EditImage(context,"");
			compressedBmp = editImage.compressImage(bmp);
		}

		Bitmap bmpWithBorder = Bitmap.createBitmap(compressedBmp.getWidth() + borderSize * 2,
				compressedBmp.getHeight()+ borderSize * 2, compressedBmp.getConfig());

		int color = Color.parseColor(colour);
		Canvas canvas = new Canvas(bmpWithBorder);
		final Paint paint = new Paint();

		paint.setAntiAlias(true);

		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);

		canvas.drawCircle(bmpWithBorder.getWidth() / 2, (bmpWithBorder.getHeight() - 20) / 2,
				(bmpWithBorder.getWidth() - 20) / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		//canvas.drawColor(colour);
		canvas.drawBitmap(bmp, borderSize, borderSize, null);
		return bmpWithBorder;
	}

	/* Get a shape defined xml resource into bitmap*/
	public static Bitmap getBitmap(Context context, int drawableRes, int width, int height) {
		Drawable drawable = context.getResources().getDrawable(drawableRes);
		Canvas canvas = new Canvas();
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		canvas.setBitmap(bitmap);
		drawable.setBounds(0, 0, width, height);
		drawable.draw(canvas);

		return bitmap;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public int getMaxWidth() {
		return maxWidth;
	}

	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}
}
