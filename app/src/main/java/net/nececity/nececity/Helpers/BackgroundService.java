package net.nececity.nececity.Helpers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.ENotificationType;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.LocationResult;
import net.nececity.nececity.Data.PostsDataSet;
import net.nececity.nececity.Data.RouteDataSet;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.Managers.LocationManager;
import net.nececity.nececity.Managers.NotificationManager;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.user.Views.NeceCityMapActivity;

import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by Takunda on 3/19/2019.
 */

public class BackgroundService extends Service implements IGetDataSet{

    Timer timer;
    TimerTask timerTask;
    RefreshDataSet refreshPostsDataSet;
    NotificationManager notificationManager;
    Route enRoute;
    LocationManager locationManager;
    LocationHandler myLocationHandler;
    static NotificationAppVariables appVariables;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        System.out.println("Background Service onStartCommand");
        notificationManager = new NotificationManager(this);
        createNotificationAppVariables(intent);
        startTimer();
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        //createNotificationAppVariables();
        System.out.println("Background Service onCreate");
    }

    public void createNotificationAppVariables(Intent intent){
        appVariables = new NotificationAppVariables();
        Bundle bundle = intent.getExtras();
        String companyId = bundle.getString("companyId");
        Serializable enRoute = bundle.getSerializable("enRoute");
        String routeId = bundle.getString("routeId");
        String routeCompanyId = bundle.getString("routeCompanyId");

        if (companyId != null) {
            appVariables.company = new Company(companyId);
            appVariables.user = new Employee(bundle.getString("userName"));
            ((Employee)appVariables.user).setCompanyId(companyId);
        }else{
            appVariables.user = new User(bundle.getString("userName"));
        }

        if (enRoute != null){
            appVariables.enRoute = (Route)enRoute;
            appVariables.routeId=appVariables.enRoute.getId();
            appVariables.routeCompanyId=appVariables.enRoute.getCompanyId();
        }else if (routeId != null && routeCompanyId != null){
            appVariables.routeId=routeId;
            appVariables.routeCompanyId=routeCompanyId;
        }

        System.out.println("Background Service create app var - userId: "+intent.getStringExtra("userId"));
        appVariables.user.setId(bundle.getString("userId"));
        appVariables.countryCode = bundle.getString("countryCode");
        appVariables.token = bundle.getString("token");
        appVariables.versionCode=bundle.getInt("versionCode",0);
        appVariables.versionName=bundle.getString("versionName");
        appVariables.versionSuffix=bundle.getString("versionSuffix");
    }

    public void putIntentExtras(Intent intent){
        Bundle extras = new Bundle();

        if (appVariables.company != null) {
            extras.putString("companyId",appVariables.company.getId());
        }

        if (appVariables.routeId != null && appVariables.routeCompanyId != null){
            extras.putSerializable("routeId",appVariables.routeId);
            extras.putSerializable("routeCompanyId",appVariables.routeCompanyId);
        }

        System.out.println("Background Service put intent extras - userId: "+appVariables.user.getId());
        extras.putString("userName",appVariables.user.getName());
        extras.putString("userId",appVariables.user.getId());
        extras.putString("countryCode",appVariables.countryCode);
        extras.putString("token",appVariables.token);
        extras.putInt("versionCode",appVariables.versionCode);
        extras.putString("versionName",appVariables.versionName);
        extras.putString("versionSuffix",appVariables.versionSuffix);

        intent.putExtras(extras);
    }

    public void createAppVariables(){
        if (appVariables.company != null) {
            AppVariables.company = new Company(appVariables.company.getId());
        }

        if (appVariables.user instanceof Employee){
            AppVariables.user = new Employee(appVariables.user.getName());
            ((Employee)AppVariables.user).setCompanyId(((Employee) appVariables.user).getCompanyId());
        }else{
            AppVariables.user = new User(appVariables.user.getName());
        }

        System.out.println("Background Service creating app variables - userId: "+appVariables.user.getId());

        AppVariables.context = this;
        AppVariables.user.setId(appVariables.user.getId());
        AppVariables.countryCode = appVariables.countryCode;
        AppVariables.token = appVariables.token;
        AppVariables.versionCode=appVariables.versionCode;
        AppVariables.versionName=appVariables.versionName;
        AppVariables.versionSuffix=appVariables.versionSuffix;
        AppVariables.enRoute=appVariables.enRoute;

        if (appVariables.routeId != null && appVariables.routeCompanyId != null){
            if (appVariables.enRoute == null){
                RouteDataSet routeDataSet = new RouteDataSet(this, appVariables.routeCompanyId, appVariables.routeId);
                routeDataSet.getDataSet();
            }
            monitorLocation();
        }
    }

    public void monitorLocation() {
        System.out.println("Background Service monitor location");
        LocationResult locationResult = new LocationResult(){
            @Override
            public void gotLocation(final Location location){
                if (locationManager == null){
                    locationManager = new LocationManager(location);
                }
                System.out.println("Background Service location changed");
                if (AppVariables.enRoute != null && !CityMapManager.isEnRoute(AppVariables.enRoute)) {
                    System.out.println("Background Service location monitor stopped");
                    stopLocationMonitor();
                }
                locationManager.onLocationChanged(null,location);
            }
        };

        if (appVariables.routeId == null || appVariables.routeCompanyId == null){
            return;
        }

        myLocationHandler = new LocationHandler();
        myLocationHandler.getLocation(this, locationResult);

        if (AppVariables.user.getLocation() == null){
            AppVariables.user.setLocation(LocationHandler.getLastLocation());
        }
    }

    public void stopLocationMonitor(){
        if (myLocationHandler != null) {
            myLocationHandler.stopLocationUpdates();
        }
    }

    @Override
    public void onDestroy() {
        System.out.println("Background Service onDestroy");
        stopTimertask();
        stopLocationMonitor();
        stopPostsRefreshDataSets();
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        System.out.println("Background Service onTaskRemoved");
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        putIntentExtras(restartServiceIntent);
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(this.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 0, TimeUnit.MINUTES.toMillis(AppVariables.serviceNotificationRefreshInterval)); //
        //timer.schedule(timerTask, 5000,1000); //
    }

    public void stopTimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        if (AppVariables.mainActivity == null){
                            createAppVariables();
                            System.out.println("Background Service onCreate app stopped!");
                            startPostsRefreshDataSets();
                            stopTimertask();
                        }else{
                            System.out.println("Background Service onCreate app still running");
                        }
                    }
                });
            }
        };
    }

    final Runnable updatePostsResultsRunnable = new Runnable() {
        public void run() {
            PostsDataSet postsDataSet = (PostsDataSet) refreshPostsDataSet.getResultsDataSet();
            if (postsDataSet.success) {
                System.out.println("Background Service posts obtained");
                if (postsDataSet.unseenPosts()){
                    System.out.println("Background Service posts unseen");
                    notifyPost(postsDataSet.getLatestPost());
                }
            }else{
                System.out.println("Background Service posts failure: "+postsDataSet.message);
            }
        }
    };

    public void notifyPost(Post post){
        String companyId = post.getCompanyId();
        String title = "New post from "+post.getCompanyId();
        String message = post.getPost();
        ENotificationType notificationType = ENotificationType.POST;

        Intent intent = new Intent(this,NeceCityMapActivity.class);
        notificationManager.notify(companyId,title,message,notificationType,intent);
    }

    private void startPostsRefreshDataSets(){
        if (refreshPostsDataSet != null && !refreshPostsDataSet.isStop()){
            return;
        }

        PostsDataSet postsDataSet = new PostsDataSet(null);
        refreshPostsDataSet = new RefreshDataSet(postsDataSet,handler,updatePostsResultsRunnable);
        refreshPostsDataSet.setRefreshInterval(TimeUnit.MINUTES.toMillis(AppVariables.serviceNotificationRefreshInterval));
        refreshPostsDataSet.start();
    }

    private void stopPostsRefreshDataSets(){
        if (refreshPostsDataSet != null && !refreshPostsDataSet.isStop()){
            refreshPostsDataSet.requestStop();
        }
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        if (!dataSet.success){
            return;
        }

        if (dataSet instanceof RouteDataSet){
            RouteDataSet routeDataSet = (RouteDataSet)dataSet;
            appVariables.enRoute = routeDataSet.getRoute();
            if (AppVariables.enRoute == null){
                AppVariables.enRoute = routeDataSet.getRoute();
            }
        }
    }

    private class NotificationAppVariables{
        String token;
        String versionName;
        String versionSuffix;
        int versionCode;
        User user;
        Company company;
        String countryCode;
        Route enRoute;
        String routeId;
        String routeCompanyId;
    }
}
