package net.nececity.nececity.Helpers;

import android.app.Activity;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import net.nececity.nececity.Abstracts.ListObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Takunda on 21/06/2018.
 */

public class ListAdapter extends ArrayAdapter<String> {

    protected Activity listActivity;
    protected Class listItemClassActivity;
    protected ArrayList<String> ids = new ArrayList<>();
    protected ArrayList<String> originalIds = new ArrayList<>();
    protected HashMap<String,? extends ListObject> mapObjects = new HashMap<>();
    protected ArrayList<ListObject> listObjects = new ArrayList<>();
    protected HashMap<String,View> cachedViews = new HashMap<>();
    private int layoutResource;

    public ListAdapter(Activity listActivity, Class listItemClassActivity, ArrayList<String> ids, int layoutResource) {
        super(listActivity, layoutResource, ids);

        this.listActivity = listActivity;
        this.listItemClassActivity=listItemClassActivity;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @Override
    public int getCount(){
        return ids.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        //System.out.println("adapter get view pos: "+position+" s: "+ids.size());
        String objectId = ids.get(position);

        ListObject listObject = getListObject(objectId);
        listObject.setListActivity(listActivity);
        listObject.setListItemClassActivity(listItemClassActivity);

        if (view != null){
            prepareView(view,listObject);
            return view;
        }

        if (listObject == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, null, true);

        prepareView(rowView,listObject);

        cachedViews.put(objectId,rowView);
        return rowView;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private View prepareView(View view, ListObject listObject){
        listObject.setView(view);
        listObject.setAdapter(this);
        listObject.populateData();
        listObject.setListeners();
        return view;
    }

    protected ListObject getListObject(String objectId){
        ListObject listObject;
        if (!mapObjects.isEmpty()) {
            listObject = mapObjects.get(objectId);
        }else{
            listObject = listObjects.get(Integer.parseInt(objectId));
        }

        return listObject;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ids.clear();
        if (charText == null || charText.length() == 0) {
            ids.addAll(originalIds);
        }
        else
        {
            for (String id: originalIds) {
                ListObject object = getListObject(id);
                String searchString = object.getSearchString().toLowerCase();
                if (searchString.contains(charText)) {
                    ids.add(id);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setIds(ArrayList<String> ids) {
        originalIds.clear();
        cachedViews.clear();

        this.ids = ids;
        originalIds.addAll(ids);
    }

    public void setListObjects(ArrayList<ListObject> listObjects) {
        this.listObjects = listObjects;
    }

    public void setMapObjects(HashMap<String,? extends ListObject> mapObjects) {
        this.mapObjects = mapObjects;
    }
}
