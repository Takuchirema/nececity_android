package net.nececity.nececity.Helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.DateUtils;

import net.nececity.nececity.Abstracts.IMainActivity;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by Takunda on 23/05/2018.
 */

public class AppVariables {
    public static String appLink =
            "Get all your essential services, transport, restaurants and more! " +
            "Install Now! https://play.google.com/store/apps/details?id=net.nececity.nececity.user";

    public static String token;
    public static ArrayList<Integer> supportedVersions = new ArrayList<>();
    public static String versionName;
    public static String versionSuffix;
    public static String enterpriseId;
    public static int versionCode;
    public static User user;
    public static Route enRoute;
    public static LinkedHashMap<String,Route> interestRoutes = new LinkedHashMap<>();
    public static Company company;
    public static String countryCode;

    // For logging out
    public static IMainActivity mainActivity;
    public static Context context;

    // Error Log name
    public static String errorLogName = "Error_Log.txt";

    // Map variables
    public static int zoomLevel = 15;
    public static int distanceUpdateInterval = 50; //meters
    public static int timeUpdateInterval = 0; //milliseconds
    public static float getDetailsDistance = 5000; //metres.
    public static int alarmProximity = 2000; //metres
    public static int nearbyProximity = 1000; //meters
    public static int busStopProximity=100; //metres

    public static Long defaultRefreshInterval = new Long(30000);//milliseconds

    // Default refresh intervals for Employee App
    public static Long employeeRefreshInterval = new Long(7200);// in seconds
    public static Long companyRefreshInterval = new Long(7200); // in seconds

    // Distance for changing location
    public static int employeeChangeLocationDistance = 20;
    public static int userChangeLocationDistance = 20;

    public static int employeeSendLocationDistance = 100;
    public static int userSendLocationDistance = 100;

    public static int speedForLocationUpdate = 0;// For now so people see something. 10000; // 10km an hour
    public static int speedLimitForLocationUpdate = 300000; // 300km an hour
    public static int speedMinForLocationUpdate = 20000; // 20km an hour

    public static int serviceNotificationRefreshInterval = 10; //10 mins

    // Radius to log time for a bus-stop
    public static int logTimeRadius = 100;

    public static String timeFormat = "yyyy-MM-dd_HH:mm";
    public static String fullTimeFormat = "yyyy-MM-dd_HH:mm:ss";

    public static String getDeviceId(Activity context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    public static String setVersionName(Activity context) throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        String version = pInfo.versionName;
        versionName=version;
        return version;
    }

    public static int setVersionCode(Activity context) throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        int version = pInfo.versionCode;
        versionCode = version;
        return version;
    }

    public static String setVersionNameSuffix(Activity context) throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        String version = pInfo.versionName;
        String suffix = version.substring(version.indexOf("-")+1);
        versionSuffix = suffix;
        return suffix;
    }

    public static String setEnterpriseId(Activity context) throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        String version = pInfo.versionName;
        String id = version.substring(version.lastIndexOf(".")+1);
        enterpriseId = id;
        return id;
    }

    public static String getCountryCode(){
        if (countryCode != null && !countryCode.trim().isEmpty()){
            return countryCode;
        }

        TelephonyManager tm;
        if (mainActivity == null){
            tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        }else{
            tm = (TelephonyManager)mainActivity.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        }

        String countryCodeValue = tm.getNetworkCountryIso();
        if (countryCodeValue == null || countryCodeValue.isEmpty()){
            return null;
        }
        countryCode = countryCodeValue;
        return countryCodeValue;
    }

    public static String getVersionNameSuffix(){
        return versionSuffix;
    }

    public static String getEnterpriseId(){
        System.out.println("** enterprise "+enterpriseId+" **");
        return enterpriseId;
    }

    public static Employee getEmployee(){
        return ((Employee)AppVariables.user);
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatTime(String time){

        if (time == null){
            return null;
        }

        String formatted="";
        // SimpleDateFormat can be used to control the date/time display format:
        //   E (day of week): 3E or fewer (in text xxx), >3E (in full text)
        //   M (month): M (in number), MM (in number with leading zero)
        //              3M: (in text xxx), >3M: (in full text full)
        //   h (hour): h, hh (with leading zero)
        //   m (minute)
        //   s (second)
        //   a (AM/PM)
        //   H (hour in 0 to 23)
        //   z (time zone)

        SimpleDateFormat ft = new SimpleDateFormat (timeFormat);

        Date date;
        try {
            date = ft.parse(time);
            long now = System.currentTimeMillis();
            formatted = (DateUtils.getRelativeTimeSpanString(date.getTime(), now, DateUtils.MINUTE_IN_MILLIS)).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("formatted date "+formatted);
        return formatted;
    }
}