package net.nececity.nececity.Abstracts;

import android.content.Context;

import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;

import java.util.HashMap;

/**
 * Created by Takunda on 7/28/2018.
 */

public interface IMainActivity {
    void logout();

    void deprecatedVersion();

    void showMessage(String message);

    void setAppCodes();

    void updateSettings(String key);

    void unseenPosts(boolean unseen,Post latestPost);

    void promotionsToday(HashMap<String,Promotion> promotionsToday);

    void startBackgroundService();

    void setTutorial();

    Context getContext();
}
