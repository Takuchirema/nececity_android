package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 02/07/2018.
 */

public enum ESettingKeys {
    MAP_REFRESH_INTERVAL{
        public String toString() {
            return "map_refresh_interval";
        }
    },
    POSTS_REFRESH_INTERVAL{
        public String toString() {
            return "posts_refresh_interval";
        }
    },
    PROMOTIONS_REFRESH_INTERVAL{
        public String toString() {
            return "promotions_refresh_interval";
        }
    },
    COMPANY_REFRESH_INTERVAL{
        public String toString() {
            return "company_refresh_interval";
        }
    },
    HIDE_INACTIVITY{
        public String toString() {
            return "hide_inactivity";
        }
    },
    SHOW_ROUTE_CLUSTERS{
        public String toString() {
            return "show_route_clusters";
        }
    },
    SHOW_TUTORIAL{
        public String toString() {
            return "show_tutorial";
        }
    },
    LOGOUT{
        public String toString() {
            return "logout";
        }
    }
}
