package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 9/12/2018.
 */

public enum ECompanyType {
    PUBLIC{
        public String toString() {
            return "public";
        }
    },
    PRIVATE{
        public String toString() {
            return "private";
        }
    }
}
