package net.nececity.nececity.Abstracts;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ms.square.android.expandabletextview.ExpandableTextView;

import net.nececity.nececity.Data.ActionDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.R;
import net.nececity.nececity.Managers.ViewManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Takunda on 07/07/2018.
 */

public abstract class PostListObject extends ListObject implements IGetDataSet {

    protected View view;
    protected ArrayAdapter<String> adapter;

    protected ImageView likeIV;
    protected ImageView commentIV;
    protected EditText commentET;
    protected ImageButton postCommentIB;
    protected TextView postText;
    protected TextView viewLikes;

    protected Object postObject;

    public Class commentsClassActivity;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void populateData(){
        super.populateData();

        likeIV = view.findViewById(R.id.like);
        commentIV = view.findViewById(R.id.comment);
        commentET = view.findViewById(R.id.commentText);
        postCommentIB = view.findViewById(R.id.postComment);
        postText = view.findViewById(R.id.expandable_text);
        viewLikes = view.findViewById(R.id.viewLikes);

        ((TextView)view.findViewById(R.id.secondaryTV)).setText(((Post)this).getTime());
        ExpandableTextView expTv1 = view.findViewById(R.id.expand_text_view);
        expTv1.setText(((Post)this).getPost());
        viewLikes.setText(((Post)this).getLikes()+" likes");

        setPostListObjectPicture();
    }

    public static View updateData(View view, Post post){
        TextView viewLikes = view.findViewById(R.id.viewLikes);

        ((TextView)view.findViewById(R.id.secondaryTV)).setText(post.getTime());
        ExpandableTextView expTv1 = view.findViewById(R.id.expand_text_view);
        expTv1.setText(post.getPost());
        viewLikes.setText(post.getLikes()+" likes");

        return view;
    }

    @Override
    public void setListeners(){
        super.setSecondaryView();

        switch (listObjectType){
            case POST:
                likePost();
                commentOnPost();
                editPost();
                viewComments();
                break;
        }
    }

    public Date getDate(){
        Post post = (Post)this;
        return post.getDate();
    }

    public Post getPostObject(){
        Post post = (Post)this;
        return post;
    }

    public void likePost(){
        final String companyId  = getCompany().getId();
        final String postId = ((Post)this).getId();
        final Post post = (Post)this;

        if (post.getLiked()){
            likeIV.setImageResource(R.drawable.like_yellow);
            return;
        }

        likeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = UrlConstructor.likeCompanyUserPostsUrl(companyId,AppVariables.user.getId(),postId);
                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.LIKE_POST;

                ViewManager viewManager = new ViewManager(likeIV,actionType,listActivity);
                viewManager.setViewActivity(listActivity);
                viewManager.setAdapter(adapter);
                viewManager.setObject(post);
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.likePost();
            }
        });
    }

    private void commentOnPost(){
        final String companyId  = getCompany().getId();
        final String postId = ((Post)this).getId();
        final Post post = (Post)this;

        postCommentIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = commentET.getText().toString();
                commentET.setText("");
                if (comment.isEmpty()){
                    return;
                }

                String url = UrlConstructor.postUserCommentUrl(AppVariables.user.getName(), companyId, postId);
                if (AppVariables.user instanceof Employee) {
                    url = UrlConstructor.postUserCommentUrl(EPrivilege.ADMIN.toString(), companyId, postId);
                }

                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.COMMENT_ON_POST;

                ViewManager viewManager = new ViewManager(null,actionType,listActivity);
                viewManager.setObject(post);
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.commentOnPost(comment);
            }
        });
    }

    public void viewComments(){
        final Post post = (Post)this;

        commentIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listObjectType){
                    case POST:
                        Intent intent = new Intent(listActivity, commentsClassActivity);
                        intent.putExtra("postId",post.getId());
                        intent.putExtra("companyId",post.getCompanyId());
                        listActivity.startActivity(intent);
                        break;
                }
            }
        });
    }

    public void editPost(){
        final Post post = (Post)this;

        if (!(AppVariables.user instanceof Employee)){
            return;
        }

        listViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listObjectType) {
                    case POST:
                        Intent intent = new Intent(listActivity, listItemClassActivity);
                        intent.putExtra("postId", post.getId());
                        intent.putExtra("companyId", post.getCompanyId());
                        listActivity.startActivity(intent);
                        break;
                }
            }
        });


    }

    public void postSeen(){
        Post post = (Post)this;

        String url = UrlConstructor.seenCompanyUserPostsUrl(post.getCompanyId(),AppVariables.user.getId(),post.getId());
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("postId", post.getId()));

        ActionDataSet actionDataSet = new ActionDataSet(this,EActionType.POST_SEEN);
        actionDataSet.setUrl(url);
        actionDataSet.setParams(params);
        actionDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet){
        if (dataSet.success){
            ((Post)this).setSeen("true");
            //NeceCityMapActivity.unseenPosts(false);
        }
    }

    @Override
    public void setView(View view){
        super.setView(view);
        this.view=view;
    }

    @Override
    public String getMainText(){
        return this.getCompany().getMainText();
    }

    @Override
    public String getSecondaryText(){
        return this.getCompany().getSecondaryText();
    }

    public ArrayAdapter<String> getAdapter() {
        return adapter;
    }

    public void setAdapter(ArrayAdapter<String> adapter) {
        super.setAdapter(adapter);
        this.adapter = adapter;
    }

    public Class getCommentsClassActivity() {
        return commentsClassActivity;
    }

    public void setCommentsClassActivity(Class commentsClassActivity) {
        this.commentsClassActivity = commentsClassActivity;
    }
}
