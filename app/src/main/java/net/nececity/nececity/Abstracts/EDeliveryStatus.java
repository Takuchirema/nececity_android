package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 5/15/2019.
 */

public enum EDeliveryStatus {
    PENDING{
        public String toString() {
            return "Pending";
        }
    },
    INTRANSIT{
        public String toString() {
            return "In Transit";
        }
    },
    DELIVERED{
        public String toString() {
            return "Delivered";
        }
    },
    CANCELLED{
        public String toString() {
            return "Cancelled";
        }
    }
}
