package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 8/28/2018.
 */

public enum EAmeneties {
    WIFI{
        public String toString() {
            return "Wifi";
        }
    },
    POOL{
        public String toString() {
            return "Pool";
        }
    },
    PETS{
        public String toString() {
            return "Pets";
        }
    },
    PARKING{
        public String toString() {
            return "Parking";
        }
    },
    SPA{
        public String toString() {
            return "Spa";
        }
    },
    GYM{
        public String toString() {
            return "Gym";
        }
    },
    AC{
        public String toString() {
            return "AC";
        }
    },
    RESTAURANT{
        public String toString() {
            return "Restaurant";
        }
    }
}
