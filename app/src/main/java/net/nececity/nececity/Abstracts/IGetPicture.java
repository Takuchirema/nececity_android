package net.nececity.nececity.Abstracts;

import android.graphics.Bitmap;

/**
 * Created by Takunda on 23/06/2018.
 */

public interface IGetPicture {

    void postGetPicture(Bitmap bmp);
}
