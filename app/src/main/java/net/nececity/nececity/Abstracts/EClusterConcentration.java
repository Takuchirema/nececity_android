package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 4/12/2019.
 */

public enum EClusterConcentration {
    LOW{
        public String toString() {
            return "low";
        }
    },
    MEDIUM{
        public String toString() {
            return "medium";
        }
    },
    HIGH{
        public String toString() {
            return "high";
        }
    }
}
