package net.nececity.nececity.Abstracts;

import android.net.Uri;

/**
 * Created by Takunda on 12/29/2018.
 */

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Uri uri);
}
