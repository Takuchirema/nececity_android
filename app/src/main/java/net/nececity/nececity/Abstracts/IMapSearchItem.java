package net.nececity.nececity.Abstracts;

import com.lapism.searchview.widget.SearchItem;

import java.util.HashMap;

/**
 * Created by Takunda on 9/29/2019.
 */

public interface IMapSearchItem {
    void insertSearchItem();

    void removeSearchItem();

    /**
     * Should return the static variable for the that class containing all the search items inserted in it.
     *
     * @return
     */
    HashMap<String, SearchItem> getSearchItems();

    SearchItem getSearchItem();

    void clearSearchItems();

    boolean clearOnMapRemoval();
}
