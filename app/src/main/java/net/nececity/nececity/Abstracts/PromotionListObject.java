package net.nececity.nececity.Abstracts;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ms.square.android.expandabletextview.ExpandableTextView;

import net.nececity.nececity.Data.ActionDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Takunda on 8/4/2018.
 */

public abstract class PromotionListObject extends ListObject implements IGetDataSet {

    protected View view;
    protected ArrayAdapter<String> adapter;

    protected ImageView likeIV;
    protected ImageView commentIV;
    protected EditText commentET;
    protected ImageButton postCommentIB;
    protected TextView promotionText;
    protected TextView viewLikes;

    private Object promotionObject;

    public Class commentsClassActivity;

    private RadioButton mon,tue,wed,thu,fri,sat,sun;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void populateData(){
        super.populateData();

        sun = view.findViewById(R.id.sun);
        mon = view.findViewById(R.id.mon);
        tue = view.findViewById(R.id.tue);
        wed = view.findViewById(R.id.wed);
        thu = view.findViewById(R.id.thu);
        fri = view.findViewById(R.id.fri);
        sat = view.findViewById(R.id.sat);

        populateDays();

        likeIV = view.findViewById(R.id.like);
        commentIV = view.findViewById(R.id.comment);
        commentET = view.findViewById(R.id.commentText);
        postCommentIB = view.findViewById(R.id.postComment);
        promotionText = view.findViewById(R.id.expandable_text);
        viewLikes = view.findViewById(R.id.viewLikes);

        ((TextView)view.findViewById(R.id.secondaryTV)).setText(((Promotion)this).getTime());
        ExpandableTextView expTv1 = view.findViewById(R.id.expand_text_view);

        ((TextView)view.findViewById(R.id.primaryTV)).setText(((Promotion)this).getTitle());
        ((TextView)view.findViewById(R.id.secondaryTV)).setText(((Promotion)this).getFrom()+" - "+((Promotion)this).getTo());

        TextView priceMainTV = view.findViewById(R.id.priceMainText);
        priceMainTV.setText(((Promotion)this).getPrice()+" for "+((Promotion)this).getUnits());

        expTv1.setText(((Promotion)this).getPromotion());
        viewLikes.setText(((Promotion)this).getLikes()+" likes");

        setPostListObjectPicture();
    }

    public static View updateData(View view, Promotion promotion){
        TextView viewLikes = view.findViewById(R.id.viewLikes);

        ((TextView)view.findViewById(R.id.secondaryTV)).setText(promotion.getTime());
        ExpandableTextView expTv1 = view.findViewById(R.id.expand_text_view);

        ((TextView)view.findViewById(R.id.primaryTV)).setText(promotion.getTitle());
        ((TextView)view.findViewById(R.id.secondaryTV)).setText(promotion.getFrom()+" - "+promotion.getTo());

        TextView priceMainTV = view.findViewById(R.id.priceMainText);
        priceMainTV.setText(promotion.getPrice()+" for "+promotion.getUnits());

        expTv1.setText(promotion.getPromotion());
        viewLikes.setText(promotion.getLikes()+" likes");

        return view;
    }

    public void populateDays(){
        Promotion promotion = (Promotion)this;

        String days = promotion.getDays();

        if (days == null){
            return;
        }

        String[] daysArr = days.split(",");

        for (String day:daysArr){
            if (day.equalsIgnoreCase("mon")){
                mon.setChecked(true);
            }

            if (day.equalsIgnoreCase("tue")){
                tue.setChecked(true);
            }

            if (day.equalsIgnoreCase("wed")){
                wed.setChecked(true);
            }

            if (day.equalsIgnoreCase("thu")){
                thu.setChecked(true);
            }

            if (day.equalsIgnoreCase("fri")){
                fri.setChecked(true);
            }

            if (day.equalsIgnoreCase("sat")){
                sat.setChecked(true);
            }

            if (day.equalsIgnoreCase("sun")){
                sun.setChecked(true);
            }
        }
    }

    @Override
    public void setListeners(){
        super.setSecondaryView();

        switch (listObjectType){
            case PROMOTION:
                likePromotion();
                commentOnPromotion();
                editPromotion();
                viewComments();
                break;
        }
    }

    public String getDays(){
        Promotion promotion = (Promotion)this;
        return promotion.getDays();
    }

    public Date getDate(){
        Promotion promotion = (Promotion)this;
        return promotion.getDate();
    }

    public Promotion getPromotionObject(){
        Promotion promotion = (Promotion)this;
        return promotion;
    }

    public void likePromotion(){
        final String companyId  = getCompany().getId();
        final String promotionId = ((Promotion)this).getId();
        final Promotion promotion = (Promotion)this;

        if (promotion.getLiked()){
            likeIV.setImageResource(R.drawable.like_yellow);
            return;
        }

        likeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = UrlConstructor.likeCompanyUserPromotionUrl(companyId, AppVariables.user.getId(),promotionId);
                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.LIKE_PROMOTION;

                ViewManager viewManager = new ViewManager(likeIV,actionType,listActivity);
                viewManager.setViewActivity(listActivity);
                viewManager.setAdapter(adapter);
                viewManager.setObject(promotion);
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.likePost();
            }
        });
    }

    private void commentOnPromotion(){
        final String companyId  = getCompany().getId();
        final String promotionId = ((Promotion)this).getId();
        final Promotion promotion = (Promotion)this;

        postCommentIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = commentET.getText().toString();
                commentET.setText("");
                if (comment.isEmpty()){
                    return;
                }

                String url = UrlConstructor.postUserPromotionCommentUrl(AppVariables.user.getName(), companyId, promotionId);
                if (AppVariables.user instanceof Employee) {
                    url = UrlConstructor.postUserPromotionCommentUrl(EPrivilege.ADMIN.toString(), companyId, promotionId);
                }

                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.COMMENT_ON_POST;

                ViewManager viewManager = new ViewManager(null,actionType,listActivity);
                viewManager.setObject(promotion);
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.commentOnPost(comment);
            }
        });
    }

    public void viewComments(){
        final Promotion promotion = (Promotion)this;

        commentIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listObjectType){
                    case PROMOTION:
                        Intent intent = new Intent(listActivity, commentsClassActivity);
                        intent.putExtra("promotionId",promotion.getId());
                        intent.putExtra("companyId",promotion.getCompanyId());
                        listActivity.startActivity(intent);
                        break;
                }
            }
        });
    }

    public void editPromotion(){
        final Promotion promotion = (Promotion)this;

        if (!(AppVariables.user instanceof Employee)){
            return;
        }

        listViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listObjectType) {
                    case PROMOTION:
                        Intent intent = new Intent(listActivity, listItemClassActivity);
                        intent.putExtra("promotionId", promotion.getId());
                        intent.putExtra("companyId", promotion.getCompanyId());
                        listActivity.startActivity(intent);
                        break;
                }
            }
        });


    }

    public void promotionSeen(){
        Promotion promotion = (Promotion)this;

        String url = UrlConstructor.seenCompanyUserPromotionsUrl(promotion.getCompanyId(),AppVariables.user.getId(),promotion.getId());
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("promotionId", promotion.getId()));

        ActionDataSet actionDataSet = new ActionDataSet(this,EActionType.POST_SEEN);
        actionDataSet.setUrl(url);
        actionDataSet.setParams(params);
        actionDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet){
        if (dataSet.success){
            ((Promotion)this).setSeen("true");
            //NeceCityMapActivity.unseenPromotions(false);
        }
    }

    @Override
    public void setView(View view){
        super.setView(view);
        this.view=view;
    }

    @Override
    public String getMainText(){
        return this.getCompany().getMainText();
    }

    @Override
    public String getSecondaryText(){
        return this.getCompany().getSecondaryText();
    }

    public ArrayAdapter<String> getAdapter() {
        return adapter;
    }

    public void setAdapter(ArrayAdapter<String> adapter) {
        super.setAdapter(adapter);
        this.adapter = adapter;
    }

    public Class getCommentsClassActivity() {
        return commentsClassActivity;
    }

    public void setCommentsClassActivity(Class commentsClassActivity) {
        this.commentsClassActivity = commentsClassActivity;
    }
}
