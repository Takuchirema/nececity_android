package net.nececity.nececity.Abstracts;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ms.square.android.expandabletextview.ExpandableTextView;

import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.CustomRadioButton;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.R;

import java.util.Date;

/**
 * Created by Takunda on 8/28/2018.
 */

public abstract class CataloguePostListObject extends ListObject implements IGetDataSet{

    protected View view;
    protected ArrayAdapter<String> adapter;

    protected ImageView likeIV;
    protected ImageView commentIV;
    protected EditText commentET;
    protected ImageButton postCommentIB;
    protected TextView viewLikes;

    public Class commentsClassActivity;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void populateData(){
        super.populateData();

        populateAmenities();

        likeIV = view.findViewById(R.id.like);
        commentIV = view.findViewById(R.id.comment);
        commentET = view.findViewById(R.id.commentText);
        postCommentIB = view.findViewById(R.id.postComment);
        viewLikes = view.findViewById(R.id.viewLikes);

        TextView priceMainTV = view.findViewById(R.id.priceMainText);
        ExpandableTextView expTv1 = view.findViewById(R.id.expand_text_view);

        expTv1.setText(((CataloguePost)this).getPost());
        priceMainTV.setText(((CataloguePost)this).getPrice()+" for "+((CataloguePost)this).getUnits());
        viewLikes.setText(((CataloguePost)this).getLikes()+" likes");

        setPostListObjectPicture();
    }

    public static View updateData(View view, CataloguePost post){
        TextView viewLikes = view.findViewById(R.id.viewLikes);

        ((TextView)view.findViewById(R.id.primaryTV)).setText(post.getTitle());
        ((TextView)view.findViewById(R.id.secondaryTV)).setText(post.getFrom()+" - "+(post.getTo()));

        TextView priceMainTV = view.findViewById(R.id.priceMainText);
        ExpandableTextView expTv1 = view.findViewById(R.id.expand_text_view);

        expTv1.setText(post.getPost());
        priceMainTV.setText(post.getPrice()+" for "+(post.getUnits()));
        viewLikes.setText(post.getLikes()+" likes");

        return view;
    }

    public void populateAmenities(){
        CataloguePost post = (CataloguePost)this;

        GridLayout amenitiesGrid = view.findViewById(R.id.amenitiesGrid);

        LayoutInflater inflater = listActivity.getLayoutInflater();
        String[] amenities = post.getAmenities().split(",");

        for (String amenity:amenities){
            if (amenity.isEmpty()){
                continue;
            }
            View amenityLayout = inflater.inflate(R.layout.amenities_item, null);
            CustomRadioButton amenityView = amenityLayout.findViewById(R.id.amenity);
            amenityView.setText(amenity);
            amenitiesGrid.addView(amenityLayout);
        }
    }

    @Override
    public void setListeners(){
        super.setSecondaryView();

        switch (listObjectType){
            case CATALOGUE_POST:
                likeCataloguePost();
                commentOnCataloguePost();
                editCataloguePost();
                viewComments();
                break;
        }
    }

    public Date getDate(){
        CataloguePost post = (CataloguePost)this;
        return post.getDate();
    }

    public CataloguePost getCataloguePostObject(){
        CataloguePost post = (CataloguePost)this;
        return post;
    }

    public void likeCataloguePost(){
        final String companyId  = getCompany().getId();
        final String postId = ((CataloguePost)this).getId();
        final CataloguePost post = (CataloguePost)this;

        if (post.getLiked()){
            likeIV.setImageResource(R.drawable.like_yellow);
            return;
        }

        likeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = UrlConstructor.likeCompanyUserCataloguePostsUrl(companyId, AppVariables.user.getId(),postId);
                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.LIKE_CATALOGUE_POST;

                ViewManager viewManager = new ViewManager(likeIV,actionType,listActivity);
                viewManager.setViewActivity(listActivity);
                viewManager.setAdapter(adapter);
                viewManager.setObject(post);
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.likePost();
            }
        });
    }

    private void commentOnCataloguePost(){
        final String companyId  = getCompany().getId();
        final String postId = ((CataloguePost)this).getId();
        final CataloguePost post = (CataloguePost)this;

        postCommentIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = commentET.getText().toString();
                commentET.setText("");
                if (comment.isEmpty()){
                    return;
                }

                String url = UrlConstructor.postUserCatalogueCommentUrl(AppVariables.user.getName(), companyId, postId);
                if (AppVariables.user instanceof Employee) {
                    url = UrlConstructor.postUserCatalogueCommentUrl(EPrivilege.ADMIN.toString(), companyId, postId);
                }

                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.COMMENT_ON_CATALOGUE_POST;

                ViewManager viewManager = new ViewManager(null,actionType,listActivity);
                viewManager.setObject(post);
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.commentOnPost(comment);
            }
        });
    }

    public void viewComments(){
        final CataloguePost post = (CataloguePost)this;

        commentIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listObjectType){
                    case CATALOGUE_POST:
                        Intent intent = new Intent(listActivity, commentsClassActivity);
                        intent.putExtra("cataloguePostId",post.getId());
                        intent.putExtra("companyId",post.getCompanyId());
                        listActivity.startActivity(intent);
                        break;
                }
            }
        });
    }

    public void editCataloguePost(){
        final CataloguePost post = (CataloguePost)this;

        if (!(AppVariables.user instanceof Employee)){
            return;
        }

        listViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listObjectType) {
                    case CATALOGUE_POST:
                        Intent intent = new Intent(listActivity, listItemClassActivity);
                        intent.putExtra("cataloguePostId", post.getId());
                        intent.putExtra("companyId", post.getCompanyId());
                        listActivity.startActivity(intent);
                        break;
                }
            }
        });
    }

    @Override
    public void postGetDataSet(DataSet dataSet){
        if (dataSet.success){
            ((CataloguePost)this).setSeen("true");
            //NeceCityMapActivity.unseenCataloguePosts(false);
        }
    }

    @Override
    public void setView(View view){
        super.setView(view);
        this.view=view;
    }

    @Override
    public String getMainText(){
        return ((CataloguePost)this).getTitle();
    }

    @Override
    public String getSecondaryText(){
        return ((CataloguePost)this).getFrom()+" - "+((CataloguePost)this).getTo();
    }

    public ArrayAdapter<String> getAdapter() {
        return adapter;
    }

    public void setAdapter(ArrayAdapter<String> adapter) {
        super.setAdapter(adapter);
        this.adapter = adapter;
    }

    public Class getCommentsClassActivity() {
        return commentsClassActivity;
    }

    public void setCommentsClassActivity(Class commentsClassActivity) {
        this.commentsClassActivity = commentsClassActivity;
    }
}
