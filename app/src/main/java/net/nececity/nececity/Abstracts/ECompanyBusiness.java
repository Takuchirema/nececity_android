package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 2/28/2019.
 */

public enum ECompanyBusiness {
    SECURITY{
        public String toString() {
            return "Security";
        }
    },
    TRANSPORTATION{
        public String toString() {
            return "Transportation";
        }
    },
    RETAIL_WHOLESALE{
        public String toString() {
            return "Retail_Wholesale";
        }
    },
    LEGAL_SERVICES{
        public String toString() {
            return "Legal_Services";
        }
    },
    RESTAURANT{
        public String toString() {
            return "Restaurant";
        }
    },
    REAL_ESTATE{
        public String toString() {
            return "Real_Estate";
        }
    },
    PERSONAL_SERVICES{
        public String toString() {
            return "Personal_Services";
        }
    },
    ENVIRONMENTAL{
        public String toString() {
            return "Environmental";
        }
    },
    MOTOR_VEHICLES{
        public String toString() {
            return "Motor_Vehicles";
        }
    },
    HEALTH_SERVICES{
        public String toString() {
            return "Health_Services";
        }
    },
    HOSPITALITY{
        public String toString() {
            return "Hospitality";
        }
    },
    FINANCE{
        public String toString() {
            return "Finance";
        }
    },
    EDUCATION{
        public String toString() {
            return "Education";
        }
    },
    CONSTRUCTION{
        public String toString() {
            return "Construction";
        }
    },
    TECHNOLOGY{
        public String toString() {
            return "Technology";
        }
    },
    AGRICULTURE{
        public String toString() {
            return "Agriculture";
        }
    },
    OTHER{
        public String toString() {
            return "Other";
        }
    }
}
