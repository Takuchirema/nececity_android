package net.nececity.nececity.Abstracts;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Takunda on 26/05/2018.
 */

public interface ISendServerPicture {
    void postSendServerPicture(JSONObject data) throws JSONException;
}
