package net.nececity.nececity.Abstracts;

import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.Objects.MapMarker;

/**
 * Created by Takunda on 2/23/2019.
 */

public interface IMapActivity extends IMainActivity {

    void stopMapRefresh(DataSet dataSet);

    void showPlotRoute();

    CityMapManager getMapManager();

    void updateDashboard();

    void collapseDashboard();

    void onMapMarkerClick(MapMarker mapMarker);
}
