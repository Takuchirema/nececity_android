package net.nececity.nececity.Abstracts;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

import net.nececity.nececity.Objects.Delivery;

/**
 * Created by Takunda on 5/23/2019.
 */

public abstract class UserListObject extends ListObject implements IGetDataSet{
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void populateData(){
        super.populateData();
    }

    public static View updateData(View view, Delivery delivery){
        return view;
    }

    @Override
    public void setView(View view) {
        super.setView(view);
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {

    }

    @Override
    public CharSequence getMainText(){
        return null;
    }

    @Override
    public String getSecondaryText(){
        return null;
    }
}
