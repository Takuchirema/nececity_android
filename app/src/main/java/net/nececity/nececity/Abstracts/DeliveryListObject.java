package net.nececity.nececity.Abstracts;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import net.nececity.nececity.R;

import net.nececity.nececity.Objects.Delivery;

/**
 * Created by Takunda on 5/15/2019.
 */

public abstract class DeliveryListObject extends ListObject implements IGetDataSet{

    protected View view;
    protected ArrayAdapter<String> adapter;

    protected TextView itemsTV;
    protected TextView totalTV;
    protected TextView addressTV;
    protected TextView phoneNumberTV;
    protected TextView statusTV;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void populateData(){
        super.populateData();

        Delivery delivery = (Delivery)this;

        itemsTV = view.findViewById(R.id.items);
        totalTV = view.findViewById(R.id.subtotal);
        addressTV = view.findViewById(R.id.address);
        phoneNumberTV = view.findViewById(R.id.phoneNumber);
        statusTV = view.findViewById(R.id.status);

        itemsTV.setText(delivery.getDeliveryItems().size()+" items");
        addressTV.setText(delivery.getAddress());
        phoneNumberTV.setText(delivery.getPhoneNumber());
        statusTV.setText(delivery.getDeliveryStatus().toString());

        if (delivery.getTotal() != null){
            totalTV.setText(delivery.getTotal());
        }
    }

    @Override
    public void setSecondaryView(){
        Delivery delivery = (Delivery)this;

        switch (delivery.getDeliveryStatus()){
            case PENDING:
                secondaryIV.setImageResource(R.drawable.delivery_pending);
                secondaryIV.setBackgroundResource(R.drawable.round_orange_button);
            case INTRANSIT:
                secondaryIV.setImageResource(R.drawable.delivery_transit);
                secondaryIV.setBackgroundResource(R.drawable.round_orange_button);
            case DELIVERED:
                secondaryIV.setImageResource(R.drawable.delivered);
                secondaryIV.setBackgroundResource(R.drawable.round_green_button);
            case CANCELLED:
                secondaryIV.setImageResource(R.drawable.delivery_pending);
                secondaryIV.setBackgroundResource(R.drawable.round_red_button);
        }
    }

    @Override
    public void setListeners(){
        super.setSecondaryView();

        viewDeliveryItems();
        viewDeliveryLocation();
        callDeliveryNumber();
    }

    private void viewDeliveryItems(){
        Delivery delivery = (Delivery)this;

        final String companyId  = delivery.getCompanyId();
        final int deliveryId = delivery.getId();

        itemsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(listActivity, listItemClassActivity);
                intent.putExtra("deliveryId",deliveryId);
                intent.putExtra("companyId",companyId);
                listActivity.startActivity(intent);
            }
        });
    }

    private void viewDeliveryLocation(){

    }

    private void callDeliveryNumber(){
        Delivery delivery = (Delivery)this;
        if (delivery.getPhoneNumber() == null){
            return;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:"+delivery.getPhoneNumber()));
            listActivity.startActivity(intent);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static View updateData(View view, Delivery delivery){
        return view;
    }

    @Override
    public void setView(View view) {
        super.setView(view);
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {

    }

    @Override
    public String getMainText(){
        String mainText = "Delivery ID: "+((Delivery)this).getId();
        return mainText;
    }

    @Override
    public String getSecondaryText(){
        String secondaryText = this.getCompany().getId();
        return secondaryText;
    }
}
