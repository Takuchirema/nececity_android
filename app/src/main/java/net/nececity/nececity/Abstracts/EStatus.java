package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 22/07/2018.
 */

public enum EStatus {
    ONLINE {
        public String toString() {
            return "Online";
        }
    },
    OFFLINE {
        public String toString() {
            return "Offline";
        }
    }
}
