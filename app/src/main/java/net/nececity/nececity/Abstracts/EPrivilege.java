package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 8/2/2018.
 */

public enum EPrivilege {
    SUPER_USER {
        public String toString() {
            return "superuser";
        }
    },
    ADMIN {
        public String toString() {
            return "admin";
        }
    },
    EMPLOYEE {
        public String toString() {
            return "employee";
        }
    }
}
