package net.nececity.nececity.Abstracts;

import android.location.Location;

/**
 * Created by Takunda on 04/06/2018.
 */

public abstract class LocationResult {
    public abstract void gotLocation(Location location);
}
