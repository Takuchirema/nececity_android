package net.nececity.nececity.Abstracts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;

import net.nececity.nececity.Helpers.AppVariables;

import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Transformers.BaseTransformer;
import com.lapism.searchview.widget.SearchItem;

import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.R;


import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.Views.FileViewerActivity;
import net.nececity.nececity.Views.ImageViewerActivity;

import java.util.ArrayList;

/**
 * Created by Takunda on 23/06/2018.
 */

public abstract class ListObject implements Comparable<ListObject>{

    protected String id;
    protected View view;
    protected ImageView secondaryIV;
    protected ImageView primaryIV;
    protected TextView secondaryInfoTV;
    protected TextView infoTV;
    protected LinearLayout mainView;
    protected TextView primaryTV;
    protected TextView secondaryTV;
    protected View dividerV;
    public RelativeLayout listViewContainer;

    public EObjectType listObjectType;

    protected ArrayAdapter<String> adapter;
    public Bitmap bmp;
    public String pictureUrl;
    public ArrayList<String> pictureUrls = new ArrayList<>();

    public String fileUrl;

    protected String mainText="";
    protected String secondaryText="";

    // Activity the list object is being displayed in
    public Activity listActivity;
    public Class listItemClassActivity;

    public ListObject(){
        listObjectType = getListObjectType();
    }

    public void setUpView(){
        secondaryIV = view.findViewById(R.id.secondaryIV);
        primaryIV = view.findViewById(R.id.primaryIV);
        secondaryInfoTV = view.findViewById(R.id.secondaryInfoTV);
        infoTV = view.findViewById(R.id.infoTV);
        mainView = view.findViewById(R.id.mainView);
        dividerV = view.findViewById(R.id.divider);
        listViewContainer = view.findViewById(R.id.listViewContainer);
        primaryTV = view.findViewById(R.id.primaryTV);
        secondaryTV =  view.findViewById(R.id.secondaryTV);

        setViewDefaults();
    }

    public void setViewDefaults(){
        secondaryInfoTV.setVisibility(View.GONE);
        infoTV.setVisibility(View.GONE);
        dividerV.setVisibility(View.GONE);

        secondaryIV.setVisibility(View.VISIBLE);
        primaryIV.setVisibility(View.VISIBLE);
        mainView.setVisibility(View.VISIBLE);
        secondaryTV.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void populateData(){
        setUpView();
        setTextViews();
        setPrimaryView();
        setSecondaryView();
        setListObjectFile();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public View updateView(){
        populateData();
        return view;
    }

    public void setTextViews(){

        if (getMainText().length() == 0){
            primaryTV.setVisibility(View.GONE);
        }else{
            primaryTV.setSelected(true);
            primaryTV.setText(getMainText());
        }

        if (getSecondaryText().length() == 0){
            secondaryTV.setVisibility(View.GONE);
        }else{
            secondaryTV.setSelected(true);
            secondaryTV.setText(getSecondaryText());
        }
    }

    public void setListeners(){
        setSecondaryViewListener();
        setPrimaryViewListener();
    }

    public void setSecondaryView(){
        Company company = getCompany();

        if (company.isFollowing()){
            secondaryIV.setImageResource(R.drawable.done);
            secondaryIV.setBackgroundResource(R.drawable.round_green_button);
        }else{
            secondaryIV.setImageResource(R.drawable.ic_add_white);
            secondaryIV.setBackgroundResource(R.drawable.round_orange_button);
        }
        //adapter.notifyDataSetChanged();
    }

    public void setSecondaryViewListener(){
        if (listItemClassActivity != null){
            doSecondaryViewActivity();
        }else{
            doSecondaryViewAction();
        }
    }

    public void doSecondaryViewActivity(){
        final Company company = getCompany();

        secondaryIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = UrlConstructor.followCompanyUrl(AppVariables.user.getId(),company.getId());
                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.FOLLOW_COMPANY;

                if (company.isFollowing()){
                    url = UrlConstructor.unfollowCompanyUrl(AppVariables.user.getId(),company.getId());
                    httpMethod = EHttpMethod.DELETE;
                    actionType = EActionType.UNFOLLOW_COMPANY;
                }

                ViewManager viewManager = new ViewManager(secondaryIV,actionType,listActivity);
                viewManager.setObjectId(company.getId());
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.setAdapter(adapter);
                viewManager.followCompany();
            }
        });
    }

    public void doSecondaryViewAction(){
    }

    public void setPrimaryViewListener(){
        if (listItemClassActivity != null){
            doPrimaryViewActivity();
        }else{
            doPrimaryViewAction();
        }
    }

    public void doPrimaryViewActivity(){
        final Company company = getCompany();
        mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(listActivity, listItemClassActivity);
                intent.putExtra("companyId", company.getId());
                listActivity.startActivity(intent);
            }
        });
    }

    public void doPrimaryViewAction(){
        if (!(this instanceof IMapSearchItem)){
            return;
        }
        IMapSearchItem mapSearchItem = (IMapSearchItem) this;
        final SearchItem searchItem = mapSearchItem.getSearchItem();
        final IMainActivity mapActivity = AppVariables.mainActivity;
        if (searchItem == null || !(mapActivity instanceof IMapActivity)){
            return;
        }

        final CityMapManager mapManager = ((IMapActivity) mapActivity).getMapManager();
        mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((IMapActivity) mapActivity).collapseDashboard();
                mapManager.searchHandler(searchItem);
            }
        });
    }

    public Company getCompany(){
        return null;
    }

    public boolean hasPostPicture(){
        if (this instanceof Post){
            return ((Post)this).hasPicture();
        }else if (this instanceof CataloguePost){
            return ((CataloguePost)this).hasPicture();
        }else if (this instanceof Promotion){
            return ((Promotion)this).hasPicture();
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setPrimaryView() {
        System.out.println("list object list set picture");

        ViewManager viewManager = new ViewManager(view.findViewById(R.id.primaryIV), EActionType.DOWNLOAD_PICTURE, listActivity);
        viewManager.setAdapter(adapter);

        Company company = getCompany();
        viewManager.setObject(company);
        viewManager.setImageBackground(true);
        if (company.getBmp() == null) {
            System.out.println("getting pic ----");
            viewManager.setUrl(company.getPictureUrl());
            viewManager.getPicture();
        }else{
            System.out.println("post getting pic ----");
            viewManager.setPicture(company.getBmp());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setListObjectFile() {
        System.out.println("list object list set file");

        if (fileUrl == null){
            return;
        }

        LinearLayout fileContainer = view.findViewById(R.id.fileContainer);

        if (fileContainer == null){
            return;
        }

        fileContainer.setVisibility(View.VISIBLE);

        TextView fileUrlTV = fileContainer.findViewById(R.id.fileName);
        fileUrlTV.setText(getFileName());

        fileContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(listActivity, FileViewerActivity.class);
                intent.putExtra("fileUrl", fileUrl);
                listActivity.startActivity(intent);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setPostListObjectPicture() {
        System.out.println("set picture");

        if (!hasPostPicture()){
            view.findViewById(R.id.slider).setVisibility(View.GONE);
            return;
        }

        SliderLayout slider = view.findViewById(R.id.slider);
        slider.stopAutoCycle();

        for (String pictureUrl:pictureUrls) {
            TextSliderView sliderView = new TextSliderView(listActivity);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.centerCrop();

            sliderView
                    .image(pictureUrl)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setRequestOption(requestOptions)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            viewPostImage(slider.getUrl());
                        }
                    });

            //add your extra information
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", "name");

            slider.addSlider(sliderView);
        }

        if (pictureUrls.size() == 1) {
            slider.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {}
            });
        }
    }

    public void viewPostImage(String pictureUrl){
        Intent intent = new Intent(listActivity, ImageViewerActivity.class);
        intent.putExtra("companyId", getCompany().getId());
        intent.putExtra("pictureUrl", pictureUrl);

        switch(listObjectType){
            case CATALOGUE_POST:
                CataloguePost cataloguePost = (CataloguePost)this;
                intent.putExtra("title", cataloguePost.getTitle());
                intent.putExtra("cataloguePostId", cataloguePost.getId());
                break;
            case PROMOTION:
                Promotion promotion = (Promotion)this;
                intent.putExtra("title", promotion.getTitle());
                intent.putExtra("promotionId", promotion.getId());
                break;
            case POST:
                Post post = (Post)this;
                intent.putExtra("title", "Post Image");
                intent.putExtra("postId", post.getId());
                break;
        }
        listActivity.startActivity(intent);
    }

    public int compareTo(ListObject object) {
        EObjectType objectType = object.getListObjectType();
        if (objectType == listObjectType){
            return compareToSameObjectType(object);
        }
        return getSortOrder() - object.getSortOrder();
    }

    public int getSortOrder(){
        switch (listObjectType){
            case EMPLOYEE:
                return 1;
            case ROUTE_POINT:
                return 2;
            case ROUTE:
                return 3;
            case COMPANY:
                return 4;
            default:
                return 5;
        }
    }

    public int compareToSameObjectType(ListObject object){
        return 0;
    }

    public String getSearchString(){
        String searchString = getMainText() + " " + getSecondaryText();
        return searchString;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public void setAdapter(ArrayAdapter<String> adapter){
        this.adapter=adapter;
    }

    public Bitmap getBmp() {
        return bmp;
    }

    public void setBmp(Bitmap bmp) {
        this.bmp = bmp;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public ArrayList<String> getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(ArrayList<String> pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileName(){
        if (fileUrl != null){
            String fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
            fileName = fileName.replace("%20"," ");
            fileName = fileName.substring(0,fileName.lastIndexOf('.'));
            return fileName;
        }
        return null;
    }

    public Drawable getListDrawable(){
        Context context = AppVariables.mainActivity.getContext();
        Resources.Theme theme = context.getTheme();
        Drawable drawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            drawable = context.getResources().getDrawable(R.drawable.ic_action_place_orange,theme);
        }
        return drawable;
    }

    public abstract CharSequence getMainText();

    public abstract EObjectType getListObjectType();

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public abstract CharSequence getSecondaryText();

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public Activity getListActivity() {
        return listActivity;
    }

    public void setListActivity(Activity listActivity) {
        this.listActivity = listActivity;
    }

    public Class getListItemClassActivity() {
        return listItemClassActivity;
    }

    public void setListItemClassActivity(Class listItemClassActivity) {
        this.listItemClassActivity = listItemClassActivity;
    }
}
