package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 07/07/2018.
 */

public enum EObjectType {
    COMPANY {
        public String toString() {
            return "company";
        }
    },
    POST {
        public String toString() {
            return "post";
        }
    },
    CATALOGUE_POST {
        public String toString() {
            return "catalogue_post";
        }
    },
    PROMOTION {
        public String toString() {
            return "promotion";
        }
    },
    USER {
        public String toString() {
            return "user";
        }
    },
    EMPLOYEE {
        public String toString() {
            return "employee";
        }
    },
    DELIVERY {
        public String toString() {
            return "delivery";
        }
    },
    BUS_STOP {
        public String toString() {
            return "bus_stop";
        }
    },
    ROUTE {
        public String toString() {
            return "route";
        }
    },
    ROUTE_POINT {
        public String toString() {
            return "route_point";
        }
    },
    BUS {
        public String toString() {
            return "bus";
        }
    },
}
