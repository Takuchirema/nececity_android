package net.nececity.nececity.Abstracts;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;

import net.nececity.nececity.Data.JSONParser;

import org.json.JSONException;
import net.nececity.nececity.Data.CustomJSONObject;
import net.nececity.nececity.Helpers.AppVariables;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Takunda on 26/05/2018.
 */

public abstract class DataSet implements IGetServerData{
    // Used to show if the DataSet successfully go the data
    public boolean success;
    public String message;
    public IGetDataSet getDataset;

    public DataSet(IGetDataSet getDataset){
        this.getDataset=getDataset;
    }

    public void getDataSet(){}

    public void prepareDataSet(CustomJSONObject json) throws JSONException{

    }

    @Override
    public void postGetServerData(CustomJSONObject json) throws JSONException {

        if (getDataset == null){
            return;
        }

        // check your log for json response
        if (json != null){
            message = json.getString(JSONParser.TAG_MESSAGE);
            if (json.getInt(JSONParser.TAG_SUCCESS) == 1) {
                prepareDataSet(json);
                success = true;
            }else{
                success = false;
            }
            postGetDataSet(this);
        }else{
            success = false;
            message = "Could not connect to server. Please check your internet connection and try again.";
            postGetDataSetMessage();
        }
    }

    public void postGetDataSet(final DataSet dataSet){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                getDataset.postGetDataSet(dataSet);
            }
        });
    }

    public void postGetDataSetMessage() {
        if (!isNetworkAvailable()) {
            getDataset.postGetDataSet(this);
            return;
        }
        new Thread(new Runnable() {
            DataSet dataSet;

            public Runnable init(DataSet dataSet) {
                this.dataSet = dataSet;
                return this;
            }

            public void run() {
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    urlc.setConnectTimeout(1500);
                    urlc.connect();

                    if(urlc.getResponseCode() == 200){
                        message = "It's not you it's us. We are fixing the issue, please try again later. Thank you!";
                    }
                } catch (Exception e) {}
                postGetDataSet(dataSet);
            }
        }.init(this)).start();
    }

    private boolean isNetworkAvailable() {
        Context context = AppVariables.context;
        if (AppVariables.mainActivity != null){
            context = AppVariables.mainActivity.getContext();
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public IGetDataSet getGetDataset() {
        return getDataset;
    }

    public void setGetDataset(IGetDataSet getDataset) {
        this.getDataset = getDataset;
    }
}
