package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 12/29/2018.
 */

public interface IListLayout {
    void clearRefresh();
    void createListAdapter();
}
