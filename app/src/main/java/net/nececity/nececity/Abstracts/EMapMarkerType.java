package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 23/05/2018.
 */

public enum EMapMarkerType {
    USER{
        public String toString() {
            return "User";
        }
    },
    COMPANY{
        public String toString() {
            return "Company";
        }
    },
    EMPLOYEE{
        public String toString() {
            return "User";
        }
    },
    ROUTE_POINT{
        public String toString() {
            return "Bus Stop";
        }
    },
    ROUTE_LOCATION_CLUSTER{
        public String toString(){return "Route Location Cluster";}
    }
}
