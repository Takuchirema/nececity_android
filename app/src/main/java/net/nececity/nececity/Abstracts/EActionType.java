package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 23/06/2018.
 */

public enum EActionType {
    FOLLOW_COMPANY,
    UNFOLLOW_COMPANY,
    DOWNLOAD_PICTURE,
    DELETE_FILE,
    LIKE_POST,
    LIKE_CATALOGUE_POST,
    LIKE_PROMOTION,
    COMMENT_ON_POST,
    COMMENT_ON_CATALOGUE_POST,
    POST_SEEN,
    LOGOUT,
    SET_STATUS,
    SEND_LOG_TIME,
    DELETE_POST,
    DELETE_PROMOTION
}
