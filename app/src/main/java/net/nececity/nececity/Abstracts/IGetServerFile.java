package net.nececity.nececity.Abstracts;

import java.io.File;

/**
 * Created by Takunda on 7/27/2018.
 */

public interface IGetServerFile {
    void postGetServerFile(File file);
}
