package net.nececity.nececity.Abstracts;

import android.widget.LinearLayout;

import net.nececity.nececity.Objects.Comment;

/**
 * Created by Takunda on 8/4/2018.
 */

public interface ICommentActivity {
    void addComment(Comment comment, LinearLayout commentsView);
}
