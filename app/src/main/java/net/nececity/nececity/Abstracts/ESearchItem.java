package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 04/07/2018.
 */

public enum ESearchItem {
    MARKER{
        public String toString() {
            return "Marker";
        }
    },
    COMPANY{
        public String toString() {
            return "Company";
        }
    },
    ROUTE{
        public String toString() {
            return "Route";
        }
    },
    ROUTE_POINT{
        public String toString() {
            return "Route Point";
        }
    },
    ROUTE_CLUSTER{
        public String toString() {
            return "Route Cluster";
        }
    },
    EMPLOYEE{
        public String toString() {
            return "Employee";
        }
    }
}
