package net.nececity.nececity.Abstracts;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Takunda on 24/07/2018.
 */

public interface ISendServerFile {
    void postSendServerFile(JSONObject data) throws JSONException;
}
