package net.nececity.nececity.Abstracts;

/**
 * Created by Takunda on 7/31/2018.
 */

public enum EPostType {
    ADMIN {
        public String toString() {
            return "admin";
        }
    },
    COMPANY {
        public String toString() {
            return "company";
        }
    }
}
