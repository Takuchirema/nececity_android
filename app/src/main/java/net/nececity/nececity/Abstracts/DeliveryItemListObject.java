package net.nececity.nececity.Abstracts;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.nececity.nececity.Objects.Delivery;
import net.nececity.nececity.Objects.DeliveryItem;

/**
 * Created by Takunda on 5/26/2019.
 */

public abstract class DeliveryItemListObject extends ListObject {
    protected View view;
    protected ArrayAdapter<String> adapter;

    protected TextView itemsTV;
    protected TextView totalTV;
    protected TextView addressTV;
    protected TextView phoneNumberTV;
    protected TextView statusTV;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void populateData(){
        super.populateData();

        DeliveryItem delivery = (DeliveryItem)this;
    }

    @Override
    public void setSecondaryView(){

    }

    @Override
    public void setListeners(){
        setSecondaryView();

        viewItemPicture();
    }

    private void viewItemPicture(){
        DeliveryItem deliveryItem = (DeliveryItem)this;

        String pictureUrl = deliveryItem.getPictureUrl();

    }

    public static View updateData(View view, Delivery delivery){
        return view;
    }

    @Override
    public void setView(View view) {
        super.setView(view);
    }

    @Override
    public String getMainText(){
        String mainText = ((DeliveryItem)this).getName();
        return mainText;
    }

    @Override
    public String getSecondaryText(){
        String secondaryText = ((DeliveryItem)this).getDescription();
        return secondaryText;
    }
}
