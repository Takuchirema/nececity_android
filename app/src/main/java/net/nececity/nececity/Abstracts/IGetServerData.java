package net.nececity.nececity.Abstracts;

import org.json.JSONException;
import net.nececity.nececity.Data.CustomJSONObject;

/**
 * Created by Takunda on 26/05/2018.
 */

public interface IGetServerData {
    void postGetServerData(CustomJSONObject dataSet) throws JSONException;
}
