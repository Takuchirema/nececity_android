package net.nececity.nececity.Data;

import android.os.AsyncTask;
import android.os.Environment;

import net.nececity.nececity.Abstracts.ISendServerFile;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Takunda on 24/07/2018.
 */

public class SendServerFile extends AsyncTask<Void, Void, JSONObject> {

    private String fileName;
    private String url;
    private ISendServerFile sendServerFile;
    private File file;

    public SendServerFile(ISendServerFile sendServerFile, String url, String fileName){
        url = url.replace(" ","%20");
        this.fileName = fileName;
        this.url=url;
        this.sendServerFile=sendServerFile;
    }

    public SendServerFile(ISendServerFile sendServerFile, String url){
        url = url.replace(" ","%20");
        this.url=url;
        this.sendServerFile=sendServerFile;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        if (this.file == null) {
            file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
        }

        System.out.println("Logging Sending Server File "+file.getAbsolutePath());
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        MultipartEntity reqEntity = new MultipartEntity();
        reqEntity.addPart("logs",new FileBody(file));

        httppost.setEntity(reqEntity);

        HttpResponse response;
        HttpEntity resEntity;
        try {
            response = httpclient.execute(httppost);
            resEntity = response.getEntity();

            if (resEntity == null) {
                return null;
            }

            String retSrc = EntityUtils.toString(resEntity);
            System.out.println("upload response "+retSrc);
            JSONObject result = new JSONObject(retSrc); //Convert String to JSON Object
            httpclient.getConnectionManager( ).shutdown( );

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    protected void onPostExecute(JSONObject data) {
        try {
            sendServerFile.postSendServerFile(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
