package net.nececity.nececity.Data;

import android.app.Activity;
import android.location.Location;

import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.BuildConfig;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Abstracts.DataSet;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import net.nececity.nececity.Objects.Employee;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 04/06/2018.
 */

public class LocationDataSet extends DataSet{

    private Location location;
    private String locationDetailUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
    private String url;
    private String routeId;
    private String companyId;
    private String country;
    private String address;
    private int mileage;

    // These are used for crowdsourcing of transport
    private float speed;
    // This is the bearing of current location from previous one
    private float direction;

    private boolean googleDataSet;

    // Contains extra details that might be useful.
    private HashMap<String,String> details = new HashMap<>();

    public LocationDataSet(IGetDataSet getDataSet, Location location){
        super(getDataSet);
        this.location=location;
    }

    public void sendLocation(){

        if (location == null){
            return;
        }

        String timeStamp = new SimpleDateFormat(AppVariables.timeFormat).format(Calendar.getInstance().getTime());

        List<NameValuePair> params = new ArrayList<>();

        params.add(new BasicNameValuePair("lastSeen", timeStamp));
        params.add(new BasicNameValuePair("latitude", "" + location.getLatitude()));
        params.add(new BasicNameValuePair("longitude", "" + location.getLongitude()));

        if (routeId != null){
            params.add(new BasicNameValuePair("routeId", routeId));
        }

        if (direction != 0){
            params.add(new BasicNameValuePair("direction", ""+direction));
        }

        if (companyId != null){
            params.add(new BasicNameValuePair("companyId", companyId));
        }

        if (country != null){
            params.add(new BasicNameValuePair("country", country));
        }

        if (address != null){
            params.add(new BasicNameValuePair("location", address));
        }

        if (mileage > 0){
            params.add(new BasicNameValuePair("mileage", ""+mileage));
        }

        if (AppVariables.user instanceof Employee) {
            params.add(new BasicNameValuePair("employeeID", AppVariables.user.getId()));
            params.add(new BasicNameValuePair("companyID", AppVariables.getEmployee().getCompanyId()));
            //url = UrlConstructor.editEmployeeProfileUrl();
            url = setForGpsServiceTesting(params);
        }else{
            params.add(new BasicNameValuePair("userID", AppVariables.user.getId()));
            url = UrlConstructor.editUserProfileUrl();
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.PATCH);
        getServerData.execute();
    }

    private String setForGpsServiceTesting(List<NameValuePair> params){
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        //This is for GPS service testing
        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
            params.add(new BasicNameValuePair("id", AppVariables.getDeviceId((Activity)AppVariables.context)));
            params.add(new BasicNameValuePair("time", timeStamp));
            params.add(new BasicNameValuePair("speed", "60"));
            params.add(new BasicNameValuePair("bearing", "40"));
            params.add(new BasicNameValuePair("info", "location"));
        }
        return UrlConstructor.getGpsServiceUrl();
    }

    @Override
    public void getDataSet(){
        googleDataSet = true;
        locationDetailUrl = locationDetailUrl+location.getLatitude()+","+location.getLongitude() +"&sensor=true";

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(locationDetailUrl);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        if (json.has("results")){
            googleDataSet = true;
            prepareGoogleDataSet(json);
        }else{
            googleDataSet = false;
            prepareAppDataSet(json);
        }
    }

    public void prepareAppDataSet(CustomJSONObject json) throws JSONException{
        if (json.has("logout")){
            String logout = json.getString("logout");
            if (logout.equalsIgnoreCase("true")){
                if (AppVariables.user instanceof Employee){
                    AppVariables.mainActivity.logout();
                }
            }
        }
    }

    public void prepareGoogleDataSet(CustomJSONObject json) throws JSONException {
        CustomJSONArray results = json.getJSONArray("results");
        CustomJSONObject c = results.getJSONObject(1);

        CustomJSONArray addressComponents = c.getJSONArray("address_components");
        String route = "";

        /* This was for the routes for street view*/
        for (int i = 0; i < addressComponents.length(); i++) {
            CustomJSONObject add = addressComponents.getJSONObject(i);
            CustomJSONArray types = add.getJSONArray("types");

            //System.out.println("addr comps "+types.getString(0)+" "+add.getString("long_name"));
            if (types.getString(0).equals("route")){
                System.out.println("route "+ add.getString("long_name"));
                route = add.getString("long_name");
            }else if (types.getString(0).equals("political") && types.getString(1).equals("sublocality")){
                String sublocality = add.getString("long_name");
                String sublocalityLevel = types.getString(2);
                details.put(sublocalityLevel,sublocality);
            }else if (types.getString(0).contains("administrative")){
                System.out.println(types.getString(0)+" "+add.getString("long_name"));
                details.put(types.getString(0),add.getString("long_name"));
            }else if (types.getString(0).equals("country")) {
                country = add.getString("long_name");
            }
        }

        address = route +", "+c.getString("formatted_address");
    }

    public boolean isGoogleDataSet() {
        return googleDataSet;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDirection() {
        return direction;
    }

    public void setDirection(float direction) {
        this.direction = direction;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }
}
