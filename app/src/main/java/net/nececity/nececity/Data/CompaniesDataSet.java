package net.nececity.nececity.Data;

import android.location.Location;
import android.util.Log;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import net.nececity.nececity.Data.CustomJSONArray;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Takunda on 17/06/2018.
 */

public class CompaniesDataSet extends DataSet {

    private String url;
    private String userId;
    HashMap<String,Company> companies = new HashMap<>();
    
    public CompaniesDataSet(IGetDataSet getDataSet, String userId) {
        super(getDataSet);
        this.userId=userId;
    }

    @Override
    public void getDataSet() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userID", userId));

        if (url == null) {
            url = UrlConstructor.getAllCompaniesUrl(userId);
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.GET);
        getServerData.execute();
    }

    @Override
    public void postGetServerData(CustomJSONObject json) throws JSONException {
        System.out.println("before companies prep");
        super.postGetServerData(json);
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        System.out.println("start companies prep");
        if (json.has("following")) {
            try {
                loopCompanies(json.getJSONArray("following"));
            }catch (Exception ex){}
        }

        if (json.has("notFollowing")) {
            try {
                loopCompanies(json.getJSONArray("notFollowing"));
            }catch (Exception ex){}
        }

        if (json.has("companies")) {
            try {
                loopCompanies(json.getJSONArray("companies"));
            }catch (Exception ex){}
        }
    }

    public void loopCompanies (CustomJSONArray companiesJsonArr) throws Exception{

        for (int i = 0; i < companiesJsonArr.length(); i++) {
            CustomJSONObject c = companiesJsonArr.getJSONObject(i);

            System.out.println(c);

            String companyID = c.getString("companyId");
            String companyName = c.getString("companyName");
            String profilePicture = c.getString("profilePicture");
            String phone = c.getString("phoneNumber");
            String address = c.getString("address");
            String about = c.getString("about");
            String type = c.getString("type");
            String email = c.getString("email");
            String business = c.getString("sector");
            String blocked = c.getString("blocked");
            String logTime = c.getString("logTime");
            String deliveries = c.getString("deliveries");
            String following = c.getString("following");

            Company company = new Company(companyID);
            company.setName(companyName);
            company.setPictureUrl(profilePicture);
            company.setAbout(about);
            company.setAddress(address);
            company.setPhoneNumber(phone);
            company.setBusiness(business);
            company.setType(type);
            company.setEmail(email);
            company.setBlocked(blocked);

            if (logTime.equalsIgnoreCase("true")) {
                company.setLogTime(true);
            }else{
                company.setLogTime(false);
            }

            if (deliveries != null && deliveries.equalsIgnoreCase("true")) {
                company.setDeliveries(true);
            }else{
                company.setDeliveries(false);
            }

            try{
                CustomJSONObject loc = c.getJSONObject("location");
                String placeLatitude = loc.getString("latitude");
                String placeLongitude = loc.getString("longitude");

                Location location = new Location("");
                location.setLatitude(Double.parseDouble(placeLatitude));
                location.setLongitude(Double.parseDouble(placeLongitude));

                company.setLocation(location);
                company.setHasLocation(true);
            }catch (Exception ex){}

            try{
                CustomJSONArray routesJson = c.getJSONArray("routes");

                for (int r = 0; r < routesJson.length(); r++) {
                    CustomJSONObject obj = routesJson.getJSONObject(r);
                    Route route = prepareRoute(company, obj);
                    if (!company.getRoutes().containsKey(route.getId())) {
                        company.getRoutes().put(route.getId(), route);
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }

            try{
                CustomJSONObject h = c.getJSONObject("businessHours");

                company.getWorkingHours().put("mon", Arrays.asList(h.getString("mon").split(",")));
                company.getWorkingHours().put("tue",Arrays.asList(h.getString("tue").split(",")));
                company.getWorkingHours().put("wed",Arrays.asList(h.getString("wed").split(",")));
                company.getWorkingHours().put("thu",Arrays.asList(h.getString("thu").split(",")));
                company.getWorkingHours().put("fri",Arrays.asList(h.getString("fri").split(",")));
                company.getWorkingHours().put("sat",Arrays.asList(h.getString("sat").split(",")));
                company.getWorkingHours().put("sun",Arrays.asList(h.getString("sun").split(",")));

                System.out.println(companyID+" has hours");

            }catch (Exception ex){
                System.out.println(companyID+" has no business hours");
            }

            if (following.equals("true")){
                System.out.println("user is following "+company.getId());
                company.setFollowing(true);
            }

            company.insertSearchItem();
            companies.put(company.getId(),company);
        }
        System.out.println("stop companies prep");
    }

    private Route prepareRoute(Company company, CustomJSONObject routeJson) throws JSONException{
        CustomJSONArray busStopPointsJson = routeJson.getJSONArray("busStopPoints");
        RouteDataSet rd = new RouteDataSet(new IGetDataSet() {
            @Override
            public void postGetDataSet(DataSet dataSet) {

            }
        });
        Route route = rd.prepareRoute(company, routeJson);
        if (busStopPointsJson != null) {
            LinkedHashMap<String, RoutePoint> busStopPoints = rd.getRoutePoints(route, busStopPointsJson);
            route.setBusStopPoints(busStopPoints);
        }

        return route;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, Company> getCompanies() {
        return companies;
    }

    public void setCompanies(HashMap<String, Company> companies) {
        this.companies = companies;
    }
}
