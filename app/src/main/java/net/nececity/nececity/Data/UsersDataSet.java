package net.nececity.nececity.Data;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import net.nececity.nececity.Abstracts.DataSet;

import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.User;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import net.nececity.nececity.Data.CustomJSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 02/06/2018.
 */

public class UsersDataSet extends DataSet {

    private String url;
    private String userId;
    HashMap<String,User> users = new HashMap<>();

    public UsersDataSet(IGetDataSet getDataSet, String userId) {
        super(getDataSet);
        this.userId=userId;
    }

    @Override
    public void getDataSet() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userID", userId));

        if (url == null) {
            url = UrlConstructor.getUserFriendsUrl(userId);
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.GET);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        CustomJSONArray usersJSONArray = null;

        if (json.has("users")) {
            usersJSONArray = json.getJSONArray("users");
        }

        if (usersJSONArray == null){
            success = false;
            message = "No users found";
        }

        for (int i = 0; i < usersJSONArray.length(); i++) {
            CustomJSONObject c = (CustomJSONObject)usersJSONArray.getJSONObject(i);
            System.out.println(c);

            String userId = c.getString("userId");
            String userName = c.getString("name");
            String fbName = c.getString("fbName");
            String latitudeStr = c.getString("latitude");
            String longitudeStr = c.getString("longitude");
            String profilePicture = c.getString("profilePicture");
            String alertLocation = "";
            String phoneNumber;
            String visibility="";
            String setBy="";
            String lastSeen;
            String status;
            String alertSeen="";
            String location;

            double latitude;
            double longitude;

            try{
                CustomJSONObject alertJson = c.getJSONObject("alert");
                alertLocation = alertJson.getString("location");
                alertSeen = alertJson.getString("seen");
            }catch (Exception ex){
                System.out.println(userId + " has no alert");
            }

            try{
                CustomJSONObject visibilityJson = c.getJSONObject("visibility");
                visibility = visibilityJson.getString("visible");
                setBy = visibilityJson.getString("setBy");
            }catch (Exception ex){
                System.out.println("visibility not set");
            }

            phoneNumber = c.getString("phoneNumber");
            lastSeen = c.getString("lastSeen");
            status = c.getString("status");
            location = c.getString("location");

            System.out.println(userId);

            User user = new User(userName);
            user.setId(userId);
            user.setFbName(fbName);
            user.setId(userId);
            user.setVisibility(visibility);
            user.setVisibilitySetBy(setBy);
            user.setLastSeen(lastSeen);
            user.setStatus(status);
            user.setPictureUrl(profilePicture);
            user.setLocationDetail(location);

            try{
                latitude = Double.parseDouble(latitudeStr);
                longitude = Double.parseDouble(longitudeStr);

                Location userLocation = new Location("");
                userLocation.setLatitude(latitude);
                userLocation.setLongitude(longitude);

                user.setLocation(userLocation);
            }catch(Exception ex){
                System.out.println("has no location yet");
            }

            if (alertLocation != null && !alertLocation.equals("") && !alertLocation.equals("null")){
                System.out.println("alert seen "+alertSeen);
                user.setOnAlert("true");
                if (alertSeen.equals("true")){
                    user.setAlertSeen(true);
                }
            }

            if (phoneNumber != null && !phoneNumber.equals("") ){
                user.setPhoneNumber(phoneNumber);
            }

            users.put(userId, user);
        }
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, User> getUsers() {
        return users;
    }

    public void setUsers(HashMap<String, User> users) {
        this.users = users;
    }
}
