package net.nececity.nececity.Data;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.Objects.Comment;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 8/28/2018.
 */

public class CataloguePostsDataSet extends DataSet{

    private HashMap<String,HashMap<String,CataloguePost>> CataloguePosts = new HashMap<>();
    private String url;
    private EHttpMethod httpMethod = EHttpMethod.GET;
    private List<NameValuePair> params = new ArrayList<>();
    private String companyId;

    public CataloguePostsDataSet(IGetDataSet getDataset, String companyId) {
        super(getDataset);
        this.companyId=companyId;
    }

    public HashMap<String,HashMap<String,CataloguePost>> getCataloguePosts() {
        return CataloguePosts;
    }

    @Override
    public void getDataSet(){
        String url = UrlConstructor.getCataloguePostsUrl(companyId);
        params.add(new BasicNameValuePair("userID", AppVariables.user.getId()));

        if (this.url != null){
            url = this.url;
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(httpMethod);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {

        CustomJSONArray CataloguePostsJSONArray = json.getJSONArray("posts");

        if (CataloguePostsJSONArray.length() == 0){
            return;
        }

        for (int i = 0; i < CataloguePostsJSONArray.length(); i++) {

            HashMap<String, CataloguePost> companyCataloguePosts = new HashMap<>();

            CustomJSONObject postJSONObject = CataloguePostsJSONArray.getJSONObject(i);
            CustomJSONArray commentsJSONArray = postJSONObject.getJSONArray("comments");
            CustomJSONArray pictureUrlsJSONArray = postJSONObject.getJSONArray("pictureUrls");

            ArrayList<Comment> postComments = new ArrayList<>();
            ArrayList<String> pictureUrls = new ArrayList<>();

            String postId = postJSONObject.getString("postId");
            String pictureUrl = postJSONObject.getString("pictureUrl");
            String fileUrl = postJSONObject.getString("fileUrl");
            String title = postJSONObject.getString("title");
            String postDetail = postJSONObject.getString("post");
            String company=postJSONObject.getString("companyId");
            String time=postJSONObject.getString("time");
            String seen=postJSONObject.getString("seen");
            String liked=postJSONObject.getString("liked");
            String amenities=postJSONObject.getString("amenities");
            String price=postJSONObject.getString("price");
            String units=postJSONObject.getString("units");
            String duration=postJSONObject.getString("duration");

            String likes = "0";

            if (CataloguePosts.containsKey(company)){
                companyCataloguePosts = CataloguePosts.get(company);
            }else{
                CataloguePosts.put(company,companyCataloguePosts);
            }

            if (postJSONObject.has("likes")) {
                likes = postJSONObject.getString("likes");
            }

            CataloguePost post = new CataloguePost(postId);
            post.setPictureUrl(pictureUrl);
            post.setFileUrl(fileUrl);
            post.setTitle(title);
            post.setPost(postDetail);
            post.setCompanyId(company);
            post.setTime(time);
            post.setSeen(seen);
            post.setLiked(liked);
            post.setLikes(likes);
            post.setAmenities(amenities);
            post.setDuration(duration);
            post.setPrice(price);
            post.setUnits(units);

            companyCataloguePosts.put(postId,post);

            for (int j = 0; j < commentsJSONArray.length(); j++) {
                CustomJSONObject cm = commentsJSONArray.getJSONObject(j);

                String commentId = cm.getString("commentId");
                String userID = cm.getString("userId");
                String commentStr = cm.getString("comment");
                String commentCompany=cm.getString("companyId");
                String commentTime=cm.getString("time");

                Comment comment = new Comment(commentId);
                comment.setUserID(userID);
                comment.setComment(commentStr);
                comment.setCompany(commentCompany);
                comment.setTime(commentTime);

                postComments.add(comment);
            }
            post.setComments(postComments);

            for (int j = 0; j < pictureUrlsJSONArray.length(); j++) {
                String url = pictureUrlsJSONArray.getString(j);
                pictureUrls.add(url);
            }
            post.setPictureUrls(pictureUrls);
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    public void setParams(List<NameValuePair> params) {
        this.params = params;
    }

    public EHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }
}
