package net.nececity.nececity.Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Takunda on 11/07/2018.
 */

public class CustomJSONArray extends JSONArray {

    public CustomJSONArray(JSONArray array) throws JSONException {
        super(array.toString());
    }

    @Override
    /**
     * Returns the value mapped by {@code name} if it exists, coercing it if
     * necessary, or throws if no such mapping exists.
     *
     * @throws JSONException if no such mapping exists.
     */
    public String getString(int index) throws JSONException {
        String string = super.getString(index);
        if (string.equalsIgnoreCase("null")){
            return null;
        }
        if (string.isEmpty()){
            return null;
        }
        return string;
    }

    /**
     * Returns the value mapped by {@code name} if it exists and is a {@code
     * JSONObject}, or throws otherwise.
     *
     * @throws JSONException if the mapping doesn't exist or is not a {@code
     *     JSONObject}.
     */
    public CustomJSONObject getJSONObject(int index) throws JSONException {
        JSONObject object = super.getJSONObject(index);
        CustomJSONObject customJSONObject = new CustomJSONObject(object);
        return customJSONObject;
    }
}
