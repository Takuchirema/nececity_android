package net.nececity.nececity.Data;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IGetServerData;
import net.nececity.nececity.Helpers.UrlConstructor;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 28/05/2018.
 */

public class RegisterDataSet extends DataSet implements IGetServerData {

    private String username;
    private String email;
    private String password;
    private String phonenumber;

    public RegisterDataSet(IGetDataSet getDataset, String username, String email, String password, String phonenumber){
        super(getDataset);
        this.username=username;
        this.password=password;
        this.email=email;
        this.phonenumber=phonenumber;
    }

    @Override
    public void getDataSet(){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userID", username));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("phoneNumber", phonenumber));

        String url = UrlConstructor.getNewUserUrl();

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.POST);
        getServerData.execute();
    }
}
