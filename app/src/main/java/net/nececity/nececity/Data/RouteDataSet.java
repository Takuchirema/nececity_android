package net.nececity.nececity.Data;

import android.location.Location;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IGetServerData;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.LocationCluster;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import net.nececity.nececity.Data.CustomJSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Takunda on 04/07/2018.
 */

public class RouteDataSet extends DataSet implements IGetServerData {

    private String companyId;
    private String employeeId;
    private String routeId;
    private Route route;
    private HashMap<String,Route> routes = new HashMap<>();
    private EHttpMethod httpMethod=EHttpMethod.GET;
    private List<NameValuePair> params;
    private String url;

    public RouteDataSet(IGetDataSet getDataset, String companyId, String routeId) {
        super(getDataset);
        this.companyId=companyId;
        this.routeId=routeId;
    }

    public RouteDataSet(IGetDataSet getDataset){
        super(getDataset);
    }

    @Override
    public void getDataSet(){

        if (params == null) {
            params = new ArrayList<>();
            params.add(new BasicNameValuePair("companyID", companyId));
            params.add(new BasicNameValuePair("routeID", routeId));
            params.add(new BasicNameValuePair("employeeID", employeeId));
        }

        String url = UrlConstructor.getCompanyRouteUrl(companyId,routeId);
        if (this.url != null){
            url = this.url;
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(httpMethod);
        getServerData.execute();
    }

    public void setRoute(){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("companyID", companyId));
        params.add(new BasicNameValuePair("routeID", routeId));
        params.add(new BasicNameValuePair("employeeID", employeeId));

        String url = UrlConstructor.setCompanyRouteUrl();
        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.PATCH);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        if (json.has("route")){
            CustomJSONObject routeJson = json.getJSONObject("route");
            route = prepareRoute(null, routeJson);
            routes.put(route.getId(),route);
        }

        if (json.has("routes")){

            CustomJSONArray routesJson = json.getJSONArray("routes");

            outerloop:
            for (int i = 0; i < routesJson.length(); i++) {
                CustomJSONObject r = routesJson.getJSONObject(i);
                Route route = prepareRoute(null, r);
                routes.put(route.getId(),route);
            }
        }

        if (companyId == null){
            return;
        }

        Company company = ObjectCache.getCompanies().get(companyId);
        for(Map.Entry<String, Route> e: routes.entrySet()){
            if (!company.getRoutes().containsKey(e.getKey())){
                company.getRoutes().put(e.getKey(),e.getValue());
            }
        }
    }

    public Route prepareRoute(Company company, CustomJSONObject routeJson) throws JSONException{
        CustomJSONArray routePointsJson = routeJson.getJSONArray("points");
        CustomJSONArray clustersJson = routeJson.getJSONArray("locationClusters");
        String companyId = routeJson.getString("companyId");
        String routeId = routeJson.getString("routeId");
        String routeName = routeJson.getString("routeName");

        //System.out.println("** -- "+ObjectCache.getCompanies().size()+" "+companyId);
        if (company == null) {
            company = ObjectCache.getCompanies().get(companyId);
        }

        Route route = new Route(routeId);
        route.setName(routeName);
        route.setCompanyName(company.getName());
        LinkedHashMap<String,RoutePoint> routePoints;
        HashMap<Integer, LocationCluster> locationClusters;

        route.setCompanyId(companyId);
        route.setColor(routeJson.getString("basicColor"));

        routePoints = getRoutePoints(route, routePointsJson);
        route.setRoutePoints(routePoints);

        locationClusters = getLocationClusters(clustersJson, companyId, routeId);
        route.setLocationClusters(locationClusters);

        route.insertSearchItem();
        return route;
    }

    public LinkedHashMap<String,RoutePoint> getRoutePoints(Route route, CustomJSONArray routePointsJson) throws JSONException {
        LinkedHashMap<String,RoutePoint> routePoints = new LinkedHashMap<>();

        for (int i = 0; i < routePointsJson.length(); i++) {

            CustomJSONObject c = routePointsJson.getJSONObject(i);

            String pointId = c.getString("id");
            String latitude = c.getString("latitude");
            String longitude = c.getString("longitude");
            String name = c.getString("name");
            String busStop = c.getString("busStop");

            RoutePoint routePoint = new RoutePoint(pointId);

            if (busStop.equals("true")) {
                routePoint.setBusStop(true);

                ArrayList<String> timeTable = new ArrayList<String>();
                if (c.has("timeTable")){
                    CustomJSONArray timesJson = c.getJSONArray("timeTable");

                    for (int k = 0; k < timesJson.length(); k++) {
                        String time = timesJson.getString(k);
                        timeTable.add(time);
                    }
                    routePoint.setStopTimeTable(timeTable);
                }


            }else {
                routePoint.setBusStop(false);
            }

            routePoint.setName(name);
            routePoint.setLatitude(Double.parseDouble(latitude));
            routePoint.setLongitude(Double.parseDouble(longitude));
            routePoint.setRouteId(route.getId());
            routePoint.setCompanyId(route.getCompanyId());
            routePoint.setRouteName(route.getName());

            routePoint.insertSearchItem();
            routePoints.put(pointId,routePoint);
        }

        return routePoints;
    }

    public HashMap<Integer, LocationCluster> getLocationClusters(CustomJSONArray locationClustersJson, String companyId, String routeId) throws JSONException {
        //System.out.println(routeId+": Location Clusters");
        HashMap<Integer,LocationCluster> locationClusters = new LinkedHashMap<>();

        if (locationClustersJson == null){
            return locationClusters;
        }

        //System.out.println("Cluster Size: "+locationClustersJson.length());
        for (int i = 0; i < locationClustersJson.length(); i++) {

            CustomJSONObject c = locationClustersJson.getJSONObject(i);

            int id = c.getInt("id");
            int direction = c.getInt("direction");
            int size = c.getInt("clusterSize");

            String latitude = c.getString("latitude");
            String longitude = c.getString("longitude");
            String lastSeen = c.getString("calculatedAt");

            LocationCluster cluster = new LocationCluster(id,Double.parseDouble(latitude),Double.parseDouble(longitude));
            cluster.setDirection(direction);
            cluster.setSize(size);
            cluster.setLastSeen(lastSeen);
            cluster.setCompanyId(companyId);
            cluster.setRouteId(routeId);

            cluster.insertSearchItem();
            locationClusters.put(id,cluster);
        }

        return locationClusters;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public void setEmployeeId(String employeeId){
        this.employeeId=employeeId;
    }

    public EHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    public void setParams(List<NameValuePair> params) {
        this.params = params;
    }

    public HashMap<String, Route> getRoutes() {
        return routes;
    }

    public Route getRoute() {
        return route;
    }
}
