package net.nececity.nececity.Data;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import net.nececity.nececity.Data.CustomJSONArray;
import net.nececity.nececity.Objects.ShiftSummary;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Takunda on 28/06/2018.
 */

public class EmployeesDataSet extends DataSet {

    private String companyId;
    private String url;
    private EHttpMethod httpMethod = EHttpMethod.GET;
    private List<NameValuePair> params = new ArrayList<>();;
    private HashMap<String, HashMap<String,Employee>> employees = new HashMap<>();

    public EmployeesDataSet(IGetDataSet getDataset, String companyId) {
        super(getDataset);
        this.companyId=companyId;
    }

    @Override
    public void getDataSet() {
        if (url == null) {
            url = UrlConstructor.getUserEmployeesUrl(AppVariables.user.getId());
        }

        if (params.isEmpty()) {
            params.add(new BasicNameValuePair("company", companyId));
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(httpMethod);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        CustomJSONArray employeesJson = json.getJSONArray("employees");

        for (int i = 0; i < employeesJson.length(); i++) {
            HashMap<String, Employee> companyEmployees = new HashMap<>();

            CustomJSONObject c = employeesJson.getJSONObject(i);

            String employeeName = c.getString("name");
            String id = c.getString("employeeId");

            String latitudeStr = c.getString("latitude");
            String longitudeStr = c.getString("longitude");
            String profilePicture = c.getString("profilePicture");
            String companyId = c.getString("companyId");
            String status = c.getString("status");
            String location = c.getString("location");
            String lastSeen = c.getString("lastSeen");
            String privilege = c.getString("privilege");

            String phoneNumber = c.getString("phoneNumber");
            String email = c.getString("email");
            Route route = null;
            Location userLocation = null;
            ShiftSummary shiftSummary = null;

            if (employees.containsKey(companyId)){
                companyEmployees = employees.get(companyId);
            }else{
                employees.put(companyId,companyEmployees);
            }

            try {
                route = getRoute(c.getJSONObject("route"));
            }catch (Exception ex){
                //ex.printStackTrace();
            }

            try {
                Double userLat = Double.parseDouble(latitudeStr);
                Double userLong = Double.parseDouble(longitudeStr);

                userLocation = new Location("");
                userLocation.setLatitude(userLat);
                userLocation.setLongitude(userLong);

            } catch (Exception ex) {
                //ex.printStackTrace();
            }

            if (!c.isNull("shiftSummary")) {
                shiftSummary = getShiftSummary(c.getJSONObject("shiftSummary"));
            }

            Employee employee = new Employee(employeeName);
            employee.setLocation(userLocation);
            employee.setPictureUrl(profilePicture);
            employee.setCompanyId(companyId);
            employee.setStatus(status);
            employee.setLocationDetail(location);
            employee.setRoute(route);
            employee.setPrivilege(privilege);
            employee.setLastSeen(lastSeen);
            employee.setEmail(email);
            employee.setShiftSummary(shiftSummary);

            employee.setId(id);

            if (phoneNumber != null && !phoneNumber.equals("")) {
                employee.setPhoneNumber(phoneNumber);
            }

            employee.insertSearchItem();
            companyEmployees.put(id, employee);
        }
    }

    private ShiftSummary getShiftSummary(CustomJSONObject shiftSummaryJson){
        if (shiftSummaryJson == null){
            return null;
        }

        if (shiftSummaryJson.isNull("lastStop")){
            return null;
        }

        try {
            String shiftId = shiftSummaryJson.getString("shiftId");
            String routeId = shiftSummaryJson.getString("routeId");
            String companyId = shiftSummaryJson.getString("companyId");

            ShiftSummary shiftSummary = new ShiftSummary(shiftId);

            RoutePoint lastStop = getRoutePoint(shiftSummaryJson.getJSONObject("lastStop"),null,null);
            RoutePoint nextStop = getRoutePoint(shiftSummaryJson.getJSONObject("nextStop"),null,null);

            shiftSummary.setLastStop(lastStop);
            shiftSummary.setNextStop(nextStop);

            // Update the nextStop route points arrival time
            if (companyId != null && routeId != null){
                Route route = ObjectCache.getCompanies().get(companyId).getRoutes().get(routeId);
                if (route != null){
                    RoutePoint routePoint = route.getBusStopPoints().get(nextStop.getId());
                    if (routePoint != null){
                        routePoint.setEstimatedArrivalTime(nextStop.getEstimatedArrivalTime());
                    }
                }
            }

            return shiftSummary;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Route getRoute(CustomJSONObject routeJson){
        Route route = null;
        LinkedHashMap<String,RoutePoint> routePoints = new LinkedHashMap<>();

        try{
            CustomJSONArray routePointsJson = routeJson.getJSONArray("points");

            route = new Route(routeJson.getString("routeId"));
            route.setName(routeJson.getString("routeName"));
            route.setCompanyId(routeJson.getString("companyId"));
            route.setColor(routeJson.getString("color"));

            System.out.println("length "+routePointsJson.length());

            for (int p = 0; p < routePointsJson.length(); p++) {

                CustomJSONObject routPointJson = routePointsJson.getJSONObject(p);

                RoutePoint routePoint = getRoutePoint(routPointJson,route.getId(),route.getCompanyId());

                routePoints.put(routePoint.getId(),routePoint);
            }
            route.setRoutePoints(routePoints);

        }catch (Exception ex){
            //ex.printStackTrace();
        }

        return route;
    }

    private RoutePoint getRoutePoint(CustomJSONObject routePointJson, String routeId, String companyId){
        try {
            String pointId = routePointJson.getString("id");
            RoutePoint routePoint = new RoutePoint(pointId);

            String latitude = routePointJson.getString("latitude");
            String longitude = routePointJson.getString("longitude");
            String name = routePointJson.getString("name");
            String busStop = routePointJson.getString("busStop");
            String eta = routePointJson.getString("estimatedArrival");

            if (busStop.equals("true")) {
                routePoint.setBusStop(true);

                ArrayList<String> timeTable = new ArrayList<String>();
                if (routePointJson.has("timeTable")){
                    CustomJSONArray timesJson = routePointJson.getJSONArray("timeTable");

                    for (int k = 0; k < timesJson.length(); k++) {
                        String time = timesJson.getString(k);
                        timeTable.add(time);
                    }
                    routePoint.setStopTimeTable(timeTable);
                }
            }else {
                routePoint.setBusStop(false);
            }

            routePoint.setName(name);
            routePoint.setLatitude(Double.parseDouble(latitude));
            routePoint.setLongitude(Double.parseDouble(longitude));
            routePoint.setRouteId(routeId);
            routePoint.setCompanyId(companyId);
            routePoint.setEstimatedArrivalTime(eta);

            return routePoint;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HashMap<String, HashMap<String,Employee>> getEmployees() {
        return employees;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public void setParams(List<NameValuePair> params) {
        this.params = params;
    }
}
