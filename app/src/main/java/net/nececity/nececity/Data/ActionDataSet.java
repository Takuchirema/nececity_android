package net.nececity.nececity.Data;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 23/06/2018.
 */

public class ActionDataSet extends DataSet {

    private String url;
    private String userId;
    private String companyId;
    private EActionType actionType;
    private EHttpMethod httpMethod = EHttpMethod.POST;
    private CustomJSONObject jsonObject;

    List<NameValuePair> params = new ArrayList<NameValuePair>();

    public ActionDataSet(IGetDataSet getDataSet, EActionType actionType) {
        super(getDataSet);
        this.actionType=actionType;
    }

    @Override
    public void getDataSet(){

        if (params.isEmpty()) {
            params.add(new BasicNameValuePair("userID", userId));
            params.add(new BasicNameValuePair("companyID", companyId));
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(httpMethod);
        getServerData.execute();
    }

    @Override
    public void postGetServerData(CustomJSONObject json) throws JSONException {
        jsonObject = json;
        super.postGetServerData(json);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public EHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    public void setParams(List<NameValuePair> params) {
        this.params = params;
    }

    public CustomJSONObject getJsonObject() {
        return jsonObject;
    }

    public EActionType getActionType() {
        return actionType;
    }

    public void setActionType(EActionType actionType) {
        this.actionType = actionType;
    }
}
