package net.nececity.nececity.Data;

import android.util.Log;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Comment;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 8/4/2018.
 */

public class PromotionsDataSet extends DataSet{

    private HashMap<String,HashMap<String,Promotion>> promotions = new HashMap<>();
    private HashMap<String,Promotion> promotionsToday = new HashMap<>();
    private String url;
    private EHttpMethod httpMethod = EHttpMethod.GET;
    private List<NameValuePair> params = new ArrayList<>();

    public PromotionsDataSet(IGetDataSet getDataset) {
        super(getDataset);
    }

    @Override
    public void getDataSet(){
        String url = UrlConstructor.getUserPromotionsUrl(AppVariables.user.getId());

        if (params.isEmpty()) {
            params.add(new BasicNameValuePair("userID", AppVariables.user.getId()));
        }

        if (this.url != null){
            url = this.url;
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(httpMethod);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        CustomJSONArray promotionsJSONArray = json.getJSONArray("promotions");

        for (int i = 0; i < promotionsJSONArray.length(); i++) {
            HashMap<String, Promotion> companyPromotions = new HashMap<>();

            CustomJSONObject c = promotionsJSONArray.getJSONObject(i);
            CustomJSONArray comments = c.getJSONArray("comments");
            CustomJSONArray pictureUrlsJSONArray = c.getJSONArray("pictureUrls");

            ArrayList<Comment> promotionComments = new ArrayList<Comment>();
            ArrayList<String> pictureUrls = new ArrayList<>();

            String promotionId = c.getString("promotionId");
            String pictureUrl = c.getString("pictureUrl");
            String promotionDetail = c.getString("promotion");
            String companyId = c.getString("companyId");
            String time = c.getString("time");
            String seen = c.getString("seen");
            String weekly = c.getString("weekly");
            String onceOff = c.getString("onceOff");
            String days = c.getString("days");
            String liked=c.getString("liked");
            String title=c.getString("title");
            String price=c.getString("price");
            String units=c.getString("units");
            String duration=c.getString("duration");


            if (promotions.containsKey(companyId)){
                companyPromotions = promotions.get(companyId);
            }else{
                promotions.put(companyId,companyPromotions);
            }

            String likes = "0";
            if (c.has("likes")) {
                likes = c.getString("likes");
            }

            Promotion promotion = new Promotion(promotionId);
            promotion.setPictureUrl(pictureUrl);
            promotion.setPromotion(promotionDetail);
            promotion.setCompanyId(companyId);
            promotion.setTime(time);
            promotion.setSeen(seen);
            promotion.setLikes(likes);
            promotion.setDays(days);
            promotion.setWeekly(weekly);
            promotion.setOnceOff(onceOff);
            promotion.setLiked(liked);
            promotion.setTitle(title);
            promotion.setDuration(duration);
            promotion.setPrice(price);
            promotion.setUnits(units);

            companyPromotions.put(promotionId,promotion);
            if (hasPromotionToday(promotion)){
                promotionsToday.put(promotionId,promotion);
            }

            for (int j = 0; j < comments.length(); j++) {
                JSONObject cm = comments.getJSONObject(j);

                String commentId = cm.getString("commentId");
                String userID = cm.getString("userId");
                String commentStr = cm.getString("comment");
                String commentCompany = cm.getString("companyId");
                String commentTime = cm.getString("time");

                Comment comment = new Comment(commentId);
                comment.setUserID(userID);
                comment.setComment(commentStr);
                comment.setCompany(commentCompany);
                comment.setTime(commentTime);

                promotionComments.add(comment);
            }
            promotion.setComments(promotionComments);

            for (int j = 0; j < pictureUrlsJSONArray.length(); j++) {
                String url = pictureUrlsJSONArray.getString(j);
                pictureUrls.add(url);
            }
            promotion.setPictureUrls(pictureUrls);
        }
    }

    String day;
    public boolean hasPromotionToday(Promotion promotion){
        String days = promotion.getDays();
        if (day == null) {
            SimpleDateFormat format = new SimpleDateFormat("EEE");
            day = format.format(new Date()).toLowerCase();
        }

        if (days.contains(day)){
            return true;
        }

        return false;
    }

    public HashMap<String, HashMap<String, Promotion>> getPromotions() {
        return promotions;
    }

    public void setPromotions(HashMap<String, HashMap<String, Promotion>> promotions) {
        this.promotions = promotions;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public EHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    public void setParams(List<NameValuePair> params) {
        this.params = params;
    }

    public HashMap<String, Promotion> getPromotionsToday() {
        return promotionsToday;
    }

    public void setPromotionsToday(HashMap<String, Promotion> promotionsToday) {
        this.promotionsToday = promotionsToday;
    }
}
