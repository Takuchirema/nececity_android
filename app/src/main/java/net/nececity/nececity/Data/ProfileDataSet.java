package net.nececity.nececity.Data;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Helpers.UrlConstructor;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Takunda on 11/07/2018.
 */

public class ProfileDataSet extends DataSet {

    private List<NameValuePair> params = new ArrayList<NameValuePair>();
    private String url;

    public ProfileDataSet(IGetDataSet getDataset) {
        super(getDataset);
    }

    public void updateDataSet(){

        if (url == null){
            url = UrlConstructor.editUserProfileUrl();
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(EHttpMethod.PATCH);
        getServerData.execute();
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    public void setParams(List<NameValuePair> params) {
        this.params = params;
    }

    public void setUrl(String url){
        this.url=url;
    }
}
