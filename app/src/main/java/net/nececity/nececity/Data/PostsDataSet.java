package net.nececity.nececity.Data;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Comment;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Post;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 05/07/2018.
 */

public class PostsDataSet extends DataSet {

    private HashMap<String,HashMap<String,Post>> posts = new HashMap<>();
    private Post latestPost;
    private String url;
    private EHttpMethod httpMethod = EHttpMethod.GET;
    private List<NameValuePair> params = new ArrayList<NameValuePair>();

    private boolean unseenPosts;

    public PostsDataSet(IGetDataSet getDataset) {
        super(getDataset);
    }

    public HashMap<String,HashMap<String,Post>> getPosts() {
        return posts;
    }

    @Override
    public void getDataSet(){
        String url = UrlConstructor.getUserPostsUrl(AppVariables.user.getId());

        if (params.isEmpty()) {
            params.add(new BasicNameValuePair("userID", AppVariables.user.getId()));
        }

        if (this.url != null){
            url = this.url;
        }

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setParameters(params);
        getServerData.setHttpMethod(httpMethod);
        getServerData.execute();
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {

        CustomJSONArray postsJSONArray = json.getJSONArray("posts");

        if (postsJSONArray.length() == 0){
            return;
        }

        for (int i = 0; i < postsJSONArray.length(); i++) {

            HashMap<String, Post> companyPosts = new HashMap<>();

            CustomJSONObject postJSONObject = postsJSONArray.getJSONObject(i);
            CustomJSONArray commentsJSONArray = postJSONObject.getJSONArray("comments");
            CustomJSONArray pictureUrlsJSONArray = postJSONObject.getJSONArray("pictureUrls");

            ArrayList<Comment> postComments = new ArrayList<>();
            ArrayList<String> pictureUrls = new ArrayList<>();

            String postId = postJSONObject.getString("postId");
            String pictureUrl = postJSONObject.getString("pictureUrl");
            String postDetail = postJSONObject.getString("post");
            String companyId=postJSONObject.getString("companyId");
            String time=postJSONObject.getString("time");
            String seen=postJSONObject.getString("seen");
            String liked=postJSONObject.getString("liked");

            String likes = "0";

            if (posts.containsKey(companyId)){
                companyPosts = posts.get(companyId);
            }else{
                posts.put(companyId,companyPosts);
            }

            if (postJSONObject.has("likes")) {
                likes = postJSONObject.getString("likes");
            }

            Post post = new Post(postId);
            post.setPictureUrl(pictureUrl);
            post.setPost(postDetail);
            post.setCompanyId(companyId);
            post.setTime(time);
            post.setSeen(seen);
            post.setLiked(liked);
            post.setLikes(likes);

            if (latestPost == null){
                latestPost = post;
            }else{
                if (latestPost.getDate().before(post.getDate())){
                    latestPost = post;
                }
            }

            companyPosts.put(postId,post);

            for (int j = 0; j < commentsJSONArray.length(); j++) {
                CustomJSONObject cm = commentsJSONArray.getJSONObject(j);

                String commentId = cm.getString("commentId");
                String userID = cm.getString("userId");
                String commentStr = cm.getString("comment");
                String commentCompany=cm.getString("companyId");
                String commentTime=cm.getString("time");

                Comment comment = new Comment(commentId);
                comment.setUserID(userID);
                comment.setComment(commentStr);
                comment.setCompany(commentCompany);
                comment.setTime(commentTime);

                postComments.add(comment);
            }
            post.setComments(postComments);

            for (int j = 0; j < pictureUrlsJSONArray.length(); j++) {
                String url = pictureUrlsJSONArray.getString(j);
                pictureUrls.add(url);
            }
            post.setPictureUrls(pictureUrls);
        }

        if (!latestPost.isSeen()){
            System.out.println("unseen posts");
            unseenPosts=true;
        }else{
            System.out.println("seen posts");
        }
    }

    public boolean unseenPosts(){
        return unseenPosts;
    }

    public Post getLatestPost() {
        return latestPost;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    public void setParams(List<NameValuePair> params) {
        this.params = params;
    }

    public EHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }
}
