package net.nececity.nececity.Data;

import android.os.AsyncTask;

import net.nececity.nececity.Abstracts.IGetServerFile;
import net.nececity.nececity.Helpers.AppVariables;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Takunda on 7/27/2018.
 */

public class GetServerFile extends AsyncTask<String, String, Integer> {

    private String fileUrl;
    private IGetServerFile getServerFile;
    private InputStream inputStream;
    private File file;
    private String extension;

    public GetServerFile(IGetServerFile getServerFile, String extension){
        this.getServerFile=getServerFile;
        this.extension=extension;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(String... args) {
        try {

            if (getSavedFile() != null){
                return 1;
            }

            System.out.println("url is "+fileUrl);
            URL url = new URL(fileUrl);
            URLConnection connection = url.openConnection();
            connection.connect();
            System.out.println("getting files 2 ");

            inputStream = new BufferedInputStream(url.openStream(), 8192);

            file = new File(AppVariables.mainActivity.getContext().getFilesDir(),getFileName());
            if(file.exists()) {
                file.delete();
            }
            file.createNewFile();

            inputStreamToFile(inputStream,file);

            System.out.println("getting files 3");
            return 1;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getFileName(){
        if (fileUrl != null){
            String fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
            return fileName;
        }
        return null;
    }

    public File getSavedFile(){
        file = new File(AppVariables.mainActivity.getContext().getFilesDir(),getFileName());
        if(file.exists()) {
            System.out.println("got saved file!!");
            return file;
        }
        return null;
    }

    private void inputStreamToFile(InputStream in, File file) {
        OutputStream out = null;

        try {
            out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if ( out != null ) {
                    out.close();
                }
                in.close();
            }
            catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
    protected void onPostExecute(Integer success) {
        if (success == 1){
            getServerFile.postGetServerFile(file);
            System.out.println("got file "+file.getAbsolutePath());
        }else{
            getServerFile.postGetServerFile(null);
        }
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
