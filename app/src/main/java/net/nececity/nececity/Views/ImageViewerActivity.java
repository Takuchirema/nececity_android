package net.nececity.nececity.Views;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.TouchImageView;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.R;

public class ImageViewerActivity extends AppCompatActivity {

    private String objectId;
    private String companyId;
    private String pictureUrl;
    private EObjectType objectType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        String title = getIntent().getExtras().getString("title");
        actionBar.setTitle(title);

        companyId = getIntent().getExtras().getString("companyId");
        pictureUrl = getIntent().getExtras().getString("pictureUrl");

        if (getIntent().getExtras().containsKey("postId")){
            objectId = getIntent().getExtras().getString("postId");
            objectType = EObjectType.POST;
        }else if (getIntent().getExtras().containsKey("promotionId")){
            objectId = getIntent().getExtras().getString("promotionId");
            objectType = EObjectType.PROMOTION;
        }else if (getIntent().getExtras().containsKey("cataloguePostId")){
            objectId = getIntent().getExtras().getString("cataloguePostId");
            objectType = EObjectType.CATALOGUE_POST;
        }

        setImage();
    }

    private void setImage(){
        ImageView imageView = (TouchImageView)findViewById(R.id.image);
        ViewManager viewManager = new ViewManager(imageView, EActionType.DOWNLOAD_PICTURE,this);
        viewManager.getPicture();

        viewManager.setUrl(pictureUrl);
        viewManager.setObject(this);
        viewManager.getPicture();
    }
}
