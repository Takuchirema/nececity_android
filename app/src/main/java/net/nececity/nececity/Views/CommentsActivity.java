package net.nececity.nececity.Views;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EPrivilege;
import net.nececity.nececity.Abstracts.ICommentActivity;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ColourCodes;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.Objects.Comment;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.R;
import net.nececity.nececity.Managers.ViewManager;

import java.util.ArrayList;

public class CommentsActivity extends AppCompatActivity implements ICommentActivity{

    private String postId;
    private String promotionId;
    private String cataloguePostId;
    private String companyId;
    private static LayoutInflater layoutInflater;
    private LinearLayout commentsView;
    private ImageButton postCommentIB;
    private Post post;
    private CataloguePost cataloguePost;
    private Promotion promotion;
    private EditText commentET;
    private CommentsActivity commentsActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Comments");
        actionBar.setDisplayHomeAsUpEnabled(true);

        commentsActivity = this;

        commentsView = findViewById(R.id.comments);
        postCommentIB = findViewById(R.id.postComment);
        commentET = findViewById(R.id.commentText);

        layoutInflater = (LayoutInflater) this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        TextView expTv1 = findViewById(R.id.postText);

        companyId = getIntent().getExtras().getString("companyId");
        if (getIntent().getExtras().containsKey("postId")) {
            postId = getIntent().getExtras().getString("postId");
            post = ObjectCache.getCompanies().get(companyId).getPosts().get(postId);
            expTv1.setText(post.getPost());
            addComments(post.getComments());
        }else if (getIntent().getExtras().containsKey("cataloguePostId")) {
            cataloguePostId = getIntent().getExtras().getString("cataloguePostId");
            cataloguePost = ObjectCache.getCompanies().get(companyId).getCatalogue().get(cataloguePostId);
            expTv1.setText(cataloguePost.getPost());
            addComments(cataloguePost.getComments());
        }else if (getIntent().getExtras().containsKey("promotionId")) {
            promotionId = getIntent().getExtras().getString("promotionId");
            promotion = ObjectCache.getCompanies().get(companyId).getPromotions().get(promotionId);
            expTv1.setText(promotion.getPromotion());
            addComments(promotion.getComments());
        }

        setListeners();
    }

    public void setListeners(){
        postCommentIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EHttpMethod httpMethod = EHttpMethod.POST;
                EActionType actionType = EActionType.COMMENT_ON_POST;
                ViewManager viewManager = new ViewManager(commentsView,actionType,commentsActivity);
                String userName = AppVariables.user.getName();

                if (AppVariables.user instanceof Employee){
                    if (AppVariables.getEmployee().isAdmin() || AppVariables.getEmployee().isSuperUser()){
                        userName = EPrivilege.ADMIN.toString();
                    }
                }

                String comment = commentET.getText().toString();
                commentET.setText("");
                if (comment.isEmpty()){
                    return;
                }

                String url = UrlConstructor.postUserCommentUrl(userName,companyId,postId);
                viewManager.setObject(post);

                if (promotionId != null){
                    url = UrlConstructor.postUserPromotionCommentUrl(userName,companyId,promotionId);
                    viewManager.setObject(promotion);
                }else if (cataloguePostId != null){
                    url = UrlConstructor.postUserCatalogueCommentUrl(userName,companyId,cataloguePostId);
                    viewManager.setObject(cataloguePost);
                }

                viewManager.setViewActivity(commentsActivity);
                viewManager.setUrl(url);
                viewManager.setHttpMethod(httpMethod);
                viewManager.commentOnPost(comment);
            }
        });
    }

    public void addComments(ArrayList<Comment> comments){

        for (Comment comment:comments){
            addComment(comment, commentsView);
        }
    }

    @Override
    public void addComment(Comment comment, LinearLayout commentsView){
        int numColours = ColourCodes.colourCodes.length;

        View commentView = layoutInflater.inflate(R.layout.content_comment,commentsView,false);

        TextView commentor = commentView.findViewById(R.id.commentor);
        String color = ColourCodes.colourCodes[Math.abs(comment.getUserID().hashCode()%numColours)];
        GradientDrawable background = (GradientDrawable)commentor.getBackground();
        background.setColor(Color.parseColor(color));

        TextView commentTime = commentView.findViewById(R.id.commentTime);
        TextView commentDetail = commentView.findViewById(R.id.comment);

        if (comment.getUserID().equalsIgnoreCase(EPrivilege.ADMIN.toString())){
            commentor.setText("ADMIN");
            background.setColor(R.attr.colorPrimary);
        }else{
            commentor.setText(comment.getUserID());
        }

        commentTime.setText(AppVariables.formatTime(comment.getTime()));
        commentDetail.setText(comment.getComment());

        commentsView.addView(commentView);
        commentsView.invalidate();
    }
}
