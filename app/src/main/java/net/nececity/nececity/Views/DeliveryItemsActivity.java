package net.nececity.nececity.Views;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import net.nececity.nececity.Helpers.ListAdapter;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Objects.Delivery;
import net.nececity.nececity.Objects.DeliveryItem;
import net.nececity.nececity.R;

import java.util.ArrayList;
import java.util.HashMap;

public class DeliveryItemsActivity extends AppCompatActivity {

    private String deliveryId;
    private String companyId;
    private ListAdapter adapter;
    private static ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_items);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");

        createListAdapter();
    }

    public void createListAdapter(){

        Delivery delivery = ObjectCache.getDeliveries().get(deliveryId);
        HashMap<String, DeliveryItem> deliveryItems =  delivery.getDeliveryItems();

        ArrayList<String> deliveryItemsIds = new ArrayList<>(deliveryItems.keySet());
        adapter = new ListAdapter(this,ImageViewerActivity.class, deliveryItemsIds, R.layout.delivery_list_view);
        adapter.setMapObjects(deliveryItems);

        listView.setAdapter(adapter);
    }
}
