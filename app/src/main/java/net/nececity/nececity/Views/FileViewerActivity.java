package net.nececity.nececity.Views;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.github.barteksc.pdfviewer.PDFView;

import net.nececity.nececity.Abstracts.IGetServerFile;
import net.nececity.nececity.Managers.FileManager;
import net.nececity.nececity.R;

import java.io.File;

public class FileViewerActivity extends AppCompatActivity implements IGetServerFile{

    private String fileUrl;
    private PDFView pdfView;
    private FileManager fileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_viewer);

        fileUrl = getIntent().getExtras().getString("fileUrl");
        pdfView = findViewById(R.id.pdfView);

        fileManager = new FileManager();
        fileManager.getServerFile(this,fileUrl,"pdf");
    }

    private void setViewer(File file){
        System.out.println("set viewer: "+fileUrl);
        pdfView.fromFile(file).load();
    }

    @Override
    public void postGetServerFile(File file) {
        setViewer(file);
    }
}
