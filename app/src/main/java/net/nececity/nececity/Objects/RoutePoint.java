package net.nececity.nececity.Objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.view.View;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.Marker;
import com.lapism.searchview.widget.SearchItem;

import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.ESearchItem;
import net.nececity.nececity.Abstracts.IMapSearchItem;
import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ColourCodes;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RescaleBitmap;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.R;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Takunda on 28/06/2018.
 */

public class RoutePoint extends ListObject implements Serializable, IMapSearchItem{

    private MapMarker mapMarker;
    private Circle circle;
    private String id;
    private String routeId;
    private String routeName;
    private String companyId;
    private String name;
    private double latitude;
    private double longitude;
    private boolean busStop = false;
    private TimeLog arrivalTimeLog;
    private TimeLog departureTimeLog;

    private String estimatedArrivalTime;
    //is the distance between stop and location when the time was logged
    private float logDistance;

    private ArrayList<String> stopTimeTable = new ArrayList<String>();

    private static HashMap<String, SearchItem> searchItems = new HashMap<>();

    public RoutePoint(MapMarker mapMarker, String id){
        Marker marker= mapMarker.getMarker();
        this.mapMarker = mapMarker;
        this.id = id;
        this.latitude = marker.getPosition().latitude;
        this.longitude= marker.getPosition().longitude;
    }

    public RoutePoint(String id){
        this.id = id;
    }

    /* Proximity is in metres */
    public boolean isApproaching(Location currentLocation, Location previousLocation, int proximity){

        if (currentLocation == null || previousLocation == null){
            return false;
        }

        Location pointLocation = new Location("Route Point");
        pointLocation.setLatitude(latitude);
        pointLocation.setLongitude(longitude);

        float distance = distanceBearingBetween(pointLocation,currentLocation,false);
        float prevDistance = distanceBearingBetween(pointLocation,previousLocation,false);

        //must check if bus is moving towards or away from stop
        if (distance < proximity && distance <= prevDistance){
            return true;
        }

        return false;
    }

    public static float distanceBearingBetween(Location locationA,Location locationB,boolean bearing){

        float result;
        if (locationA == null || locationB == null){
            return 0;
        }

        if (!bearing) {
            result = locationA.distanceTo(locationB);
        }
        else{
            result = locationA.bearingTo(locationB);
        }

        return result;
    }

    public float distanceFrom(Location location){
        Location pointLocation = new Location("Route Point");
        pointLocation.setLatitude(latitude);
        pointLocation.setLongitude(longitude);

        float distance = distanceBearingBetween(pointLocation,location,false);

        return distance;
    }

    public String getFormattedETA(){
        String formattedTime = AppVariables.formatTime(estimatedArrivalTime);
        return formattedTime;
    }

    public MapMarker getMapMarker() {
        return mapMarker;
    }

    public void setMapMarker(MapMarker mapMarker) {
        this.mapMarker = mapMarker;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        if (name == null){
            return "Point "+id;
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBusStop() {
        return busStop;
    }

    public void setBusStop(boolean busStop) {
        this.busStop = busStop;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public ArrayList<String> getStopTimeTable() {
        return stopTimeTable;
    }

    public void setStopTimeTable(ArrayList<String> stopTimeTable) {
        Collections.sort(stopTimeTable, new StringDateComparator());
        this.stopTimeTable = stopTimeTable;
    }

    public String getNextArrivalTime(Date currentTime){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date nextArrivalTime;
        String nextArrivalTimeString;

        try {

            if (currentTime == null) {
                currentTime = dateFormat.parse(dateFormat.format(new Date()));
            }

            if (stopTimeTable.size() > 0) {
                nextArrivalTimeString = stopTimeTable.get(0);
            } else {
                return null;
            }

            for (int i = 0; i < stopTimeTable.size(); i++) {
                String timeTableTime = stopTimeTable.get(i);

                Date time1 = dateFormat.parse(timeTableTime);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(time1);

                if (time1.after(currentTime)) {
                    System.out.println(timeTableTime + " is after current time");
                    return timeTableTime;
                } else {
                    System.out.println(timeTableTime + " is before current time");
                }
            }

            nextArrivalTime = new SimpleDateFormat("HH:mm").parse(nextArrivalTimeString);

            if (currentTime.after(nextArrivalTime)) {
                return "No schedule beyond "+stopTimeTable.get(stopTimeTable.size()-1);
            }
        }catch (Exception ex){
            return null;
        }

        return nextArrivalTimeString;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public float getLogDistance() {
        return logDistance;
    }

    public void setLogDistance(float logDistance) {
        this.logDistance = logDistance;
    }

    public TimeLog getArrivalTimeLog() {
        return arrivalTimeLog;
    }

    public void setArrivalTimeLog(TimeLog arrivalTimeLog) {
        this.arrivalTimeLog = arrivalTimeLog;
    }

    public TimeLog getDepartureTimeLog() {
        return departureTimeLog;
    }

    public void setDepartureTimeLog(TimeLog departureTimeLog) {
        this.departureTimeLog = departureTimeLog;
    }

    //region search item overrides
    @Override
    public void insertSearchItem() {
        if (searchItems.containsKey(id)){
            ((IMapSearchItem)searchItems.get(getId()).getObject()).removeSearchItem();
        }

        if (!isBusStop()){
            return;
        }

        SearchItem suggestion = new SearchItem(AppVariables.context);
        suggestion.setTitle(name +" - " + getRouteName());
        suggestion.setIcon1Resource(R.drawable.bus_stop);
        suggestion.setSubtitle("Bus Stop");
        suggestion.setSearchItem(ESearchItem.ROUTE_POINT);
        suggestion.setId(id);
        suggestion.setObject(this);

        searchItems.put(id,suggestion);
        CityMapManager.suggestions.add(suggestion);
        CityMapManager.refreshSearchAdapter();
    }

    @Override
    public void removeSearchItem() {
        //For now route points should continue appearing in search box
        if (searchItems.containsKey(id)){
            SearchItem suggestion = searchItems.get(id);

            CityMapManager.suggestions.remove(suggestion);
            CityMapManager.refreshSearchAdapter();
            searchItems.remove(id);
        }
    }

    @Override
    public HashMap<String, SearchItem> getSearchItems() {
        return searchItems;
    }

    @Override
    public SearchItem getSearchItem() {
        if (searchItems.containsKey(id)){
            SearchItem suggestion = searchItems.get(id);
            return suggestion;
        }
        return null;
    }

    @Override
    public void clearSearchItems() {

    }

    @Override
    public boolean clearOnMapRemoval() {
        return false;
    }
    //endregion

    class StringDateComparator implements Comparator<String>
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        public int compare(String lhs, String rhs)
        {
            try {
                return dateFormat.parse(lhs).compareTo(dateFormat.parse(rhs));
            }catch (Exception ex){
                return 1;
            }
        }
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteName(String routeName){
        this.routeName = routeName;
    }
    public String getRouteName(){
        if (routeName == null) {
            return ObjectCache.getCompanies().get(companyId).getRoutes().get(routeId).getName();
        }
        return routeName;
    }

    public Route getRoute(){
        return ObjectCache.getCompanies().get(companyId).getRoutes().get(routeId);
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getEstimatedArrivalTime() {
        return estimatedArrivalTime;
    }

    public void setEstimatedArrivalTime(String estimatedArrivalTime) {
        this.estimatedArrivalTime = estimatedArrivalTime;
    }

    //region list object overrides
    @Override
    public String getMainText(){
        return name;
    }

    @Override
    public String getSecondaryText(){
        return "Bus Stop - " + getRouteName();
    }

    @Override
    public void setViewDefaults(){

    }

    @Override
    public void setPrimaryView() {
        System.out.println("list object list set picture");
        View imageView = view.findViewById(R.id.primaryIV);
        Drawable drawable = getListDrawable();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            imageView.setBackground(drawable);
        }
    }

    @Override
    public Drawable getListDrawable(){
        Bitmap bmp;

        RescaleBitmap rescale = new RescaleBitmap(AppVariables.mainActivity.getContext(),200,200);
        bmp = BitmapFactory.decodeResource(AppVariables.mainActivity.getContext().getResources(),R.drawable.bus_stop_white);
        bmp = rescale.CropBitmap(bmp);

        int numColours= ColourCodes.colourCodes.length;
        String color = ColourCodes.colourCodes[Math.abs(companyId.hashCode()%numColours)];
        bmp=rescale.addBorder(bmp,40,color);

        Drawable drawable = new BitmapDrawable(AppVariables.mainActivity.getContext().getResources(), bmp);
        return drawable;
    }

    @Override
    public void setSecondaryView(){
        if (estimatedArrivalTime == null){
            secondaryIV.setImageResource(R.drawable.ic_access_time_white);
            secondaryIV.setBackgroundResource(R.drawable.round_orange_button);
            //adapter.notifyDataSetChanged();
            return;
        }

        primaryTV.setVisibility(View.VISIBLE);
        String eta = getFormattedETA();
        secondaryTV.setText(eta);
        //adapter.notifyDataSetChanged();
    }

    @Override
    public Company getCompany() {
        System.out.println("** route point get company **");
        Company company = ObjectCache.getCompanies().get(companyId);
        return company;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.ROUTE_POINT;
    }
    //endregion
}
