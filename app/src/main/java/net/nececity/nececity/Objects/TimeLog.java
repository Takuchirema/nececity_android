package net.nececity.nececity.Objects;

/**
 * Created by Takunda on 28/06/2018.
 */

public class TimeLog {
    private String busStop;
    private String route;
    private String time;
    private LogType logType;

    public String getBusStop() {
        return busStop;
    }

    public void setBusStop(String busStop) {
        this.busStop = busStop;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public LogType getLogType() {
        return logType;
    }

    public void setLogType(LogType logType) {
        this.logType = logType;
    }

    public enum LogType{
        ARRIVAL,
        DEPATURE
    }
}
