package net.nececity.nececity.Objects;

import net.nececity.nececity.Helpers.AppVariables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Takunda on 11/1/2019.
 */

public class ShiftSummary {
    private String shiftId;
    private String routeId;
    private String companyId;
    private RoutePoint lastStop;
    private RoutePoint nextStop;

    public ShiftSummary(String shiftId) {
        this.shiftId = shiftId;
    }

    public RoutePoint getLastStop() {
        return lastStop;
    }

    public void setLastStop(RoutePoint lastStop) {
        this.lastStop = lastStop;
    }

    public RoutePoint getNextStop() {
        return nextStop;
    }

    public void setNextStop(RoutePoint nextStop) {
        this.nextStop = nextStop;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getExpectedArrival() {
        if (nextStop == null){
            return null;
        }
        return nextStop.getEstimatedArrivalTime();
    }

    public Date getExpectedArrivalTime() {
        if (nextStop == null){
            return null;
        }

        SimpleDateFormat ft = new SimpleDateFormat (AppVariables.timeFormat);

        Date date = null;
        try {
            date = ft.parse(getExpectedArrival());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public String getFormattedETA(){
        String formattedTime = AppVariables.formatTime(getExpectedArrival());
        return formattedTime;
    }

}
