package net.nececity.nececity.Objects;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lapism.searchview.widget.SearchItem;

import net.nececity.nececity.Abstracts.EMapMarkerType;
import net.nececity.nececity.Abstracts.ESearchItem;
import net.nececity.nececity.Abstracts.IMapSearchItem;
import net.nececity.nececity.Helpers.DoubleArrayEvaluator;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RescaleBitmap;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Takunda on 23/05/2018.
 */

public class MapMarker{

    private Marker marker;
    private String objectId;
    private EMapMarkerType markerType;
    private Object object;
    private SearchItem suggestion;
    private GoogleMap map;
    private Context context;

    public MapMarker(EMapMarkerType markerType, Object object, String objectId, GoogleMap map, Context context) {
        this.markerType=markerType;
        this.object=object;
        this.objectId=objectId;
        this.map=map;
        this.context=context;
    }

    public void createMarker(){
        switch (markerType) {
            case COMPANY:
                createCompanyMarker();
                break;
            case EMPLOYEE:
                createEmployeeMarker();
                break;
            case ROUTE_POINT:
                createRoutePointMarker();
                break;
            case ROUTE_LOCATION_CLUSTER:
                createRouteClusterMarker();
        }
    }

    public void updateMarker(){
        switch (markerType) {
            case COMPANY:
                break;
            case EMPLOYEE:
                break;
            case ROUTE_POINT:
                break;
            case ROUTE_LOCATION_CLUSTER:
                updateRouteClusterMarker();
        }
    }

    public void createCompanyMarker(){
        Company company = (Company)object;

        LatLng locationLatLng = new LatLng(company.getLocation().getLatitude(),company.getLocation().getLongitude());

        RescaleBitmap rescaleBitmap = new RescaleBitmap(context,100,100);
        Bitmap bmp = rescaleBitmap.resize(company.getLocationIcon(context));

        marker = map.addMarker(new MarkerOptions()
                .position(locationLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(bmp)));

        marker.setTag(this);

        addRoutesSearchItem(company);
        addMarkerSearchItem(company.getName(),markerType.toString(), R.drawable.ic_location_city);
    }

    public void createEmployeeMarker(){
        Employee employee = (Employee)object;
        Company company = ObjectCache.getCompanies().get(employee.getCompanyId());

        if (employee.getLocation() == null){
            return;
        }

        LatLng locationLatLng = new LatLng(employee.getLocation().getLatitude(),employee.getLocation().getLongitude());

        RescaleBitmap rescaleBitmap = new RescaleBitmap(context,100,100);
        Bitmap bmp = rescaleBitmap.resize(employee.getMapIcon(context));

        marker = map.addMarker(new MarkerOptions()
                .position(locationLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(bmp)));

        marker.setTag(this);

        if (employee.getRouteId() != null) {
            addMarkerSearchItem(employee.getRouteName() + " - " + company.getName() + " - " + " Employee",employee.getName(), R.drawable.ic_directions_bus);
        }else{
            addMarkerSearchItem(company.getName() + " - " + " Employee",employee.getName(), R.drawable.ic_directions_bus);
        }
    }

    public void createRoutePointMarker(){
        RoutePoint point = (RoutePoint)object;
        Bitmap bmp = RescaleBitmap.getBitmap(context,R.drawable.route_bus_stop,70,70);

        marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(point.getLatitude(), point.getLongitude()))
                .icon(BitmapDescriptorFactory.fromBitmap(bmp))
                .anchor(0.5f, 0.5f));
        marker.setTag(this);

        addMarkerSearchItem(point.getName()+" - "+point.getRouteName(),markerType.toString(),R.drawable.bus_stop);
    }

    public void createRouteClusterMarker(){
        LocationCluster cluster = (LocationCluster) object;

        RescaleBitmap rescaleBitmap = new RescaleBitmap(context,100,100);
        Bitmap bmp = rescaleBitmap.resize(Company.getClusterIcon(context,cluster.getConcentrationColor()));

        marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(cluster.getLatitude(), cluster.getLongitude()))
                .icon(BitmapDescriptorFactory.fromBitmap(bmp))
                .anchor(0.5f, 0.5f));
        marker.setTag(this);

        addMarkerSearchItem(cluster.getRouteName() + " - cluster",markerType.toString(),R.drawable.ic_directions_bus);
    }

    public void updateRouteClusterMarker(){
        LocationCluster cluster = (LocationCluster) object;

        RescaleBitmap rescaleBitmap = new RescaleBitmap(context,100,100);
        Bitmap bmp = rescaleBitmap.resize(Company.getClusterIcon(context,cluster.getConcentrationColor()));

        marker.setIcon(BitmapDescriptorFactory.fromBitmap(bmp));
    }

    /* All search items are markers that are displayed or have been displayed before.
    *  Clicking on it will pan/zoom to that marker
    *  */
    public void addMarkerSearchItem(String title, String subtitle, int resource){
        /*suggestion = new SearchItem(context);
        suggestion.setTitle(title);
        suggestion.setIcon1Resource(resource);
        suggestion.setSubtitle(subtitle);
        suggestion.setSearchItem(ESearchItem.MARKER);
        suggestion.setObject(this);

        CityMapManager.suggestions.add(suggestion);
        CityMapManager.refreshSearchAdapter();*/
    }

    public void removeMarkerSearchItem(){
        /*if (suggestion != null){
            CityMapManager.suggestions.remove(suggestion);
            CityMapManager.refreshSearchAdapter();
        }*/
        if (object instanceof IMapSearchItem){
            IMapSearchItem searchItem = (IMapSearchItem)object;
            if (searchItem.clearOnMapRemoval()) {
                searchItem.removeSearchItem();
            }
        }
    }

    /* Adding routes that might not have a bus at the time to be searchable.*/
    public void addRoutesSearchItem(Company company){
        /*
        if (company.getRoutes().isEmpty()){
            return;
        }

        for(Map.Entry<String,Route> e: company.getRoutes().entrySet()) {
            Route route = e.getValue();
            SearchItem suggestion = new SearchItem(context);
            suggestion.setTitle(route.getName() +" - " + company.getName());
            suggestion.setIcon1Resource(R.drawable.route_small);
            suggestion.setSubtitle(company.getName()+" - Route");
            suggestion.setSearchItem(ESearchItem.ROUTE);
            suggestion.setId(route.getId());
            suggestion.setObject(this);

            CityMapManager.suggestions.add(suggestion);
        }
        CityMapManager.refreshSearchAdapter();*/
    }

    public void changeLocation(Location newLocation){
        double[] startValues = new double[]{marker.getPosition().latitude, marker.getPosition().longitude};
        double[] endValues = new double[]{newLocation.getLatitude(), newLocation.getLongitude()};
        ValueAnimator latLngAnimator = ValueAnimator.ofObject(new DoubleArrayEvaluator(), startValues, endValues);
        latLngAnimator.setDuration(2000);
        latLngAnimator.setInterpolator(new DecelerateInterpolator());
        latLngAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                double[] animatedValue = (double[]) animation.getAnimatedValue();
                marker.setPosition(new LatLng(animatedValue[0], animatedValue[1]));
            }
        });
        latLngAnimator.start();
    }

    public View getMarkerView(LayoutInflater inflater){
        View view=null;
        switch (markerType) {
            case COMPANY:
                view = getCompanyMarkerView(inflater);
                break;
            case EMPLOYEE:
                view = getEmployeeMarkerView(inflater);
                break;
            case ROUTE_LOCATION_CLUSTER:
                view = getClusterMarkerView(inflater);
                break;
            case ROUTE_POINT:
                view = getRoutePointMarkerView(inflater);
        }
        return view;
    }

    public View getCompanyMarkerView(LayoutInflater inflater){
        View markerView = inflater.inflate(R.layout.custom_marker, null);
        setCompanyMarkerView(markerView);
        return markerView;
    }

    public void setCompanyMarkerView(View markerView){
        if (markerView == null){
            return;
        }

        Company company = (Company)object;
        String companyId = company.getId();
        if (ObjectCache.getCompanies().containsKey(companyId)){
            company = ObjectCache.getCompanies().get(companyId);
        }

        TextView titleView = markerView.findViewById(R.id.title);
        TextView subTitleView = markerView.findViewById(R.id.subTitle);

        titleView.setText(company.getName());
        if (company.isOpen()){
            subTitleView.setTextColor(Color.parseColor("#00b200"));
            subTitleView.setText("Open");
        }else{
            subTitleView.setTextColor(Color.parseColor("#800000"));
            subTitleView.setText("Closed");
        }
        markerView.invalidate();
    }

    public View getEmployeeMarkerView(LayoutInflater inflater){
        View markerView = inflater.inflate(R.layout.custom_marker, null);
        setEmployeeMarkerView(markerView);
        return markerView;
    }

    public View getClusterMarkerView(LayoutInflater inflater){
        View markerView = inflater.inflate(R.layout.custom_marker, null);
        setClusterMarkerView(markerView);
        return markerView;
    }

    public void setEmployeeMarkerView(View markerView){
        if (markerView == null){
            return;
        }

        Employee employee = (Employee) object;
        String companyId = employee.getCompanyId();
        Company company = ObjectCache.getCompanies().get(companyId);

        if (company.getEmployees().containsKey(employee.getId())){
            employee = company.getEmployees().get(employee.getId());
        }

        TextView titleView = markerView.findViewById(R.id.title);
        if (employee.getRouteId() != null){
            System.out.println("map marker - route id: "+employee.getRouteId()+" route name - "+employee.getRouteName());
            titleView.setText(employee.getRouteName());
        }else {
            titleView.setText(employee.getName());
        }

        TextView subTitleView = markerView.findViewById(R.id.subTitle);
        subTitleView.setText(company.getName());

        TextView subExtraInfoView = markerView.findViewById(R.id.subExtraInfo);
        subExtraInfoView.setVisibility(View.VISIBLE);
        subExtraInfoView.setText(employee.getLastSeen());

        TextView mainExtraInfoView = markerView.findViewById(R.id.mainExtraInfo);

        if (employee.getPhoneNumber() != null){
            mainExtraInfoView.setVisibility(View.VISIBLE);
            mainExtraInfoView.setText(employee.getPhoneNumber());
        }

        markerView.invalidate();
    }

    public void setClusterMarkerView(View markerView){
        if (markerView == null){
            return;
        }

        LocationCluster cluster = (LocationCluster) object;
        String companyId = cluster.getCompanyName();
        String routeName = cluster.getRouteName();

        TextView titleView = markerView.findViewById(R.id.title);
        titleView.setText(companyId);

        TextView subTitleView = markerView.findViewById(R.id.subTitle);
        subTitleView.setText(routeName);

        TextView mainExtraInfoView = markerView.findViewById(R.id.mainExtraInfo);
        mainExtraInfoView.setVisibility(View.VISIBLE);

        TextView subExtraInfoView = markerView.findViewById(R.id.subExtraInfo);
        subExtraInfoView.setVisibility(View.VISIBLE);

        mainExtraInfoView.setText(cluster.getLastSeen());
        subExtraInfoView.setText("Concentration - "+cluster.getConcentration().toString());
        subExtraInfoView.setTextColor(Color.parseColor(cluster.getConcentrationColor()));

        markerView.invalidate();
    }

    public View getRoutePointMarkerView(LayoutInflater inflater){
        View markerView = inflater.inflate(R.layout.custom_marker, null);
        setRoutePointMarkerView(markerView);
        return markerView;
    }

    public void setRoutePointMarkerView(View markerView){
        if (markerView == null){
            return;
        }

        RoutePoint routePoint = (RoutePoint) object;
        String companyName = routePoint.getCompany().getName();
        String routeName = routePoint.getRouteName();
        String stopName = routePoint.getName();

        TextView titleView = markerView.findViewById(R.id.title);
        TextView subTitleView = markerView.findViewById(R.id.subTitle);

        titleView.setText(stopName);
        subTitleView.setTextColor(Color.parseColor("#00b200"));
        subTitleView.setText(routeName);

        TextView mainExtraInfoView = markerView.findViewById(R.id.mainExtraInfo);
        mainExtraInfoView.setVisibility(View.VISIBLE);
        mainExtraInfoView.setText(companyName);

        markerView.invalidate();
    }

    public void removeMarker(){
        marker.remove();
        removeMarkerSearchItem();
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public EMapMarkerType getMarkerType() {
        return markerType;
    }

    public void setMarkerType(EMapMarkerType markerType) {
        this.markerType = markerType;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public SearchItem getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(SearchItem suggestion) {
        this.suggestion = suggestion;
    }
}
