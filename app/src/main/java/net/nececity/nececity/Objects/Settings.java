package net.nececity.nececity.Objects;

/**
 * Created by Takunda on 26/05/2018.
 */

public class Settings {

    // Intervals are in seconds
    int usersRefreshInterval = 30;
    int messagesRefreshInterval = 30;
    int postsRefreshInterval = 30;
    int companiesRefreshInterval = 10;
    String regionVisibility;


    public int getCompaniesRefreshInterval() {
        return companiesRefreshInterval;
    }
    public void setCompaniesRefreshInterval(int companiesRefreshInterval) {
        if (companiesRefreshInterval > 0)
            this.companiesRefreshInterval = companiesRefreshInterval;
    }
    public int getUsersRefreshInterval() {
        return usersRefreshInterval;
    }
    public void setUsersRefreshInterval(int usersRefreshInterval) {
        if (usersRefreshInterval > 0)
            this.usersRefreshInterval = usersRefreshInterval;
    }
    public int getMessagesRefreshInterval() {
        return messagesRefreshInterval;
    }
    public void setMessagesRefreshInterval(int messagesRefreshInterval) {
        if (messagesRefreshInterval > 0)
            this.messagesRefreshInterval = messagesRefreshInterval;
    }
    public String getRegionVisibility() {
        return regionVisibility;
    }
    public void setRegionVisibility(String regionVisibility) {
        if (!regionVisibility.isEmpty() && !regionVisibility.equals("null"))
            this.regionVisibility = regionVisibility;
    }
    public int getPostsRefreshInterval() {
        return postsRefreshInterval;
    }
    public void setPostsRefreshInterval(int postsRefreshInterval) {
        if (postsRefreshInterval > 0)
            this.postsRefreshInterval = postsRefreshInterval;
    }
}
