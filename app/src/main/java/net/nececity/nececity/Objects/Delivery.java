package net.nececity.nececity.Objects;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.DeliveryListObject;
import net.nececity.nececity.Abstracts.EDeliveryStatus;
import net.nececity.nececity.Abstracts.EObjectType;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Takunda on 5/15/2019.
 */

public class Delivery extends DeliveryListObject{

    private int id;
    private String companyId;
    private String userId;
    private String address;
    private String total;
    private double latitude;
    private double longitude;
    private String phoneNumber;
    private String timeDelivered;
    private EDeliveryStatus deliveryStatus;

    private HashMap<String, DeliveryItem> deliveryItems = new HashMap<>();

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTimeDelivered() {
        return timeDelivered;
    }

    public Date getDateDelivered(){
        return null;
    }

    public void setTimeDelivered(String timeDelivered) {
        this.timeDelivered = timeDelivered;
    }

    public EDeliveryStatus getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        if (deliveryStatus.equalsIgnoreCase(EDeliveryStatus.PENDING.toString())){
            this.deliveryStatus = EDeliveryStatus.PENDING;
        }else if (deliveryStatus.equalsIgnoreCase(EDeliveryStatus.INTRANSIT.toString())){
            this.deliveryStatus = EDeliveryStatus.INTRANSIT;
        }else if (deliveryStatus.equalsIgnoreCase(EDeliveryStatus.DELIVERED.toString())){
            this.deliveryStatus = EDeliveryStatus.DELIVERED;
        }
    }

    public HashMap<String, DeliveryItem> getDeliveryItems() {
        return deliveryItems;
    }

    public void setDeliveryItems(HashMap<String, DeliveryItem> deliveryItems) {
        this.deliveryItems = deliveryItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public void setDeliveryStatus(EDeliveryStatus deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.DELIVERY;
    }
}
