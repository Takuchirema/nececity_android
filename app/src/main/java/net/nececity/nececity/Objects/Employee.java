package net.nececity.nececity.Objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Html;
import android.view.View;

import com.lapism.searchview.widget.SearchItem;

import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.EPrivilege;
import net.nececity.nececity.Abstracts.ESearchItem;
import net.nececity.nececity.Abstracts.EStatus;
import net.nececity.nececity.Abstracts.IMapSearchItem;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ColourCodes;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RescaleBitmap;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.R;

import java.util.HashMap;

/**
 * Created by Takunda on 23/05/2018.
 */

public class Employee extends User implements IMapSearchItem{

    private Route route;
    private String routeId;
    private String companyId;
    private Company company;
    private ShiftSummary shiftSummary;

    private static HashMap<String, SearchItem> searchItems = new HashMap<>();

    public Employee(String name){
        super(name);
    }

    public Route getRoute() {
        if (route != null && !route.isReady()){
            Route route = ObjectCache.getCompanies().get(companyId).getRoutes().get(this.route.getId());
            if (route != null){
                return route;
            }
        }
        return route;
    }

    public void setRoute(Route route) {
        if (route == null){
            return;
        }
        routeId = route.getId();

        Company company = ObjectCache.getCompanies().get(companyId);
        if (company != null && !company.getRoutes().containsKey(route.getId())){
            company.getRoutes().put(route.getId(), route);
        }

        this.route = route;
    }

    public String getRouteId() {
        if (route != null){
            return route.getId();
        }

        if (routeId == null){
            return routeId;
        }

        if (routeId.isEmpty()){
            return null;
        }

        return routeId;
    }

    public String getRouteName(){
        if (route != null){
            return route.getName();
        }

        String routeId = getRouteId();
        if (routeId == null){
            return null;
        }

        Route route = ObjectCache.getCompanies().get(companyId).getRoutes().get(routeId);
        if (route != null){
            return route.getName();
        }

        return null;
    }

    public Bitmap getMapIcon(Context context) {
        Company company = getCompany();
        String business = company.getBusiness();

        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_action_place_orange);
        int numColours= ColourCodes.colourCodes.length;

        RescaleBitmap rescale = new RescaleBitmap(context,150,150);

        if (business == null){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.other_marker);
        }else if (business.equalsIgnoreCase("Security")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.security_marker);
        }else if (business.equalsIgnoreCase("Transportation")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.transport_marker);
        }

        bmp = rescale.CropBitmap(bmp);
        String color = ColourCodes.colourCodes[Math.abs(company.getId().hashCode()%numColours)];
        bmp=rescale.addBorder(bmp,20,color);

        return bmp;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ShiftSummary getShiftSummary() {
        return shiftSummary;
    }

    public void setShiftSummary(ShiftSummary shiftSummary) {
        this.shiftSummary = shiftSummary;
    }

    public boolean isAdmin(){
        if (getPrivilege().equalsIgnoreCase(EPrivilege.ADMIN.toString())){
            return true;
        }
        return false;
    }

    public boolean isSuperUser(){
        if (getPrivilege().equalsIgnoreCase(EPrivilege.SUPER_USER.toString())){
            return true;
        }
        return false;
    }

    //region search item overrides
    @Override
    public void insertSearchItem() {
        if (searchItems.containsKey(getId())){
            ((IMapSearchItem)searchItems.get(getId()).getObject()).removeSearchItem();
        }

        if (getStatus() == EStatus.OFFLINE){
            return;
        }

        Company company = ObjectCache.getCompanies().get(companyId);

        if (!company.isFollowing()){
            return;
        }

        String title = company.getName() + " - " + " Employee";
        String subtitle = getName();

        if (getRouteId() != null) {
            title = getRouteName() + " - " + company.getName() + " - " + " Employee";
            subtitle = getName();
        }

        SearchItem suggestion = new SearchItem(AppVariables.context);
        suggestion.setTitle(title);
        suggestion.setIcon1Resource(R.drawable.ic_directions_bus);
        suggestion.setSubtitle(subtitle);
        suggestion.setSearchItem(ESearchItem.EMPLOYEE);
        suggestion.setId(getId());
        suggestion.setObject(this);

        searchItems.put(getId(),suggestion);
        CityMapManager.suggestions.add(suggestion);
        CityMapManager.refreshSearchAdapter();
    }

    @Override
    public void removeSearchItem() {
        if (searchItems.containsKey(getId())){
            SearchItem suggestion = searchItems.get(getId());

            CityMapManager.suggestions.remove(suggestion);
            CityMapManager.refreshSearchAdapter();
            searchItems.remove(getId());
        }
    }

    @Override
    public HashMap<String, SearchItem> getSearchItems() {
        return null;
    }

    @Override
    public SearchItem getSearchItem() {
        if (searchItems.containsKey(getId())){
            SearchItem suggestion = searchItems.get(getId());
            return suggestion;
        }
        return null;
    }

    @Override
    public void clearSearchItems() {}

    @Override
    public boolean clearOnMapRemoval() {
        return true;
    }
    //endregion

    //region list object overrides
    @Override
    public CharSequence getMainText(){
        if (shiftSummary == null){
            if (getRoute() != null){
                return getRouteName();
            }
            return getName();
        }
        RoutePoint lastStop = shiftSummary.getLastStop();
        RoutePoint nextStop = shiftSummary.getNextStop();

        String lastStopName = lastStop.getName();
        if (nextStop == null){
            return lastStopName;
        }

        String nextStopName = nextStop.getName();
        String to = "<font color='#EE0000'> - TO - </font>";
        return Html.fromHtml(lastStopName + to + nextStopName);
    }

    @Override
    public String getSecondaryText(){
        if (shiftSummary == null){
            return getLastSeen() + " - " + getCompany().getName();
        }

        if (getRoute() != null){
            return  getLastSeen() + " - " + getRouteName() + " - " + getCompany().getName();
        }

        return getLastSeen() + " - " + getCompany().getName();
    }

    @Override
    public Drawable getListDrawable(){
        Bitmap bmp;

        RescaleBitmap rescale = new RescaleBitmap(AppVariables.mainActivity.getContext(),200,200);
        bmp = BitmapFactory.decodeResource(AppVariables.mainActivity.getContext().getResources(),R.drawable.ic_directions_bus_white_48dp);
        bmp = rescale.CropBitmap(bmp);

        int numColours= ColourCodes.colourCodes.length;
        String color = ColourCodes.colourCodes[Math.abs(companyId.hashCode()%numColours)];
        bmp=rescale.addBorder(bmp,40,color);

        Drawable drawable = new BitmapDrawable(AppVariables.mainActivity.getContext().getResources(), bmp);
        return drawable;
    }

    @Override
    public void setPrimaryView() {
        System.out.println("list object list set picture");
        View imageView = view.findViewById(R.id.primaryIV);
        Drawable drawable = getListDrawable();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            imageView.setBackground(drawable);
        }
    }

    @Override
    public void setSecondaryView(){
        System.out.println("** employee set secondary view **");
        if (shiftSummary == null || shiftSummary.getNextStop() == null){
            secondaryIV.setImageResource(R.drawable.ic_access_time_white);
            secondaryIV.setBackgroundResource(R.drawable.round_orange_button);

            //adapter.notifyDataSetChanged();
            return;
        }

        String eta = shiftSummary.getExpectedArrival();

        secondaryIV.setVisibility(View.GONE);
        secondaryInfoTV.setVisibility(View.VISIBLE);
        infoTV.setVisibility(View.VISIBLE);
        secondaryInfoTV.setBackgroundResource(R.drawable.round_green_button);

        infoTV.setText("eta");
        secondaryInfoTV.setText(eta);
        secondaryTV.setText(getSecondaryText()+" - estimate time of arrival - "+eta);

        //adapter.notifyDataSetChanged();
    }

    @Override
    public Company getCompany() {
        System.out.println("** employee get company **");
        if (company != null) {
            return company;
        }
        company = ObjectCache.getCompanies().get(companyId);
        return company;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.EMPLOYEE;
    }
    //endregion
}
