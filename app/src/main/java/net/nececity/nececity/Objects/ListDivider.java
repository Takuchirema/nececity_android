package net.nececity.nececity.Objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ColourCodes;
import net.nececity.nececity.Helpers.RescaleBitmap;
import net.nececity.nececity.R;

import net.nececity.nececity.Abstracts.ListObject;

/**
 * Created by Takunda on 11/2/2019.
 */

public class ListDivider extends ListObject {

    private String caption;

    public ListDivider(String caption){
        this.caption=caption;
    }

    //region list object overrides
    @Override
    public String getMainText() {
        return caption;
    }

    @Override
    public String getSecondaryText() {
        return "";
    }

    @Override
    public void setPrimaryView() {
        primaryIV.setVisibility(View.INVISIBLE);
    }

    @Override
    public Drawable getListDrawable(){
        Bitmap bmp;

        RescaleBitmap rescale = new RescaleBitmap(AppVariables.mainActivity.getContext(),200,200);
        bmp = BitmapFactory.decodeResource(AppVariables.mainActivity.getContext().getResources(),R.drawable.companies_white);
        bmp = rescale.CropBitmap(bmp);

        String color = ColourCodes.colourCodes[0];
        bmp=rescale.addBorder(bmp,40,color);

        Drawable drawable = new BitmapDrawable(AppVariables.mainActivity.getContext().getResources(), bmp);
        return drawable;
    }

    @Override
    public void setSecondaryView(){
        dividerV.setVisibility(View.VISIBLE);
        secondaryIV.setVisibility(View.INVISIBLE);
    }

    @Override
    public Company getCompany() {
        return null;
    }

    @Override
    public void setSecondaryViewListener(){
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.DELIVERY;
    }
    //endregion
}
