package net.nececity.nececity.Objects;

import net.nececity.nececity.Abstracts.DeliveryItemListObject;
import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.ListObject;

/**
 * Created by Takunda on 5/15/2019.
 */

public class DeliveryItem extends DeliveryItemListObject{
    private String id;
    private String name;
    private String description;
    private String price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.DELIVERY;
    }
}
