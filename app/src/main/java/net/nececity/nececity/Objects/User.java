package net.nececity.nececity.Objects;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.text.format.DateUtils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.EStatus;
import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Abstracts.UserListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.LocationHandler;
import net.nececity.nececity.Managers.LocationManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Takunda on 22/05/2018.
 */

public class User extends UserListObject {

    private String name;
    private String fbId;
    private String fbName;
    private Location location;
    private Location prevLocation;
    private String pictureUrl;
    private String phoneNumber;
    private Marker marker;
    private String visibility;
    private String visibilitySetBy;
    private String details;

    private String email;
    private String privilege;

    private Drawable pictureDrawable;

    private boolean blocked=false;
    private boolean regionVisible = false;
    private boolean hasPic = false;
    private boolean setNewMarker = true;

    private boolean alertSeen = false;
    private String onAlert;

    private String locationDetail;

    private String status;
    private String lastSeen;
    private String lastSeenRaw;
    private long lastSeenAgo;
    private float speed=0;
    private float prevSpeed=0;
    private int mileage;

    private String id;
    private Settings settings;

    public User(String name){
        if (name != null && !name.equals("null") && !name.trim().equals(""))
            this.name = name;
    }

    public String getName() {
        if (fbName != null && !fbName.isEmpty()){
            return fbName;
        }else if (name != null && !name.isEmpty()){
            return name;
        }
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getFbName() {
        return fbName;
    }

    public void setFbName(String fbName) {
        this.fbName = fbName;
    }

    public Location getLocation() {
        return location;
    }

    public float getSpeed(){
        return speed;
    }

    public float getPrevSpeed(){
        return prevSpeed;
    }

    private void calculateSpeed(){
        if (getLocation() != null && getPrevLocation() != null){
            float distance = getLocation().distanceTo(getPrevLocation());
            long timeMillis = Math.abs(getLocation().getTime() - getPrevLocation().getTime());
            float timeHours = ((timeMillis / (float)(1000*60*60)));
            System.out.println("get location dist: "+distance+" time "+timeHours);

            prevSpeed = speed;
            speed = distance/timeHours;
        }
    }

    public void setLocation(Location newLocation) {

        if (newLocation == null){
            return;
        }

        newLocation.setTime(System.currentTimeMillis());

        if (this.location != null) {
            Location prevLocation = new Location("");
            prevLocation.setLatitude(this.location.getLatitude());
            prevLocation.setLongitude(this.location.getLongitude());
            prevLocation.setTime(this.location.getTime());
            this.prevLocation = prevLocation;
        }else{
            this.prevLocation = newLocation;
        }

        this.location = newLocation;

        calculateSpeed();
    }

    public Location getPrevLocation() {
        return prevLocation;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;

        if (pictureUrl == null){
            return;
        }

        int i = pictureUrl.lastIndexOf('.');
        if (i != pictureUrl.length()-1) {
            hasPic = true;
        }
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getVisibilitySetBy() {
        return visibilitySetBy;
    }

    public void setVisibilitySetBy(String visibilitySetBy) {
        this.visibilitySetBy = visibilitySetBy;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    public Bitmap getPictureBmp() {
        return bmp;
    }

    public void setPictureBmp(Bitmap pictureBmp) {
        this.bmp = pictureBmp;
    }

    public Drawable getPictureDrawable() {
        return pictureDrawable;
    }

    public void setPictureDrawable(Drawable pictureDrawable) {
        this.pictureDrawable = pictureDrawable;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isRegionVisible() {
        return regionVisible;
    }

    public void setRegionVisible(boolean regionVisible) {
        this.regionVisible = regionVisible;
    }

    public boolean hasPic() {
        return hasPic;
    }

    public void setHasPic(boolean hasPic) {
        this.hasPic = hasPic;
    }

    public boolean isSetNewMarker() {
        return setNewMarker;
    }

    public void setSetNewMarker(boolean setNewMarker) {
        this.setNewMarker = setNewMarker;
    }

    public boolean isAlertSeen() {
        return alertSeen;
    }

    public void setAlertSeen(boolean alertSeen) {
        this.alertSeen = alertSeen;
    }

    public String getLocationDetail() {
        return locationDetail;
    }

    public void setLocationDetail(String locationDetail) {
        this.locationDetail = locationDetail;
    }

    public EStatus getStatus() {
        if (status.equalsIgnoreCase("online")){
            return EStatus.ONLINE;
        }
        return EStatus.OFFLINE;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    @SuppressLint("SimpleDateFormat")
    public void setLastSeen(String lastSeen) {

        if (lastSeen == null){
            this.lastSeen = "New User";
            this.lastSeenRaw = "2018-01-01_00:00";
            return;
        }

        this.lastSeen = lastSeen;
        this.lastSeenRaw = lastSeen;

        //yyyy-MM-dd_HH:mm
        SimpleDateFormat sdf = new SimpleDateFormat(AppVariables.timeFormat);

        Date date;
        try {
            date = sdf.parse(lastSeen);
            long now = System.currentTimeMillis();

            long diffInMs = now - date.getTime();
            lastSeenAgo = Math.abs(TimeUnit.MILLISECONDS.toSeconds(diffInMs));

            this.lastSeen = (DateUtils.getRelativeTimeSpanString(date.getTime(), now, DateUtils.MINUTE_IN_MILLIS)).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getLastSeenRaw() {
        return lastSeenRaw;
    }

    public long getLastSeenAgo(){
        return lastSeenAgo;
    }

    public void setLastSeenRaw(String lastSeenRaw) {
        this.lastSeenRaw = lastSeenRaw;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOnAlert() {
        return onAlert;
    }

    public void setOnAlert(String onAlert) {
        this.onAlert = onAlert;
    }

    public boolean isOnAlert(){
        if (onAlert == null) {
            return false;
        }

        if (onAlert.equalsIgnoreCase("true")){
            return true;
        }

        return false;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public int getMileage() {
        return mileage;
    }

    public void addMileage(int mileage) {
        this.mileage = this.mileage + mileage;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.USER;
    }
}
