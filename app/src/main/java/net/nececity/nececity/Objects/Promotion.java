package net.nececity.nececity.Objects;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.format.DateUtils;
import android.view.View;

import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.PromotionListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.R;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Takunda on 17/06/2018.
 */

public class Promotion extends PromotionListObject{

    private String promotion;
    private Date date;
    private String id;
    private String title;
    private String userId;
    private Drawable picture;
    private Drawable originalPicture;
    private String companyId;
    private String time;
    private String rawTime;
    private String seen;
    private View headerView;
    private Bitmap pictureBitmap;
    private String type; //either company or admin promotion
    private String likes="0";
    private boolean liked;
    private ArrayList<String> promotionDays = new ArrayList<String>();
    private String weekly;
    private String days;
    private String duration;
    private String price = "View menu";
    private String units = "details";
    private String onceOff;
    private String from = "Daily";
    private String to = "Daily";

    private ArrayList<Comment> comments=new ArrayList<Comment>();

    public Promotion(String id){
        this.id=id;
    }

    public boolean hasPicture(){
        System.out.println("promotion has pic? "+pictureUrl);
        if (pictureUrl == null){
            return false;
        }

        int i = pictureUrl.lastIndexOf('.');

        if (i != pictureUrl.length()-1 && i > 0) {
            return true;
        }else{
            return false;
        }
    }

    public void setUserId(String id){
        userId = id;
    }

    public Bitmap getPictureBitmap() {
        return pictureBitmap;
    }

    public void setPictureBitmap(Bitmap pictureBitmap) {
        this.pictureBitmap = pictureBitmap;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public String getTime() {
        return time;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        if (seen==null)
            seen = "false";
        this.seen = seen;
    }

    public boolean isSeen(){
        if (seen.equalsIgnoreCase("true")){
            return true;
        }
        return false;
    }

    public View getHeaderView() {
        return headerView;
    }

    public void setHeaderView(View headerView) {
        this.headerView = headerView;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @SuppressLint("SimpleDateFormat")
    public void setTime(String time) {
        this.rawTime = time;
        this.time = time;

        SimpleDateFormat ft = new SimpleDateFormat (AppVariables.timeFormat);

        Date date;
        try {
            date = ft.parse(time);
            long now = System.currentTimeMillis();
            this.time = (DateUtils.getRelativeTimeSpanString(date.getTime(), now, DateUtils.MINUTE_IN_MILLIS)).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getRawTime() {
        return rawTime;
    }

    public void setRawTime(String rawTime) {
        this.rawTime = rawTime;
    }

    public String getCompanyId() {
        return companyId;
    }

    public String getCompanyName() {
        Company company = ObjectCache.getCompanies().get(companyId);
        return company.getName();
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Drawable getPicture() {
        return picture;
    }

    public void setPicture(Drawable picture) {
        this.picture = picture;
    }

    public Drawable getOriginalPicture() {
        return originalPicture;
    }

    public void setOriginalPicture(Drawable originalPicture) {
        this.originalPicture = originalPicture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {

        if (this.pictureUrl == null) {
            this.pictureUrl = pictureUrl;
        }else if (!this.pictureUrl.equals(pictureUrl)){
            this.pictureUrl = pictureUrl;
            pictureBitmap = null;
            originalPicture = null;
            picture = null;
        }
    }

    public ArrayList<String> getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(ArrayList<String> pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        if (likes == null)
            likes =""+0;
        this.likes = likes;
    }

    public boolean getLiked() {
        return liked;
    }

    public void setLiked(String liked) {

        if (liked == null){
            return;
        }

        if (liked.equalsIgnoreCase("true")){
            this.liked = true;
            return;
        }
        this.liked=false;
    }

    public String getUserId() {
        return userId;
    }

    public ArrayList<String> getPromotionDays() {
        return promotionDays;
    }

    public void setPromotionDays(ArrayList<String> promotionDays) {
        this.promotionDays = promotionDays;
    }

    public String getWeekly() {
        return weekly;
    }

    public void setWeekly(String weekly) {
        this.weekly = weekly;
    }

    public String getDays() {
        if (days == null){
            return "";
        }
        return days;
    }

    public void setDays(String days) {
        if (days == null){
            return;
        }

        this.days = days;
        String[] daysArray = days.split(",");

        for (String day:daysArray){
            if (!day.isEmpty()){
                promotionDays.add(day);
            }
        }
    }

    public String getOnceOff() {
        return onceOff;
    }

    public void setOnceOff(String onceOff) {
        this.onceOff = onceOff;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        if (duration == null){
            return "";
        }
        return duration;
    }

    public void setDuration(String duration) {
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        SimpleDateFormat rd = new SimpleDateFormat("dd MMM yyyy");

        if (duration != null){
            String[] range = duration.split(",");
            if (range.length == 2) {
                try {
                    Date fromDate = ft.parse(range[0]);
                    Date toDate = ft.parse(range[1]);

                    from = rd.format(fromDate);
                    to = rd.format(toDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        if (price == null){
            return;
        }
        this.price = price;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        if (units == null){
            return;
        }
        this.units = units;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    //region list object overrides
    @Override
    public Company getCompany() {
        Company company = ObjectCache.getCompanies().get(companyId);
        return company;
    }

    @Override
    public String getSearchString(){
        return title + " "+promotion;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.PROMOTION;
    }
    //endregion
}
