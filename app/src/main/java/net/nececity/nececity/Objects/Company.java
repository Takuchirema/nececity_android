package net.nececity.nececity.Objects;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.view.View;

import com.google.android.gms.maps.model.Marker;
import com.lapism.searchview.widget.SearchItem;

import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.ESearchItem;
import net.nececity.nececity.Abstracts.IMainActivity;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Abstracts.IMapSearchItem;
import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ColourCodes;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RescaleBitmap;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 22/05/2018.
 */

public class Company extends ListObject implements IMapSearchItem {

    private String id;
    private String name;
    private String type;
    private String business;
    private String businessHours;
    private String address;
    private String about;
    private String phoneNumber;
    private String email;
    private Boolean logTime=false;
    private Boolean deliveries=false;

    private Location location;
    private Bitmap locationIcon;
    private Marker locationMarker;
    private boolean hasLocation =false;

    //set when getting user companies to check if company has blocked the user
    private String blocked;
    private boolean following=false;


    private HashMap<String, Employee> employees = new HashMap<>();
    private HashMap<String,List<String>> workingHours = new HashMap<>();
    private HashMap<String,Route> routes = new HashMap<>();
    private HashMap<String, Post> posts = new HashMap<>();
    private HashMap<String,CataloguePost> catalogue = new HashMap<>();
    private HashMap<String, Promotion> promotions = new HashMap<>();

    private static HashMap<String, SearchItem> searchItems = new HashMap<>();

    public Company(String id){
        this.id=id;
    }

    //region getters and setters
    public String getId() {
        return id;
    }

    public String getType() {
        if (type == null){
            return "";
        }
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getLogTime() {
        return logTime;
    }

    public void setLogTime(Boolean logTime) {
        this.logTime = logTime;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isHasLocation() {
        return hasLocation;
    }

    public void setHasLocation(boolean hasLocation) {
        this.hasLocation = hasLocation;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public HashMap<String, Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(HashMap<String, Employee> employees) {
        this.employees = employees;
    }

    public HashMap<String, List<String>> getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(HashMap<String, List<String>> workingHours) {
        this.workingHours = workingHours;
    }

    public HashMap<String, Route> getRoutes() {
        return routes;
    }

    public void setRoutes(HashMap<String, Route> routes) {
        this.routes = routes;
    }

    public HashMap<String, Post> getPosts() {
        return posts;
    }

    public void setPosts(HashMap<String, Post>  posts) {
        this.posts = posts;
    }

    public HashMap<String, CataloguePost> getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(HashMap<String, CataloguePost> catalogue) {
        this.catalogue = catalogue;
    }

    public HashMap<String, Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(HashMap<String, Promotion> promotions) {
        this.promotions = promotions;
    }

    public String getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setLocationIcon(Bitmap locationIcon) {
        this.locationIcon = locationIcon;
    }

    public Marker getLocationMarker() {
        return locationMarker;
    }

    public void setLocationMarker(Marker locationMarker) {
        this.locationMarker = locationMarker;
    }

    public Boolean getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(Boolean deliveries) {
        this.deliveries = deliveries;
    }
    //endregion

    public Bitmap getLocationIcon(Context context) {
        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.other);
        int numColours= ColourCodes.colourCodes.length;
        
        RescaleBitmap rescale = new RescaleBitmap(context,150,150);
        
        if (business == null || business.equals("Other")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.other);
        }else if (business.equalsIgnoreCase("Security")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.security);
        }else if (business.equalsIgnoreCase("Transportation")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.transport);
        }else if (business.equalsIgnoreCase("Retail_Wholesale")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.retail);
        }else if (business.equalsIgnoreCase("Legal_Services")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.legal);
        }else if (business.equalsIgnoreCase("Restaurant")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.restaurant);
        }else if (business.equalsIgnoreCase("Real_Estate")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.real_estate);
        }else if (business.equalsIgnoreCase("Personal_Services")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.personal);
        }else if (business.equalsIgnoreCase("Environmental")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.environment);
        }else if (business.equalsIgnoreCase("Motor_Vehicles")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.vehicle);
        }else if (business.equalsIgnoreCase("Health_Services")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.health);
        }else if (business.equalsIgnoreCase("Hospitality")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.hospitality);
        }else if (business.equalsIgnoreCase("Finance")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.finance);
        }else if (business.equalsIgnoreCase("Education")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.education);
        }else if (business.equalsIgnoreCase("Construction")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.construction);
        }else if (business.equalsIgnoreCase("Technology")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.technology);
        }else if (business.equalsIgnoreCase("Agriculture")){
            bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.agriculture);
        }

        bmp = rescale.CropBitmap(bmp);
        String color = "#"+Integer.toHexString(Color.LTGRAY).substring(2);
        if (isOpen()){
            color = ColourCodes.colourCodes[Math.abs(id.hashCode()%numColours)];
        }
        bmp=rescale.addBorder(bmp,20,color);

        return bmp;
    }

    public static Bitmap getClusterIcon(Context context, String color) {
        Bitmap bmp;

        RescaleBitmap rescale = new RescaleBitmap(context,200,200);
        bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_directions_bus_white_48dp);
        bmp = rescale.CropBitmap(bmp);

        bmp=rescale.addBorder(bmp,40,color);
        return bmp;
    }

    public boolean isOpen(){
        String[] days = new String[]{"sun","mon","tue","wed","thu","fri","sat"};
        boolean open = false;

        Calendar dt = Calendar.getInstance();
        int dayNum = dt.get(Calendar.DAY_OF_WEEK)-1;

        String day = days[dayNum];

        List<String> dayWorkingHours = workingHours.get(day);
        if (dayWorkingHours == null || dayWorkingHours.isEmpty()){
            open = false;
        }else{
            try {
                DateFormat f = new SimpleDateFormat("HH:mm");
                Date d1 = f.parse(dayWorkingHours.get(0));
                Date d2 = f.parse(dayWorkingHours.get(1));
                Date dc = new Date(System.currentTimeMillis());

                //System.out.println("checking is open: "+compareTimes(dc,d1)+" "+compareTimes(d2,dc));
                if (compareTimes(dc,d1) > 0  && compareTimes(d2,dc) > 0){
                    open = true;
                }
            }catch (Exception ex){
                return false;
            }
        }
        return open;
    }

    public int compareTimes(Date d1, Date d2)
    {
        int     t1;
        int     t2;

        t1 = (int) (d1.getTime() % (24*60*60*1000L));
        t2 = (int) (d2.getTime() % (24*60*60*1000L));
        return (t1 - t2);
    }

    //region list item overrides
    @Override
    public String getMainText(){
        return name;
    }

    @Override
    public String getSecondaryText(){
        return about;
    }
    //endregion

    //region search item overrides
    @Override
    public void insertSearchItem() {
        if (searchItems.containsKey(id)){
            return;
        }

        SearchItem suggestion = new SearchItem(AppVariables.context);
        suggestion.setTitle(name);
        suggestion.setIcon1Resource(R.drawable.ic_location_city);
        suggestion.setSubtitle("Company");
        suggestion.setSearchItem(ESearchItem.COMPANY);
        suggestion.setObject(this);

        searchItems.put(id,suggestion);
        CityMapManager.suggestions.add(suggestion);
        CityMapManager.refreshSearchAdapter();
    }

    @Override
    public void removeSearchItem() {
        if (searchItems.containsKey(id)){
            SearchItem suggestion = searchItems.get(id);

            CityMapManager.suggestions.remove(suggestion);
            CityMapManager.refreshSearchAdapter();
            searchItems.remove(id);
        }
    }

    @Override
    public HashMap<String, SearchItem> getSearchItems() {
        return searchItems;
    }

    @Override
    public SearchItem getSearchItem() {
        if (searchItems.containsKey(id)){
            SearchItem suggestion = searchItems.get(id);
            return suggestion;
        }
        return null;
    }

    @Override
    public void clearSearchItems() {

    }

    @Override
    public boolean clearOnMapRemoval() {
        return true;
    }
    //endregion

    //region list object overrides
    @Override
    public Company getCompany() {
        return this;
    }

    @Override
    public String getSearchString(){
        return name;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.COMPANY;
    }
    //endregion
}
