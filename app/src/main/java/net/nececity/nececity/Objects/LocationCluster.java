package net.nececity.nececity.Objects;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;

import com.lapism.searchview.widget.SearchItem;

import net.nececity.nececity.Abstracts.EClusterConcentration;
import net.nececity.nececity.Abstracts.ESearchItem;
import net.nececity.nececity.Abstracts.IMapSearchItem;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.R;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Takunda on 11/10/2018.
 */

public class LocationCluster implements Serializable, IMapSearchItem {

    private int id;
    private String companyId;
    private String routeId;
    private double latitude;
    private double longitude;
    private int direction;
    private int size;
    private int previousSize;
    private String lastSeenRaw;
    private String lastSeen;
    private Long lastSeenAgo;
    private MapMarker mapMarker;

    private static HashMap<String, SearchItem> searchItems = new HashMap<>();

    public LocationCluster(int id, double latitude, double longitude){
        this.id=id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @SuppressLint("SimpleDateFormat")
    public void setLastSeen(String lastSeen) {

        if (lastSeen == null){
            this.lastSeen = "New Cluster";
            this.lastSeenRaw = "2018-01-01_00:00:00";
            return;
        }

        this.lastSeen = lastSeen;
        this.lastSeenRaw = lastSeen;

        //yyyy-MM-dd_HH:mm
        SimpleDateFormat sdf = new SimpleDateFormat(AppVariables.fullTimeFormat);

        Date date;
        try {
            date = sdf.parse(lastSeen);
            long now = System.currentTimeMillis();

            long diffInMs = now - date.getTime();
            lastSeenAgo = Math.abs(TimeUnit.MILLISECONDS.toSeconds(diffInMs));

            this.lastSeen = (DateUtils.getRelativeTimeSpanString(date.getTime(), now, DateUtils.MINUTE_IN_MILLIS)).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public EClusterConcentration getConcentration(){
        EClusterConcentration concentration = EClusterConcentration.HIGH;
        if (size < 5){
            concentration = EClusterConcentration.LOW;
        }else if (size >= 5 && size < 10 ){
            concentration = EClusterConcentration.MEDIUM;
        }
        return concentration;
    }

    /**
     * Gives the string hex representation of a color e.g. #ffffff for white.
     * @return
     */
    public String getConcentrationColor(){
        Context context = AppVariables.mainActivity.getContext();
        String color = null;

        switch (getConcentration()){
            case HIGH:
                color = "#" + Integer.toHexString(ContextCompat.getColor(context, R.color.green));
                break;
            case MEDIUM:
                color = "#" + Integer.toHexString(ContextCompat.getColor(context, R.color.yellow));
                break;
            case LOW:
                color = "#" + Integer.toHexString(ContextCompat.getColor(context, R.color.maroon));
                break;
        }
        return color;
    }

    /**
     * This Id is companyId_routeId_id
     * It's unique for each cluster on the app.
     * @return
     */
    public String getStructuredId(){
        return companyId+"_"+routeId+"_"+id;
    }

    public int getId() {
        return id;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getLastSeen() {
        SimpleDateFormat sdf = new SimpleDateFormat(AppVariables.fullTimeFormat);
        Date date;
        try {
            date = sdf.parse(lastSeenRaw);
            long now = System.currentTimeMillis();
            lastSeen = (DateUtils.getRelativeTimeSpanString(date.getTime(), now, DateUtils.MINUTE_IN_MILLIS)).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return lastSeen;
    }

    public String getCompanyId() {
        return companyId;
    }

    public String getCompanyName(){
        Company company = ObjectCache.getCompanies().get(companyId);
        if (company != null){
            return company.getName();
        }
        return null;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRouteId() {
        return routeId;
    }

    public String getRouteName(){
        return ObjectCache.getCompanies().get(companyId).getRoutes().get(routeId).getName();
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public Long getLastSeenAgo() {
        return lastSeenAgo;
    }

    public void setLastSeenAgo(Long lastSeenAgo) {
        this.lastSeenAgo = lastSeenAgo;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getPreviousSize() {
        return previousSize;
    }

    public void setPreviousSize(int previousSize) {
        this.previousSize = previousSize;
    }

    public MapMarker getMapMarker() {
        return mapMarker;
    }

    public void setMapMarker(MapMarker mapMarker) {
        this.mapMarker = mapMarker;
    }

    @Override
    public void insertSearchItem() {
        if (searchItems.containsKey(Integer.toString(id))){
            return;
        }

        SearchItem suggestion = new SearchItem(AppVariables.context);
        suggestion.setTitle(getRouteName() +" - Cluster");
        suggestion.setIcon1Resource(R.drawable.ic_directions_bus);
        suggestion.setSubtitle("Route Cluster");
        suggestion.setSearchItem(ESearchItem.ROUTE_CLUSTER);
        suggestion.setId(Integer.toString(id));
        suggestion.setObject(this);

        searchItems.put(Integer.toString(id),suggestion);
        CityMapManager.suggestions.add(suggestion);
        CityMapManager.refreshSearchAdapter();
    }

    @Override
    public void removeSearchItem() {
        if (searchItems.containsKey(Integer.toString(id))){
            SearchItem suggestion = searchItems.get(Integer.toString(id));

            CityMapManager.suggestions.remove(suggestion);
            CityMapManager.refreshSearchAdapter();
            searchItems.remove(Integer.toString(id));
        }
    }

    @Override
    public HashMap<String, SearchItem> getSearchItems() {
        return null;
    }

    @Override
    public SearchItem getSearchItem() {
        if (searchItems.containsKey(id)){
            SearchItem suggestion = searchItems.get(id);
            return suggestion;
        }
        return null;
    }

    @Override
    public void clearSearchItems() {

    }

    @Override
    public boolean clearOnMapRemoval() {
        return true;
    }
}
