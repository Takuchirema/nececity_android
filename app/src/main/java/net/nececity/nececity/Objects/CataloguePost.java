package net.nececity.nececity.Objects;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.format.DateUtils;
import android.view.View;

import net.nececity.nececity.Abstracts.CataloguePostListObject;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Takunda on 8/28/2018.
 */

public class CataloguePost extends CataloguePostListObject {

    private String post;
    private String title;
    private String id;
    private String companyId;
    private String time;
    private String rawTime;
    private String seen;
    private boolean liked;
    private int likes=0;
    private String duration;
    private String from = "Daily";
    private String to = "Daily";
    private String price = "View menu";
    private String units = "details";
    private String amenities;
    private Date date;

    private ArrayList<Comment> comments=new ArrayList<Comment>();

    public CataloguePost(String id){
        this.id=id;
    }

    public boolean hasPicture(){

        if (pictureUrl == null){
            System.out.println("post has no pic "+pictureUrl);
            return false;
        }

        int i = pictureUrl.lastIndexOf('.');

        if (i != pictureUrl.length()-1 && i > 0) {
            System.out.println("post has pic "+pictureUrl);
            return true;
        }else{
            System.out.println("post has no pic "+pictureUrl);
            return false;
        }
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public String getTime() {
        return time;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        if (seen == null) {
            seen = "false";
        }
        this.seen = seen;
    }

    public boolean isSeen(){
        if (seen.equalsIgnoreCase("true")){
            return true;
        }
        return false;
    }

    @SuppressLint("SimpleDateFormat")
    public void setTime(String time) {
        this.rawTime = time;
        this.time = time;

        SimpleDateFormat ft = new SimpleDateFormat (AppVariables.timeFormat);

        try {
            date = ft.parse(time);
            long now = System.currentTimeMillis();
            this.time = (DateUtils.getRelativeTimeSpanString(date.getTime(), now, DateUtils.MINUTE_IN_MILLIS)).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl=pictureUrl;
    }

    public ArrayList<String> getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(ArrayList<String> pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(String likes) {

        if (likes == null){
            return;
        }

        try {
            this.likes = Integer.parseInt(likes);
        }catch (Exception ex){}
    }

    public boolean getLiked() {
        return liked;
    }

    public void setLiked(String liked) {

        if (liked == null){
            return;
        }

        if (liked.equalsIgnoreCase("true")){
            this.liked = true;
            return;
        }
        this.liked=false;
    }

    public String getDuration() {
        if (duration == null){
            return "";
        }
        return duration;
    }

    public void setDuration(String duration) {
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        SimpleDateFormat rd = new SimpleDateFormat("dd MMM yyyy");

        if (duration != null){
            String[] range = duration.split(",");
            if (range.length == 2) {
                try {
                    Date fromDate = ft.parse(range[0]);
                    Date toDate = ft.parse(range[1]);

                    from = rd.format(fromDate);
                    to = rd.format(toDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        this.duration = duration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        if (price == null){
            return;
        }
        this.price = price;
    }

    public String getAmenities() {
        if (amenities == null){
            return "";
        }
        return amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        if (units == null){
            return;
        }

        this.units = units;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    //region list object overrides
    @Override
    public Company getCompany() {
        Company company = ObjectCache.getCompanies().get(companyId);
        return company;
    }

    @Override
    public String getSearchString(){
        return title + " "+post;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.CATALOGUE_POST;
    }
    //endregion
}
