package net.nececity.nececity.Objects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

import com.google.android.gms.maps.model.Polyline;
import com.lapism.searchview.widget.SearchItem;

import net.nececity.nececity.Abstracts.EObjectType;
import net.nececity.nececity.Abstracts.ESearchItem;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Abstracts.IMapSearchItem;
import net.nececity.nececity.Abstracts.ListObject;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ColourCodes;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RescaleBitmap;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.R;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda on 23/05/2018.
 */

public class Route extends ListObject implements Serializable, IMapSearchItem {

    private String id;
    private String name;
    private String color;
    private String companyId;
    private String companyName;
    private boolean isReady = false;

    //For use by the map manager
    private Polyline polyline;

    LinkedHashMap<String,RoutePoint> routePoints = new LinkedHashMap<>();
    private LinkedHashMap<String,RoutePoint> busStopPoints = new LinkedHashMap<>();
    private HashMap<Integer, LocationCluster> locationClusters = new HashMap<>();

    private static HashMap<String, SearchItem> searchItems = new HashMap<>();

    public Route(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getStructuredId(){
        return companyId+"_"+id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        if (companyName != null){
            return companyName;
        }

        Company company = ObjectCache.getCompanies().get(companyId);

        if (company != null){
            return company.getName();
        }

        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public LinkedHashMap<String, RoutePoint> getRoutePoints() {
        return routePoints;
    }

    /**
     * If we set the routePoints the route is now ready for use.
     * We will mark isReady as true.
     * @param routePoints
     */
    public void setRoutePoints(LinkedHashMap<String, RoutePoint> routePoints) {

        if (routePoints == null || routePoints.isEmpty()){
            return;
        }

        this.isReady = true;
        this.routePoints = routePoints;
        busStopPoints.clear();

        for (Map.Entry<String,RoutePoint> e:routePoints.entrySet()){
            RoutePoint point = e.getValue();
            if (point.isBusStop()){
                busStopPoints.put(point.getId(),point);
            }
        }
    }

    public LinkedHashMap<String, RoutePoint> getBusStopPoints() {
        return busStopPoints;
    }

    public void setBusStopPoints(LinkedHashMap<String, RoutePoint> busStopPoints) {
        this.busStopPoints = busStopPoints;
    }

    public HashMap<Integer, LocationCluster> getLocationClusters() {
        return locationClusters;
    }

    public void setLocationClusters(HashMap<Integer, LocationCluster> locationClusters) {
        this.locationClusters = locationClusters;
    }

    public boolean isReady() {
        return isReady;
    }

    public Polyline getPolyline() {
        return polyline;
    }

    public void setPolyline(Polyline polyline) {
        this.polyline = polyline;
    }

    //region search item overrides
    @Override
    public void insertSearchItem() {
        if (searchItems.containsKey(id)){
            ((IMapSearchItem)searchItems.get(getId()).getObject()).removeSearchItem();
        }

        String companyName = getCompanyName();

        SearchItem suggestion = new SearchItem(AppVariables.context);
        suggestion.setTitle(name +" - " + companyName);
        suggestion.setIcon1Resource(R.drawable.route_small);
        suggestion.setSubtitle(companyName + " - Route");
        suggestion.setSearchItem(ESearchItem.ROUTE);
        suggestion.setId(id);
        suggestion.setObject(this);

        searchItems.put(id,suggestion);
        CityMapManager.suggestions.add(suggestion);
        CityMapManager.refreshSearchAdapter();
    }

    @Override
    public void removeSearchItem() {
        if (searchItems.containsKey(id)){
            SearchItem suggestion = searchItems.get(id);

            CityMapManager.suggestions.remove(suggestion);
            CityMapManager.refreshSearchAdapter();
            searchItems.remove(id);
        }
    }

    @Override
    public HashMap<String, SearchItem> getSearchItems() {
        return searchItems;
    }

    @Override
    public SearchItem getSearchItem() {
        if (searchItems.containsKey(id)){
            SearchItem suggestion = searchItems.get(id);
            return suggestion;
        }
        return null;
    }

    @Override
    public void clearSearchItems() {

    }

    @Override
    public boolean clearOnMapRemoval() {
        return false;
    }
    //endregion

    //region list object overrides
    @Override
    public String getMainText(){
        return name;
    }

    @Override
    public String getSecondaryText(){
        return getCompanyName() + " - Route";
    }

    @Override
    public void setPrimaryView() {
        System.out.println("list object list set picture");
        View imageView = view.findViewById(R.id.primaryIV);
        Drawable drawable = getListDrawable();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            imageView.setBackground(drawable);
        }
    }

    @Override
    public Drawable getListDrawable(){
        Bitmap bmp;

        RescaleBitmap rescale = new RescaleBitmap(AppVariables.mainActivity.getContext(),200,200);
        bmp = BitmapFactory.decodeResource(AppVariables.mainActivity.getContext().getResources(),R.drawable.route_white);
        bmp = rescale.CropBitmap(bmp);

        int numColours= ColourCodes.colourCodes.length;
        String color = ColourCodes.colourCodes[Math.abs(companyId.hashCode()%numColours)];
        bmp=rescale.addBorder(bmp,40,color);

        Drawable drawable = new BitmapDrawable(AppVariables.mainActivity.getContext().getResources(), bmp);
        return drawable;
    }

    @Override
    public void setSecondaryView(){

        if (!(AppVariables.mainActivity instanceof IMapActivity)){
            return;
        }

        CityMapManager mapManager = ((IMapActivity)AppVariables.mainActivity).getMapManager();
        // Get number of employees on route
        HashMap<String, Employee> routeEmployees = mapManager.routeMapEmployees.get(id);

        if (routeEmployees == null || routeEmployees.isEmpty()){
            secondaryIV.setImageResource(R.drawable.ic_access_time_white);
            secondaryIV.setBackgroundResource(R.drawable.round_orange_button);

            //adapter.notifyDataSetChanged();
            return;
        }

        secondaryIV.setVisibility(View.GONE);
        secondaryInfoTV.setVisibility(View.VISIBLE);
        infoTV.setVisibility(View.VISIBLE);

        infoTV.setText("buses");
        secondaryInfoTV.setText(""+routeEmployees.size());
        secondaryInfoTV.setBackgroundResource(R.drawable.round_green_button);
        secondaryTV.setText(getCompanyName()+" - "+routeEmployees.size()+" en route");

        //adapter.notifyDataSetChanged();
    }

    @Override
    public Company getCompany() {
        System.out.println("** route get company **");
        Company company = ObjectCache.getCompanies().get(companyId);
        return company;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.ROUTE;
    }
    //endregion
}
