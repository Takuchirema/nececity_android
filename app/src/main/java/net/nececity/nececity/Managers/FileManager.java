package net.nececity.nececity.Managers;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import net.nececity.nececity.Abstracts.IGetServerFile;
import net.nececity.nececity.Abstracts.ISendServerFile;
import net.nececity.nececity.Data.GetServerFile;
import net.nececity.nececity.Data.SendServerFile;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.UrlConstructor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Takunda on 7/28/2018.
 */

public class FileManager implements ISendServerFile, IGetServerFile{

    private Context context;
    private IGetServerFile returnServerFile;

    public FileManager(Context context){
        this.context=context;
    }

    public FileManager(){
    }

    public void checkErrorLogs(){
        if (fileExists(AppVariables.errorLogName)){
            sendErrorLogs();
        }
    }

    public void sendLogs(String fileName, String url){
        SendServerFile sendLogs = new SendServerFile(this, url, fileName);
        sendLogs.setFile(getFile(fileName));
        sendLogs.execute();
    }

    public void sendErrorLogs(){
        String url = UrlConstructor.sendErrorLogUrl(AppVariables.getDeviceId((Activity) context));
        SendServerFile sendLogs = new SendServerFile(this, url,AppVariables.errorLogName);
        sendLogs.execute();
    }

    /* File name includes the extension*/
    public  void writeToFile(String fileName, String body)
    {
        FileOutputStream fos;

        try {
            File file = getFile(fileName);

            fos = new FileOutputStream(file,true);

            fos.write((body+"\n").getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Checking the main storage only
    public File getFile(String fileName){
        final File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+fileName);

        try{
            if (file.exists()){
                return file;
            }else{
                return createNewFile(fileName);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    // Checking the main storage only
    public boolean fileExists(String fileName){
        final File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+fileName);

        if (file.exists()){
            return true;
        }
        return false;
    }

    /* Will delete existing file*/
    public File createNewFile(String fileName) throws IOException {
        final File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+fileName);

        if (file.exists()){
            file.delete();
        }

        file.createNewFile();

        return file;
    }

    public void getServerFile(IGetServerFile returnServerFile,String url, String extension){
        this.returnServerFile = returnServerFile;
        GetServerFile getServerFile = new GetServerFile(this,extension);
        getServerFile.setFileUrl(url);
        getServerFile.execute();
    }

    @Override
    public void postGetServerFile(File file) {
        if (this.returnServerFile != null){
            this.returnServerFile.postGetServerFile(file);
        }
    }

    @Override
    public void postSendServerFile(JSONObject data) throws JSONException {

        if (data == null){
            return;
        }

        int success = data.getInt("success");

        if (success == 1){
            String fileName = data.getString("fileName");
            deleteFile(fileName);
        }
    }

    public void deleteFile(String fileName){
        final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" );

        if (!dir.exists())
        {
            if(!dir.mkdirs()){
                Log.e("ALERT","could not create the directories");
            }
        }

        final File myFile = new File(dir, fileName);
        myFile.delete();
    }
}
