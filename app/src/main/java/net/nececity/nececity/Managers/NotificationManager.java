package net.nececity.nececity.Managers;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import net.nececity.nececity.Abstracts.ENotificationType;
import net.nececity.nececity.R;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Takunda on 8/5/2018.
 */

public class NotificationManager {

    private Context context;
    private Activity activity;
    private Intent notificationIntent;
    private int notificationCount = 1;
    private int notificationId;
    private ENotificationType type;
    private int drawableResource;

    public static HashMap<String,ArrayList<String>> notificationIds = new HashMap<>();

    public NotificationManager(Context context){
        this.context=context;
    }

    public void setActivity(Activity activity){
        this.activity=activity;
    }

    public void setIntent(Intent intent){
        this.notificationIntent=intent;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void notify(String notificationId, String notificationTitle, String notificationMessage,ENotificationType type,Intent notificationIntent){
        android.app.NotificationManager notificationManager = (android.app.NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);

        if (notificationIds.containsKey(type.toString()) && notificationIds.get(type.toString()).contains(notificationId)){
            System.out.println("notification there "+notificationId);
            return;
        }else if (!notificationIds.containsKey(type.toString())){
            ArrayList<String> ids = new ArrayList<>();
            ids.add(notificationId);

            System.out.println("notification not there!! "+notificationId);
            notificationIds.put(type.toString(),ids);
        }else{
            notificationIds.get(type.toString()).add(notificationId);
        }

        @SuppressWarnings("deprecation")
        Notification notification = new Notification(getDrawableInt(),"New Message", System.currentTimeMillis());
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(getDrawableInt())
                .setContentTitle(notificationTitle)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationMessage))
                .setContentText(notificationMessage)
                ;
        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setVibrate(new long[] { 1000, 1000});
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);

        mBuilder.setLights(Color.BLUE, 3000, 3000);
        //System.out.println("notify!!! "+type+" id: "+notificationId);

        notificationManager.notify(notificationCount, mBuilder.build());
        notificationCount++;
    }

    public int getDrawableInt(){
        if (drawableResource == 0) {
            return R.drawable.notifications;
        }
        return drawableResource;
    }

    public int getDrawableResource() {
        return drawableResource;
    }

    public void setDrawableResource(int drawableResource) {
        this.drawableResource = drawableResource;
    }

    public void remove(){
        cancelNotification(context,notificationId);
    }

    public void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        try {
            android.app.NotificationManager nMgr = (android.app.NotificationManager) ctx.getSystemService(ns);
            nMgr.cancel(notifyId);
        }catch (Exception ex){}
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ENotificationType getType() {
        return type;
    }

    public Activity getActivity() {
        return activity;
    }

}
