package net.nececity.nececity.Managers;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.EHttpMethod;
import net.nececity.nececity.Abstracts.ICommentActivity;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IGetPicture;
import net.nececity.nececity.Data.ActionDataSet;
import net.nececity.nececity.Data.CustomJSONObject;
import net.nececity.nececity.Data.PictureDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Objects.CataloguePost;
import net.nececity.nececity.Objects.Comment;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Takunda on 26/06/2018.
 *
 * This class is for handling what happens to a view after receiving database response.
 */

public class ViewManager implements IGetDataSet, IGetPicture{

    private EActionType actionType;
    private View view;
    private ArrayAdapter<String> adapter;

    private String objectId;
    private Object object;

    private String url;
    private EHttpMethod httpMethod = EHttpMethod.POST;
    private ViewManager context;
    private Activity viewActivity;

    private String comment;

    private boolean setImageBackground = false;

    public ViewManager(View view, EActionType actionType, Activity viewActivity){
        context=this;
        this.actionType=actionType;
        this.view=view;
        this.viewActivity=viewActivity;
    }

    public void getPicture(){
        PictureDataSet pictureDataSet = new PictureDataSet(url,this);
        pictureDataSet.getDataSet();
    }

    public void followCompany(){
        ActionDataSet actionDataSet = new ActionDataSet(context,actionType);
        actionDataSet.setCompanyId(objectId);
        actionDataSet.setHttpMethod(httpMethod);
        actionDataSet.setUrl(url);
        actionDataSet.getDataSet();
    }

    public void likePost(){
        ActionDataSet actionDataSet = new ActionDataSet(context,actionType);
        actionDataSet.setCompanyId(objectId);
        actionDataSet.setHttpMethod(httpMethod);
        actionDataSet.setUrl(url);
        actionDataSet.getDataSet();
    }

    public void commentOnPost(String comment){
        this.comment = comment;

        String timeStamp = new SimpleDateFormat(AppVariables.timeFormat).format(Calendar.getInstance().getTime());
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("comment", comment));
        params.add(new BasicNameValuePair("time", timeStamp));

        ActionDataSet actionDataSet = new ActionDataSet(context,actionType);
        actionDataSet.setCompanyId(objectId);
        actionDataSet.setHttpMethod(httpMethod);
        actionDataSet.setUrl(url);
        actionDataSet.setParams(params);
        actionDataSet.getDataSet();
    }

    public void postFollowCompany(boolean follow){
        Company company = ObjectCache.getCompanies().get(objectId);
        if (follow){
            company.setFollowing(true);
            ((ImageView)view).setImageResource(R.drawable.done);
            (view).setBackgroundResource(R.drawable.round_green_button);
        }else{
            company.setFollowing(false);
            ((ImageView)view).setImageResource(R.drawable.ic_add_white);
            (view).setBackgroundResource(R.drawable.round_orange_button);
        }
        view.invalidate();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void postLikePost(){
        Post post = (Post)object;
        Company company = ObjectCache.getCompanies().get(post.getCompanyId());
        post = company.getPosts().get(post.getId());

        post.setLiked("true");
        post.setLikes(""+(post.getLikes()+1));
        ((ImageView)view).setImageDrawable(viewActivity.getResources().getDrawable(R.drawable.like_yellow));
        view.invalidate();

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void postLikePromotion(){
        Promotion promotion = (Promotion)object;
        Company company = ObjectCache.getCompanies().get(promotion.getCompanyId());
        promotion = company.getPromotions().get(promotion.getId());

        promotion.setLiked("true");
        promotion.setLikes(""+(promotion.getLikes()+1));
        ((ImageView)view).setImageDrawable(viewActivity.getResources().getDrawable(R.drawable.like_yellow));
        view.invalidate();

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void postLikeCataloguePost(){
        CataloguePost post = (CataloguePost)object;
        Company company = ObjectCache.getCompanies().get(post.getCompanyId());
        post = company.getCatalogue().get(post.getId());

        post.setLiked("true");
        post.setLikes(""+(post.getLikes()+1));
        ((ImageView)view).setImageDrawable(viewActivity.getResources().getDrawable(R.drawable.like_yellow));
        view.invalidate();

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void postCommentOnPost(DataSet dataSet) throws JSONException {
        CustomJSONObject json = ((ActionDataSet) dataSet).getJsonObject();

        String companyId = null;
        ArrayList<Comment> comments = new ArrayList<>();
        if (object instanceof Post){
            Post post = (Post) object;
            Company company = ObjectCache.getCompanies().get(post.getCompanyId());
            post = company.getPosts().get(post.getId());

            companyId = post.getCompanyId();
            comments = post.getComments();
        }else if (object instanceof Promotion){
            Promotion promotion = (Promotion) object;
            Company company = ObjectCache.getCompanies().get(promotion.getCompanyId());
            promotion = company.getPromotions().get(promotion.getId());

            companyId = promotion.getCompanyId();
            comments = promotion.getComments();
        }

        String commentId = json.getString("commentId");
        String commentStr = json.getString("comment");
        String commentCompany=companyId;
        String commentTime=json.getString("time");

        Comment comment = new Comment(commentId);
        comment.setUserID(AppVariables.user.getName());
        comment.setComment(commentStr);
        comment.setCompany(commentCompany);
        comment.setTime(commentTime);

        comments.add(comment);

        if (viewActivity != null && viewActivity instanceof ICommentActivity){
            if (view != null){
                ((ICommentActivity)viewActivity).addComment(comment,(LinearLayout) view);
            }
        }
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        if (dataSet.success){
            switch (actionType){
                case FOLLOW_COMPANY:
                    postFollowCompany(true);
                    break;
                case UNFOLLOW_COMPANY:
                    postFollowCompany(false);
                    break;
                case LIKE_POST:
                    postLikePost();
                    break;
                case LIKE_CATALOGUE_POST:
                    postLikeCataloguePost();
                    break;
                case LIKE_PROMOTION:
                    postLikePromotion();
                    break;
                case COMMENT_ON_POST:
                    try {
                        postCommentOnPost(dataSet);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }else{
            if (viewActivity != null) {
                Toast.makeText(viewActivity, dataSet.message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void postGetPicture(Bitmap bmp) {
        if (bmp == null){
            return;
        }

        if (object instanceof Company){
            System.out.println("post get pic company 0");
            String companyId = ((Company)object).getId();
            ObjectCache.getCompanies().get(companyId).setBmp(bmp);
        }else if (object instanceof Post){
            ((Post)object).setBmp(bmp);
        }else if (object instanceof CataloguePost){
            ((CataloguePost)object).setBmp(bmp);
        }else if (object instanceof Promotion){
            ((Promotion)object).setBmp(bmp);
        }else if (object instanceof User){
            ((User)object).setPictureBmp(bmp);
        }

        setPicture(bmp);

        if (adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setPicture(Bitmap bmp){
        System.out.println("post get pic 1");
        if (view != null) {
            System.out.println("post get pic 2");
            Drawable drawable = new BitmapDrawable(viewActivity.getResources(), bmp);
            if (setImageBackground) {
                view.setBackground(drawable);
                System.out.println("post get pic 3 adapter");
                view.invalidate();
            }else{
                ((ImageView)view).setImageDrawable(drawable);
                System.out.println("post get pic 3 view");
                view.invalidate();
            }
        }

    }

    public void setAdapter(ArrayAdapter<String> adapter) {
        this.adapter = adapter;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public EHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public void setImageBackground(boolean setImageBackground) {
        this.setImageBackground = setImageBackground;
    }

    public Activity getViewActivity() {
        return viewActivity;
    }

    public void setViewActivity(Activity viewActivity) {
        this.viewActivity = viewActivity;
    }
}
