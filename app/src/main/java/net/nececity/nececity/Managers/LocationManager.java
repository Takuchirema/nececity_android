package net.nececity.nececity.Managers;

import android.app.Activity;
import android.location.Location;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Data.LocationDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;
import net.nececity.nececity.Objects.TimeLog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Takunda on 04/06/2018.
 */

public class LocationManager implements IGetDataSet{

    private static Location currentLocation;

    private static Location previousLocation; // used to track distance moved for location sending
    private static Location previousGoogleLocation; // used to track distance moved for google details

    public LocationManager(Location location){
        this.currentLocation=location;
    }

    public void sendLocation(){
        // update prev location for sending location
        previousLocation  = new Location("");
        previousLocation.setLatitude(currentLocation.getLatitude());
        previousLocation.setLongitude(currentLocation.getLongitude());

        System.out.println("sending location");
        LocationDataSet locationDataSet = new LocationDataSet(this, currentLocation);

        float speed = AppVariables.user.getSpeed();
        if (speed >= AppVariables.speedForLocationUpdate){
            setEnRouteLocationDataset(locationDataSet);
        }else{
            System.out.println("location speed bad "+speed+" "+AppVariables.user.getLocation().getTime());
        }
        locationDataSet.sendLocation();
    }

    public static void sendLocation(Location location){
        System.out.println("static sending location");
        LocationDataSet locationDataSet = new LocationDataSet(null, location);
        setEnRouteLocationDataset(locationDataSet);
        locationDataSet.sendLocation();
    }

    public static void setEnRouteLocationDataset(LocationDataSet locationDataSet){
        float speed = AppVariables.user.getSpeed();
        if (CityMapManager.isEnRoute()) {
            System.out.println("location en route");
            //Update mileage for user or employee to be used for benefits and mileage for employees.
            AppVariables.user.addMileage(AppVariables.userSendLocationDistance);

            float direction = RoutePoint.distanceBearingBetween(AppVariables.user.getLocation(), AppVariables.user.getPrevLocation(), true);
            System.out.println("location speed good " + speed + " " + AppVariables.user.getLocation().getTime());
            locationDataSet.setRouteId(AppVariables.enRoute.getId());
            locationDataSet.setCompanyId(AppVariables.enRoute.getCompanyId());
            locationDataSet.setSpeed(speed);
            locationDataSet.setDirection(direction);
        }
    }


    public void getLocationDetails(){
        LocationDataSet locationDataSet = new LocationDataSet(this, currentLocation);
        locationDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        LocationDataSet locationDataSet = (LocationDataSet) dataSet;
        if (dataSet.success && locationDataSet.isGoogleDataSet()){
            locationDataSet.sendLocation();
        }

        if (!(AppVariables.user instanceof Employee)){
            return;
        }

        if (dataSet.success){
            if (locationDataSet.isGoogleDataSet()){
                Toast.makeText(AppVariables.mainActivity.getContext(), "Google details received", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(AppVariables.mainActivity.getContext(), "Location sent", Toast.LENGTH_LONG).show();
            }
        }else{
            if (locationDataSet.isGoogleDataSet()){
                Toast.makeText(AppVariables.mainActivity.getContext(), "Google details failed", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(AppVariables.mainActivity.getContext(), "Location sending failed", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onLocationChanged(GoogleMap map, Location newLocation){
        System.out.println("location changed");
        if (newLocation == null) {
            return;
        }

        panToPosition(map, newLocation.getLatitude(),newLocation.getLongitude());

        float distanceToChangeLocation = AppVariables.userChangeLocationDistance;

        if (AppVariables.user instanceof Employee){
            distanceToChangeLocation = AppVariables.employeeChangeLocationDistance;
        }

        if (AppVariables.user.getLocation() == null){
            AppVariables.user.setLocation(newLocation);
        }

        // log stop times
        logTime(newLocation);

        // Check if user has changed enough to send their location
        float distanceBetween = AppVariables.user.getLocation().distanceTo(newLocation);

        System.out.println("location changed: "+distanceBetween);
        if (distanceBetween < distanceToChangeLocation){
            return;
        }

        AppVariables.user.setLocation(newLocation);
        currentLocation = new Location("");
        currentLocation.setLatitude(newLocation.getLatitude());
        currentLocation.setLongitude(newLocation.getLongitude());

        // Check if the speed is reasonable.
        // Sometimes the speed is too much because the location has jumped.
        float speed = AppVariables.user.getSpeed();
        System.out.println("speed "+speed);
        if (speed > AppVariables.speedLimitForLocationUpdate){
            System.out.println("speed too big "+speed);
            return;
        }

        // If we are drawing a route
        if (AppVariables.mainActivity != null && AppVariables.mainActivity instanceof IMapActivity) {
            System.out.println("plot route 0 ");
            LatLng latLng = new LatLng(newLocation.getLatitude(),newLocation.getLongitude());
            ((IMapActivity)AppVariables.mainActivity).getMapManager().plotRoute(latLng);
        }

        handleSendLocation();

        if (map != null) {
            handleGoogleLocation();
        }
    }

    public void handleGoogleLocation(){
        // Check distance for getting google location details
        float comparisonDistance = AppVariables.getDetailsDistance;
        float distance=0;

        if (previousGoogleLocation != null) {
            distance = currentLocation.distanceTo(previousGoogleLocation);
        }

        //Only get location details if distance is > 2km.
        if (distance == 0 || distance >= comparisonDistance) {
            previousGoogleLocation  = new Location("");
            previousGoogleLocation.setLatitude(currentLocation.getLatitude());
            previousGoogleLocation.setLongitude(currentLocation.getLongitude());

            getLocationDetails();
        }
    }

    public void handleSendLocation(){
        System.out.println("handle sending location: ");

        float comparisonDistance = AppVariables.userSendLocationDistance;
        float distance=0;

        if (AppVariables.user instanceof Employee){
            comparisonDistance = AppVariables.employeeSendLocationDistance;
        }

        if (previousLocation != null) {
            distance = currentLocation.distanceTo(previousLocation);
        }

        //Only send location if distance moved > sth.
        if (distance == 0 || distance >= comparisonDistance) {
            sendLocation();
            return;
        }else{
            System.out.println("Location distance moved too short: "+distance);
        }

        float speed = AppVariables.user.getSpeed();
        float prevSpeed = AppVariables.user.getPrevSpeed();

        // If the user is en-route and user is stopping, send location.
        if (speed < AppVariables.speedMinForLocationUpdate && speed < prevSpeed ){
            sendLocation();
        }
    }

    public static void panToPosition(final GoogleMap map, final Double latitude, final Double longitude){
        if (map == null){
            return;
        }
        Activity mapActivity = (Activity)AppVariables.mainActivity.getContext();
        mapActivity.runOnUiThread(new Runnable(){
            public void run(){
                float currentZoomLevel = map.getCameraPosition().zoom;
                System.out.println("zoom level "+currentZoomLevel);

                if (currentZoomLevel < 5){
                    CameraUpdate update = CameraUpdateFactory.
                            newLatLngZoom(new LatLng(latitude, longitude), AppVariables.zoomLevel);
                    map.animateCamera(update);
                }
            }
        });
    }

    public void logTime(Location location){
        System.out.println("Logging Time");
        if (!(AppVariables.user instanceof Employee)){
            return;
        }

        Employee employee = AppVariables.getEmployee();
        Company company = ObjectCache.getCompanies().get(employee.getCompanyId());
        Route route = employee.getRoute();

        if (route == null || company == null || !company.getLogTime()){
            return;
        }

        LinkedHashMap<String,RoutePoint> busStopPoints = route.getBusStopPoints();

        for (Map.Entry<String,RoutePoint> e:busStopPoints.entrySet()){
            RoutePoint point = e.getValue();
            Location stopLocation = new Location("stop");
            stopLocation.setLatitude(point.getLatitude());
            stopLocation.setLongitude(point.getLongitude());

            //check if close enough to stop
            float distanceToStop = stopLocation.distanceTo(location);

            String timeStamp = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());

            if (arrivingAtStop(point, location)){
                System.out.println("Logging Time - Arriving");
                //check if we should log another time
                //if we have not yet left then the arrival times in busStopPoint will still be there
                //We should then just check if we are closer to the stop and log that time
                if (distanceToStop > AppVariables.logTimeRadius || point.getArrivalTimeLog() != null){
                    continue;
                }

                TimeLog arrivalTimeLog = new TimeLog();
                arrivalTimeLog.setBusStop(point.getName());
                arrivalTimeLog.setTime(timeStamp);
                arrivalTimeLog.setLogType(TimeLog.LogType.ARRIVAL);
                arrivalTimeLog.setRoute(route.getId());

                point.setArrivalTimeLog(arrivalTimeLog);
            }else{
                System.out.println("Logging Time - Departing");
                // every arrival must have a departure
                // We are departing only when we are outside the stop radius i.e. when distanceToStop > Radius
                if (point.getArrivalTimeLog() != null && distanceToStop > AppVariables.logTimeRadius){
                    System.out.println("Logging Time - Sending Log Time");
                    TimeLog departureTimeLog = new TimeLog();
                    departureTimeLog.setBusStop(point.getName());
                    departureTimeLog.setTime(timeStamp);
                    departureTimeLog.setLogType(TimeLog.LogType.DEPATURE);
                    departureTimeLog.setRoute(route.getId());

                    point.setDepartureTimeLog(departureTimeLog);

                    sendLogTime(point.getArrivalTimeLog(), point.getDepartureTimeLog());

                    point.setArrivalTimeLog(null);
                    point.setDepartureTimeLog(null);
                }
            }
        }
    }

    // We are arriving if the newLocation is BETWEEN the stop and the prevLocation
    // We are going if the newLcation is NOT BETWEEN the stop and the prevLocation
    public boolean arrivingAtStop(RoutePoint stop, Location newLocation){
        Location stopLocation = new Location("stop");
        stopLocation.setLatitude(stop.getLatitude());
        stopLocation.setLongitude(stop.getLongitude());

        float b1 = newLocation.bearingTo(AppVariables.user.getLocation());
        float b2 = newLocation.bearingTo(stopLocation);

        float diff = Math.abs(b1 - b2);
        System.out.println("bearings - b1: "+b1+" b2: "+b2+" diff:"+diff);
        if (diff > 110){
            System.out.println("arriving - stop distance: "+stop.getName());
            return true;
        }else{
            System.out.println("going - stop distance: "+stop.getName());
            return false;
        }
    }

    public void sendLogTime(TimeLog arrival,TimeLog depart){
        Employee employee = AppVariables.getEmployee();

        String fileName= employee.getId()+"_"+employee.getRouteId()+"_"+(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()) );
        fileName = fileName+".txt";
        String timeLog = arrival.getBusStop()+","+arrival.getTime()+","+depart.getTime()+",";

        System.out.println("sending log time: "+fileName+" "+timeLog);
        String url = UrlConstructor.uploadEmployeeLog(employee.getId(),employee.getCompanyId());
        FileManager fileManager = new FileManager();
        fileManager.writeToFile(fileName, timeLog);
        fileManager.sendLogs(fileName,url);
    }

    public static Location getCurrentLocation() {
        return currentLocation;
    }

    public static void setCurrentLocation(Location currentLocation) {
        LocationManager.currentLocation = currentLocation;
    }
}
