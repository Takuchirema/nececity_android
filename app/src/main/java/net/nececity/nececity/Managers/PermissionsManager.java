package net.nececity.nececity.Managers;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * Created by Takunda on 07/06/2018.
 */

public class PermissionsManager {

    private static Context context;

    private static final String[] INITIAL_PERMS={
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public static final int INITIAL_REQUEST=1337;

    public PermissionsManager(Context context){
        this.context=context;
    }

    public void requestPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ((Activity)context).requestPermissions(INITIAL_PERMS,INITIAL_REQUEST);
        }
    }

    public boolean canAccessLocation() {
        return(hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    public boolean canReadExternalStorage() {
        return(hasPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE));
    }

    public boolean canReadContacts() {
        return(hasPermission(android.Manifest.permission.READ_CONTACTS));
    }

    public static boolean hasPermission(String perm) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return(PackageManager.PERMISSION_GRANTED== context.checkSelfPermission(perm));
        }else{
            return false;
        }
    }
}
