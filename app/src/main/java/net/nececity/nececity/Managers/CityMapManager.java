package net.nececity.nececity.Managers;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.lapism.searchview.database.SearchHistoryTable;
import com.lapism.searchview.widget.SearchAdapter;
import com.lapism.searchview.widget.SearchItem;
import com.lapism.searchview.widget.SearchView;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.EMapMarkerType;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IMapActivity;
import net.nececity.nececity.Data.RouteDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.CustomRunnable;
import net.nececity.nececity.Helpers.GetDataSet;
import net.nececity.nececity.Helpers.LocationHandler;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Objects.Company;
import net.nececity.nececity.Objects.Employee;
import net.nececity.nececity.Objects.LocationCluster;
import net.nececity.nececity.Objects.MapMarker;
import net.nececity.nececity.Objects.Route;
import net.nececity.nececity.Objects.RoutePoint;
import net.nececity.nececity.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Takunda on 21/07/2018.
 */

public class CityMapManager implements IGetDataSet {

    // Key is the routeId, ArrayList are the map markers
    public static HashMap<String, Route> routes = new HashMap<>();
    // Has map markers of employees on the routes
    public static HashMap<String,HashMap<String,Employee>> routeMapEmployees = new HashMap<>();

    // For watching stops to raise alarm
    public static HashMap<String,ArrayList<String>> watchRoutesPoints = new HashMap<>();

    // Contains history of user searches
    final SearchHistoryTable mHistoryDatabase;
    public static ArrayList<SearchItem> suggestions = new ArrayList<>();
    public static SearchAdapter searchAdapter;

    final protected Handler handler = new Handler();

    public GoogleMap map;
    public Context context;

    public CityMapManager(GoogleMap map, Context context){
        this.map=map;
        this.context=context;

        mHistoryDatabase = new SearchHistoryTable(context);
        searchAdapter = new SearchAdapter(context);
    }

    public void setMap(GoogleMap map) {
        this.map = map;
    }

    public boolean isShowing(Route route){
        if (routes.get(route.getId()) == null){
            return false;
        }

        Polyline routePolyLine = routes.get(route.getId()).getPolyline();
        if (routePolyLine != null && routePolyLine.isVisible()){
            return true;
        }
        return false;
    }

    public static void refreshSearchAdapter(){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                searchAdapter.notifyDataSetChanged();
            }
        });
    }

    public void prepareSearch(final SearchView searchView){

    }

    public void onSearchItemClicked(SearchItem item) {

    }

    public void searchHandler(SearchItem item){

    }

    public CustomRunnable createRouteRunnable(final boolean panToRoute){
        CustomRunnable runnable = new CustomRunnable() {
            public void run() {
                GetDataSet getRouteDataSet = (GetDataSet)getObject();
                RouteDataSet routeDataSet = (RouteDataSet) getRouteDataSet.getDataSet();
                if (routeDataSet.success) {
                    Route route = routeDataSet.getRoute();
                    ObjectCache.getCompanies().get(route.getCompanyId()).getRoutes().put(route.getId(),route);
                    postGetRoute(routeDataSet.getRoute(),panToRoute);
                }
            }
        };
        return runnable;
    }

    public void postGetRoute(Route route, boolean panToRoute){
    }

    public void getRoute(String companyId, String routeId, boolean panToRoute){
        RouteDataSet routeDataSet = new RouteDataSet(null, companyId, routeId);
        CustomRunnable customRunnable = createRouteRunnable(panToRoute);
        GetDataSet getRouteDataSet = new GetDataSet(routeDataSet,handler,customRunnable);
        customRunnable.setObject(getRouteDataSet);
        getRouteDataSet.retrieveDataSet();
    }

    public boolean showRoute(GoogleMap map, String routeId, String companyId, boolean panToRoute, boolean fetchRoute){
        Route route = ObjectCache.getRoute(routeId,companyId);
        if (route == null) {
            return false;
        }

        if (!route.isReady() && fetchRoute) {
            getRoute(route.getCompanyId(),route.getId(),panToRoute);
            return false;
        }

        if (!routes.containsKey(route.getId())){
            routes.put(route.getId(),route);
        }

        int color = Color.BLUE;
        try {
            //System.out.println("route color: "+route.getColor());
            color = Color.parseColor(route.getColor());
        }catch (Exception ex){
            System.out.println("route color error: "+ex.getMessage());
        }

        PolylineOptions routeOptions = new PolylineOptions().width(10).color(color).geodesic(true);

        if (isShowing(route)){
            //hideRoute(route.getCompanyId(),route.getId(),true);
            return true;
        }

        for (int i =1;i <= route.getRoutePoints().size();i++){

            RoutePoint point = route.getRoutePoints().get(""+i);

            if (point.isBusStop()) {
                showStopMarker(point);
                routeOptions.add(new LatLng(point.getLatitude(), point.getLongitude()));
            }else{
                routeOptions.add(new LatLng(point.getLatitude(), point.getLongitude()));
            }
        }

        Polyline polyline = map.addPolyline(routeOptions);
        route.setPolyline(polyline);

        if (!route.getRoutePoints().isEmpty() && panToRoute){
            RoutePoint point = route.getRoutePoints().get("1");
            panToPosition(null, point.getLatitude(),point.getLongitude());
        }

        return true;
    }

    public void panToPosition(final MapMarker mapMarker, final double latitude, final double longitude){

        LatLng position = new LatLng(latitude,longitude);
        if (mapMarker!=null){
            position=mapMarker.getMarker().getPosition();
        }
        float currentZoomLevel = map.getCameraPosition().zoom;
        System.out.println("zoom level "+currentZoomLevel);

        if (currentZoomLevel < 5) {
            currentZoomLevel = AppVariables.zoomLevel;
        }

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(position.latitude, position.longitude),currentZoomLevel),
                500 , new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                if (mapMarker!=null) {
                    IMapActivity mapActivity = (IMapActivity)AppVariables.mainActivity;
                    mapActivity.onMapMarkerClick(mapMarker);
                }
            }

            @Override
            public void onCancel() {}
        });
    }

    public void hideRoutes(){
        Iterator<Map.Entry<String, Route>> iterator = routes.entrySet().iterator();
        while(iterator.hasNext()){
            String routeId = iterator.next().getKey();
            Polyline polyline = routes.get(routeId).getPolyline();
            if (polyline != null && polyline.isVisible()) {
                hideRoute(routeId,false);
            }
        }
    }

    public void hideRoute(String routeId, boolean remove){
        LinkedHashMap<String,RoutePoint> busStopPoints = routes.get(routeId).getBusStopPoints();
        HashMap<Integer, LocationCluster> locationClusters =routes.get(routeId).getLocationClusters();
        Polyline polyline = routes.get(routeId).getPolyline();

        for (Map.Entry<String,RoutePoint> e: busStopPoints.entrySet()){
            RoutePoint point = e.getValue();
            MapMarker mapMarker = point.getMapMarker();
            //Some points are put individually
            if (mapMarker != null) {
                mapMarker.getMarker().remove();
                point.setMapMarker(null);
            }
        }

        for (Map.Entry<Integer,LocationCluster> e: locationClusters.entrySet()){
            LocationCluster cluster = e.getValue();
            cluster.removeSearchItem();
            MapMarker mapMarker = cluster.getMapMarker();
            mapMarker.getMarker().remove();
            cluster.setMapMarker(null);
        }

        if (polyline != null) {
            polyline.setVisible(false);
            polyline.remove();
        }

        if (remove) {
            routes.remove(routeId);
        }
    }

    public void showStopInformation(MapMarker mapMarker){
        RoutePoint routePoint = (RoutePoint) mapMarker.getObject();
        showStopInformation(routePoint, false);
    }

    public void showStopInformation(RoutePoint routePoint, boolean createMarker){
        ArrayList<String> times = routePoint.getStopTimeTable();

        View setTimesView;
        RelativeLayout mRelativeLayout;
        final RelativeLayout watchRoute;
        final PopupWindow stopOptionsPopup;
        if (createMarker) {
            MapMarker mapMarker = showStopMarker(routePoint);
            mapMarker.getMarker().showInfoWindow();
        }

        // Initialize a new instance of LayoutInflater service
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        setTimesView = inflater.inflate(R.layout.content_bus_stop_info,null);
        mRelativeLayout = setTimesView.findViewById(R.id.busStopTime);

        // Initialize a new instance of popup window
        stopOptionsPopup = new PopupWindow(
                setTimesView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        stopOptionsPopup.setFocusable(true);
        stopOptionsPopup.update();

        // Set an elevation value for popup window
        // Call requires API level 21
        if(Build.VERSION.SDK_INT>=21){
            stopOptionsPopup.setElevation(5.0f);
        }

        // Get a reference for the custom view close button
        TextView cancel = setTimesView.findViewById(R.id.cancel);
        LinearLayout timeList = setTimesView.findViewById(R.id.timeList);
        TextView routeName = setTimesView.findViewById(R.id.routeName);
        TextView stopName = setTimesView.findViewById(R.id.stopName);
        watchRoute = setTimesView.findViewById(R.id.watchRoute);

        if (AppVariables.user instanceof Employee){
            watchRoute.setVisibility(View.GONE);
        }

        routeName.setText(routePoint.getRouteName());
        stopName.setText(routePoint.getName());

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        boolean nextArrivalSet = false;

        // The next arrival will be put to the top of the list
        int nextArrivalCount = 0;
        for (int i=0;i<times.size();i++){

            View timeView = inflater.inflate(R.layout.content_time_view,null);

            ((TextView)timeView.findViewById(R.id.timeView)).setText(times.get(i));

            String timeTableTime = times.get(i);

            if (i == times.size()-1){
                timeView.findViewById(R.id.endDivider).setVisibility(View.VISIBLE);
            }

            if (nextArrivalSet){
                timeList.addView(timeView,nextArrivalCount);
                nextArrivalCount++;
                continue;
            }

            try {
                Date time1 = new SimpleDateFormat("HH:mm").parse(timeTableTime);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(time1);

                Date currentTime = dateFormat.parse(dateFormat.format(new Date()));

                if (time1.after(currentTime)) {
                    timeView.setBackgroundColor(Color.parseColor("#80FFA500"));
                    timeList.addView(timeView,nextArrivalCount);
                    nextArrivalSet = true;
                    nextArrivalCount++;
                }else{
                    timeList.addView(timeView);
                }
            }catch (Exception ex){
                continue;
            }
        }

        if (!nextArrivalSet){
            View view = timeList.getChildAt(0);
            if (view != null) {
                timeList.getChildAt(0).setBackgroundColor(Color.parseColor("#80FFA500"));
            }
        }

        watchRoutePoint(watchRoute,routePoint);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopOptionsPopup.dismiss();
            }
        });

        stopOptionsPopup.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 40);

        panToPosition(null, routePoint.getLatitude(),routePoint.getLongitude());
    }

    public MapMarker showStopMarker(RoutePoint point){
        MapMarker mapMarker = new MapMarker(EMapMarkerType.ROUTE_POINT,point,point.getName(),map,context);
        mapMarker.createMarker();
        //in the map markers the bus stop is identified by the stop and its point id
        point.setMapMarker(mapMarker);
        return mapMarker;
    }

    public void watchRoutePoint(final View watchRoute,final RoutePoint routePoint){
        final String routeId = routePoint.getRouteId();
        final String pointId = routePoint.getId();

        if (watchRoutesPoints.containsKey(routeId) && watchRoutesPoints.get(routeId).contains(pointId)){
            watchRoute.setBackgroundResource(R.drawable.round_green_button);
        }

        watchRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (watchRoutesPoints.containsKey(routeId)){
                    watch();
                }else{
                    watchRoutesPoints.put(routeId,new ArrayList<String>());
                    watch();
                }
            }

            public void watch(){
                if (!watchRoutesPoints.get(routeId).contains(pointId)){
                    watchRoutesPoints.get(routeId).add(pointId);
                    watchRoute.setBackgroundResource(R.drawable.round_green_button);
                }else{
                    watchRoutesPoints.get(routeId).remove(pointId);
                    watchRoute.setBackgroundResource(R.drawable.round_orange_button);
                }
            }
        });
    }

    public void plotRoute(LatLng point){}

    /**
     * Checks whether the user is close enough to the route they are searching for
     * So as to contribute to crowdsourcing.
     * @param route
     */
    public static void setEnRoute(Route route){

        AppVariables.interestRoutes.put(route.getStructuredId(),route);

        if (AppVariables.enRoute == null || CityMapManager.isEnRoute(route)) {
            AppVariables.enRoute = route;

            //Set the background service to have the new enroute
            AppVariables.mainActivity.startBackgroundService();
        }
        
        if (isEnRoute(route)){
            LocationManager.sendLocation(AppVariables.user.getLocation());
        }
    }

    /**
     * Checks all routes user has shown interest in if the user is not on the latest one.
     * @return
     */
    public static boolean isEnRoute(){
        if (CityMapManager.isEnRoute(AppVariables.enRoute)){
            return true;
        }

        for (Route route:AppVariables.interestRoutes.values()){
            if (CityMapManager.isEnRoute(route)){
                AppVariables.enRoute = route;
                return true;
            }
        }

        return false;
    }

    public static boolean isEnRoute(Route route) {

        if (route == null){
            return false;
        }

        ArrayList<RoutePoint> routePoints = new ArrayList<RoutePoint>(route.getRoutePoints().values());

        for (RoutePoint routePoint:routePoints){
            float distance = routePoint.distanceFrom(AppVariables.user.getLocation());
            System.out.println("set en route: "+distance);
            if (distance < AppVariables.busStopProximity){
                AppVariables.enRoute = route;
                return true;
            }
        }
        return false;
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
    }
}
