package net.nececity.nececity.enterprise.Managers;

import android.content.Intent;
import android.widget.Toast;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Data.RegisterDataSet;
import net.nececity.nececity.enterprise.Views.LoginActivity;

/**
 * Created by Takunda on 12/28/2018.
 */

public class RegisterManager extends net.nececity.nececity.user.Managers.RegisterManager {

    public RegisterManager(String username, String email, String password, String phoneNumber) {
        super(username, email, password, phoneNumber);
    }

    public RegisterManager(){}

    @Override
    public void postGetDataSet(DataSet dataSet) {
        RegisterDataSet registerDataSet = (RegisterDataSet)dataSet;

        if (registerDataSet.success){
            Toast.makeText(registerActivity,"Registration Successful",Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(registerActivity, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            registerActivity.startActivity(intent);
            registerActivity.finish();
        }else{
            registerActivity.registrationFailed(registerDataSet.message);
        }
    }

}
