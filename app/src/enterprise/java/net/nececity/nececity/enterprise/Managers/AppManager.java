package net.nececity.nececity.enterprise.Managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;

import net.nececity.nececity.Abstracts.ESettingKeys;
import net.nececity.nececity.Data.CompaniesDataSet;
import net.nececity.nececity.Data.PostsDataSet;
import net.nececity.nececity.Data.PromotionsDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.RefreshDataSet;
import net.nececity.nececity.Helpers.UrlConstructor;

import java.util.concurrent.TimeUnit;

/**
 * Created by Takunda on 12/30/2018.
 */

public class AppManager {
    // Need handler for callbacks to the UI thread
    final Handler handler = new Handler();

    public static RefreshDataSet refreshPostsDataSet;
    public static RefreshDataSet refreshPromotionsDataSet;
    public static RefreshDataSet refreshCompaniesDataSet;

    public AppManager(Context context) {

    }

    public void startManager(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AppVariables.mainActivity.getContext());
        int companyInterval = Integer.parseInt(prefs.getString(ESettingKeys.COMPANY_REFRESH_INTERVAL.toString(),"3600"));

        String url = UrlConstructor.getCompanyDetailsUrl(AppVariables.getEnterpriseId());
        CompaniesDataSet companiesDataSet = new CompaniesDataSet(null, AppVariables.user.getId());
        companiesDataSet.setUrl(url);

        refreshCompaniesDataSet = new RefreshDataSet(companiesDataSet,handler,updateCompanyResultsRunnable);
        refreshCompaniesDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(companyInterval));
        refreshCompaniesDataSet.start();
    }

    final Runnable updatePostsResultsRunnable = new Runnable() {
        public void run() {
            PostsDataSet postsDataSet = (PostsDataSet) refreshPostsDataSet.getResultsDataSet();
            if (postsDataSet.success) {
                ObjectCache.refreshPosts(postsDataSet.getPosts());
                AppVariables.mainActivity.unseenPosts(postsDataSet.unseenPosts(),postsDataSet.getLatestPost());
            }
        }
    };

    final Runnable updatePromotionsResultsRunnable = new Runnable() {
        public void run() {
            PromotionsDataSet promotionsDataSet = (PromotionsDataSet) refreshPromotionsDataSet.getResultsDataSet();
            if (promotionsDataSet.success) {
                ObjectCache.refreshPromotions(promotionsDataSet.getPromotions());
                AppVariables.mainActivity.promotionsToday(promotionsDataSet.getPromotionsToday());
            }
        }
    };

    final Runnable updateCompanyResultsRunnable = new Runnable() {
        public void run() {
            CompaniesDataSet companiesDataSet = (CompaniesDataSet)refreshCompaniesDataSet.getResultsDataSet();
            if (companiesDataSet.success) {
                ObjectCache.refreshCompanies(companiesDataSet.getCompanies());
            }
            updatePostsRefreshDataSets();
            updatePromotionsRefreshDataSets();
        }
    };

    private void updatePostsRefreshDataSets(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AppVariables.mainActivity.getContext());
        int postsInterval = Integer.parseInt(prefs.getString(ESettingKeys.POSTS_REFRESH_INTERVAL.toString(),"60"));

        if (refreshPostsDataSet != null){
            return;
        }

        PostsDataSet postsDataSet = new PostsDataSet(null);
        refreshPostsDataSet = new RefreshDataSet(postsDataSet,handler,updatePostsResultsRunnable);
        refreshPostsDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(postsInterval));
        refreshPostsDataSet.start();
    }

    public void updateSettings(String key){

        if (key.equals(ESettingKeys.POSTS_REFRESH_INTERVAL.toString())){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AppVariables.mainActivity.getContext());
            int postsInterval = Integer.parseInt(prefs.getString(ESettingKeys.POSTS_REFRESH_INTERVAL.toString(),"60"));
            refreshPostsDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(postsInterval));
        }else if (key.equals(ESettingKeys.PROMOTIONS_REFRESH_INTERVAL.toString())){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AppVariables.mainActivity.getContext());
            int promotionsInterval = Integer.parseInt(prefs.getString(ESettingKeys.PROMOTIONS_REFRESH_INTERVAL.toString(),"60"));
            refreshPromotionsDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(promotionsInterval));
        }
    }

    private void updatePromotionsRefreshDataSets(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AppVariables.mainActivity.getContext());
        int promotionsInterval = Integer.parseInt(prefs.getString(ESettingKeys.PROMOTIONS_REFRESH_INTERVAL.toString(),"60"));

        if (refreshPromotionsDataSet != null){
            return;
        }

        PromotionsDataSet promotionsDataSet = new PromotionsDataSet(null);
        promotionsDataSet.setUrl(UrlConstructor.getCompanyPromotionsUrl(AppVariables.getEnterpriseId()));
        refreshPromotionsDataSet = new RefreshDataSet(promotionsDataSet,handler,updatePromotionsResultsRunnable);
        refreshPromotionsDataSet.setRefreshInterval(TimeUnit.SECONDS.toMillis(promotionsInterval));
        refreshPromotionsDataSet.start();
    }
}
