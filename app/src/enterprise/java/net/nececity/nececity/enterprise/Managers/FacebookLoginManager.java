package net.nececity.nececity.enterprise.Managers;

import android.content.Intent;

import com.facebook.login.*;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.enterprise.Views.EnterprisePagerActivity;
import net.nececity.nececity.user.Data.FacebookLoginDataSet;
import net.nececity.nececity.user.Views.LoginActivity;
import net.nececity.nececity.user.Views.NeceCityMapActivity;

/**
 * Created by Takunda on 12/29/2018.
 */

public class FacebookLoginManager extends net.nececity.nececity.user.Managers.FacebookLoginManager {

    public FacebookLoginManager(LoginActivity loginActivity) {
        super(loginActivity);
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        FacebookLoginDataSet loginDataset = (FacebookLoginDataSet)dataSet;

        if (loginDataset.success){
            Intent intent = new Intent(loginActivity, EnterprisePagerActivity.class);
            loginActivity.startActivity(intent);
            loginActivity.finish();
        }else{
            com.facebook.login.LoginManager.getInstance().logOut();
            loginActivity.loginFailed(loginDataset.message);
        }
    }
}
