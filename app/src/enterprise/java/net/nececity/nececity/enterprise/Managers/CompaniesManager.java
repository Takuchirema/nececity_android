package net.nececity.nececity.enterprise.Managers;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Abstracts.IGetDataSet;
import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Data.CompaniesDataSet;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.Helpers.UrlConstructor;
import net.nececity.nececity.Objects.Company;

/**
 * Created by Takunda on 12/29/2018.
 */

public class CompaniesManager implements IGetDataSet{

    private IListLayout listLayout;

    public CompaniesManager(){}

    public void getCompany(IListLayout listLayout, String companyId){
        this.listLayout = listLayout;
        String url = UrlConstructor.getCompanyDetailsUrl(companyId);
        CompaniesDataSet companiesDataSet = new CompaniesDataSet(this, AppVariables.user.getId());
        companiesDataSet.setUrl(url);
        companiesDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        CompaniesDataSet companiesDataSet = (CompaniesDataSet)dataSet;
        if (companiesDataSet.success) {
            ObjectCache.refreshCompanies(companiesDataSet.getCompanies());
            if (listLayout != null){
                listLayout.createListAdapter();
            }
        }else{
            if (listLayout != null){
                listLayout.clearRefresh();
            }
            AppVariables.mainActivity.showMessage(companiesDataSet.message);
        }
    }

    public static void follow(String companyId){
        Company company = ObjectCache.getCompanies().get(companyId);
        if (company != null)
            company.setFollowing(true);
    }

    public static void unFollow(String companyId){
        Company company = ObjectCache.getCompanies().get(companyId);
        if (company != null)
            company.setFollowing(false);
    }
}
