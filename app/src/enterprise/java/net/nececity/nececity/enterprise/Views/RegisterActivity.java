package net.nececity.nececity.enterprise.Views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import net.nececity.nececity.R;
import net.nececity.nececity.enterprise.Managers.RegisterManager;

public class RegisterActivity extends net.nececity.nececity.user.Views.RegisterActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerManager = new RegisterManager();

        setUpMainView();
    }

    public void setUpMainView(){
        ImageView logo = findViewById(R.id.logo);
        logo.setImageResource(R.drawable.logo);

        View loginView = findViewById(R.id.loginView);
        loginView.setBackgroundResource(R.drawable.login_image);
    }
}
