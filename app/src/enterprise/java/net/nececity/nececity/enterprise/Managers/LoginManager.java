package net.nececity.nececity.enterprise.Managers;

import android.content.Intent;

import net.nececity.nececity.Abstracts.DataSet;
import net.nececity.nececity.Data.JSONParser;
import net.nececity.nececity.enterprise.Views.EnterprisePagerActivity;
import net.nececity.nececity.user.Data.LoginDataSet;

/**
 * Created by Takunda on 12/28/2018.
 */

public class LoginManager extends net.nececity.nececity.user.Managers.LoginManager {

    public LoginManager(String userId,String password){
        super(userId,password);
    }

    public LoginManager(){super();}

    @Override
    public void postGetDataSet(DataSet dataSet){
        LoginDataSet loginDataSet = (LoginDataSet)dataSet;

        if (loginDataSet.success){
            Intent intent = new Intent(loginActivity, EnterprisePagerActivity.class);
            loginActivity.startActivity(intent);
            loginActivity.finish();
        }else{
            if (!userId.isEmpty() && !password.isEmpty()) {
                loginActivity.loginFailed(loginDataSet.message);
            }
        }
    }
}
