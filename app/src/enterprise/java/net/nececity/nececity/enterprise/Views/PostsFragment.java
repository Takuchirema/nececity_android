package net.nececity.nececity.enterprise.Views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import net.nececity.nececity.Abstracts.PostListObject;
import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Abstracts.OnFragmentInteractionListener;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.PostListAdapter;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.CommentsActivity;
import net.nececity.nececity.enterprise.Managers.CompaniesManager;
import net.nececity.nececity.user.Managers.PostsManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PostsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PostsFragment extends Fragment implements IListLayout {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View myFragmentView;

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static PostsManager postsManager;
    private String companyId;
    private IListLayout listLayout;
    private static PostListAdapter adapter;

    public PostsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PostsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PostsFragment newInstance(String param1, String param2) {
        PostsFragment fragment = new PostsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmentView = inflater.inflate(R.layout.activity_post_list, container, false);

        listLayout = this;

        postsManager = new PostsManager();

        swipeRefreshLayout = myFragmentView.findViewById(R.id.swipe_refresh);

        setListeners();

        companyId = AppVariables.getEnterpriseId();
        if (ObjectCache.getCompanies().get(companyId) == null){
            getCompanyDetails();
            return myFragmentView;
        }

        if (ObjectCache.getCompanies().get(companyId).getCatalogue().isEmpty()){
            postsManager.refreshPosts(this, companyId);
        }else {
            createListAdapter();
        }

        return myFragmentView;
    }

    public void getCompanyDetails(){
        CompaniesManager companiesManager = new CompaniesManager();
        companiesManager.getCompany(this,companyId);
    }

    public void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (ObjectCache.getCompanies().get(companyId) == null){
                            getCompanyDetails();
                            return;
                        }
                        postsManager.refreshPosts(listLayout, companyId);
                    }
                }
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void clearRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void createListAdapter(){
        clearRefresh();

        HashMap<String,? extends PostListObject> posts =  ObjectCache.getPosts();

        if (posts.isEmpty()){
            postsManager.refreshPosts(listLayout, companyId);
            return;
        }

        ArrayList<String> accountIds = new ArrayList<>(posts.keySet());
        adapter = new PostListAdapter(getActivity(), null, CommentsActivity.class, accountIds, R.layout.post_list_view);
        adapter.setMapObjects(posts);
        adapter.sort();

        listView = myFragmentView.findViewById(R.id.posts_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public static void filter(String searchString){
        adapter.filter(searchString);
    }
}
