package net.nececity.nececity.enterprise.Views;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import net.nececity.nececity.Abstracts.ESettingKeys;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.R;

/**
 * Created by Takunda on 12/30/2018.
 */

public class SettingsActivity extends net.nececity.nececity.user.Views.SettingsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new GeneralPreferenceFragment())
                .commit();
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    @Override
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName)
                || DataSyncPreferenceFragment.class.getName().equals(fragmentName)
                || NotificationPreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.enterprise_pref_general);
            setHasOptionsMenu(true);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference(ESettingKeys.COMPANY_REFRESH_INTERVAL.toString()));
            bindPreferenceSummaryToValue(findPreference(ESettingKeys.POSTS_REFRESH_INTERVAL.toString()));
            bindPreferenceSummaryToValue(findPreference(ESettingKeys.PROMOTIONS_REFRESH_INTERVAL.toString()));

            Preference button = findPreference(ESettingKeys.LOGOUT.toString());
            button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    AppVariables.mainActivity.logout();
                    return true;
                }
            });
        }
    }
}
