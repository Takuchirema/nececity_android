package net.nececity.nececity.enterprise.Views;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import net.nececity.nececity.Abstracts.CataloguePostListObject;
import net.nececity.nececity.Abstracts.IListLayout;
import net.nececity.nececity.Abstracts.OnFragmentInteractionListener;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Helpers.CataloguePostListAdapter;
import net.nececity.nececity.Helpers.ObjectCache;
import net.nececity.nececity.R;
import net.nececity.nececity.Views.CommentsActivity;
import net.nececity.nececity.enterprise.Managers.CompaniesManager;
import net.nececity.nececity.user.Managers.CataloguePostsManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OffersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OffersFragment extends Fragment implements IListLayout {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View myFragmentView;

    private static ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static CataloguePostsManager cataloguePostsManager;
    private String companyId;
    private IListLayout listLayout;
    private static CataloguePostListAdapter adapter;

    public OffersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OffersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OffersFragment newInstance(String param1, String param2) {
        OffersFragment fragment = new OffersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmentView = inflater.inflate(R.layout.activity_post_list, container, false);

        listLayout = this;

        cataloguePostsManager = new CataloguePostsManager();

        swipeRefreshLayout = myFragmentView.findViewById(R.id.swipe_refresh);

        setListeners();

        companyId = AppVariables.getEnterpriseId();
        if (ObjectCache.getCompanies().get(companyId) == null){
            getCompanyDetails();
            return myFragmentView;
        }

        if (ObjectCache.getCompanies().get(companyId).getCatalogue().isEmpty()){
            cataloguePostsManager.refreshCataloguePosts(this, companyId);
        }else {
            createListAdapter();
        }

        return myFragmentView;
    }

    public void getCompanyDetails(){
        CompaniesManager companiesManager = new CompaniesManager();
        companiesManager.getCompany(this,companyId);
    }

    public void setListeners(){
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (ObjectCache.getCompanies().get(companyId) == null){
                            getCompanyDetails();
                            return;
                        }
                        cataloguePostsManager.refreshCataloguePosts(listLayout, companyId);
                    }
                }
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void clearRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void createListAdapter() {
        clearRefresh();

        HashMap<String, ? extends CataloguePostListObject> catalogue_posts = new HashMap<>();

        if (ObjectCache.getCompanies().get(companyId) != null) {
            catalogue_posts = ObjectCache.getCompanies().get(companyId).getCatalogue();
        }

        if (catalogue_posts.isEmpty()){
            cataloguePostsManager.refreshCataloguePosts(listLayout, companyId);
            return;
        }

        ArrayList<String> accountIds = new ArrayList<>(catalogue_posts.keySet());
        adapter = new CataloguePostListAdapter(getActivity(), null, CommentsActivity.class, accountIds, R.layout.catalogue_post_list_view);
        adapter.setMapObjects(catalogue_posts);
        adapter.sort();

        listView = myFragmentView.findViewById(R.id.posts_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                System.out.println(view.getClass().getName()+" "+arg3+" "+position);
            }
        });

        listView.setAdapter(adapter);
    }

    public static void filter(String searchString){
        adapter.filter(searchString);
    }
}
