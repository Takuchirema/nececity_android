package net.nececity.nececity.enterprise.Views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.lapism.searchview.Search;
import com.lapism.searchview.widget.SearchView;
import com.squareup.picasso.Picasso;

import net.nececity.nececity.Abstracts.EActionType;
import net.nececity.nececity.Abstracts.ENotificationType;
import net.nececity.nececity.Abstracts.IMainActivity;
import net.nececity.nececity.Abstracts.OnFragmentInteractionListener;
import net.nececity.nececity.Helpers.AppVariables;
import net.nececity.nececity.Managers.CityMapManager;
import net.nececity.nececity.Managers.NotificationManager;
import net.nececity.nececity.Managers.PermissionsManager;
import net.nececity.nececity.Managers.ViewManager;
import net.nececity.nececity.Objects.Post;
import net.nececity.nececity.Objects.Promotion;
import net.nececity.nececity.Objects.User;
import net.nececity.nececity.R;
import net.nececity.nececity.enterprise.Helpers.PagerAdapter;
import net.nececity.nececity.enterprise.Managers.AppManager;
import net.nececity.nececity.user.Views.*;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EnterprisePagerActivity extends AppCompatActivity implements OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener, IMainActivity{

    private ViewPager mViewPager;
    private TabLayout tabLayout;

    private PermissionsManager permissionsManager;
    private AppManager appManager;
    private NotificationManager notificationManager;

    private Context context;
    public DrawerLayout drawer;
    public SearchView searchView;
    public String companyId;

    private static int currentTabPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise_pager);
        context = this;

        uiSetUp();

        appManagerSetUp();

        setAppCodes();

        startApp();
    }

    public void uiSetUp(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.viewPager);
        searchView = findViewById(R.id.searchView);
        searchView.setLogoIcon(R.drawable.ic_action_search_24);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        tabLayout = findViewById(R.id.tabs);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        PagerAdapter adapter = new PagerAdapter(this,getSupportFragmentManager(), tabLayout.getTabCount());
        mViewPager.setAdapter(adapter);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.news_white);
        tabLayout.getTabAt(1).setIcon(R.drawable.catalogue_white);
        tabLayout.getTabAt(2).setIcon(R.drawable.specials_white);

        int tabIconColor = ContextCompat.getColor(context, R.color.colorPrimaryDark);
        tabLayout.getTabAt(1).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
    }

    public void appManagerSetUp(){
        permissionsManager = new PermissionsManager(this);
        appManager = new AppManager(this);
        notificationManager = new NotificationManager(this);
    }

    public void startApp(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsManager.canAccessLocation()) {
                System.out.println("can access location");
                appSetUp();
            } else {
                System.out.println("cannot access location");
                permissionsManager.requestPermissions();
            }
        }else{
            appSetUp();
        }
        appManager.startManager();
    }

    public void appSetUp(){
        termsAndConditions();
        setListeners();
        //setTutorial();
        setPicasso();
    }

    public void termsAndConditions(){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = preferences.edit();
        if (preferences.contains("termsAndConditions")){
            return;
        }

        AlertDialog.Builder termsAndConditionsDialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup termsAndConditions = findViewById(R.id.termsAndConditions);

        View layout = inflater.inflate(R.layout.terms_and_conditions,
                termsAndConditions);
        termsAndConditionsDialog.setView(layout);
        termsAndConditionsDialog.setPositiveButton("Accept", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                editor.putString("termsAndConditions","accepted");
                editor.apply();
                dialog.dismiss();
            }
        });

        termsAndConditionsDialog.setNegativeButton("Reject", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        });

        AlertDialog alert = termsAndConditionsDialog.create();
        alert.show();
        Button ok = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        ok.setTextColor(Color.WHITE);
        ok.setBackgroundColor(Color.parseColor("#ffa500"));

        Button cancel = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        cancel.setTextColor(Color.WHITE);
        cancel.setBackgroundColor(Color.parseColor("#db2929"));

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ok.getLayoutParams();
        layoutParams.weight = 10;

        ok.setLayoutParams(layoutParams);
        cancel.setLayoutParams(layoutParams);
    }

    public void setListeners(){
        setTabListeners();
        setDrawerListeners();
    }

    public void setDrawerListeners(){
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                //setDrawerTutorial(drawerView);
                ImageView userIcon = drawerView.findViewById(R.id.userIcon);
                TextView userName = drawerView.findViewById(R.id.userName);
                User user = AppVariables.user;
                if (user.getPictureBmp() == null){
                    String url = user.getPictureUrl();
                    System.out.println("picture url - ");
                    if (!user.hasPic() && user.getFbId() != null){
                        url = "https://graph.facebook.com/" + AppVariables.user.getId() + "/picture?type=large";
                    }
                    ViewManager viewManager = new ViewManager(userIcon, EActionType.DOWNLOAD_PICTURE, (Activity)context);
                    viewManager.setObject(AppVariables.user);
                    viewManager.setImageBackground(true);
                    viewManager.setUrl(url);
                    viewManager.getPicture();
                }else {
                    Drawable drawable = new BitmapDrawable(context.getResources(), user.getPictureBmp());
                    userIcon.setBackground(drawable);
                }

                LinearLayout profileLayout = drawerView.findViewById(R.id.profileLayout);
                profileLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, UserProfileActivity.class);
                        startActivity(intent);
                    }
                });

                userName.setText(user.getName());
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void setTabListeners(){

        searchView.setOnQueryTextListener(new Search.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(CharSequence query) {
                String text = query.toString().toLowerCase(Locale.getDefault()).trim();
                System.out.println("text change: "+text);
                filterView(text);
                return false;
            }

            @Override
            public void onQueryTextChange(CharSequence newText) {
                String text = newText.toString().toLowerCase(Locale.getDefault()).trim();
                System.out.println("text change: "+text);
                filterView(text);
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                currentTabPosition = tab.getPosition();
                int tabIconColor = ContextCompat.getColor(context, R.color.white);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(context, R.color.colorPrimaryDark);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(context, R.color.white);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }
        });
    }

    public void setPicasso(){
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }

    @Override
    public void logout(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        if (preferences.contains("userID")){
            editor.remove("userID").commit();
        }

        if (preferences.contains("fbID") && preferences.contains("fbEmail") && preferences.contains("fbName")){
            editor.remove("fbID").commit();
            editor.remove("fbEmail").commit();
            editor.remove("fbName").commit();
        }

        LoginManager.getInstance().logOut();

        Intent a = new Intent(context, net.nececity.nececity.enterprise.Views.LoginActivity.class);
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(a);
        System.exit(0);
    }

    public void filterView(String searchString){
        if (currentTabPosition == 0){
            PostsFragment.filter(searchString);
        }
        else if (currentTabPosition == 1){
            OffersFragment.filter(searchString);
        }
        else if (currentTabPosition == 2){
            PromotionsFragment.filter(searchString);
        }
    }

    @Override
    public void deprecatedVersion() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_enterprise_pager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.nav_posts:
                mViewPager.setCurrentItem(0);
                drawer.closeDrawer(Gravity.LEFT);
                break;
            case R.id.nav_companies:
                mViewPager.setCurrentItem(1);
                drawer.closeDrawer(Gravity.LEFT);
                break;
            case R.id.nav_promotions:
                mViewPager.setCurrentItem(2);
                drawer.closeDrawer(Gravity.LEFT);
                break;
            case R.id.nav_settings:
                startSettingsActivity();
                break;
            case R.id.nav_share:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage("com.whatsapp");
                intent.putExtra(Intent.EXTRA_TEXT, AppVariables.appLink);
                try {
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException ex) {
                    showMessage("Whatsapp has not been installed.");
                }
                break;
        }
        return false;
    }

    public void startSettingsActivity(){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setAppCodes() {
        AppVariables.mainActivity = this;
        try {
            AppVariables.setVersionCode(this);
            AppVariables.setVersionName(this);
            AppVariables.setVersionNameSuffix(this);
            AppVariables.setEnterpriseId(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateSettings(String key) {

    }

    @Override
    public void unseenPosts(boolean unseen, Post latestPost) {
        ImageView view = ((Activity)context).findViewById(R.id.post_notification);
        if (unseen) {
            notifyPost(latestPost);
            view.setVisibility(View.VISIBLE);
        }else{
            view.setVisibility(View.GONE);
        }
        view.invalidate();
    }

    @Override
    public void promotionsToday(HashMap<String, Promotion> promotionsToday) {
        System.out.println("promotions today: "+promotionsToday.size());
        ImageView view = ((Activity)context).findViewById(R.id.promotion_notification);
        if (!promotionsToday.isEmpty()) {
            view.setVisibility(View.VISIBLE);
        }else{
            view.setVisibility(View.GONE);
        }

        for (Map.Entry<String, Promotion> e:promotionsToday.entrySet()){
            notifyPromotion(e.getValue());
        }

        view.invalidate();
    }

    @Override
    public void startBackgroundService() {

    }

    @Override
    public void setTutorial() {

    }

    public void notifyPromotion(Promotion promotion){
        System.out.println("notify promotion "+promotion.getPromotion());
        int id = Integer.parseInt(promotion.getId());
        String title = promotion.getCompanyId()+" Special!";
        String message = "Check today's promotion from "+promotion.getCompanyId();
        ENotificationType notificationType = ENotificationType.PROMOTION;

        Intent intent = new Intent(context,EnterprisePagerActivity.class);
        intent.putExtra("view_pager_position",1);
        notificationManager.notify(id+"",title,message,notificationType,intent);
    }

    public void notifyPost(Post post){
        int id = Integer.parseInt(post.getId());
        String title = "New post from "+post.getCompanyId();
        String message = post.getPost();
        ENotificationType notificationType = ENotificationType.POST;

        Intent intent = new Intent(context,EnterprisePagerActivity.class);
        intent.putExtra("view_pager_position",2);
        notificationManager.notify(id+"",title,message,notificationType,intent);
    }

    @Override
    public Context getContext() {
        return context;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_enterprise_pager, container, false);
            TextView textView = rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }
}
