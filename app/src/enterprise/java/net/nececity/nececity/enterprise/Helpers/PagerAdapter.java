package net.nececity.nececity.enterprise.Helpers;

/**
 * Created by Takunda on 12/29/2018.
 */
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import net.nececity.nececity.enterprise.Views.OffersFragment;
import net.nececity.nececity.enterprise.Views.PostsFragment;
import net.nececity.nececity.enterprise.Views.PromotionsFragment;

//Extending FragmentStatePagerAdapter
public class PagerAdapter extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;
    Context context;

    //Constructor to the class
    public PagerAdapter(Context context, FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        this.context=context;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        System.out.println("** get fragment "+position+" **");
        //Returning the current tabs
        switch (position) {
            default:
            case 0:
                PostsFragment tab1 = new PostsFragment();
                return tab1;
            case 1:
                OffersFragment tab2 = new OffersFragment();
                return tab2;
            case 2:
                PromotionsFragment tab3 = new PromotionsFragment();
                return tab3;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}

