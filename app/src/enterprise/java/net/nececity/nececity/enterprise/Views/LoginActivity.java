package net.nececity.nececity.enterprise.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import net.nececity.nececity.R;
import net.nececity.nececity.enterprise.Managers.FacebookLoginManager;
import net.nececity.nececity.enterprise.Managers.LoginManager;

public class LoginActivity extends net.nececity.nececity.user.Views.LoginActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uiSetUp();

        appManagerSetUp();
    }

    public void appManagerSetUp(){
        loginManager = new LoginManager();
        fbLoginManager = new FacebookLoginManager(this);
    }

    public void uiSetUp(){
        ImageView logo = findViewById(R.id.logo);
        logo.setImageResource(R.drawable.logo);

        View loginView = findViewById(R.id.loginView);
        loginView.setBackgroundResource(R.drawable.login_image);
    }

    @Override
    public void attemptSharedPreferenceLogin(){
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        LoginManager loginManager = new LoginManager(email,password);
        loginManager.attemptLogin(this);
    }

    @Override
    public void signUp(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
